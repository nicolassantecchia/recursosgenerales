<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : etiqueta101X51_CBarras2anotaciones.xsl
    Created on : 15 de Octubre de 2016, 10:21 AM
    Author     : Usuario406
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************

		DETALLE: 	|Define la estructura de la página

		CREADO POR: Fernando Prieto 
		
	************************************
-->        
	<xsl:include href="estructura_pagina.xsl"/>
        
        <!-- PARÁMETROS (deben ser establecidos luego del include para que 
                                     puedan sobre-escribir los parámetros por defecto establecidos en cada xsl incluido) -->

        <xsl:param name="papel-tipo">ET101X51</xsl:param>
        <xsl:param name="papel-orientacion">landscape</xsl:param>
        
        <xsl:param name="ancho-utilizable">95mm</xsl:param>
        
        <!-- 
		**********************
		  	TEMPLATES
		**********************
	--> 
	<xsl:template name="template-body">
            
            <fo:table>
                <fo:table-column column-number="1"/>

                <fo:table-body width="{$ancho-utilizable}" table-layout="fixed">
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="texto-alineacion-centro" height="30mm">
                            <fo:block>
                                <xsl:if test="etiqueta/@codigoBarras != ''">
                                    <fo:external-graphic>
                                        <xsl:attribute name="src">								
                                            http://192.168.0.6:8080/recursosGenerales/generadorCodigoBarras?tipo=code128&amp;offsetX=0&amp;offsetY=0&amp;cod=<xsl:value-of select="etiqueta/@codigoBarras" />&amp;altoMod=73&amp;anchoMod=2&amp;hri=false
                                        </xsl:attribute>
                                    </fo:external-graphic>
                                </xsl:if>
                            </fo:block>
                            <fo:block xsl:use-attribute-sets="textoCourier texto-alineacion-centro texto-bold" font-size="10pt">
                                <xsl:value-of select="etiqueta/@codigoBarras" />
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="textoCourier texto-bold" font-size="12pt" padding-top="2.5mm" height="8mm">                                                              
                            <fo:block>
                                <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                    <xsl:with-param name="texto" select="substring(etiqueta/@anotacion0, 1, 74)"/>  
                                    <xsl:with-param name="from" select="37"/>  
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="textoCourier texto-bold" padding-top="2mm" font-size="9pt">
                            <fo:block>
                                <!-- <xsl:value-of name="texto" select="substring(etiqueta/@anotacion2, 1, 45)"/> -->
                                <xsl:value-of select="substring(etiqueta/@anotacion1, 1, 49)"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
	</xsl:template>
</xsl:stylesheet>
