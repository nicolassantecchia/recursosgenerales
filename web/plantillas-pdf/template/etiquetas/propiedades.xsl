<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <!-- Redefino las propiedades generales de propiedades_default_pdf.xsl -->
	
	<xsl:attribute-set name="propiedades-body">
		<xsl:attribute name="margin-top">0mm</xsl:attribute>
		<xsl:attribute name="margin-bottom">0mm</xsl:attribute>
		<xsl:attribute name="space-before">0mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="propiedades-pagina">
            
		<xsl:attribute name="margin-top">3mm</xsl:attribute>
		<xsl:attribute name="margin-bottom">3mm</xsl:attribute>
		<xsl:attribute name="margin-left">3mm</xsl:attribute>
		<xsl:attribute name="margin-right">3mm</xsl:attribute>		
	</xsl:attribute-set>
        
</xsl:stylesheet>
