<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************

		DETALLE: 	Define la estructura de la página

		CREADO POR: Fernando Prieto 
		
	************************************
-->        
	<xsl:include href="../pagina.xsl"/>
        <xsl:include href="propiedades.xsl"/>
        
        <xsl:param name="ancho-utilizable">100mm</xsl:param>
	        
        <xsl:template match="/">
            <fo:root>			
                <fo:layout-master-set>

                    <fo:simple-page-master master-name="pagina" xsl:use-attribute-sets="propiedades-pagina">

                    <!-- Body -->
                    <fo:region-body xsl:use-attribute-sets="propiedades-body" />

                    </fo:simple-page-master>

                </fo:layout-master-set>	

                <fo:page-sequence master-reference="pagina">

                    <fo:flow flow-name="xsl-region-body"><!-- Body -->

                        <fo:table width="{$ancho-utilizable}" table-layout="fixed">
                            <fo:table-column column-width="{$ancho-utilizable}" />
                            <fo:table-body>
                                <fo:table-row>								
                                        <fo:table-cell>
                                            <xsl:call-template name="template-body"/>
                                        </fo:table-cell> 
                                </fo:table-row>
                            </fo:table-body>				
                        </fo:table>

                    </fo:flow>	
                </fo:page-sequence>
            </fo:root>
	</xsl:template>	
</xsl:stylesheet>