<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************

		DETALLE: 	Define las características de la página

		CREADO POR: Fernando Prieto 

		
		file://INTRA_SERVER\INTRA_1\Sistema\intranet\portal2\cocoon2.2\template\
		
	************************************
-->

	<xsl:include href="propiedades_default.xsl"/>

	<!-- 
		Valores posibles para los parmetros:

		$papel-tipo: 			A4
		$papel-orientacion: 	portrait | landscape

		Valores por defecto corresponden a una hoja A4 horizontal (landsacape).
	-->
	<xsl:param name="papel-tipo">A4</xsl:param>
	<xsl:param name="papel-orientacion">landscape</xsl:param>
	<xsl:param name="header-titulo">Parmetro 'header-titulo' no especificado!</xsl:param>	
	
	<xsl:param name="papel-ancho">
		<xsl:choose>		
			<xsl:when test="$papel-tipo = 'A4'">
   				<xsl:choose>
		   			<xsl:when test="$papel-orientacion = 'portrait'">21cm</xsl:when>
		   			<xsl:when test="$papel-orientacion = 'landscape'">29.7cm</xsl:when>
		   		</xsl:choose>
                        </xsl:when>
                        <xsl:when test="$papel-tipo = 'ET101X51'">
                            <xsl:choose>
                                <xsl:when test="$papel-orientacion = 'portrait'">51mm</xsl:when>
                                <xsl:when test="$papel-orientacion = 'landscape'">101mm</xsl:when>
                            </xsl:choose>
   			</xsl:when> 
                        
   		</xsl:choose>
   	</xsl:param>

   	<xsl:param name="papel-alto">
   		<xsl:choose>
   		
   			<xsl:when test="$papel-tipo = 'A4'">
                            <xsl:choose>
                                <xsl:when test="$papel-orientacion = 'portrait'">29.7cm</xsl:when>
                                <xsl:when test="$papel-orientacion = 'landscape'">21cm</xsl:when> 
                            </xsl:choose>
   			</xsl:when>
                        <xsl:when test="$papel-tipo = 'ET101X51'">
   				<xsl:choose>
		   			<xsl:when test="$papel-orientacion = 'portrait'">101mm</xsl:when>
		   			<xsl:when test="$papel-orientacion = 'landscape'">51mm</xsl:when>
		   		</xsl:choose>
   			</xsl:when>
                          			
   		</xsl:choose>
   	</xsl:param>
	
</xsl:stylesheet>