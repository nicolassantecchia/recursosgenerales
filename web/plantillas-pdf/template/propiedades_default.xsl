<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!-- 
	************************************

		DETALLE: 	Define las caractersticas de la pgina

		CREADO POR: Fernando Prieto 

	************************************
-->        

	<!-- Especifica el formateo del objeto que haga match con propiedades-pagina (fo:simple-page-master) -->
	<!-- Uso: xsl:use-attribute-sets="propiedades-pagina" -->
	<xsl:attribute-set name="propiedades-pagina">		
		<xsl:attribute name="page-width">
			<xsl:value-of select="$papel-ancho"/>
		</xsl:attribute>
		<xsl:attribute name="page-height">
			<xsl:value-of select="$papel-alto"/>
		</xsl:attribute>
	</xsl:attribute-set>

	<!-- 
		**********************
		  ESTILOS  GENERALES
		**********************
	--> 

        <xsl:variable name="color-stripped">#E1E1E1</xsl:variable>
        <xsl:variable name="color-azul">#2D6695</xsl:variable>
        <xsl:variable name="color-gris">#999999</xsl:variable>
        <xsl:variable name="color-error">#D55353</xsl:variable>    
        <xsl:variable name="color-link">blue</xsl:variable>   
        <xsl:variable name="color-borde-tabla-azul">#2D6695</xsl:variable>
        <xsl:variable name="color-borde-tabla-gris">#FFFFFF</xsl:variable>
        
        <xsl:variable name="grosor-linea">0.5pt</xsl:variable>
        <xsl:variable name="grosor-borde">0.5pt</xsl:variable>
                
        <xsl:attribute-set name="background-azul">
            <xsl:attribute name="background-color"><xsl:value-of select="$color-azul"/></xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="background-gris">
            <xsl:attribute name="background-color"><xsl:value-of select="$color-gris"/></xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="mensaje-error">
		<xsl:attribute name="color"><xsl:value-of select="$color-error"/></xsl:attribute>
		<xsl:attribute name="font-size">18pt</xsl:attribute>
		<xsl:attribute name="space-before">5mm</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="link">
            <xsl:attribute name="color"><xsl:value-of select="$color-link"/></xsl:attribute>
            <xsl:attribute name="text-decoration">underline</xsl:attribute>
        </xsl:attribute-set>
        
        <!-- Template para imprimir fila cebra -->
        <xsl:template name="tablaStripped">
        
            <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="background-color"><xsl:value-of select="$color-stripped"/></xsl:attribute>
            </xsl:if>
            
        </xsl:template>        
        
        <!-- ************ -->
        <!-- BORDES TABLA -->
        <!-- ************ -->
        
        <xsl:attribute-set name="tabla-azul-colorBorde">
            <xsl:attribute name="border-top-color"><xsl:value-of select="$color-borde-tabla-azul"/></xsl:attribute>
            <xsl:attribute name="border-bottom-color"><xsl:value-of select="$color-borde-tabla-azul"/></xsl:attribute>
            <xsl:attribute name="border-left-color"><xsl:value-of select="$color-borde-tabla-azul"/></xsl:attribute>
            <xsl:attribute name="border-right-color"><xsl:value-of select="$color-borde-tabla-azul"/></xsl:attribute>
            
        </xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-gris-colorBorde">
            <xsl:attribute name="border-top-color"><xsl:value-of select="$color-borde-tabla-gris"/></xsl:attribute>
            <xsl:attribute name="border-bottom-color"><xsl:value-of select="$color-borde-tabla-gris"/></xsl:attribute>
            <xsl:attribute name="border-left-color"><xsl:value-of select="$color-borde-tabla-gris"/></xsl:attribute>
            <xsl:attribute name="border-right-color"><xsl:value-of select="$color-borde-tabla-gris"/></xsl:attribute>            
        </xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-borde-left">
            <xsl:attribute name="border-left-style">solid</xsl:attribute>
            <xsl:attribute name="border-left-width"><xsl:value-of select="$grosor-borde"/></xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-borde-right">
            <xsl:attribute name="border-right-style">solid</xsl:attribute>
            <xsl:attribute name="border-right-width"><xsl:value-of select="$grosor-borde"/></xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-borde-top">
            <xsl:attribute name="border-top-style">solid</xsl:attribute>
            <xsl:attribute name="border-top-width"><xsl:value-of select="$grosor-borde"/></xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-borde-bottom">
            <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
            <xsl:attribute name="border-bottom-width"><xsl:value-of select="$grosor-borde"/></xsl:attribute>
	</xsl:attribute-set>
        
        <!-- ************ -->
        <!--     TABLA    -->
        <!-- ************ -->
        
        <xsl:attribute-set name="tabla-header-cell">
            <xsl:attribute name="display-align">center</xsl:attribute>
            <xsl:attribute name="text-align">left</xsl:attribute>
        </xsl:attribute-set>
            
        <xsl:attribute-set name="tabla-header-block">
		<xsl:attribute name="space-before">0.6mm</xsl:attribute>
		<xsl:attribute name="space-after">0.5mm</xsl:attribute>
		<xsl:attribute name="start-indent">2mm</xsl:attribute>
		<xsl:attribute name="end-indent">2mm</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-header-azul">
            <xsl:attribute name="background-color"><xsl:value-of select="$color-azul"/></xsl:attribute>
            <xsl:attribute name="color">#FFFFFF</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-header-gris">
            <xsl:attribute name="background-color"><xsl:value-of select="$color-gris"/></xsl:attribute>
            <xsl:attribute name="color">#FFFFFF</xsl:attribute>
	</xsl:attribute-set> 
        
        <xsl:attribute-set name="subtabla-header-block">
                <xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="space-before">0.5mm</xsl:attribute>
		<xsl:attribute name="space-after">0.5mm</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="subtabla-header-block-izquierda">
                <xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="space-before">0.5mm</xsl:attribute>
		<xsl:attribute name="space-after">0.5mm</xsl:attribute>
	</xsl:attribute-set>
        
                        
        <!-- ************ -->
        <!--    LINEAS    -->
        <!-- ************ -->       

        <xsl:attribute-set name="linea-azul">
            <xsl:attribute name="border-color"><xsl:value-of select="$color-azul"/></xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="linea-top">
		<xsl:attribute name="border-top-style">solid</xsl:attribute>
		<xsl:attribute name="border-top-width"><xsl:value-of select="$grosor-linea"/></xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="linea-bottom">
		<xsl:attribute name="border-bottom-style">solid</xsl:attribute>
		<xsl:attribute name="border-bottom-width"><xsl:value-of select="$grosor-linea"/></xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="cell-contenedora-tabla">
            <xsl:attribute name="padding-left">2mm</xsl:attribute>
            <xsl:attribute name="padding-right">2mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="block-contenedor-tabla">
		<xsl:attribute name="space-before">2mm</xsl:attribute>
		<xsl:attribute name="space-after">2mm</xsl:attribute>
	</xsl:attribute-set>	

        <!-- ***************** -->
        <!--      TEXTOS       -->
        <!-- ***************** -->

        <xsl:attribute-set name="texto">
            <xsl:attribute name="font-family">Arial, Helvetica, sans-serif</xsl:attribute>
            <xsl:attribute name="font-size">8pt</xsl:attribute>
            <xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="textoCourier">
            <xsl:attribute name="font-family">Courier New, Courier, mono</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-azul">
            <xsl:attribute name="color"><xsl:value-of select="$color-azul"/></xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-tabla">            
            <xsl:attribute name="font-size">7pt</xsl:attribute>            
            <xsl:attribute name="space-before">0.5mm</xsl:attribute>
            <xsl:attribute name="space-after">0.5mm</xsl:attribute>
            <xsl:attribute name="start-indent">1mm</xsl:attribute>
            <xsl:attribute name="end-indent">1mm</xsl:attribute>		
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-bold">
            <xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="texto-alineacion-numero">
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="texto-alineacion-fecha">
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="texto-alineacion-derecha">
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>    
        
        <xsl:attribute-set name="texto-alineacion-centro">
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>    
        
        <xsl:attribute-set name="block-container-overflow">
            <xsl:attribute name="position">relative</xsl:attribute>
            <xsl:attribute name="overflow">hidden</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="block-texto-detalle-articulo">
            <xsl:attribute name="text-align">justify</xsl:attribute> 
        </xsl:attribute-set> 
        
        <!-- perpara un string para que púeda ser truncado al alzanzar el final de la columna -->
        <xsl:template name="prepararStringConZeroSpaceCharacters">
            
            <xsl:param name="texto"/>             
            <xsl:param name="from"/>  <!-- A partir de que caracter insertar los zero spaces -->

            <xsl:variable name="longitud" select="string-length($texto)" />
        
            <xsl:choose>
                
                <xsl:when test="$longitud > $from">
                    
                    <xsl:value-of select="substring($texto, 1, $from)" />&#8203;
                
                    <xsl:call-template name="agregar-zeroSpaceCharacters">
                        
                        <xsl:with-param name="text" select="substring($texto, $from + 1, $longitud)" />
                        
                    </xsl:call-template>
                    
                </xsl:when> 
                  
                <xsl:otherwise>
                    <xsl:value-of select="$texto" />
                </xsl:otherwise>
                
            </xsl:choose> 
            
        </xsl:template> 
        
        
        <xsl:template name="agregar-zeroSpaceCharacters">
            <xsl:param name="text"/>
            
            <xsl:if test="$text != ''">
                
                <xsl:variable name="letter" select="substring($text, 1, 1)" />
                
                <xsl:if test="$letter != ' '">
                    <xsl:value-of select="$letter" />&#8203;
                </xsl:if>
                
                <xsl:call-template name="agregar-zeroSpaceCharacters">
                    <xsl:with-param name="text" select="substring-after($text, $letter)" />
                </xsl:call-template>
            </xsl:if>
            
        </xsl:template>

</xsl:stylesheet>