<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"> 

<!-- 
	************************************
	
		DETALLE: 	Define las caractersticas del FOOTER

		CREADO POR: Fernando Prieto 

	************************************
-->
	<xsl:template name="template-footer">
		
		<fo:block>
			<fo:table table-layout="fixed">
				
				<fo:table-column/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="linea-top linea-azul">
							<fo:block xsl:use-attribute-sets="pie-fecha">
								Informe Generado: <xsl:value-of select="$footer-generado"/>
							</fo:block>
							<fo:block xsl:use-attribute-sets="pie-log">
								Id: <xsl:value-of select="$footer-logId"/>
							</fo:block>
						</fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="linea-top linea-azul">
							<fo:block xsl:use-attribute-sets="pie-fecha">
								Informe Generado: <xsl:value-of select="$footer-generado"/>
							</fo:block>
							<fo:block xsl:use-attribute-sets="pie-log">
								Id: <xsl:value-of select="$footer-logId"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
                
	</xsl:template>
</xsl:stylesheet>