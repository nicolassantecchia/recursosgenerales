<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************
	
		DETALLE: 	Define las caractersticas del HEADER

		CREADO POR: Fernando Prieto 

	************************************
-->
	<xsl:template name="template-header">		
		<fo:block>
			
			<fo:table width="29.1cm" table-layout="fixed">
				
				<fo:table-column column-number="1" column-width="4cm"/>
				<fo:table-column column-number="2" column-width="14cm"/>
				<fo:table-column column-number="3" column-width="4cm"/>
				
				<fo:table-body>
					
					<fo:table-row height="1.6cm">
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell">
							<fo:block>
								<fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
									<xsl:attribute name="src">
										http://192.168.0.6:8080/recursosGenerales/recursos/imagenes/logos/gral_logosoci.gif
									</xsl:attribute>
								</fo:external-graphic>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell encabezado-titulo-cell">
							<fo:block xsl:use-attribute-sets="encabezado-titulo texto-azul">
								<xsl:value-of select="$header-titulo"/>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell">
							<fo:block xsl:use-attribute-sets="encabezado-paginas">
								Página <fo:page-number/> de <fo:page-number-citation ref-id="block-fin"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>

	</xsl:template> 
</xsl:stylesheet>