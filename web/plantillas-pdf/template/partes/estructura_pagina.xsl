<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************

		DETALLE: 	Define la estructura de la página

		CREADO POR: Nicolas Santecchia
				
		
	************************************
-->
        <xsl:include href="propiedades.xsl"/>
	<xsl:include href="../pagina.xsl"/>
	
      
	<xsl:template match="/">
		<fo:root>			
			<fo:layout-master-set>

				<fo:simple-page-master master-name="pagina" xsl:use-attribute-sets="propiedades-pagina">										
					<!-- Header -->
					<fo:region-before xsl:use-attribute-sets="propiedades-header"/>					
					<!-- Body -->
					<fo:region-body xsl:use-attribute-sets="propiedades-body"/>					
					<!-- Footer -->
					<fo:region-after xsl:use-attribute-sets="propiedades-footer"/>
                                                                                
                                        <!--fo:region-start xsl:use-attribute-sets="propiedades-start"/>                                        
                                        <fo:region-end xsl:use-attribute-sets="propiedades-end"/-->
				</fo:simple-page-master>

			</fo:layout-master-set>	

		
			<fo:page-sequence master-reference="pagina">

                               <!--fo:flow flow-name="xsl-region-start">
				</fo:flow-->	
                            
				<fo:static-content flow-name="xsl-region-before"><!-- Header -->
					<fo:block xsl:use-attribute-sets="texto">
						<xsl:call-template name="template-header"/>
					</fo:block>
				</fo:static-content>

				<fo:static-content flow-name="xsl-region-after" ><!-- Footer -->
					<fo:block xsl:use-attribute-sets="texto">
						<xsl:call-template name="template-footer"/>
					</fo:block>
				</fo:static-content>

				<fo:flow flow-name="xsl-region-body"><!-- Body -->
					<fo:block xsl:use-attribute-sets="texto">
						<xsl:call-template name="template-body"/>
						<fo:block id="block-fin"/> <!-- Bloque utilizado para encontrar el número total de páginas -->
					</fo:block>
				</fo:flow>		
	

                        </fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>