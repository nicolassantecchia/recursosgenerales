<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<!-- 
		**********************
		  ESTILOS  GENERALES PARTES
		**********************
	--> 
        
        <!-- Especifica el formateo del objeto que haga match con propiedades-pagina (fo:simple-page-master) -->
	<!-- Uso: xsl:use-attribute-sets="propiedades-pagina" -->
	<xsl:attribute-set name="propiedades-pagina">	
		<xsl:attribute name="margin-top">0.6cm</xsl:attribute>
		<xsl:attribute name="margin-bottom">0.6cm</xsl:attribute>
		<xsl:attribute name="margin-left">0.3cm</xsl:attribute>
		<xsl:attribute name="margin-right">0.3cm</xsl:attribute>
	</xsl:attribute-set>

	<!-- 
		**********************
		  ESTILOS  GENERALES
		**********************
	--> 


	<xsl:attribute-set name="propiedades-body">
		<xsl:attribute name="margin-top">1.7cm</xsl:attribute>
		<xsl:attribute name="margin-bottom">1.9cm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="propiedades-header">		
		<xsl:attribute name="extent">1.7cm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="propiedades-footer">		
		<xsl:attribute name="extent">1.9cm</xsl:attribute>
	</xsl:attribute-set> 
        
	<!--xsl:attribute-set name="propiedades-start">		
		<xsl:attribute name="extent">0cm</xsl:attribute>
	</xsl:attribute-set> 
	<xsl:attribute-set name="propiedades-end">		
		<xsl:attribute name="extent">0cm</xsl:attribute>
	</xsl:attribute-set--> 
        
        
        
        <xsl:attribute-set name="texto-tabla-informe">
            <xsl:attribute name="line-height">10pt</xsl:attribute>
        </xsl:attribute-set>   
        
        
        <!-- ENCABEZADO -->
        
        <xsl:attribute-set name="encabezado-titulo">
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="space-before">0mm</xsl:attribute>
		<xsl:attribute name="space-after">0mm</xsl:attribute>
		<xsl:attribute name="start-indent">5mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="encabezado-cell">
		<xsl:attribute name="space-after">0mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="encabezado-titulo-cell">
		<xsl:attribute name="display-align">center</xsl:attribute>
	</xsl:attribute-set>
							
	<xsl:attribute-set name="encabezado-paginas">
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="start-indent">5mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="encabezado-logo">
		<xsl:attribute name="content-width">2cm</xsl:attribute>
		<xsl:attribute name="content-height">0.50cm</xsl:attribute>
	</xsl:attribute-set>
        
        <!-- PIE -->
        <xsl:attribute-set name="pie-fecha">
		<xsl:attribute name="font-size">7pt</xsl:attribute>
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="pie-log">
		<xsl:attribute name="font-size">11pt</xsl:attribute>
		<xsl:attribute name="line-height">11pt</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>        

</xsl:stylesheet>