<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************
	
		DETALLE: 	Define las caractersticas del HEADER

		CREADO POR: Nicolas Santecchia 

	************************************
-->
	<xsl:template name="template-header">		
		<fo:block>
			
			<fo:table width="19.7cm" table-layout="fixed" >
				
				<fo:table-column column-number="1" column-width="2.2cm"/>
				<fo:table-column column-number="2" column-width="8cm"/>
				<fo:table-column column-number="3" column-width="2.2cm"/>
				
				<fo:table-body>
					
					<fo:table-row height="1.6cm">
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell">
							<fo:block>
								<fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
									<xsl:attribute name="src">
										http://192.168.0.6:8080/recursosGenerales/recursos/imagenes/logos/gral_logosoci.gif
									</xsl:attribute>
								</fo:external-graphic>                                                                
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell encabezado-titulo-cell">
							<fo:block xsl:use-attribute-sets="encabezado-titulo texto-azul">                                                        
                                                                <xsl:choose>
                                                                        <xsl:when test="$header-titulo = 1">
                                                                            GALPÓN 1
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 2">
                                                                            GALPÓN 2
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 3">
                                                                            GALPÓN 3
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 4">
                                                                            GALPÓN 4
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 5">
                                                                            GALPÓN 5
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 6">
                                                                            PINTURERIA
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 7">
                                                                            GALPÓN 7
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 8">
                                                                            GALPÓN 8
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-titulo = 9">
                                                                            MEGA 40
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            DEPÓSITO <xsl:value-of select="$header-titulo"></xsl:value-of>
                                                                        </xsl:otherwise>
                                                                </xsl:choose>                                                                                                                    
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell xsl:use-attribute-sets="encabezado-cell">
                                                   
							<!--fo:block xsl:use-attribute-sets="encabezado-paginas">                					
                                                            	Página <fo:page-number/> de <fo:page-number-citation ref-id="block-fin"/>
							</fo:block-->
							<fo:block>
                                                                <xsl:choose>
                                                                        <xsl:when test="$header-tipoEntrega = 1">
                                                                            <fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
                                                                                    <xsl:attribute name="src">
                                                                                        http://localhost:8080/expedicion/recursos/imagenes/partes/ex_partecarga.jpg										
                                                                                    </xsl:attribute>
                                                                            </fo:external-graphic>                                                                
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-tipoEntrega = 3">
                                                                            <fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
                                                                                    <xsl:attribute name="src">
                                                                                        http://localhost:8080/expedicion/recursos/imagenes/partes/ex_partetercerizado.jpg										
                                                                                    </xsl:attribute>
                                                                            </fo:external-graphic>           
                                                                        </xsl:when>
                                                                        <xsl:when test="$header-tipoEntrega = 4">
                                                                            <fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
                                                                                    <xsl:attribute name="src">
                                                                                        http://localhost:8080/expedicion/recursos/imagenes/partes/ex_partereparto.jpg										
                                                                                    </xsl:attribute>
                                                                            </fo:external-graphic>           
                                                                        </xsl:when>

                                                                        <xsl:when test="$header-tipoEntrega = 7">
                                                                            <fo:external-graphic xsl:use-attribute-sets="encabezado-logo">
                                                                                    <xsl:attribute name="src">
                                                                                        http://localhost:8080/expedicion/recursos/imagenes/partes/ex_partezonal.jpg										
                                                                                    </xsl:attribute>
                                                                            </fo:external-graphic>                                                                                                                                        
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            Parte no identificado
                                                                        </xsl:otherwise>                                                                        
                                                                </xsl:choose>                                                                                                                    
                                                            
							</fo:block>                                                        
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>

	</xsl:template> 
</xsl:stylesheet>