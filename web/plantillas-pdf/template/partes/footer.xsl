<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"> 

<!-- 
	************************************
	
		DETALLE: 	Define las caractersticas del FOOTER

		CREADO POR: Nicolas Santecchia 

	************************************
-->
	<xsl:template name="template-footer">
		
		<fo:block>
			<fo:table table-layout="fixed">
				
                        <fo:table-column column-number="1" column-width="10cm"/>
                        <fo:table-column column-number="2" column-width="9.5cm"/>

				<fo:table-body>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="linea-top linea-azul"  padding-before="3pt" padding-after="3pt"  >
                                                    <fo:block>                                                        
                                                        <xsl:if test="respuesta/parte/bultos/bulto/@codigoBarras != ''">                                                            
                                                            <fo:external-graphic>
                                                                <xsl:attribute name="src">								
                                                                    http://192.168.0.6:8080/recursosGenerales/generadorCodigoBarras?tipo=code128&amp;offsetX=0&amp;offsetY=0&amp;cod=<xsl:value-of select="respuesta/parte/bultos/bulto/@codigoBarras" />&amp;altoMod=50&amp;anchoMod=2&amp;hri=true&amp;alineacionTexto=izq
                                                                </xsl:attribute>
                                                            </fo:external-graphic>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                                        </xsl:if>                                                                                                                
                                                    </fo:block>
						</fo:table-cell>
                                                                    
                                                                                                                            
						<fo:table-cell xsl:use-attribute-sets="linea-top linea-azul"   display-align="after"   padding-before="3pt" padding-after="3pt" >
							<fo:block xsl:use-attribute-sets="pie-log">
								Número:<xsl:value-of select="$footer-numeroParte"/>
							</fo:block>
                                                        
							<fo:block xsl:use-attribute-sets="pie-fecha">
								Parte Generado: <xsl:value-of select="$footer-generado"/>
							</fo:block>                                                        
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
                
	</xsl:template>
</xsl:stylesheet>