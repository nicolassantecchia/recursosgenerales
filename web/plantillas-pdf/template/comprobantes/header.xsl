<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************
	
		DETALLE: 	Define las caractersticas del HEADER para comprobantes

		CREADO POR: Fernando Prieto 

	************************************
-->
	<xsl:template name="template-header">
            
                <xsl:param name="codBarras" />
                <xsl:param name="letra" />
                <xsl:param name="letraCod" />
                
                <!-- Admito que pasen por parámetro el numero completo de cprb ej: X000100000001
                    o bien el prefijo y el sufijo -->
                <xsl:param name="nroComprobante"/>
                <xsl:param name="prefijoCprbNro"/>
                <xsl:param name="sufijoCprbNro"/>
                
                <xsl:param name="localidad"/>
                <xsl:param name="fecha"/>
                
                <xsl:param name="detalleTipoCprb" /> 
            
		<fo:table table-layout="fixed">			
			<fo:table-column column-number="1" column-width="8.6cm"/>
			<fo:table-column column-number="2" column-width="1.45cm"/>
			<fo:table-column column-number="3"/>
			
			<fo:table-body>								
				<fo:table-row>									
					<fo:table-cell padding-top="16mm">
						
						<fo:table table-layout="fixed">		
						
							<fo:table-column column-number="1" column-width="4.8cm" column-height="1cm"/>
							
							<fo:table-body>
							
								<fo:table-row>
									<fo:table-cell>
										<fo:block xsl:use-attribute-sets="texto-alineacion-derecha">
											<fo:external-graphic xsl:use-attribute-sets="encabezado-cbarras">
												<xsl:attribute name="src">								
                                                                                                    http://192.168.0.6:8080/recursosGenerales/generadorCodigoBarras?tipo=code128&amp;offsetX=0&amp;offsetY=0&amp;cod=<xsl:value-of select="$codBarras"/>&amp;altoMod=29
												</xsl:attribute>
											</fo:external-graphic>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>	
								
							</fo:table-body>
						</fo:table>											
						
					</fo:table-cell>
					
					<fo:table-cell>
					
                                            <fo:block xsl:use-attribute-sets="encabezado-tipoCprb texto-alineacion-centro"><xsl:value-of select="$letra"/></fo:block>

                                            <xsl:if test="$letraCod and $letraCod != ''">
                                                <fo:block xsl:use-attribute-sets="encabezado-tipoCprb-cod texto-alineacion-centro">Código N°&#160;<xsl:value-of select="format-number($letraCod, '00')"/></fo:block>
                                            </xsl:if>
                                            
					</fo:table-cell>
					
					<fo:table-cell xsl:use-attribute-sets="encabezado-datos-cell">
						<fo:block xsl:use-attribute-sets="encabezado-datos-tipoCprb-detalle texto-alineacion-derecha"><xsl:value-of select="$detalleTipoCprb"/></fo:block>
                                                
						<fo:block xsl:use-attribute-sets="encabezado-datos-tipoCprb-detalle texto-alineacion-derecha">N° 
                                                    <xsl:choose>
                                                        <!-- si mandaron prefijo y sufijo los utilizo, sino utilizo el nro completo -->
                                                        <xsl:when test="$prefijoCprbNro and $prefijoCprbNro != '' and $sufijoCprbNro and $sufijoCprbNro != ''">
                                                            <xsl:value-of select="$prefijoCprbNro"/>&#160;-&#160;<xsl:value-of select="$sufijoCprbNro"/>
                                                        </xsl:when>
                                                        
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="substring($nroComprobante,2,4)"/>&#160;-&#160;<xsl:value-of select="substring($nroComprobante,6,8)"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    
                                                </fo:block>
                                                
						<fo:block xsl:use-attribute-sets="texto texto-alineacion-derecha">
                                                    <xsl:choose>
                                                        <xsl:when test="$localidad and $localidad != ''">
                                                            <xsl:value-of select="$localidad"/>,                                                            
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            Bahía Blanca,
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    <xsl:value-of select="$fecha"/>
                                                </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	

	</xsl:template> 
</xsl:stylesheet>