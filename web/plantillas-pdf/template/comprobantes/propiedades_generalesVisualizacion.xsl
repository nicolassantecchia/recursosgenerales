<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:attribute-set name="tabla-totales-descuento">
        <xsl:attribute name="font-size">7pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="totales-descuento">
        <xsl:attribute name="padding-top">-12mm</xsl:attribute>
        <xsl:attribute name="padding-bottom">10mm</xsl:attribute>
    </xsl:attribute-set> 

    <xsl:attribute-set name="textosAdicionales">
        <xsl:attribute name="font-size">7pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="block-firma-aclaracion">
        <xsl:attribute name="space-before">8mm</xsl:attribute>
        <xsl:attribute name="padding-top">2mm</xsl:attribute>
        <xsl:attribute name="font-size">8pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="block-firma-aclaracion-footer">
        <xsl:attribute name="padding-top">1mm</xsl:attribute>
        <xsl:attribute name="font-size">7pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="block-conformidad-footer">
        <xsl:attribute name="space-before">5mm</xsl:attribute>            
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-datosEmpleado">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="textos-cprRelacionados">
        <xsl:attribute name="font-size">6pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="textos-arba-bienuso">
        <xsl:attribute name="font-size">6pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="textos-detalle-cprRelacionados"> 
        <xsl:attribute name="padding-top">1mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="textos-detalle-cprRelacionados-nombre"> 
        <xsl:attribute name="start-indent">1mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="textos-detalle-cprRelacionados-numero"> 
        <xsl:attribute name="end-indent">1mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="celda-contado-cte">
        <xsl:attribute name="border-left-style">solid</xsl:attribute>
        <xsl:attribute name="height">2.4cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="celda-texto-copiavirtual">
        <xsl:attribute name="padding-top">10mm</xsl:attribute>
        <xsl:attribute name="font-size">10pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="ubicacion-footer">
        <xsl:attribute name="padding-top">5mm</xsl:attribute>
        <xsl:attribute name="height">42mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tabla-articulos">
        <xsl:attribute name="padding-top">4mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="altura-totales">
        <xsl:attribute name="height">5cm</xsl:attribute>
    </xsl:attribute-set> 

    <xsl:attribute-set name="celda-img-conformidad">
        <xsl:attribute name="height">17mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="celda-firmas">
        <xsl:attribute name="start-indent">2mm</xsl:attribute>
        <xsl:attribute name="end-indent">2mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="texto-leyendaschapa-aberturas">
        <xsl:attribute name="font-size">6pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="celda-aberturas">
        <xsl:attribute name="padding-top">3mm</xsl:attribute>
        <xsl:attribute name="height">9mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="celda-leyendachapa">
        <xsl:attribute name="height">7mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-fila-leyendas"> 
        <xsl:attribute name="height">1.5cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-fila-condiciones"> 
        <xsl:attribute name="height">1.8cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-celda-condiciones"> 
        <xsl:attribute name="border-top-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">2pt</xsl:attribute>
    </xsl:attribute-set>    

    <xsl:attribute-set name="footer-fila-atencion"> 
        <xsl:attribute name="height">0.9cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-celda-atencion"> 
        <xsl:attribute name="border-top-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">2pt</xsl:attribute>
        <xsl:attribute name="padding-top">0.3mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-celda-envio"> 
        <xsl:attribute name="padding-top">2mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-celda-texto-leyendas">
        <xsl:attribute name="padding-top">1.5mm</xsl:attribute>
        <xsl:attribute name="font-size">8pt</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="footer-texto-condiciones">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="footer-celda-img-condiciones">
        <xsl:attribute name="height">17mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="texto-condiciones-pequeno">
        <xsl:attribute name="font-size">6pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="footer-datosEmpleado-PrimeraLinea">
        <xsl:attribute name="padding-top">1mm</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="footer-datosTransporte">
        <xsl:attribute name="border-left-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">0.5pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="texto-cai">
        <xsl:attribute name="padding-top">1.5mm</xsl:attribute>
        <xsl:attribute name="font-size">7pt</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="texto-transporte-detalles">            
        <xsl:attribute name="left">-15mm</xsl:attribute>         
        <xsl:attribute name="width">45mm</xsl:attribute>
        <xsl:attribute name="top">0mm</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:variable name="cantMaximaArticulosPorPagina">
        35
    </xsl:variable>
        
    <xsl:template name="template-datos-transporte">
            
        <fo:table>
            <fo:table-column column-number="1" column-width="1.5cm" />
            <fo:table-column column-number="2"/>

            <fo:table-body>
                <xsl:choose>
                    <xsl:when test="tipotransporte = 1">

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm" padding-top="0.5mm"><fo:block>Transportista</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" padding-top="0.5mm" >

                                <fo:block-container xsl:use-attribute-sets="block-container-overflow texto-detalle-articulo texto-transporte-detalles" top="0.5mm">

                                    <fo:block>

                                        <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                            <xsl:with-param name="texto" select="concat('(',codigotransporte, ') - ', transportista)"/>           
                                            <xsl:with-param name="from" select="22"/>  
                                        </xsl:call-template>

                                    </fo:block>

                                </fo:block-container>   
                            </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>Transporte</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno">
                                <fo:block-container xsl:use-attribute-sets="block-container-overflow texto-detalle-articulo texto-transporte-detalles">

                                    <fo:block>

                                        <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                            <xsl:with-param name="texto" select="transporte"/>           
                                            <xsl:with-param name="from" select="22"/>  
                                        </xsl:call-template>

                                    </fo:block>

                                </fo:block-container> 
                            </fo:table-cell>  
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>Domicilio</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno">

                                <fo:block-container xsl:use-attribute-sets="block-container-overflow texto-detalle-articulo texto-transporte-detalles">

                                    <fo:block>

                                        <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                            <xsl:with-param name="texto" select="domiciliotransporte"/>           
                                            <xsl:with-param name="from" select="22"/>  
                                        </xsl:call-template>

                                    </fo:block>

                                </fo:block-container> 

                            </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>Tel.</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" >
                                <fo:block>
                                    <xsl:value-of select="telefonotransporte"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <xsl:if test="cuittransporte != '00-00000000-0'">
                            <fo:table-row>
                                <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>C.U.I.T</fo:block></fo:table-cell> 
                                <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" ><fo:block><xsl:value-of select="cuittransporte"/></fo:block></fo:table-cell>
                            </fo:table-row>
                        </xsl:if>

                    </xsl:when>
                    <xsl:when test="tipotransporte = 2 or tipotransporte = 3">
                        <fo:table-row>                                                        
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm" padding-top="0.5mm"><fo:block>Transporte</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" padding-top="0.5mm" >

                                <fo:block-container xsl:use-attribute-sets="block-container-overflow texto-detalle-articulo texto-transporte-detalles" top="0.5mm">

                                    <fo:block>

                                        <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                            <xsl:with-param name="texto" select="concat('(',codigotransporte, ') - ', transporte)"/>           
                                            <xsl:with-param name="from" select="22"/>  
                                        </xsl:call-template>

                                    </fo:block>

                                </fo:block-container>
                            </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>Domicilio</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" >
                                <fo:block-container xsl:use-attribute-sets="block-container-overflow texto-detalle-articulo texto-transporte-detalles">

                                    <fo:block>

                                        <xsl:call-template name="prepararStringConZeroSpaceCharacters">			
                                            <xsl:with-param name="texto" select="domiciliotransporte"/>           
                                            <xsl:with-param name="from" select="22"/>  
                                        </xsl:call-template>

                                    </fo:block>

                                </fo:block-container> 
                            </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>Tel.</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" >
                                <fo:block>
                                    <xsl:value-of select="telefonotransporte"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" start-indent="1mm"><fo:block>C.U.I.T</fo:block></fo:table-cell> 
                            <fo:table-cell xsl:use-attribute-sets="texto-condiciones-pequeno" ><fo:block><xsl:value-of select="cuittransporte"/></fo:block></fo:table-cell>
                        </fo:table-row>
                    </xsl:when>
                </xsl:choose>
            </fo:table-body>
        </fo:table>
            
    </xsl:template>
        
</xsl:stylesheet>