<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <!-- Redefino las propiedades generales de propiedades_default_pdf.xsl -->
	
	<xsl:attribute-set name="propiedades-body">
		<xsl:attribute name="margin-top">0cm</xsl:attribute>
		<xsl:attribute name="margin-bottom">0cm</xsl:attribute>
		<xsl:attribute name="space-before">9.5mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="propiedades-pagina">	
		<xsl:attribute name="margin-top">0cm</xsl:attribute>
		<xsl:attribute name="margin-bottom">0cm</xsl:attribute>
		<xsl:attribute name="margin-left">0cm</xsl:attribute>
		<xsl:attribute name="margin-right">0cm</xsl:attribute>		
	</xsl:attribute-set>
        
        <xsl:variable name="grosor-borde">0.1pt</xsl:variable>
        <xsl:variable name="grosor-borde-finalDetalle">1pt</xsl:variable>
        <xsl:variable name="color-cuadro-totales">#CCCCCC</xsl:variable>
    
    <!-- ENCABEZADO -->
	
	<xsl:attribute-set name="encabezado-cbarras">
		<xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
		<xsl:attribute name="content-height">scale-to-fit</xsl:attribute> 
		<xsl:attribute name="width">100%</xsl:attribute> 
		<xsl:attribute name="height">0.9cm</xsl:attribute>
		<xsl:attribute name="scaling">uniform</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="encabezado-tipoCprb">
		<xsl:attribute name="font-size">42pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="space-before">5mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="encabezado-tipoCprb-cod">
		<xsl:attribute name="font-size">5.5pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="space-before">-2.5mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="encabezado-datos-cell">
		<xsl:attribute name="start-indent">3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="encabezado-datos-tipoCprb-detalle">
		<xsl:attribute name="font-size">13pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>        

    <!-- SUBHEADER -->
    
        <xsl:attribute-set name="subencabezado-titulo">
            <xsl:attribute name="font-size">9pt</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-estado-cprb">
            <xsl:attribute name="font-size">30pt</xsl:attribute>
            <xsl:attribute name="color">#E20612</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-estadoRecepcionRecibido-cprb">
            <xsl:attribute name="font-size">10pt</xsl:attribute>
            <xsl:attribute name="color">#088A4B</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-estadoRecepcionPendiente-cprb">
            <xsl:attribute name="font-size">10pt</xsl:attribute>
            <xsl:attribute name="color">#E20612</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="columna-imgTextoVertical-ancho">
            <xsl:attribute name="column-width">0.5cm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="columna-tipoCopia-ancho">
            <xsl:attribute name="column-width">2.9cm</xsl:attribute>
        </xsl:attribute-set>
        
    <!-- BODY -->
    
        <xsl:attribute-set name="texto-tabla">
            <xsl:attribute name="space-after">0.1mm</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-header-bock">
            <xsl:attribute name="font-weight">bold</xsl:attribute>
            <xsl:attribute name="space-after">0.3mm</xsl:attribute>
	</xsl:attribute-set>
    
        <xsl:attribute-set name="tabla-borde-finalDetalle">
           <xsl:attribute name="border-top-style">solid</xsl:attribute>
           <xsl:attribute name="border-width"><xsl:value-of select="$grosor-borde-finalDetalle"/></xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="block-container-link-sitioweb">
            <xsl:attribute name="position">absolute</xsl:attribute>
            <xsl:attribute name="top">-56.10mm</xsl:attribute> 
            <xsl:attribute name="left">17mm</xsl:attribute> 
            <xsl:attribute name="height">4mm</xsl:attribute>
        </xsl:attribute-set>
        
        <!--    
            SECCION TOTALES DEL COMPROBANTE (Parte del comprobante en la que se muestra el subtotal, total, percepciones, etc)    
          -->  
     
        <xsl:attribute-set name="seccion-totales-y-leyendas-ancho">
            <xsl:attribute name="column-width">10.5cm</xsl:attribute>
        </xsl:attribute-set> 

        <xsl:attribute-set name="seccion-totales-ancho">
            <xsl:attribute name="column-width">4.9cm</xsl:attribute>
        </xsl:attribute-set>

        <xsl:attribute-set name="seccion-totales-alto">
            <xsl:attribute name="height">3.2cm</xsl:attribute>
        </xsl:attribute-set>  

        <xsl:attribute-set name="seccion-totales-columnaDescripcion-ancho">
            <xsl:attribute name="column-width">4.9cm</xsl:attribute>
        </xsl:attribute-set>

        <xsl:attribute-set name="seccion-totales-columnaValor-ancho">
            <xsl:attribute name="column-width">2.7cm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="seccion-totales-columnaValor-separacionVertical">
            <xsl:attribute name="padding-bottom">2mm</xsl:attribute>
        </xsl:attribute-set>
        
        
        <!-- CUADRO TOTALES -->
        
        <xsl:attribute-set name="tabla-totales-block">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="tabla-totales-block-cd">
            <xsl:attribute name="space-before">2mm</xsl:attribute>
            <xsl:attribute name="space-after">1.2mm</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="totales">
            <xsl:attribute name="background-color"><xsl:value-of select="$color-cuadro-totales"/></xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="totales-titulo">
            <xsl:attribute name="start-indent">2mm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="totales-valor">
            <xsl:attribute name="end-indent">2mm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="totales-sd">
            <xsl:attribute name="padding-top">5mm</xsl:attribute>
            <xsl:attribute name="padding-bottom">2mm</xsl:attribute>
        </xsl:attribute-set> 
        
        <xsl:attribute-set name="totales-sd-titulo">
            <xsl:attribute name="start-indent">2mm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="totales-sd-valor">
            <xsl:attribute name="end-indent">2mm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="footer-text">
            <xsl:attribute name="font-size">9pt</xsl:attribute>
        </xsl:attribute-set>   
            
        <xsl:attribute-set name="footer-titulo">
            <xsl:attribute name="font-weight">bold</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-detalle-articulo">
            <xsl:attribute name="height">9pt</xsl:attribute>
            <xsl:attribute name="top">-0.5mm</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="texto-terminos-y-condiciones">
            <xsl:attribute name="font-size">8pt</xsl:attribute>
            <xsl:attribute name="padding-top">0mm</xsl:attribute>
            <xsl:attribute name="font-style">italic</xsl:attribute>
            <xsl:attribute name="background-color">white</xsl:attribute>
        </xsl:attribute-set>
        
        <xsl:attribute-set name="cbarras-codigocomprobante">
		<xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
		<xsl:attribute name="content-height">scale-to-fit</xsl:attribute> 
		
		<xsl:attribute name="height">0.8cm</xsl:attribute>
		<xsl:attribute name="scaling">uniform</xsl:attribute>
	</xsl:attribute-set>
        
        <xsl:attribute-set name="link-condiciones">
            <xsl:attribute name="color">#428BCA</xsl:attribute>
        </xsl:attribute-set>
        
        
        <!-- GENERACION DE FILAS VACIAS -->

        <xsl:template name="template-iteradorFilaVacia">
	
            <xsl:param name="parametroParaTemplateVacio"/> 
            <xsl:param name="from"/> 
            <xsl:param name="to"/> 

            <xsl:if test="$from &lt; $to"> <!-- hago una iteración-->

                <!-- Repeated content Here --> 
                <!-- use value-of from to get loop index --> 	
                <xsl:call-template name="template-filaVacia">
                    <xsl:with-param name="parametroParaTemplateVacio" select="$parametroParaTemplateVacio"/> 
                </xsl:call-template>	
                
                <xsl:value-of select="$from"/>

                <xsl:call-template name="template-iteradorFilaVacia"> 
                    <xsl:with-param name="parametroParaTemplateVacio" select="$parametroParaTemplateVacio"/> 
                    <xsl:with-param name="from" select="$from + 1"/> 
                    <xsl:with-param name="to" select="$to"/> 
                </xsl:call-template> 
            </xsl:if> 
	</xsl:template>
        
        <xsl:template name="terminos-y-condiciones-comprobante">
            
            <xsl:param name="mostrarTexto" />
            <xsl:param name="papelPreImpreso" />
            
            <fo:block-container xsl:use-attribute-sets="block-container-link-sitioweb" width="{$ancho-utilizable}">
                <fo:block xsl:use-attribute-sets="texto-terminos-y-condiciones texto-alineacion-centro">
                    
                    <xsl:if test="$mostrarTexto and $papelPreImpreso = 'false'">
                        La presente operación comercial queda sujeta a las condiciones generales que figuran en nuestro
                        <fo:inline xsl:use-attribute-sets="link-condiciones">
                            <fo:basic-link external-destination="http://www.codimat.com.ar/envios/condiciones/condiciones.htm" show-destination="new">sitio web</fo:basic-link>
                        </fo:inline>
                        y que el adquirente declara conocer.
                    </xsl:if>
                    &#160;
                </fo:block>
            </fo:block-container>
             
        </xsl:template>
        
        <xsl:template name="template-comprobanteNoEncontrado">
        
            <xsl:param name="nroCprb"/>
            
            <!-- Título a mostrar en la hoja. Si se desea utilizar el titulo default -> no enviar este parámetro -->
            <xsl:param name="titulo"/>
            
            <!-- Mensaje a mostrar en la hoja. Si se desea utilizar el msj default -> no enviar este parámetro -->
            <xsl:param name="mensaje"/>

            <fo:table xsl:use-attribute-sets="texto" font-size="11pt">
                <fo:table-column column-number="1" column-width="5cm"/>
                <fo:table-column column-number="2" />

                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>
                                <fo:external-graphic>
                                    <xsl:attribute name="src">								
                                        http://192.168.0.6:8080/recursosGenerales/recursos/imagenes/logos/gral_logosoci.gif
                                    </xsl:attribute>
                                </fo:external-graphic>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                                <fo:block xsl:use-attribute-sets="mensaje-error" font-weight="1000" text-align="center">                                
                                    <xsl:choose>
                                        <xsl:when test="$titulo">                                    
                                            <xsl:value-of select="$titulo" />
                                        </xsl:when>
                                        <!-- Generación automática pero no debe enviarse por mail -> puede provenir de la consulta de algún listado-->
                                        <xsl:otherwise>
                                            COMPROBANTE NO ENCONTRADO
                                        </xsl:otherwise>
                                    </xsl:choose>   
                                </fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row>
                        <fo:table-cell >
                            <fo:block space-before="2cm">
                                <fo:inline xsl:use-attribute-sets="texto-azul texto-bold">NRO. COMPROBANTE:</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block space-before="2cm" >
                                <fo:inline xsl:use-attribute-sets="texto-bold">
                                    <xsl:value-of select="concat(substring($nroCprb, 1, 1), ' ',substring($nroCprb, 2, 4), ' - ', substring($nroCprb, 6, 8))" />
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row>
                        <fo:table-cell number-columns-spanned="2" > <!-- xsl:use-attribute-sets="tabla-borde-finalDetalle"> -->
                            <fo:block space-before="1cm">
                                <xsl:choose>
                                    <xsl:when test="$mensaje">                                    
                                        <xsl:value-of select="$mensaje" />
                                    </xsl:when>
                                    <!-- Generación automática pero no debe enviarse por mail -> puede provenir de la consulta de algún listado-->
                                    <xsl:otherwise>
                                        Por favor, verifique que el número de comprobante ingresado se corresponda con el tipo de comprobante que solicitó. Si aún así este error persiste, comuníquese con el Area Sistemas.
                                    </xsl:otherwise>
                                </xsl:choose>                                
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </xsl:template>
        
        <xsl:template name="templateFormaEntrega">
            
            <xsl:param name="mostrarTexto" />
            <xsl:param name="papelPreImpreso" />
            <fo:table>
                <fo:table-column column-number="1" column-width="10cm" />

                <fo:table-body>
                    <fo:table-row>                     
                        <fo:table-cell background-color="red">

                            <fo:block-container xsl:use-attribute-sets="block-container-link-sitioweb" width="19cm">

                                <xsl:attribute name="position">relative</xsl:attribute>
                                <xsl:attribute name="top">5mm</xsl:attribute> 
                                <xsl:attribute name="left">0mm</xsl:attribute> 
                                <xsl:attribute name="height">40mm</xsl:attribute>

                                <fo:block xsl:use-attribute-sets="texto-alineacion-centro texto-bold" font-size="10pt"><xsl:value-of select="../sujeto/formaEntrega" /></fo:block>

                            </fo:block-container>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            

    </xsl:template>
        
</xsl:stylesheet>
