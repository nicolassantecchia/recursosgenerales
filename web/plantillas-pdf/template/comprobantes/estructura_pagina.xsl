<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- 
	************************************

		DETALLE: 	Define la estructura de la página

		CREADO POR: Fernando Prieto 
		
	************************************
-->        
	<xsl:include href="../pagina.xsl"/>
        <xsl:include href="propiedades.xsl"/>
	
        <!-- PARÁMETROS PROVENIENTES DEL SITEMAP -->
	<!-- parámetro que especifica si se debe adaptar o no el xsl para la impresion en papel preimpreso -->
	<xsl:param name="papelPreImpreso"/>
        
        <xsl:param name="ancho-utilizable">18.1cm</xsl:param>
	
	<xsl:template match="/">
		<fo:root>			
			<fo:layout-master-set>

				<fo:simple-page-master master-name="pagina" xsl:use-attribute-sets="propiedades-pagina">
				
				<!-- Body -->
				
				<xsl:choose>   		
					<xsl:when test="respuesta/mensaje/@estadoOperacion = 'error'">						
						<fo:region-body/>
					</xsl:when> 
					<xsl:otherwise>
						<fo:region-body xsl:use-attribute-sets="propiedades-body" />
					</xsl:otherwise>  			
				</xsl:choose>
				
				</fo:simple-page-master>

			</fo:layout-master-set>	
			
			<fo:page-sequence master-reference="pagina">
				
				<fo:flow flow-name="xsl-region-body"><!-- Body -->

						<fo:table>
							<fo:table-column column-width="21cm" />
							<fo:table-body>
								<fo:table-row >								
									<fo:table-cell padding-left="14mm" padding-top="11.5mm">
                                                                            <fo:table>
                                                                                <fo:table-column column-width="18.7cm"/>
                                                                                <fo:table-body>
                                                                                    <fo:table-row>							
                                                                                        <fo:table-cell padding-left="3mm" padding-right="3mm">								
                                                                                            <xsl:call-template name="template-body"/>								
                                                                                        </fo:table-cell> 
                                                                                    </fo:table-row>
                                                                                </fo:table-body>				
                                                                            </fo:table>	
										
									</fo:table-cell> 
								</fo:table-row>
							</fo:table-body>				
						</fo:table>
					
				</fo:flow>	
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template name="fondoPapel"> 
		<xsl:if test="$papelPreImpreso = 'false'">
		
			<fo:block-container left="-3.6mm" top="-29.8cm" position="absolute"> 
				
				<xsl:attribute name="width"><xsl:value-of select="$papel-ancho"/></xsl:attribute>
				<xsl:attribute name="height"><xsl:value-of select="$papel-alto"/></xsl:attribute>
				
				<fo:block> 				
					<fo:external-graphic>
						<xsl:attribute name="src">										
							http://192.168.0.6:8080/recursosGenerales/recursos/imagenes/comprobantes/Frente2479x3508.gif
						</xsl:attribute>
					</fo:external-graphic>
				</fo:block> 
			</fo:block-container>
			
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>