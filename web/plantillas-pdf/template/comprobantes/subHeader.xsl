<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!-- 
	************************************
	
		DETALLE: Define las caractersticas del HEADER para comprobantes

		CREADO POR: Fernando Prieto 

	************************************
-->
	<xsl:template name="template-subheader">
            
            <xsl:param name="razonSocialNombre" />
            <xsl:param name="razonSocialID" />
            <xsl:param name="grupoGestion" />
            <xsl:param name="grupoGestionConParentesisIncluido" />
            <xsl:param name="codigoPostalConParentesisIncluido" />
            <xsl:param name="codigoPostal" />
            <xsl:param name="localidad" />
            <xsl:param name="domicilio" />
            <xsl:param name="iva" />
            <xsl:param name="etiquetaDoc" />
            <xsl:param name="documento" />
            <xsl:param name="telefono" />

		<fo:block space-before="3mm">
			<!-- SUB-HEADER -->
			<fo:table>
			
				<fo:table-column column-number="1" column-width="10.6cm"/>
				<fo:table-column column-number="2"/>
				
				<fo:table-body >
					<fo:table-row>		
					
                                            <fo:table-cell>
                                                    <xsl:call-template name="template-subheader-razonSocial">
                                                        
                                                        <xsl:with-param name="razonSocialNombre" select="$razonSocialNombre"/>
                                                        <xsl:with-param name="razonSocialID" select="$razonSocialID"/>
                                                        <xsl:with-param name="grupoGestion" select="$grupoGestion"/>
                                                        <xsl:with-param name="grupoGestionConParentesisIncluido"  select="$grupoGestionConParentesisIncluido" />
                                                        <xsl:with-param name="codigoPostalConParentesisIncluido"  select="$codigoPostalConParentesisIncluido" />
                                                        <xsl:with-param name="codigoPostal" select="$codigoPostal"/>
                                                        <xsl:with-param name="localidad" select="$localidad"/>
                                                        <xsl:with-param name="domicilio" select="$domicilio"/>
                                                        <xsl:with-param name="iva" select="$iva"/>
                                                        <xsl:with-param name="etiquetaDoc" select="$etiquetaDoc"/>
                                                        <xsl:with-param name="documento" select="$documento"/>
                                                        <xsl:with-param name="telefono" select="$telefono"/>
                                                        
                                                    </xsl:call-template>
						</fo:table-cell>
						
						<fo:table-cell>
							<!-- Este template tiene que ser definido en cada "body" -->
							<xsl:call-template name="template-subheader-infoAdicional" /> 
						</fo:table-cell>
						
					</fo:table-row>
				</fo:table-body>
			</fo:table>
				
		</fo:block>
	</xsl:template>
	
	<xsl:template name="template-subheader-razonSocial">

            <xsl:param name="razonSocialNombre" />
            <xsl:param name="razonSocialID" />
            <xsl:param name="grupoGestionConParentesisIncluido" />
            <xsl:param name="codigoPostalConParentesisIncluido" />
            <xsl:param name="grupoGestion" />
            <xsl:param name="codigoPostal" />
            <xsl:param name="localidad" />
            <xsl:param name="domicilio" />
            <xsl:param name="iva" />
            <xsl:param name="etiquetaDoc" />
            <xsl:param name="documento" />
            <xsl:param name="telefono" />
            
		<xsl:variable name="textoRSoc"><xsl:value-of select="substring($razonSocialNombre,1, 42)"/></xsl:variable>
	
		<fo:table>
			<fo:table-column column-number="1" column-width="2.4cm"/>
			<fo:table-column column-number="2"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell >			
                                            <fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="-0.1mm">
                                                Razón Social:
                                            </fo:block>				
					</fo:table-cell>
					
					<fo:table-cell>
                                            <fo:block xsl:use-attribute-sets="texto" font-weight="bold" font-size="9pt">
                                                <xsl:value-of select="$textoRSoc" />
                                            </fo:block>
                                            
                                            <fo:block xsl:use-attribute-sets="texto" font-weight="bold" font-size="9pt" >
                                                <xsl:choose>
                                                    <xsl:when test="$grupoGestion and $grupoGestion != '' and $razonSocialID != ''">  
                                                        <xsl:choose>
                                                            <xsl:when test="$grupoGestionConParentesisIncluido">
                                                               <xsl:value-of select="concat($razonSocialID, ' ',$grupoGestion)"/>&#160;
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                               <xsl:value-of select="concat($razonSocialID, ' (',format-number($grupoGestion, '000'), ')')"/>&#160;
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                        
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$razonSocialID"/>&#160;
                                                    </xsl:otherwise>
                                                </xsl:choose>	
                                            </fo:block>	                                            
                                            		
					</fo:table-cell>
				</fo:table-row>
                                
				<fo:table-row>					
                                    <fo:table-cell >							
                                            <fo:block xsl:use-attribute-sets="texto subencabezado-titulo">
                                                    Domicilio:
                                            </fo:block>							
                                    </fo:table-cell>
                                    <fo:table-cell>
                                            <fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="0.4mm" font-size="7.5pt">										
                                                    <xsl:value-of select="$domicilio"/> 
                                            </fo:block>
                                    </fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>							
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="-0.1mm">
							Localidad:
						</fo:block>							
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="0.3mm" font-size="8pt">
                                                    
                                                    <xsl:choose>
                                                        <xsl:when test="$codigoPostal and $codigoPostal != ''">  
                                                            <xsl:choose>
                                                                <xsl:when test="$codigoPostalConParentesisIncluido">
                                                                   <xsl:value-of select="concat($codigoPostal, ' ', $localidad)"/>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                  <xsl:value-of select="concat('(', $codigoPostal, ') ', $localidad)"/> 
                                                                </xsl:otherwise>
                                                            </xsl:choose>                                                           
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="$localidad"/>&#160;
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>							
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="-0.1mm">
							I.V.A:
						</fo:block>							
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="0.2mm" font-size="8pt">										
							<xsl:value-of select="$iva"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>							
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="-0.2mm">
							<xsl:value-of select="$etiquetaDoc"/>:
						</fo:block>							
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="0.2mm" font-size="8pt">
							<xsl:value-of select="$documento"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>							
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="-0.2mm">
							Teléfono:
						</fo:block>							
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="texto subencabezado-titulo" space-before="0.2mm" font-size="8pt">										
							<xsl:value-of select="$telefono"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
</xsl:stylesheet>