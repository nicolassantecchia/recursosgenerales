CONFIG.proyectos.recursosGenerales = {
    url : CONFIG.servidores.tomcat + '/recursosGenerales',
}

CONFIG.proyectos.recursosGenerales.directorios = {     
        
    controladores : {
        busquedaRapida : CONFIG.proyectos.recursosGenerales.url,
        recuperacionDatos : CONFIG.proyectos.recursosGenerales.url
    },
    
    vistas : {        
        jsp : CONFIG.proyectos.recursosGenerales.url + '/vistas'
    },
    
    plantillas : {
        xml : CONFIG.proyectos.recursosGenerales.url,
        xsl : CONFIG.proyectos.recursosGenerales.url + '/plantillas-pdf/template/etiquetas',
    }
};

CONFIG.proyectos.recursosGenerales.controladores = {
    
    recuperarUsuarioPorId : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarUsuarioPorId',
    recuperarPermisosUsuario : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarPermisosUsuario',
    
    recuperarTarjetaPorIdUsuario : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarTarjetaPorIdUsuario',
    
    recuperarProveedorPorId : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarProveedorPorId',
    
    recuperarClientePorId : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarClientePorId',        
    
    recuperarArticuloCodimat : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticuloCodimat',    
    recuperarArticuloProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticuloProveedor',
    recuperarArticulosPorSubrubro : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticulosPorSubrubro',
    recuperarArticuloInventario : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticuloInventario',
    recuperarArticuloFueraDeLista : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticuloFueraDeLista',

    recuperarFamiliasProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarFamiliasProveedor',
    recuperarRubrosProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarRubrosProveedor',
    recuperarSubRubrosProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarSubRubrosProveedor',

    recuperarVinculacionesArticuloProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarVinculacionesArticuloProveedor',
    recuperarVariacionesPorProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarVariacionesPorProveedor',

    recuperarTransporte : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarTransporte',

    recuperarTiposDeComprobante : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarTiposDeComprobante',
    
    recuperarArticulosEnStock : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticulosEnStock',
    
    recuperarDatosActualizacionCostos : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarDatosActualizacionCostos',
    
    recuperarComprobanteEnCot : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarComprobanteEnCot',
    
    recuperarTerminalesDetalleInventario : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarTerminalesDetalleInventario',
    
    recuperarArticulosSegunRubroSubrubro : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticulosSegunRubroSubrubro',
    
    recuperarArticuloConPrecioUltimaCompra : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarArticuloConPrecioUltimaCompra',
    
    recuperarDatosTotalesModificacionCostosPorSubrubro : CONFIG.proyectos.recursosGenerales.directorios.controladores.recuperacionDatos + '/recuperarDatosTotalesModificacionCostosPorSubrubro'
};

CONFIG.proyectos.recursosGenerales.busquedaRapida = {
    
    articulosActivosCodimat : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNArticulosActivosCodimatJSON',
    articulosActivosInventario : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNArticulosActivosInventarioJSON',
    articulosFueraListaInventario : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNArticulosFueraListaInventarioJSON',
    proveedoresActivos : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNProveedoresActivosJSON',
    clientesActivos : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNClientesActivosJSON',    
    articulosActivosPorProveedor : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNArticulosActivosPorProveedorJSON',
    transportesActivos : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNTransportesActivosJSON',
    usuariosActivos : CONFIG.proyectos.recursosGenerales.directorios.controladores.busquedaRapida + '/recuperarNUsuariosPorStringJSON'
};

CONFIG.proyectos.recursosGenerales.vistas = {
    
    //Proveedores
    contenedorPDF : CONFIG.proyectos.recursosGenerales.directorios.vistas.jsp + '/contenedorPDF.jsp'
};

CONFIG.proyectos.recursosGenerales.xml = {
    
    etiquetaCBarraConAnotaciones : CONFIG.proyectos.recursosGenerales.directorios.plantillas.xml + '/generarXMLEtiquetaCBarraConAnotaciones'
};

CONFIG.proyectos.recursosGenerales.xsl = {
    
    e101X51CBarras2anotaciones : CONFIG.proyectos.recursosGenerales.directorios.plantillas.xsl + '/etiqueta101X51CBarras2anotaciones.xsl'
};


$(document).ready (function(){
    
    $('#link-logout').click(function(event) {

        event.preventDefault();
        
        var operacionesSobreServidorSistema = new OperacionesSobreServidorSistema();

        operacionesSobreServidorSistema.sesionLogout({
            urlLogout : $(this).attr('href'),
            onFailure : handleLogOutFailure
        });
    });
});

function handleLogOutFailure(exception) {
    mostrarModalError(exception.getMensaje());
}
