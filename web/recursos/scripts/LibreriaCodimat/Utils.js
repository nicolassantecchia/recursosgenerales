/*
 *  -------------------------------------------
 *  Fernando Prieto - Julio 2014
 *  Debe ser siempre importada.
 *  -------------------------------------------
 */

/**
 * Definicion de objeto Utiles. - Todas las funciones de representen utilidades deben ser añadidas a este objeto.
 * @returns {Utiles.Anonym$0}
 */
var Utiles = function() {     
    
    var completarCerosIzquierda = function(inputId, longitudMaxima) {
    
        var $input = $('#'+inputId);

        if($input.val().length != 0)
        {
            for(var i = $input.val().length; i < longitudMaxima; i++)
            {
                $input.val('0'+$input.val());
            }
        }
    },
            
    /**
     * Retorna un String con la URL de cocoon correspondiente a un PDF.
     * @param parametros es un arreglo con los parametros que formaran la url, los cuales deben estar en el orden en el que son considerados en el pattern del sitemap.
     * @param parametros es un arreglo con los parametros que formaran la url, los cuales deben estar en el orden en el que son considerados en el pattern del sitemap.
     */
    prepararURLPatternPDFCocoon = function(prefijo, parametros) {
        
        var pattern = prefijo;
        
        for(var i = 0; i < parametros.length; i++) {
            pattern = pattern + "_" + parametros[i];
        }
        
        return pattern + ".pdf";
    },
            
    /**
     * Retorna un String con la URL del generador de PDF online.
     * 
     * @param urlGenerado url del generador de PDF 
     * @param urlXML url del xml
     * @param urlXSL url del xsl
     * @param parametrosXML es un arreglo con los parametros  que deben pasarse al XML.
     * @param parametrosXSL es un arreglo con los parametros  que deben pasarse al XSL.
     * 
     * @param parametrosXSL es un arreglo con los parametros que formaran la url, los cuales deben estar en el orden en el que son considerados en el pattern del sitemap.
     * @param nombrePDF es el nombre que se le asigna al archivo PDF.
     */
    prepararURLGeneradorPDFOnline = function(urlXML, urlXSL, parametrosXMLnombre, parametrosXMLvalue, parametrosXSLnombre, parametrosXSLvalue, nombrePDF) {
        
        var urlGenerador = CONFIG.proyectos.generadorPDFOnline;
        
        var paramXML = CONFIG.variables.sistem.generadorPDFOnline.nombreParametroXML;
        var paramXSL = CONFIG.variables.sistem.generadorPDFOnline.nombreParametroXSL;
        var paramNOMBRE = CONFIG.variables.sistem.generadorPDFOnline.nombreParametroNombrePDF;
        
        var separadorGrupos = CONFIG.variables.sistem.generadorPDFOnline.separadorGeneral;
        var separadorParametros = CONFIG.variables.sistem.generadorPDFOnline.separadorParametros;
        var separadorParametroNombreValor = CONFIG.variables.sistem.generadorPDFOnline.separadorParametroNombreValor;
        
        var pattern = urlGenerador + separadorGrupos + paramXML + separadorParametroNombreValor + urlXML;
                
        if(parametrosXMLnombre != null && parametrosXMLvalue != null && parametrosXMLnombre.length == parametrosXMLvalue.length) {
                
            for(var i = 0; i < parametrosXMLnombre.length; i++) {
                pattern = pattern + separadorParametros + parametrosXMLnombre[i] + separadorParametroNombreValor + parametrosXMLvalue[i];
            }
        }
        
        pattern = pattern + separadorGrupos + paramXSL + separadorParametroNombreValor + urlXSL;
        
        if(parametrosXSLnombre != null && parametrosXSLvalue != null && parametrosXSLnombre.length == parametrosXSLvalue.length) {
                
            for(var i = 0; i < parametrosXSLnombre.length; i++) {
                pattern = pattern + separadorParametros + parametrosXSLnombre[i] + separadorParametroNombreValor + parametrosXSLvalue[i];
            }
        }

        if(nombrePDF != null && nombrePDF != "") {
            pattern = pattern + separadorGrupos + paramNOMBRE + separadorParametroNombreValor + nombrePDF;
        }
        
       return encodeURI(pattern);
    },
            
    /**
     * Retorna un String con la URL pasada por parámetro y sus respecivos parámetros.
     * 
     * @param url es la url que se quiere utilizar.
     * @param parametrosNombre es un arreglo con los nombres de los parametros.
     * @param parametrosValue es un arreglo con los valores de los parametros los cuales deben encontrarse en el mismo orden en que se especificaron sus nombres.
     */
    prepararURL = function(url, parametrosNombre, parametrosValue) {
                
        var pattern = url;
                
        if(parametrosNombre != null && parametrosValue != null && parametrosNombre.length == parametrosValue.length) {
                
            for(var i = 0; i < parametrosNombre.length; i++) {
                
                if(i == 0) {
                    pattern = pattern + "?";
                }
                else {
                    pattern = pattern + "&";
                }
                
                pattern = pattern + parametrosNombre[i] + "=" + parametrosValue[i];
            }
        }
        
       return pattern;
    },
            
    prepararUrlVisualizacionComprobante = function(sistemaOrigen, tipoCprb, numeroCprb, forzarGeneracion) {        
        return prepararUrlEspecificadaVisualizacionComprobante(CONFIG.proyectos.ventas.controladores.visualizacionComprobante, sistemaOrigen, tipoCprb, numeroCprb, forzarGeneracion);
    },
            
    prepararUrlEspecificadaVisualizacionComprobante = function(url, sistemaOrigen, tipoCprb, numeroCprb, forzarGeneracion) {
        
        var nombreParametro = new Array();
            nombreParametro[0] = 'tipoCprb';
            nombreParametro[1] = 'numeroCprb';
        
        if(sistemaOrigen != null) {
            nombreParametro[nombreParametro.length] = 'sistemaOrigen';
        }
        
        if(forzarGeneracion != null) {
            nombreParametro[nombreParametro.length] = 'forzarGeneracion';
        }        
        
        var valorParametro = new Array();
            valorParametro[0] = tipoCprb;
            valorParametro[1] = numeroCprb;
        
        if(sistemaOrigen != null) {
            valorParametro[valorParametro.length] = sistemaOrigen;
        }
        
        if(forzarGeneracion != null) {
            valorParametro[valorParametro.length] = forzarGeneracion;
        }
                
        return prepararURL(url, nombreParametro, valorParametro);
    }
            
    /*
    * Verifica que la expresión $expr cumpla con la espresión regular
    * pattern
    */
    controlarExpresionRegular = function(pattern, expression)
    {	
        if(pattern.test(expression.toLowerCase()))
        {
            return true;
        }
        else 
        {
            return false;
        }
    },
            
    getExpresionRegularNroComprobante = function() {
        return expresionesRegulares['numeroComprobante'];
    },
            
    getExpresionRegularCodBarras = function() {
        return expresionesRegulares['codBarrasComprobante'];
    },            
            
    /**
     * Formatea el número de comprobante <code>nroCprb</code> separando la letra del mismo, su prefijo y sufijo con el separador <code>separador</code>.
     * Ejemplo: si <code>nroCprb = R000100000001</code>  y separador = "-" retorna <code>R-0001-00000001</code>
     * @param nroCprb
     * @param separador
     * @return 
     */
    formatearNumeroComprobante = function(nroCprb, separador) {
        
        var nro = desglosarNumeroComprobante(nroCprb);
        
        return nro[0] + separador + nro[1] + separador + nro[2];
    },
    
    /**
     * Formatea el número de comprobante <code>nroCprb</code> separando la letra del mismo, su prefijo y sufijo con un espacio.
     * Ejemplo: si <code>nroCprb = R000100000001</code> retorna <code>R 0001 00000001</code>
     * @param nroCprb
     * @return 
     */
    formatearNumeroComprobanteEspacio = function(nroCprb) {
        
        return formatearNumeroComprobante(nroCprb, " ");
    },   
        
    /**
     * Dado el número de comprobante <code>nroCprb</code> retorna un arreglo con las tres partes del número, esto es, LETRA | PREFIJO | NUMERO.
     * Ejemplo: si <code>nroCprb = R000100000001</code> retorna [[1] => 'R', [2] => '0001', [3] => '00000001']</code> 
     * @param {type} nroCprb
     * @returns {undefined}
     */
    desglosarNumeroComprobante = function(nroCprb) {
        
        return [nroCprb.substring(0,1), nroCprb.substring(1,5), nroCprb.substring(5,13)];
    }
    
    checkearKeysInputLetra = function(charCode) {
        // tab
        if((charCode >= 65 && charCode <= 90) || charCode == 9 || checkearTeclasBorradoras(charCode) || checkearTeclasFuncionales(charCode)) 
        {
            return true;
        }
        else 
        {
            return false;
        } 
    },
            
    checkearKeysFlechas = function(charCode) {
        // tab
        if(charCode == 37 || charCode == 38 || charCode == 39 || charCode == 40) 
        {
            return true;
        }
        else 
        {
            return false;
        } 
    },        
            
    convertirStringABoolean = function(parametro) {
        
        var parametro = parametro.toLowerCase().trim();
        
        if(parametro == "true" || parametro == "yes" || parametro == "1") {
            return true;
        }
        else {
            return false;
        }
    };
        
    return {
    
        completarCerosIzquierda : completarCerosIzquierda,
        prepararURLPatternPDFCocoon : prepararURLPatternPDFCocoon,
        prepararURLGeneradorPDFOnline : prepararURLGeneradorPDFOnline,
        prepararURL : prepararURL,
        prepararUrlVisualizacionComprobante : prepararUrlVisualizacionComprobante,
        prepararUrlEspecificadaVisualizacionComprobante : prepararUrlEspecificadaVisualizacionComprobante,
        
        //Mapeo de funciones sueltas (deberian estar todas aca pero por una cuestion de compatibilidad con otros scrips no las pongo)
        inicializar_deteccion_capslock : inicializar_deteccion_capslock,
        evitarSubmitConEnterEnInputs : evitarSubmitConEnterEnInputs,
        habilitarAccionTab : habilitarAccionTab,
        abrirSelect : abrirSelect,
        descargarDesdeURL : descargarDesdeURL,
        transformarComaEnPunto : transformarComaEnPunto,
        obtenerVersionBrowser : obtenerVersionBrowser,
        inicializarExpresionesRegulares : inicializarExpresionesRegulares,
        controlExpresionRegular : controlExpresionRegular,
        controlarExpresionRegular : controlarExpresionRegular,
        formatearInputConDecimales : formatearInputConDecimales,
        formatearNumeroConSeparadorMiles : formatearNumeroConSeparadorMiles,
        checkearKeysInputDecimales : checkearKeysInputDecimales,
        checkearKeysInputNumerico : checkearKeysInputNumerico,
        checkearKeysFlechas : checkearKeysFlechas,
        checkearCaracteresPermitidosFecha : checkearCaracteresPermitidosFecha,
        checkearCaracteresPermitidosInputFecha : checkearCaracteresPermitidosInputFecha,
        checkearCaracteresPermitidos : checkearCaracteresPermitidos,
        checkerCaracteresPermitidosURL : checkerCaracteresPermitidosURL,
        checkearCaracteresParaId : checkearCaracteresParaId,
        checkearTeclasFuncionales : checkearTeclasFuncionales,
        checkearTeclasNumericas : checkearTeclasNumericas,
        checkearTeclasBorradoras : checkearTeclasBorradoras,
        checkearTeclasPuntuacion : checkearTeclasPuntuacion,
        checkearKeysInputLetra : checkearKeysInputLetra,
        obtenerKeyCode : obtenerKeyCode,
        trim : trim,
        ltrim : ltrim,
        rtrim : rtrim,
        reemplazarTodos : reemplazarTodos,
        fechaActual : fechaActual,
        abrirUrlEnNuevoTab : abrirUrlEnNuevoTab,
        abrirNuevaVentana : abrirNuevaVentana,
        abrirNuevaVentanaParaPDF : abrirNuevaVentanaParaPDF,
        completarA2Digitos : completarA2Digitos,
        getExpresionRegularNroComprobante : getExpresionRegularNroComprobante,
        getExpresionRegularCodBarras : getExpresionRegularCodBarras,
        formatearNumeroComprobante : formatearNumeroComprobante,
        formatearNumeroComprobanteEspacio : formatearNumeroComprobanteEspacio,
        desglosarNumeroComprobante : desglosarNumeroComprobante,
        convertirStringABoolean : convertirStringABoolean
    }
}

var expresionesRegulares = new Array();
var accionTabHabilitada = true;

CONFIG.utils = {};

CONFIG.utils.mensajes = {
    
    bloqueoVentanaEmergente : "Su navegador ha bloqueado las ventanas emergentes de esta p&aacute;gina. Por favor, deshabilite el bloqueo de las mismas desde la configuraci&oacute;n del navegador.</br></br>Si no sabe como hacerlo, <strong>no cierre este cuadro</strong><em> y comun&iacute;quese con el &aacute;rea de sistemas.</em>"
}

$(document).ready (function() { 
     
    inicializarExpresionesRegulares();

    $('body').on('focus', "input[type='text']:not('.input-virtualkeyboard'), input[type='password']:not('.input-virtualkeyboard')", function (event) {
        
        $(this).one('mouseup', function(event){
            event.preventDefault();
        }).select();
        
    });
    
    //Evita que el input-proveedor-id gane el foco cuando se presiona tab mientras se hace ajax para traer info de un articulo
    $('body').on('keydown', function(event) {

        var charCode = obtenerKeyCode(event); 

        if(charCode == 9 && !accionTabHabilitada)  //tab 
        {
            event.preventDefault();
        }
    });
    
    //conversion automatica de coma a punto.
    $('body').on('blur', ".input-numerico", function () {
        $(this).val(transformarComaEnPunto($(this).val()));
    }); 
    
    /*
    console.log(formatearInputConDecimales("0.1E-7", 10));
    console.log(formatearInputConDecimales("0.1e-7", 10));
    console.log(formatearInputConDecimales("1.0e7", 10));
    console.log(formatearInputConDecimales("1.0e+7", 10));
    console.log(formatearInputConDecimales("2e7", 10));
    console.log(formatearInputConDecimales("3e+7", 10));
    console.log(formatearInputConDecimales("4e-7", 10));
    console.log(formatearInputConDecimales("5e-7", 10));
    console.log(formatearInputConDecimales("5", 10));
    console.log(formatearInputConDecimales("1"));
    console.log(formatearInputConDecimales("1.0"));
    console.log(formatearInputConDecimales("1.3"));
    console.log(formatearInputConDecimales("1.33"));
    console.log(formatearInputConDecimales("1.334"));
    console.log(formatearInputConDecimales("1.30"));
    console.log(formatearInputConDecimales("1.330"));
    console.log(formatearInputConDecimales("1.3360"));
    console.log(formatearInputConDecimales("1."));
    console.log(formatearInputConDecimales(".1"));
    console.log("======================================");*/
    
});

/* OBSOLETO
 * 
convierte numeros de notacion cientifica a notacion decimal. Por ej: 1e-3 -> 0.001

quitarNotacionExponencial= function(input){
    
    if(input == null || input == "")
    {
        return input;
    }
    var number = Number(input);

    var data= String(number).split(/[eE]/);
    if(data.length== 1)return data[0]; 

    var  z= '', sign= number<0? '-':'',
    str= data[0].replace('.', ''),
    mag = Number(data[1])+ 1;

    if(mag<0){
        z= sign + '0.';
        while(mag++) z += '0';
        
        return z + str.replace(/^\-/,'');
    }
    mag -= str.length;  
    while(mag--) z += '0';
    
    return str + z;
}*/

// Parche para que funcione en ie8    
if (!Object.create) {
    Object.create = (function(){
        function F(){}

        return function(o){
            if (arguments.length != 1) {
                throw new Error('Object.create implementation only accepts one parameter. - Error LibreriaCodimat.js Line 14');
            }
            F.prototype = o;
            return new F()
        }
    })()
}
// Parche para que indexOf() funcione en ie8   
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function inicializar_deteccion_capslock(id_input, id_warning) {
    
//Bind to capslockstate events and update display based on state 
    $(window).bind("capsOn", function(event) {
        if ($("#"+id_input+":focus").length > 0) {
            $("#"+id_warning).show();
        }
    });

    $(window).bind("capsOff capsUnknown", function(event) {
        $("#"+id_warning).hide();
    });

    $("#"+id_input).bind("focusout", function(event) {
        $("#"+id_warning).hide();
    });

    $("#"+id_input).bind("focusin", function(event) {
        if ($(window).capslockstate("state") === true) {
            $("#"+id_warning).show();
        }
    });
 
    //Initialize the capslockstate plugin.
    //Monitoring is happening at the window level.
    $(window).capslockstate();
}

/**
 * Evita que la presionar enter en cualquier input se realice el submit
 * @param {type} idSubmit
 * @returns {undefined}
 */
function evitarSubmitConEnterEnInputs(idSubmit) {
    
    $('body').on('keydown', "input[type='text']", function(event){ // evito que al presionar en cualquier input se envie el formulario. Lo permito solo para el submit
		
            var charCode = obtenerKeyCode(event);

            if(charCode == 13 && event.target.id != idSubmit) {
                    event.preventDefault();
            }
    });
}

/*
 *	Habilita o Deshabilita la accion del tab. Se usa por ejemplo, cuando se esta
 *	realizando un ajax para traer informacion de un articulo para evitar que gane el foco otro campo
 *	ante la reiterada presion del tab.
 */
function habilitarAccionTab(habilitar) {

	accionTabHabilitada = habilitar;
}

/*
 *  Despliega la lista del combobox
 */
function abrirSelect(id_select) {
    var event = document.createEvent('MouseEvents');
    event.initMouseEvent('mousedown', true, true, window);
    $('#'+id_select)[0].dispatchEvent(event);
}

/*
 *  Crea un iframe para la descarga de archivos
 *  desde url
 */
function descargarDesdeURL(url) {

    var hiddenIFrameID = 'hiddenDownloader',

    iframe = document.getElementById(hiddenIFrameID);

    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }

    iframe.src = url;
};


function transformarComaEnPunto(string) {
	
	return string.replace(",",".");
}

function obtenerVersionBrowser() {
    
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];

    return M.join(' ');
}

function inicializarExpresionesRegulares()
{
	expresionesRegulares['integer'] = /^\d+$/;
	expresionesRegulares['float'] = /^\d+(([,]|[.]){1}\d{1,2})?$/;        
        expresionesRegulares['float4'] = /^\d+(([,]|[.]){1}\d{1,4})?$/;
        expresionesRegulares['numeroValido'] = /^\d+(([,]|[.]){1}\d+)?$/;
        expresionesRegulares['numeroNotacionCientifica'] = /^\d+(([,]|[.]){1}\d+)?((([e]|[E])([+]|[-]){0,1}\d+){0,1})?$/;
        
        expresionesRegulares['codigoBarrasComprobante'] = /^[a-zA-Z]{1}\d{12}?$/;
        
        //En comprobantes visucomp
        expresionesRegulares['codBarrasComprobante'] = /^\d{1}[a-zA-Z]{1}\d{15}?$/;
        expresionesRegulares['numeroComprobante'] = /^[a-zA-Z]{1}\d{12}?$/;
        
        expresionesRegulares['code128'] = /^[a-zA-Z0-9]{1,42}$/;
        
        //No permite : 
        // \u0026        -> &
        // \u0022        -> "
        // #
        //_
        expresionesRegulares['generadorPDFTexto'] = /^[^\u0022\u0026#_]*$/; //
        
        // \u00C1 \u00E1 -> Á á
	// \u00C9 \u00E9 -> É é
	// \u00CD \u00ED -> Í í
	// \u00D3 \u00F3 -> Ó ó
	// \u00DA \u00FA -> Ú ú
        
        // \u00C4 \u00E4 -> Ä ä
        // \u00CB \u00EB -> Ë ë
        // \u00CF \u00EF -> Ï ï
        // \u00D6 \u00F6 -> Ö ö
	// \u00DC \u00FC -> Ü ü

	// \u00D1 \u00F1 -> Ñ ñ
	// \u0026        -> &
        // \u0022        -> "
        expresionesRegulares['cacarteresPermitidos'] = /^(?![\u0022-+,'()°_\.\-\/])[-+,'()°_\.\-\s\/\w\u0022\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00C4\u00E4\u00CB\u00EB\u00CF\u00EF\u00D6\u00F6\u00DC\u00FC\u00D1\u00F1\u0026]+$/; //string que contenga cualquier caracter alfanumerico incluyendo ñÑ y & . - ó _ pero que no comience con . - _
        
        //\u00D1 \u00F1 -> Ñ ñ
        //\u0026        -> &
	expresionesRegulares['cacarteresPermitidosURL'] = /^[ /\.\:_\-\w\u00D1\u00F1\u0026]+$/; //string que contenga cualquier caracter alfanumerico - _ . : /& Ñ ñ
 
        expresionesRegulares['cacarteresPermitidosID'] = /^[\-A-Za-z0-9]+$/; //string que contenga cualquier caracter alfanumerico -
        
        expresionesRegulares['cacarteresPermitidosIDArticulo'] = /^(?![\s\/\.])[\-\s\/\.\w]+$/; //string que contenga cualquier caracter alfanumerico - ó _
        
        expresionesRegulares['double8y8'] = /^\d{1,8}(([,]|[.]){1}\d{2,8})?$/;        
        
        expresionesRegulares['codigoBarrasPesaje'] = /^[b|B]{1}[0-9]{9}$/;
}

/*
 * Verifica que la expresión $expr cumpla con la espresión regular
 * correspondiente a $type
 */
function controlExpresionRegular(type, expression)
{	
    if(expresionesRegulares[type].test(expression))//.toLowerCase()))
    {
        return true;
    }
    else 
    {
        return false;
    }
}

/*
 *  Parsea el input para que quede con cantDecimales decimales
 *  Acepta tambien numeros en formato cientifico
 *  
 *  cantCeros cantidad de 0 a mostrar si en numero a formatear no posee parte decimal.
 */
function formatearInputConDecimales(input, cantDecimales, cantCeros) {
    
    if(cantDecimales == null)
    {
        cantDecimales = 2;
    }  
    
    if(cantCeros == null || cantCeros > cantDecimales)
    {
        cantCeros = cantDecimales;
    }
    
    input = "" + input; //convierto el input a string por si es un float

    if(input == null || input == "")
    {
        input = "0.";
        for(var p = 0; p < cantCeros; p++) {
            input = input + "0";
        }
    }
    else
    {
        if(input.charAt(0) == CONFIG.variables.sistem.separador.decimal) {
            input = "0" + input;
        }
        
        if(input.charAt(input.length - 1) == CONFIG.variables.sistem.separador.decimal) {
            input = input + "0";
        }
        
        if(controlExpresionRegular('numeroValido', input) || controlExpresionRegular('numeroNotacionCientifica', input)) {            
            input = "" + Number(input).toFixed(cantDecimales);
        }    
        
        var posPunto = input.indexOf(CONFIG.variables.sistem.separador.decimal); //necesariamente hay un punto decimal.
        
        var posPrimerCero = 0;
        var hayCeros = false;

        //verifica y establece la variab le hayCeros en true si todos los decimales son 0
        for(var j = posPunto; j < input.length; j++) {

            if(input.charAt(j) == '0' && !hayCeros)
            {                
                posPrimerCero = j;
                hayCeros = true;
            }
            else            
            {
                if(input.charAt(j) != '0')
                {
                    hayCeros = false;
                }                
            }
        }

        //si todos los decimales son 0 -> trunco a cantCeros
        if(hayCeros) {
            
            if(posPrimerCero <= posPunto + cantCeros)
            {
                if(posPunto + cantCeros < input.length)
                {
                    posPrimerCero = posPunto + cantCeros;
                }
                else
                {
                    posPrimerCero = input.length;
                }
                
                posPrimerCero = posPrimerCero + 1;
            }

            input = input.substring(0, posPrimerCero);
        }
    }

    return input;
}

/*
 * Añade separador de miles.
 */
function formatearNumeroConSeparadorMiles(input) {
 
    var parteEntera = "";
    var parteDecimal = "";
    
    input = "" + input; //convierto el input a string por si es un float
    
    var indexSeparador = input.indexOf(CONFIG.variables.sistem.separador.decimal);
    
    var numeroFinal = "";
    
    if(indexSeparador != -1)
    {
        parteEntera = input.substring(0, indexSeparador);
        parteDecimal = input.substring(indexSeparador, input.length);
    }
    else
    {
        parteEntera = input;
    }
    
    if(parteEntera.length > 3) //hace falta separador de miles.
    {
        var modulo = parteEntera.length % 3;
       
        if(modulo != 0)
        {
            numeroFinal = parteEntera.substring(0, modulo) + CONFIG.variables.sistem.separador.miles; 
           
            parteEntera = parteEntera.substring(modulo, parteEntera.length);
        }
        
        var veces = parteEntera.length / 3;
        
        for(var i = 1; i < veces; i++)
        {
            numeroFinal = numeroFinal + parteEntera.substring(0, 3) + CONFIG.variables.sistem.separador.miles; 
            parteEntera = parteEntera.substring(3, parteEntera.length);
        }        
        
        //numeroFinal = numeroFinal.substring(0, numeroFinal.length - 1);
        
        return numeroFinal + parteEntera + parteDecimal;
    }
    else
    {
        return parteEntera + parteDecimal; 
    }
}
    
/*
 * Verifica si charCode es un codigo de tecla presionada, valido para ingresar
 * un float.
 */
function checkearKeysInputDecimales(charCode) {

    if(checkearKeysInputNumerico(charCode) || checkearTeclasPuntuacion(charCode)) 
    {
        return true;
    }
    else 
    {
        return false;
    }  
}

function checkearKeysInputNumerico(charCode) {
    // tab
    if(checkearTeclasNumericas(charCode) || charCode == 9 || checkearTeclasBorradoras(charCode) || checkearTeclasFuncionales(charCode)) 
    {
        return true;
    }
    else 
    {
        return false;
    } 
}

/*
 * Verifica si charCode es un codigo de tecla presionada, valido para ingresar
 * una fecha.
 */
function checkearCaracteresPermitidosFecha(charCode) {

    // numeros, numeros teclado numerico ó -
    if((charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106) || charCode == 189 || charCode == 109) {

        return true;
    }
    else {
        return false;
    }  
}

/*
 * Verifica si charCode es un codigo de tecla presionada, valido para un input de fecha.
 */
function checkearCaracteresPermitidosInputFecha(charCode) {
    
    if(!checkearCaracteresPermitidosFecha(charCode) && !checkearTeclasBorradoras(charCode) && !checkearTeclasFuncionales(charCode))
    {
        return false;
    }
    else 
    {
        return true;
    }
}

/*
 *	Caracteres permitidos utilidados en nombres 
 */
function checkearCaracteresPermitidos(input){

   // var prohibidos = ['"', '&', '?'];

	// \u00C1 \u00E1 -> Á á
	// \u00C9 \u00E9 -> É é
	// \u00CD \u00ED -> Í í
	// \u00D3 \u00F3 -> Ó ó
	// \u00DA \u00FA -> Ú ú
	// \u00DC \u00FC -> Ü ü
	// \u00D1 \u00F1 -> Ñ ñ
	// \u0026        -> &
   
    var permitidos = /^(?!['-+()°_\.\-\'\/])['-+()°_\.\-\s\/\'\w\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1\u0026]+$/; //string que contenga cualquier caracter alfanumerico incluyendo ñÑ y & . - ó _ pero que no comience con . - _ 	
 
    if(permitidos.test(input)) {
        return true;
    }
    else {

        return false;
    }
}

/*
 *	Utilizado para checkeo de caracteres en URLs
 */
function checkerCaracteresPermitidosURL(input){
   
    //\u00D1 \u00F1 -> Ñ ñ
    //\u0026        -> &
	
	var permitidos = /^[ /\.\:_\-\w\u00D1\u00F1\u0026]+$/; //string que contenga cualquier caracter alfanumerico - _ . : /& Ñ ñ

    if(permitidos.test(input)) {
        return true;
    }
    else {

        return false;
    }
}

/*
 *	Utilizado para checker caracteres en codigos
 */
function checkearCaracteresParaId(input) {

    var permitidos = /^[_\-\w]+$/; //string que contenga cualquier caracter alfanumerico - ó _

    if(permitidos.test(input)) {
        return true;
    }
    else {

        return false;
    }
}

//son teclas que no afectan ninguna funcionalelidad (por eso el tab NO esta incluido - al presionar tab siempre se hace algo)
function checkearTeclasFuncionales(charCode) {

     // enter, inicio, fin, esc, bloq num, caps lock,
     // alt, ctrl, home, end, F1 - F12
     // shift, flechas: arriba, abajo, izq, der
    if(charCode == 13 || charCode == 36 || charCode == 35 || charCode == 27 || charCode == 144 || charCode == 20 ||
       charCode == 18 || charCode == 17 || charCode == 36 || charCode == 35 || (charCode >= 112 && charCode <=123) ||
       charCode == 16 || charCode == 38 || charCode == 40 || charCode == 37 || charCode == 39) {
        return true;
    }
    else {
        return false;
    } 
}

function checkearTeclasNumericas(charCode) {
    // numeros, numeros teclado numerico
    if((charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106)) 
    {
        return true;
    }
    else 
    {
        return false;
    } 
}

function checkearTeclasBorradoras(charCode) {
    // backspace, supr
    if(charCode == 8 || charCode == 46) 
    {
        return true;
    }
    else 
    {
        return false;
    } 
}

function checkearTeclasPuntuacion(charCode) {
    // ",", ".", ". teclado numerico"
    if(charCode == 188 || charCode == 110 || charCode == 190) {

        return true;
    }
    else {
        return false;
    } 
}

/*
 * Obtiene a partir de un evento javascript el codigo de la tecla presionada
 */
function obtenerKeyCode(event) {
    var charCode = (typeof event.which == "undefined") ? event.keyCode : event.which;
    return charCode;
}

/*
 * Obtiene parámetros de la URL. Name es el nombre del parametro a obtener
 * REF: http://www.jquery4u.com/snippets/url-parameters-jquery
 */

$.urlParam = function(name){
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}

/*
 * Javascript trim, ltrim, rtrim
 * http://www.webtoolkit.info/
 */
 
//unión de ambas funciones ltrim y rtrim
function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}
 
//ltrim quita los espacios o caracteres indicados al inicio de la cadena
function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
//rtrim quita los espacios o caracteres indicados al final de la cadena
function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

//rtrim quita los espacios o caracteres indicados al final de la cadena
function reemplazarTodos(str, chars, toRep) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]", "g"), toRep);
}

/*
 * Retorna la fecha actual en formato dd/mm/yyyy
 */

function fechaActual() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();

    if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} var today = dd+'/'+mm+'/'+yyyy;

    return today;
}


function abrirUrlEnNuevoTab(url)
{
    var win = window.open(url, '_blank');
    
    if(win) {
        win.focus();
    }
    else
    {
        mostrarModalAdvertencia(CONFIG.utils.mensajes.bloqueoVentanaEmergente);
    }
}

function abrirNuevaVentana(url, fullscreen, ancho, alto, titulo)
{
	var top = 0;
	var left = 0;
	var width = ancho;
	var height = alto;
	
	if(fullscreen)
	{
		width = screen.width;
		height = screen.height;
	}
	else 
	{	
		var valorTop = $(window).innerHeight() - height;
		var valorLeft = $(window).innerWidth() - width;
		
		if(valorTop > 0 ) {
			top = valorTop / 2;		
		}
		
		if(valorLeft > 0 ) {
			left = valorLeft / 2;
		}	
	}
        
	var newWindow = window.open(url, titulo, "scrollbars=1,width="+width+",height="+height+",left="+left+",top="+top);
       
        if(newWindow)
        {
            if(titulo != "")
            {
                newWindow.document.title = titulo;
            } 
        }
        else
        {
            mostrarModalAdvertencia(CONFIG.utils.mensajes.bloqueoVentanaEmergente);
        }
        
        
}

function abrirNuevaPestania(url){
	var newWindow = window.open(url);
       
        if(newWindow){
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = url;
            a.click(); 
        }
        else{
            mostrarModalAdvertencia(CONFIG.utils.mensajes.bloqueoVentanaEmergente);
        }
        
        
}

function abrirNuevaVentanaParaPDF(url, fullscreen, ancho, alto, titulo)
{   
    abrirNuevaVentana(CONFIG.proyectos.recursosGenerales.vistas.contenedorPDF+"?nombreVentana="+encodeURIComponent(titulo)+"&url="+url, fullscreen, ancho, alto, "");
}

function completarA2Digitos(entero){
    var respuesta = '00';
    if (entero < 10){
        respuesta = '0'+entero;
    }
    else{
        respuesta = entero;
    }
    return respuesta;
}