/*
 *  ---------------------------------------------
 *              Alejandro Battista
 *  ---------------------------------------------
 */

var DetalleArticuloInventario = function () {

    var codigoArticulo = "";
    var descripcionArticulo = "";
    var cantidadIngresada = 0;
    var pesoIngresado = 0;
    var fechaInsercion = null;
    var horaInsercion = null;
    var usuario = "";    
    var terminal = "";
    var contenedor = "";
    var posicion = "";
    var fueraLista = -1;
    var tipoCarga = -1;
    var estado = -1;
    var indiceDetalleInventario = -1;
    var indiceCabeceraInventario = -1;
    var indicePadre = -1;
    var indiceRegistro = -1;            
    
    getCodigoArticulo = function() {
        return codigoArticulo;
    },

    getDescripcionArticulo = function() {
        return descripcionArticulo;
    },

    getCantidadIngresada = function() {
        return cantidadIngresada;
    },

    getPesoIngresado = function() {
        return pesoIngresado;
    },

    getFechaInsercion = function() {
        return fechaInsercion;
    },

    getHoraInsercion = function() {
        return horaInsercion;
    },

    getUsuario = function() {
        return usuario;
    },

    getTerminal = function() {
        return terminal;
    },

    getContenedor = function() {
        return contenedor;
    },

    getPosicion = function() {
        return posicion;
    },

    getFueraLista = function() {
        return fueraLista;
    },

    getTipoCarga = function() {
        return tipoCarga;
    },

    getEstado = function() {
        return estado;
    },

    getIndiceDetalleInventario = function() {
        return indiceDetalleInventario;
    },

    getIndiceCabeceraInventario = function() {
        return indiceCabeceraInventario;
    },

    getIndicePadre = function() {
        return indicePadre;
    },

    getIndiceRegistro = function() {
        return indiceRegistro;
    },
    
    setCodigoArticulo = function(codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    },

    setDescripcionArticulo = function(descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    },

    setCantidadIngresada = function(cantidadIngresada) {
        this.cantidadIngresada = cantidadIngresada;
    },

    setPesoIngresado = function(pesoIngresado) {
        this.pesoIngresado = pesoIngresado;
    },

    setFechaInsercion = function(fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    },

    setHoraInsercion = function(horaInsercion) {
        this.horaInsercion = horaInsercion;
    },

    setUsuario = function(usuario) {
        this.usuario = usuario;
    },

    setTerminal = function(terminal) {
        this.terminal = terminal;
    },

    setContenedor = function(contenedor) {
        this.contenedor = contenedor;
    },

    setPosicion = function(posicion) {
        this.posicion = posicion;
    },

    setFueraLista = function(fueraLista) {
        this.fueraLista = fueraLista;
    },

    setTipoCarga = function(tipoCarga) {
        this.tipoCarga = tipoCarga;
    },

    setEstado = function(estado) {
        this.estado = estado;
    },

    setIndiceDetalleInventario = function(indiceDetalleInventario) {
        this.indiceDetalleInventario = indiceDetalleInventario;
    },

    setIndiceCabeceraInventario = function(indiceCabeceraInventario) {
        this.indiceCabeceraInventario = indiceCabeceraInventario;
    },

    setIndicePadre = function(indicePadre) {
        this.indicePadre = indicePadre;
    },

    setIndiceRegistro = function(indiceRegistro) {
        this.indiceRegistro = indiceRegistro;
    };
    
    return {
        
        getCodigoArticulo: getCodigoArticulo,        
        getDescripcionArticulo: getDescripcionArticulo,
        getCantidadIngresada: getCantidadIngresada,
        getPesoIngresado: getPesoIngresado, 
        getFechaInsercion: getFechaInsercion, 
        getHoraInsercion: getHoraInsercion,
        getUsuario: getUsuario,
        getTerminal: getTerminal, 
        getContenedor: getContenedor,
        getPosicion: getPosicion,
        getFueraLista: getFueraLista,
        getTipoCarga: getTipoCarga, 
        getEstado: getEstado,
        getIndiceDetalleInventario: getIndiceDetalleInventario, 
        getIndiceCabeceraInventario: getIndiceCabeceraInventario,
        getIndicePadre: getIndicePadre,
        getIndiceRegistro: getIndiceRegistro,
        setCodigoArticulo: setCodigoArticulo, 
        setDescripcionArticulo: setDescripcionArticulo,
        setCantidadIngresada: setCantidadIngresada, 
        setPesoIngresado: setPesoIngresado, 
        setFechaInsercion: setFechaInsercion,
        setHoraInsercion: setHoraInsercion,
        setUsuario: setUsuario,
        setTerminal: setTerminal, 
        setContenedor: setContenedor,
        setPosicion: setPosicion,
        setFueraLista: setFueraLista,
        setTipoCarga: setTipoCarga,
        setEstado: setEstado,
        setIndiceDetalleInventario: setIndiceDetalleInventario,
        setIndiceCabeceraInventario: setIndiceCabeceraInventario, 
        setIndicePadre: setIndicePadre, 
        setIndiceRegistro: setIndiceRegistro
    }
}


