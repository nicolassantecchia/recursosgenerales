/*
 *  ---------------------------------------------
 *              Alejandro Battista
 *  ---------------------------------------------
 */

var DatosTotalesModificacionCostos = function () {

    //Atributos Privados
    var codigoSubrubro = -1,
        totalRecalculado = 0,
        totalOriginal = 0,
        diferencia = 0,
        porcentajeDiferencia = 0,
        fechaModificacion = '',
        horaModificacion = '',
        idInventario = 0,
        estado = 0,
        
        getCodigoSubrubro = function() {
             return codigoSubrubro;
        },
                
        getTotalRecalculado = function() {
             return totalRecalculado;
        },
                
        getTotalOriginal = function() {
             return totalOriginal;
        },
                
        getDiferencia = function() {
             return diferencia;
        },
                
        getPorcentajeDiferencia = function() {
             return porcentajeDiferencia;
        },
                
        getFechaModificacion = function() {
             return fechaModificacion;
        },
                
        getHoraModificacion = function() {
             return horaModificacion;
        },
                
        getIdInventario = function() {
             return idInventario;
        },
                
        getEstado = function() {
             return estado;
        },

        setCodigoSubrubro = function(cs) {
             codigoSubrubro = cs;
        },
                
        setTotalRecalculado = function(tr) {
             totalRecalculado = tr;
        },
                
        setTotalOriginal = function(to) {
             totalOriginal = to;
        },
                
        setDiferencia = function(d) {
             diferencia = d;
        },
                
        setPorcentajeDiferencia = function(pd) {
             porcentajeDiferencia = pd;
        },
                
        setFechaModificacion = function(fm) {
             fechaModificacion = fm;
        },
                
        setHoraModificacion = function(hm) {
             horaModificacion = hm;
        },
                
        setIdInventario = function(id) {
             idInventario = id;
        },
                
        setEstado = function(e) {
             estado = e;
        };
        
    return  {                               
                
                getCodigoSubrubro: getCodigoSubrubro,
                getTotalRecalculado: getTotalRecalculado,                
                getTotalOriginal: getTotalOriginal,
                getDiferencia: getDiferencia,
                getPorcentajeDiferencia: getPorcentajeDiferencia,
                getFechaModificacion: getFechaModificacion,
                getHoraModificacion: getHoraModificacion,
                getIdInventario: getIdInventario,
                getEstado: getEstado,
                setCodigoSubrubro: setCodigoSubrubro,
                setTotalRecalculado: setTotalRecalculado,                
                setTotalOriginal: setTotalOriginal,
                setDiferencia: setDiferencia,
                setPorcentajeDiferencia: setPorcentajeDiferencia,
                setFechaModificacion: setFechaModificacion,
                setHoraModificacion: setHoraModificacion,
                setIdInventario: setIdInventario,
                setEstado: setEstado
            }    
}