/*
 *  -------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  -------------------------------------------
 */

var VinculacionArticuloProveedor = function(mi_articuloCodimat, mi_articuloProveedor) {

    var articuloCodimat = mi_articuloCodimat,
        articuloProveedor = mi_articuloProveedor,
        fechaVigencia = '',
        factorCorreccionCosto = 0,
        relacionEntreArticulos = 0,
        variacionCostoVinculado_clasificacion1 = 0,
        variacionCostoVinculado_porcentaje1 = 0,
        variacionCostoVinculado_clasificacion2 = 0,
        variacionCostoVinculado_porcentaje2 = 0,
        variacionCostoVinculado_clasificacion3 = 0,
        variacionCostoVinculado_porcentaje3 = 0,

        getArticuloCodimat = function() {
            return articuloCodimat;
        },

        getArticuloProveedor = function() {
            return articuloProveedor;
        },

        getFactorCorreccionCosto = function() {
            return factorCorreccionCosto;
        },

        getFechaVigencia = function() {
            return fechaVigencia;
        },

        getRelacionEntreArticulos = function() {
            return relacionEntreArticulos;
        },

        getPorcentajeVariacionCostoVinculado1 = function() {
            return variacionCostoVinculado_porcentaje1;
        },

        getPorcentajeVariacionCostoVinculado2 = function() {
            return variacionCostoVinculado_porcentaje2;
        },

        getPorcentajeVariacionCostoVinculado3 = function() {
            return variacionCostoVinculado_porcentaje3;
        },

        getClasificacionVariacionCostoVinculado1 = function() {
            return variacionCostoVinculado_clasificacion1;
        },

        getClasificacionVariacionCostoVinculado2 = function() {
            return variacionCostoVinculado_clasificacion2;
        },

        getClasificacionVariacionCostoVinculado3 = function() {
            return variacionCostoVinculado_clasificacion3;
        },

        setFactorCorreccionCosto = function(factor) {
            factorCorreccionCosto = factor;
        },

        setFechaVigencia = function(fecha) {
            fechaVigencia = fecha;
        },

        setRelacionEntreArticulos = function(relacion) {
            relacionEntreArticulos = relacion;
        },

        setPorcentajeVariacionCostoVinculado1= function(clasificacion) {
            variacionCostoVinculado_porcentaje1 = clasificacion;
        },

        setPorcentajeVariacionCostoVinculado2= function(clasificacion) {
            variacionCostoVinculado_porcentaje2 = clasificacion;
        },

        setPorcentajeVariacionCostoVinculado3= function(clasificacion) {
            variacionCostoVinculado_porcentaje3 = clasificacion;
        },

        setClasificacionVariacionCostoVinculado1= function(clasificacion) {
            variacionCostoVinculado_clasificacion1 = clasificacion;
        },

        setClasificacionVariacionCostoVinculado2= function(clasificacion) {
            variacionCostoVinculado_clasificacion2 = clasificacion;
        },

        setClasificacionVariacionCostoVinculado3= function(clasificacion) {
            variacionCostoVinculado_clasificacion3 = clasificacion;
        };

    return {

        getArticuloCodimat : getArticuloCodimat,
        getArticuloProveedor : getArticuloProveedor,
        getFactorCorreccionCosto : getFactorCorreccionCosto,
        getFechaVigencia : getFechaVigencia,
        getRelacionEntreArticulos : getRelacionEntreArticulos,
        getPorcentajeVariacionCostoVinculado1 : getPorcentajeVariacionCostoVinculado1, 
        getPorcentajeVariacionCostoVinculado2 : getPorcentajeVariacionCostoVinculado2,
        getPorcentajeVariacionCostoVinculado3 : getPorcentajeVariacionCostoVinculado3,
        getClasificacionVariacionCostoVinculado1 : getClasificacionVariacionCostoVinculado1,
        getClasificacionVariacionCostoVinculado2 : getClasificacionVariacionCostoVinculado2,
        getClasificacionVariacionCostoVinculado3 : getClasificacionVariacionCostoVinculado3,
        setFactorCorreccionCosto : setFactorCorreccionCosto,
        setFechaVigencia : setFechaVigencia,
        setRelacionEntreArticulos : setRelacionEntreArticulos,
        setPorcentajeVariacionCostoVinculado1 : setPorcentajeVariacionCostoVinculado1,
        setPorcentajeVariacionCostoVinculado2 : setPorcentajeVariacionCostoVinculado2,
        setPorcentajeVariacionCostoVinculado3 : setPorcentajeVariacionCostoVinculado3,
        setClasificacionVariacionCostoVinculado1 : setClasificacionVariacionCostoVinculado1,
        setClasificacionVariacionCostoVinculado2 : setClasificacionVariacionCostoVinculado2,
        setClasificacionVariacionCostoVinculado3 : setClasificacionVariacionCostoVinculado3

    };
}