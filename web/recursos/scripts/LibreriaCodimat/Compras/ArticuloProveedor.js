/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  ---------------------------------------------
 */

function esIDIndefinidoArticuloProveedor(input) {
    
    if(input.toUpperCase() == CONFIG.variables.compras.articuloProveedor.noDefinido || input.toUpperCase() == CONFIG.variables.compras.articuloProveedor.noDefinidoBD)
    {
        return true;        
    }
    else
    {
        return false;
    }
}

var ArticuloProveedor = function (mi_id, mi_nombre) { 

    //Atributos Privados
    var id = mi_id,                         //String
        idInterno,                          //int
        nombre = mi_nombre,                 //String
        proveedor = null,                   //Proveedor
        subrubroProveedor = null,           //SubrubroProveedor
        tipoUnidadDeEntrega = 0,            //entero
        fechaVigencia,                      //fecha
        unidadDePrecio = 0,                 //real
        unidadDeFacturacion = 0,            //real
        precioLista = 0,                    //real
        bonificacionesYRecargos = '',       //String
        precioCosto = 0,                    //real
        peso = 0,                           //real
        codigoIva = 0,                      //entero
        valorIva = 0.0 ,                     //float
        codigoDeBarras= '',                 //String
        infoAdicional = '',                 //String  
	itemOrdenCompra = null,             //ItemOrdenDeCompra
	vinculaciones = null,               //ListaDeVinculacionesArticuloProveedor
        cantidad = 0,
        
        bonificacion1 = 0,
        bonificacion2 = 0,
        bonificacion2Calculada = false,
    
        getBonificacionesYRecargos = function(){
            return bonificacionesYRecargos;
        },

        getCodigoIva = function() {
            return codigoIva;
        },
                
        getValorIva = function() {
            return valorIva;
        },

        getFechaVigencia = function() {
            return fechaVigencia;
        },

        getId = function() {
            return id.toUpperCase();
        },
                
        /**
         * Retorna el id asignado automaticamente por el sistema al articulo 
         * en el momento en que fue dado de alta (es un entero)
         */
        getIdInterno = function () {
            return idInterno;
        },        

        getNombre = function() {
            return nombre;
        },

        getPrecioCosto = function() {
            return precioCosto;
        },

        getPeso = function() {
            return peso;
        },

        getPrecioLista = function() {
            return precioLista;
        },

        getProveedor = function() {
            return proveedor;
        }

        getSubrubro = function() {
            return subrubroProveedor;
        },

        getTipoUnidadDeEntrega = function() {
            return tipoUnidadDeEntrega;
        },

        getUnidadDeFacturacion = function() {
            return unidadDeFacturacion;
        },

        getUnidadDePrecio = function() {
            return unidadDePrecio;
        },
		
        getItemOrdenDeCompra = function() {
            return itemOrdenCompra;
        },

        getVinculaciones = function() {
            return vinculaciones;
        },
                
        getBonificacion1 = function() {
            return bonificacion1;
        },

        getBonificacion2 = function() {
            return bonificacion2;
        };
        
        getCantidad = function() {
            return cantidad;
        },

        setVinculaciones = function(vinc) {
            vinculaciones = vinc;
        },

        setId = function(mi_id) {
            id = mi_id;
        },
        
        setIdInterno = function (mi_idInterno) {
            idInterno = mi_idInterno;
        },

        setProveedor = function(prov) {
            proveedor = prov;
        },

        setSubrubro = function(subrubro) {
            subrubroProveedor = subrubro;
        },

        setBonificacionesYRecargos = function(bonif) {
            bonificacionesYRecargos = bonif;
        },

        setCodigoIva = function(codIva) {
            codigoIva = codIva;
        },
                
        setValorIva = function(valIva) {
            valorIva = valIva;
        },

        setFechaVigencia = function(fecha) {
            fechaVigencia = fecha;
        },

        setPrecioCosto = function(pCosto) {
            precioCosto = pCosto;
        },

        setPeso = function(mi_peso) {
            peso = mi_peso;
        },

        setPrecioLista = function(mi_precio) {
            precioLista = mi_precio;
        },

        setTipoUnidadDeEntrega = function(tipoUnidadEntrega) {
            tipoUnidadDeEntrega = tipoUnidadEntrega;
        },

        setUnidadDeFacturacion = function(unidadFacturacion) {
            unidadDeFacturacion = unidadFacturacion;
        },

        setUnidadDePrecio = function(unidadPrecio) {
            unidadDePrecio = unidadPrecio;
        },

        getCodigoDeBarras = function() {
            return codigoDeBarras;
        },

        setCodigoDeBarras = function(codigoBarras) {
            codigoDeBarras = codigoBarras;
        },

        getInfoAdicional = function() {
            return infoAdicional;
        },

        setInfoAdicional = function(info) {
            infoAdicional = info;
        },
		
        setItemOrdenDeCompra = function(item) {
            itemOrdenCompra = item;
        },
          
        setBonificacion1 = function(bonif1) {
            bonificacion1 = bonif1;
        },

        setBonificacion2 = function(bonif2) {
            bonificacion2 = bonif2;
        },
                
        setCantidad = function(mi_cantidad) {
            cantidad = mi_cantidad;
        },
                
        /**
        * Retorna <code>true</code> si la bonificacion 2 fué obtenida a partir de
        * la agrupación de otras bonificaciones, <code>false</code> en caso contrario.
        * @return 
        */
        getBonificacion2Calculada = function() {
            return bonificacion2Calculada;
        },

       /**
        * Establece si la bonificacion 2 fué obtenida a partir de
        * la agrupación de otras bonificaciones o no, segun el valor de <code>calculada</code>.
        */
        setBonificacion2Calculada = function(calculada) {
            bonificacion2Calculada = calculada;
        };

    return  {

        getBonificacionesYRecargos : getBonificacionesYRecargos,
        getCodigoIva : getCodigoIva,
        getValorIva : getValorIva,
        getFechaVigencia : getFechaVigencia,
        getId : getId,
        getIdInterno : getIdInterno,
        getNombre : getNombre,
        getPrecioCosto : getPrecioCosto,
        getPeso : getPeso,
        getPrecioLista : getPrecioLista,
        getProveedor : getProveedor,
        getSubrubro : getSubrubro,
        getTipoUnidadDeEntrega : getTipoUnidadDeEntrega,
        getUnidadDeFacturacion : getUnidadDeFacturacion,
        getUnidadDePrecio :  getUnidadDePrecio,
        getItemOrdenDeCompra : getItemOrdenDeCompra,
        getVinculaciones : getVinculaciones,
        getBonificacion1 : getBonificacion1,
        getBonificacion2 : getBonificacion2,
        getCantidad : getCantidad,
        
        
        setVinculaciones : setVinculaciones,
        setId : setId,
        setIdInterno : setIdInterno,
        setProveedor : setProveedor,
        setSubrubro : setSubrubro,
        setBonificacionesYRecargos : setBonificacionesYRecargos,
        setCodigoIva : setCodigoIva,
        setValorIva : setValorIva,
        setFechaVigencia : setFechaVigencia,
        setPrecioCosto : setPrecioCosto,
        setPeso : setPeso,
        setPrecioLista : setPrecioLista,
        setTipoUnidadDeEntrega :  setTipoUnidadDeEntrega,
        setUnidadDeFacturacion : setUnidadDeFacturacion,
        setUnidadDePrecio : setUnidadDePrecio,
        setCodigoDeBarras : setCodigoDeBarras,
        getCodigoDeBarras : getCodigoDeBarras,
        getInfoAdicional : getInfoAdicional,
        setInfoAdicional : setInfoAdicional,
        setItemOrdenDeCompra : setItemOrdenDeCompra,
        setBonificacion1 : setBonificacion1,
        setBonificacion2 : setBonificacion2,
        setCantidad : setCantidad,
        
        getBonificacion2Calculada : getBonificacion2Calculada,
        setBonificacion2Calculada : setBonificacion2Calculada
    };   
}