/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeRubrosProveedor = function () {    

    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(id_rubro) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_rubro) {
                return itemActual;
            }                
        }
        return itemActual;
    };
    
    nuevaLista.deleteRubro = function(id_rubro) {
        
        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_rubro) {
                
                this.deleteItem(i);
                break;
            }                
        }
    };

    return nuevaLista;
}