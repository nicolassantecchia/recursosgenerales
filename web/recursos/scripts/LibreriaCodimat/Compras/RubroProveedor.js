/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  ---------------------------------------------
 */

var RubroProveedor = function (mi_id, mi_nombre, mi_familia) {

    //Atributos Privados
    var familia = mi_familia,        //FamiliaProveedor
        id = mi_id,             //String
        nombre = mi_nombre,         //String
        infoAdicional = '',  //String
        subrubros = new ListaDeSubrubrosProveedor(),      //Lista 

        getFamilia = function() {
            return familia;
        },

        getId = function() {
            return id;
        },

        getInfoAdicional = function() {
            return infoAdicional;
        },

        getNombre = function() {
            return nombre;
        },

        getSubrubros = function() {
            return subrubros;
        },
                
        setSubrubros = function(lista) {
            subrubros = lista;
        },

        setInfoAdicional = function(info) {
            infoAdicional = info;
        };
    	
        return  {                               
            getId: getId,
            getInfoAdicional: getInfoAdicional,
            getNombre: getNombre,
            getFamilia: getFamilia,
            getSubrubros : getSubrubros,
            setSubrubros : setSubrubros,
            setInfoAdicional: setInfoAdicional
        }  
}