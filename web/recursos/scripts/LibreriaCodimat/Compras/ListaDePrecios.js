/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Enero 2015
 *  ---------------------------------------------
 */

var ListaDePrecios = function () { 
    
    var fecha = null,
        fechaReemplazo = null,
        pathAbsoluto,
        tipoLista,

        getAnotacion = function() {
            return anotacion;
        },
        getFecha = function() {
            return fecha;
        },
        getFechaReemplazo = function() {
            return fechaReemplazo;
        },
        getPathAbsoluto = function() {
            return pathAbsoluto;
        },
        getTipo = function() {
            return tipoLista;
        },

        setTipo = function(mi_tipoLista) {
            tipoLista = mi_tipoLista;
        },
        setPathAbsoluto = function(mi_pathAbsoluto) {
            pathAbsoluto = mi_pathAbsoluto;
        },    
        setFechaReemplazo = function(mi_fechaReemplazo) {
            this.fechaReemplazo = mi_fechaReemplazo;
        },
        setFecha = function(mi_fecha) {
            this.fecha = mi_fecha;
        };
    
    return {
        
        getFecha : getFecha,
        getFechaReemplazo : getFechaReemplazo,
        getPathAbsoluto : getPathAbsoluto,
        getTipo : getTipo,

        setTipo : setTipo,
        setPathAbsoluto : setPathAbsoluto,    
        setFechaReemplazo : setFechaReemplazo,
        setFecha : setFecha
    };
}
