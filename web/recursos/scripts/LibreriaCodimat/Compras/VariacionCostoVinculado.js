/*
 *  -------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  -------------------------------------------
 */

CONFIG.variacionCostoVinculado = {
    
    detalle : {length : 50},
    porcentaje : {min : 0.01, max : 100.00}
}


var VariacionCostoVinculado = function(mi_calificacion, mi_porcentaje) {

    var porcentaje = mi_porcentaje,
        calificacion = mi_calificacion,
        detalle = '',
        proveedor = null,               //Proveedor

        getCalificacion = function() {
            return calificacion;
        },

        getDetalle = function() {
            return detalle;
        },

        getPorcentaje = function() {
            return porcentaje;
        },

        getProveedor = function() {
            return proveedor;
        },

        setDetalle = function(mi_detalle) {
            detalle = mi_detalle;
        },

        setProveedor = function(mi_proveedor) {
            proveedor = mi_proveedor;
        },

        setCalificacion = function(califi) {
            calificacion = califi;
        };

        return {

            getCalificacion : getCalificacion,
            getDetalle : getDetalle,
            getPorcentaje : getPorcentaje,
            getProveedor : getProveedor,
            setDetalle : setDetalle,
            setProveedor : setProveedor,
            setCalificacion : setCalificacion

        };
}