/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ArticuloRecepcion = function (mi_id, mi_nombre) { 

    //Atributos Privados
    var id = mi_id,                         //String
        idInterno,                          //int
        nombre = mi_nombre,                 //String
        proveedor = null,                   //Proveedor
        tipoUnidadDeEntrega = 0,            //entero
        unidadDeFacturacion = 0,            //real
        peso = 0,                           //real
        codigoDeBarras= '',                 //String
        infoAdicional = '',                 //String  
	vinculaciones = null,               //ListaDeVinculacionesArticuloProveedor
        cantidad = 0,
        esSobrante = false,
        cantidadRecibida = 0;
        

        getId = function() {
            return id.toUpperCase();
        },
                
        /**
         * Retorna el id asignado automaticamente por el sistema al articulo 
         * en el momento en que fue dado de alta (es un entero)
         */
        getIdInterno = function () {
            return idInterno;
        },        

        getNombre = function() {
            return nombre;
        },


        getPeso = function() {
            return peso;
        },


        getProveedor = function() {
            return proveedor;
        }


        getTipoUnidadDeEntrega = function() {
            return tipoUnidadDeEntrega;
        },

        getUnidadDeFacturacion = function() {
            return unidadDeFacturacion;
        },


        getVinculaciones = function() {
            return vinculaciones;
        },

        
        getCantidad = function() {
            return cantidad;
        },
                
        esSobrante = function() {
            return esSobrante;
        },
                
        getCantidadRecibida = function() {
            return cantidadRecibida;
        },

        setVinculaciones = function(vinc) {
            vinculaciones = vinc;
        },

        setId = function(mi_id) {
            id = mi_id;
        },
        
        setIdInterno = function (mi_idInterno) {
            idInterno = mi_idInterno;
        },

        setProveedor = function(prov) {
            proveedor = prov;
        },

        setPeso = function(mi_peso) {
            peso = mi_peso;
        },


        setTipoUnidadDeEntrega = function(tipoUnidadEntrega) {
            tipoUnidadDeEntrega = tipoUnidadEntrega;
        },



        getCodigoDeBarras = function() {
            return codigoDeBarras;
        },

        setCodigoDeBarras = function(codigoBarras) {
            codigoDeBarras = codigoBarras;
        },

        getInfoAdicional = function() {
            return infoAdicional;
        },

        setInfoAdicional = function(info) {
            infoAdicional = info;
        },
		
                
        setCantidad = function(mi_cantidad) {
            cantidad = mi_cantidad;
        },
                
        setSobrante = function(isSobrante) {
            esSobrante = isSobrante;
        },
                
        setCantidadRecibida = function(mi_cantidad) {
            cantidadRecibida = mi_cantidad;
        };

    return  {
        getId : getId,
        getIdInterno : getIdInterno,
        getNombre : getNombre,
        getPeso : getPeso,
        getProveedor : getProveedor,
        getTipoUnidadDeEntrega : getTipoUnidadDeEntrega,
        getVinculaciones : getVinculaciones,
        getCantidad : getCantidad,
        esSobrante : esSobrante,
        getCantidadRecibida: getCantidadRecibida,
        
        setVinculaciones : setVinculaciones,
        setId : setId,
        setIdInterno : setIdInterno,
        setProveedor : setProveedor,
        setPeso : setPeso,
        setTipoUnidadDeEntrega :  setTipoUnidadDeEntrega,
        setCodigoDeBarras : setCodigoDeBarras,
        getCodigoDeBarras : getCodigoDeBarras,
        getInfoAdicional : getInfoAdicional,
        setInfoAdicional : setInfoAdicional,
        setCantidad : setCantidad,
        setSobrante : setSobrante,
        setCantidadRecibida : setCantidadRecibida
    };   
}

