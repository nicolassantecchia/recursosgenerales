/*
 *  -------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  -------------------------------------------
 */

var Familia = function (nombre, id){ //String, entero

    this.nombre = nombre;                   //String
    this.ubicacionLista;                    //entero
    this.id = id;                           //entero
    this.marca;                             //entero
    this.listaDeRubros;                     //Lista
    
    this.setMarca = function(marca) {
        this.marca = marca;
    }

    this.setUbicacionLista = function(ubicacionLista) {
        this.ubicacionLista = ubicacionLista;
    }

    this.getId = function() {
        return this.id;
    }

    this.getMarca = function() {
        return this.marca;
    }

    this.getNombre = function() {
        return this.nombre;
    }

    this.getUbicacionLista = function() {
        return this.ubicacionLista;
    }

    this.asignarListaDeRubros = function(listaDeRubros){
        this.listaDeRubros = listaDeRubros;
    }

    this.getListaDeRubros = function(){
        return this.listaDeRubros;
    }
}