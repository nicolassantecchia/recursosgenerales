/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ListaDeArticulosRecepcion = function () {    

    var nuevaLista = Object.create(new Lista());
    
    nuevaLista.insertarArticuloRecepcion = function(articuloRecepcion) {

        this.insert(articuloRecepcion);
        
    },
            
    nuevaLista.recuperarArticuloRecepcion = function(posicion) {

        var articuloRecepcion;

        articuloRecepcion = this.getItem(posicion);
        
        return articuloRecepcion;
        
    },

    nuevaLista.searchItem = function(id_articulo) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {
            itemActual = this.getItem(i);
            if(itemActual.getIdInterno() == id_articulo) {
                return itemActual;
            }
            else{
                itemActual = null;
            }
        }
        return itemActual;
    };
    
    nuevaLista.eliminar = function(id_articulo) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {
            itemActual = this.getItem(i);
            if(itemActual.getIdInterno() == id_articulo) {
                this.deleteItem(i);
                break;
            }
        }
    };

    return nuevaLista;
}