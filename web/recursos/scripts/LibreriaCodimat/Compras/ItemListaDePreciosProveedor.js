var ItemListaDePreciosProveedor = function (idInternoArt) { 
    
    var itemListaDePreciosProveedor = Object.create(new ItemGenericoArticuloProveedorCompra(idInternoArt));
    
    var fechaAlta = null,
        fechaActualizacion = null,
        tipoActualizacion = 0 ,     //int
        vigente = false;             //boolean
        
    itemListaDePreciosProveedor.getFechaAlta = function() {
        return fechaAlta;
    };
    
    itemListaDePreciosProveedor.getFechaActualizacion = function() {
        return fechaActualizacion;
    };

    itemListaDePreciosProveedor.getTipoActualizacion = function() {
        return tipoActualizacion;
    };

    itemListaDePreciosProveedor.isVigente = function() {
        return vigente;
    };
    
    itemListaDePreciosProveedor.getPrecioDeLista = function(){
        return this.getPrecioUnitario();
    };
    
    itemListaDePreciosProveedor.getPrecioDeCosto = function(){
        return this.getPrecioUnitarioConBonificacion();
    };

    itemListaDePreciosProveedor.setFechaAlta = function(mi_fechaAlta) {
        fechaAlta = mi_fechaAlta;
    };

    itemListaDePreciosProveedor.setFechaActualizacion = function(mi_fechaActualizacion) {
        fechaActualizacion = mi_fechaActualizacion;
    };

    itemListaDePreciosProveedor.setTipoActualizacion = function(mi_tipoActualizacion) {
        tipoActualizacion = mi_tipoActualizacion;
    };

    itemListaDePreciosProveedor.setVigente = function(mi_vigente) {
        vigente = mi_vigente;
    };
    
    itemListaDePreciosProveedor.setPrecioDeLista = function(mi_precioDeLista){
        this.setPrecioUnitario(mi_precioDeLista);
    };
    
    return itemListaDePreciosProveedor;
}