/*
 *  ---------------------------------------------
 *  Fernando Prieto - Enero 2015
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeCondicionDeEntrega = function(mi_transporte) {    

    var nuevaLista = Object.create(new Lista()),
        transporte = mi_transporte; // Transporte
        
        nuevaLista.getTransporte = function() {
            return transporte;
        };
                
        nuevaLista.setTransporte = function(mi_transporte) {
            transporte = mi_transporte;
        };
        
    return nuevaLista;
}

