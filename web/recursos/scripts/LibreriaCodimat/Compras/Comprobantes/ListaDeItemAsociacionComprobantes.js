var ListaDeItemAsociacionComprobantes = function () {    

    var nuevaLista = Object.create(new Lista());

    nuevaLista.indexOf = function(idItem) {

        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual != null && idItem.toUpperCase() == itemActual.getNroCPRB().toUpperCase())
            {	
                return i;
            }              
        }
        
        return -1;
    };

    return nuevaLista;
}