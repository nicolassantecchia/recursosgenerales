var ItemAsociacionComprobantes = function (comprobante) { 

    //Atributos Privados
    
    var 
        comprobanteAsociado = comprobante, //ComprobanteCompra
        idProveedor = 0,                //int
        tipoCPRB = 0,                   //int
        nroCPRB = '',                   //String
        fechaAsociacion = null,         //FechaV2
        tipoAsociacionComprobante=0,    //int
        estadoAsociacion=0,             //int
        idUsuario=0,                    //int
        activa = true,                  //boolean
        
        getComprobanteAsociado = function() {
            return comprobanteAsociado;
        },

        getIdProveedor = function() {
            return idProveedor;
        },

        getTipoCPRB = function() {
            return tipoCPRB;
        },

        getNroCPRB = function() {
            return nroCPRB;
        },

        getFechaAsociacion = function() {
            return fechaAsociacion;
        },

        getTipoAsociacionComprobante = function() {            
            return tipoAsociacionComprobante;
        },        

        getEstadoAsociacion = function() {
            return estadoAsociacion;
        },

        getIdUsuario = function() {
            return idUsuario;
        },

        getActiva = function() {
            return activa;
        },
                
                
        setComprobanteAsociado = function(comprobante) {
            comprobanteAsociado = comprobante;
        },

        setIdProveedor = function(id_Proveedor) {
            idProveedor =id_Proveedor;
        },

        setTipoCPRB = function(tipo_CPRB) {
            tipoCPRB =tipo_CPRB;
        },

        setNroCPRB = function(nro_CPRB) {
            nroCPRB =nro_CPRB;
        },

        setFechaAsociacion = function(fecha_Asociacion) {
            fechaAsociacion =fecha_Asociacion;
        },

        setTipoAsociacionComprobante = function(tipo_Asociacion_Comprobante) {            
            tipoAsociacionComprobante =tipo_Asociacion_Comprobante;
        },        

        setEstadoAsociacion = function(estado_Asociacion) {
            estadoAsociacion =estado_Asociacion;
        },

        setIdUsuario = function(id_Usuario) {
            idUsuario =id_Usuario;
        },

        setActiva = function(es_activa) {
            activa =es_activa;
        };

    return {
        getComprobanteAsociado : getComprobanteAsociado,
        getIdProveedor : getIdProveedor,
        getTipoCPRB : getTipoCPRB,
        getNroCPRB : getNroCPRB,
        getFechaAsociacion : getFechaAsociacion,
        getTipoAsociacionComprobante : getTipoAsociacionComprobante,
        getEstadoAsociacion : getEstadoAsociacion,    
        getIdUsuario : getIdUsuario,
        getActiva : getActiva,    
        setComprobanteAsociado : setComprobanteAsociado,
        setIdProveedor : setIdProveedor,    
        setTipoCPRB : setTipoCPRB,     
        setNroCPRB : setNroCPRB,    
        setFechaAsociacion : setFechaAsociacion,    
        setTipoAsociacionComprobante : setTipoAsociacionComprobante,    
        setEstadoAsociacion : setEstadoAsociacion,
        setIdUsuario : setIdUsuario,    
        setActiva : setActiva 
        
    };
}
