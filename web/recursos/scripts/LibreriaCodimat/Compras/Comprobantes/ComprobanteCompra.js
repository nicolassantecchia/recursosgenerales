var ComprobanteCompra = function (mi_proveedor, mi_numeroComprobante, mi_usuario) { 

    //Atributos Privados
    
    var descuento1 = 0, //Formato porcentual
        descuento2 = 0, //Formato porcentual
        bonificacion2Calculada = false,
        descuento3 = 0, //Formato porcentual
        descuento4 = 0, //Formato porcentual
        proveedor = mi_proveedor, //Proveedor
        numeroComprobante = mi_numeroComprobante, //String
        usuario = mi_usuario, //Usuario
        fechaCreacion = null, //FechaV2
        fechaVencimiento = null, //FechaV2        
        codigoBarras = "", //String
        tipoCprb = null, //TipoComprobante
        listaDePreciosAsociada, //ListaDePrecios
        estado, //int
        estadoRecepcionDetalle,
        estadoVigenciaDetalle,

        listaDeItemComprobanteCompra = new ListaDeItemComprobanteCompra(),       
        listaDeCondicionDeEntrega = new ListaDeCondicionDeEntrega(),
        listaDeCondicionDePago = new ListaDeCondicionDePago(),
        
        getFechaCreacion = function() {
            return fechaCreacion;
        },

        getFechaVencimiento = function() {
            return fechaVencimiento;
        },

        getNumeroComprobante = function() {
            return numeroComprobante;
        },

        getProveedor = function() {
            return proveedor;
        },

        getUsuario = function() {
            return usuario;
        },

        getCodigoBarras = function() {            
            return codigoBarras;
        },        

        getTipoComprobante = function() {
            return tipoCprb;
        },

        getDescuento1 = function() {
            return descuento1;
        },

        getDescuento2 = function() {
            return descuento2;
        },

        getDescuento3 = function() {
            return descuento3;
        },

        getDescuento4 = function() {
            return descuento4;
        },

        getListaDeItemComprobanteCompra = function() {
            return listaDeItemComprobanteCompra;
        },

        getListaDePreciosAsociada = function() {
            return listaDePreciosAsociada;
        },

        getListaDeCondicionDeEntrega = function() {
            return listaDeCondicionDeEntrega;
        },

        getListaDeCondicionDePago = function() {
            return listaDeCondicionDePago;
        },

        getDescuentoEnPesos1 = function() {
            return getImporteNetoGravadoSinDescuentosGenerales() * descuento1 / 100;
        },

        getDescuentoEnPesos2 = function() {
            return getImporteNetoGravadoSinDescuentosGenerales() * (1 - descuento1 / 100) * descuento2 / 100;
        },
          
        /**
        * Retorna el importe toal de IVA 21% considerando descuentos generales
        * @return 
        */
        getImporteTotalIVA21 = function(){
        
            var totalIVA21 = 0;        
            var itemActual = null;

            for (var i=0; i < listaDeItemComprobanteCompra.size(); i++) {
                itemActual = listaDeItemComprobanteCompra.getItem(i);

                if(itemActual.getValorIva() == 21.0){
                    //totalIVA21 = totalIVA21 + itemActual.getPrecioTotal() - itemActual.getPrecioNetoGravadoConBonificacion();
                    totalIVA21 = totalIVA21 + itemActual.getTotalIVAItem();
                    
                }
            }

            totalIVA21 = totalIVA21 * (1 - descuento1 / 100) * (1 - descuento2 / 100) * (1 - descuento3 / 100) * (1 - descuento4 / 100);
            
            return totalIVA21;
        },
    
        /**
        * Retorna el importe toal de IVA 21% considerando descuentos generales
        * @return 
        */
        getImporteTotalIVA105 = function(){
        
            var totalIVA105 = 0;        
            var itemActual = null;

            for (var i=0; i < listaDeItemComprobanteCompra.size(); i++) {
                itemActual = listaDeItemComprobanteCompra.getItem(i);

                if(itemActual.getValorIva() == 10.5){
                    //totalIVA105 = totalIVA105 + itemActual.getPrecioTotal() - itemActual.getPrecioNetoGravadoConBonificacion();
                    totalIVA105 = totalIVA105 + itemActual.getTotalIVAItem();
                }
            }
            
            totalIVA105 = totalIVA105 * (1 - descuento1 / 100) * (1 - descuento2 / 100) * (1 - descuento3 / 100) * (1 - descuento4 / 100);

            return totalIVA105;
        }, 
                
        /**
        * Importe total de IVA (10.5% + 21%)
        * @return 
        */
        getImporteTotalIVA = function() {

            var totalIVA = 0;        
            var itemActual = null;

            for (var i=0; i < listaDeItemComprobanteCompra.size(); i++) {
                itemActual = listaDeItemComprobanteCompra.getItem(i);
                //totalIVA = totalIVA + itemActual.getPrecioTotal() - itemActual.getPrecioNetoGravadoConBonificacion();
                totalIVA = totalIVA + itemActual.getTotalIVAItem();
            }
            
            totalIVA = totalIVA * (1 - descuento1 / 100) * (1 - descuento2 / 100) * (1 - descuento3 / 100) * (1 - descuento4 / 100);

            return totalIVA;
       }
    
        /**
         * Importe total items CON bonificaciones, SIN IVA y SIN descuentos generales
         * @return 
         */
        getImporteNetoGravadoSinDescuentosGenerales = function(){

            var sumaNetoGravadoCondBonifItem = 0;
            var itemActual = null;
            
            for (var i=0; i < listaDeItemComprobanteCompra.size(); i++) {
                itemActual = listaDeItemComprobanteCompra.getItem(i);

                //Guarda la suma de los netos gravados con descuento de cada item
                sumaNetoGravadoCondBonifItem = sumaNetoGravadoCondBonifItem + itemActual.getPrecioNetoGravadoConBonificacion();
            }

            return sumaNetoGravadoCondBonifItem;
        },
                
        /**
         * Importe total items CON bonificaciones, SIN IVA y CON descuentos generales
         * @return 
         */
        getImporteNetoGravadoConDescuentosGenerales = function(){        

           return this.getImporteNetoGravadoSinDescuentosGenerales() * (1 - descuento1/100) * (1 - descuento2/100) * (1 - descuento3/100) * (1 - descuento4/100);
        },
    
        /**
         * Importe total items CON IVA, CON descuento de item y CON descuentos generales.
        * @return 
        */
        getImporteTotal = function(){
                        
            return this.getImporteNetoGravadoConDescuentosGenerales() + this.getImporteTotalIVA();
        },

        getPesoTotal = function(){

            var respuesta = 0;

            for (var i=0; i < listaDeItemComprobanteCompra.size(); i++) {
                respuesta = respuesta + listaDeItemComprobanteCompra.getItem(i).getPesoTotal();
            }

            return respuesta;
        },
                
        getEstado = function() {
            return estado;
        },
                
        getEstadoRecepcionDetalle = function() {
            return estadoRecepcionDetalle;
        },
                
        getEstadoVigenciaDetalle = function() {
            return estadoVigenciaDetalle;
        },

        setTipoComprobante = function(mi_tipoCprb) {
            tipoCprb = mi_tipoCprb;
        },

        setListaDeCondicionDeEntrega = function(mi_listaDeCondicionDeEntrega) {
            listaDeCondicionDeEntrega = mi_listaDeCondicionDeEntrega;
        },

        setListaDeCondicionDePago = function(mi_listaDeCondicionDePago) {
            listaDeCondicionDePago = mi_listaDeCondicionDePago;
        },

        setListaDeItemComprobanteCompra = function(mi_listaDeItemComprobanteCompra) {
            listaDeItemComprobanteCompra = mi_listaDeItemComprobanteCompra;
        },


        setListaDePreciosAsociada = function(mi_listaDePreciosAsociada) {
            listaDePreciosAsociada = mi_listaDePreciosAsociada;
        },

        setDescuento1 = function(mi_descuento1) {
            descuento1 = parseFloat(mi_descuento1);
        },

        setDescuento2 = function(mi_descuento2) {
            descuento2 = parseFloat(mi_descuento2);
        },

        setDescuento3 = function(mi_descuento3) {
            descuento3 = parseFloat(mi_descuento3);
        },

        setDescuento4 = function(mi_descuento4) {
            descuento4 = parseFloat(mi_descuento4);
        },  

        setCodigoBarras = function(mi_codigoBarras) {
            codigoBarras = mi_codigoBarras;
        },

        setFechaCreacion = function(mi_fechaCreacion) {
            fechaCreacion = mi_fechaCreacion;
        },

        setFechaVencimiento = function(mi_fechaVencimiento) {
            fechaVencimiento = mi_fechaVencimiento;
        },
                
        setEstado = function(estd) {
            estado = estd;
        },
                
        setEstadoRecepcionDetalle = function(estd) {
            estadoRecepcionDetalle = estd;
        },
                
        setEstadoVigenciaDetalle = function(estd) {
            estadoVigenciaDetalle = estd;
        },
                
        /**
        * Retorna <code>true</code> si la bonificacion 2 fué obtenida a partir de
        * la agrupación de otras bonificaciones, <code>false</code> en caso contrario.
        * @return 
        */
        getBonificacion2Calculada = function() {
            return bonificacion2Calculada;
        },

       /**
        * Establece si la bonificacion 2 fué obtenida a partir de
        * la agrupación de otras bonificaciones o no, segun el valor de <code>calculada</code>.
        */
        setBonificacion2Calculada = function(calculada) {
            bonificacion2Calculada = calculada;
        };

    return {
        getFechaCreacion : getFechaCreacion,
        getFechaVencimiento : getFechaVencimiento,
        getNumeroComprobante : getNumeroComprobante,
        getProveedor : getProveedor,
        getUsuario : getUsuario,
        getCodigoBarras : getCodigoBarras,
        getTipoComprobante : getTipoComprobante,    
        getDescuento1 : getDescuento1,
        getDescuento2 : getDescuento2,    
        getDescuento3 : getDescuento3,
        getDescuento4 : getDescuento4,    
        getListaDeItemComprobanteCompra : getListaDeItemComprobanteCompra,     
        getListaDePreciosAsociada : getListaDePreciosAsociada,    
        getListaDeCondicionDeEntrega : getListaDeCondicionDeEntrega,    
        getListaDeCondicionDePago : getListaDeCondicionDePago,    
        getDescuentoEnPesos1 : getDescuentoEnPesos1,
        getDescuentoEnPesos2 : getDescuentoEnPesos2,    
        getImporteTotalIVA21 : getImporteTotalIVA21,  
        getImporteTotalIVA105 : getImporteTotalIVA105, 
        getImporteTotalIVA : getImporteTotalIVA,
        getImporteNetoGravadoSinDescuentosGenerales : getImporteNetoGravadoSinDescuentosGenerales,
        getImporteNetoGravadoConDescuentosGenerales : getImporteNetoGravadoConDescuentosGenerales,
        getImporteTotal : getImporteTotal,
        getPesoTotal : getPesoTotal,      
        getEstado : getEstado,
        getEstadoRecepcionDetalle: getEstadoRecepcionDetalle,
        getEstadoVigenciaDetalle : getEstadoVigenciaDetalle,
        
        getBonificacion2Calculada : getBonificacion2Calculada,
        setBonificacion2Calculada : setBonificacion2Calculada,
        
        setTipoComprobante : setTipoComprobante,
        setListaDeCondicionDeEntrega : setListaDeCondicionDeEntrega,
        setListaDeCondicionDePago : setListaDeCondicionDePago,    
        setListaDeItemComprobanteCompra : setListaDeItemComprobanteCompra,      
        setListaDePreciosAsociada : setListaDePreciosAsociada,
        setDescuento1 : setDescuento1,
        setDescuento2 : setDescuento2,    
        setDescuento3 : setDescuento3,
        setDescuento4 : setDescuento4, 
        setCodigoBarras : setCodigoBarras,    
        setFechaCreacion : setFechaCreacion,
        setFechaVencimiento : setFechaVencimiento,
        setEstado : setEstado,
        setEstadoRecepcionDetalle : setEstadoRecepcionDetalle,
        setEstadoVigenciaDetalle : setEstadoVigenciaDetalle
    };
}
