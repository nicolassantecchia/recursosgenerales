var AsociacionComprobantesCompras = function (comprobanteBase) { 

    //Atributos Privados
    var 
        cprbBase = comprobanteBase, //ComprobanteCompra
        listaDeItemsAsociacionComprobantes = new ListaDeItemAsociacionComprobantes(),
        listaDeItemsAsociacionInversaComprobantes = new ListaDeItemAsociacionComprobantes(),
                
        getCprbBase = function() {
            return cprbBase;
        },
                
                
        setCprbBase = function(comprobante) {
            cprbBase = comprobante;
        },
                
        getListaDeItemsAsociacionComprobantes = function() {
            return listaDeItemsAsociacionComprobantes;
        },
                
                
        setListaDeItemsAsociacionComprobantes = function(lista) {
            listaDeItemsAsociacionComprobantes = lista;
        },
                
        getListaDeItemsAsociacionInversaComprobantes = function() {
            return listaDeItemsAsociacionInversaComprobantes;
        },
                
                
        setListaDeItemsAsociacionInversaComprobantes = function(lista) {
            listaDeItemsAsociacionInversaComprobantes = lista;
        }
    
    ;

    return {
        getCprbBase : getCprbBase,
        setCprbBase : setCprbBase,
        getListaDeItemsAsociacionComprobantes : getListaDeItemsAsociacionComprobantes,
        setListaDeItemsAsociacionComprobantes : setListaDeItemsAsociacionComprobantes,
        getListaDeItemsAsociacionInversaComprobantes : getListaDeItemsAsociacionInversaComprobantes,
        setListaDeItemsAsociacionInversaComprobantes : setListaDeItemsAsociacionInversaComprobantes
        
    };
}
