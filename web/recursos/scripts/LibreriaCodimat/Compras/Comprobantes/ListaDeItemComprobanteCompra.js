/*
 *  ---------------------------------------------
 *  Fernando Prieto - Noviembre 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeItemComprobanteCompra = function () {    

    var nuevaLista = Object.create(new Lista());

    nuevaLista.indexOf = function(idArticulo) {

        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual != null && idArticulo.toUpperCase() == itemActual.getIdInternoArticulo().toUpperCase())
            {	
                return i;
            }              
        }
        
        return -1;
    };

    return nuevaLista;
}