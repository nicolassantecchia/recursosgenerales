var RemitoProveedor = function (mi_proveedor, mi_numeroComprobante, mi_usuario) { 

    var remitoProveedor = Object.create(new ComprobanteCompra(mi_proveedor, mi_numeroComprobante, mi_usuario));
    
    return remitoProveedor;
}