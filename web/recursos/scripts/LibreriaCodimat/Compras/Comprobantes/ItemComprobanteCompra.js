var ItemComprobanteCompra = function (idArt, idInternoArt, detalle, cant, unidadDeMedida) { 
    
    var itemComprobanteCompra = Object.create(new ItemGenericoArticuloProveedorCompra(idInternoArt));

    //Atributos Privados
    var idArticulo = idArt,			//String
        
        detalleNombreArticulo = detalle,	//String Puede ser que se modifique el nombre, ejemplo: puerta izquierda
        cantidad = cant, 			//float
    //  unidadDePeso = 0,                       //UnidadDePeso 
        unidadDeMedida = unidadDeMedida,	//UnidadDeMedida
        pesoUnitario = 0, 			//float
        fecha = "",				//fecha
        

        numeroDeComprobante = "",		//String
        tipoDeComprobante = 0;			//int
        
    
        itemComprobanteCompra.getCantidad = function() {//
            return cantidad;
        };
        
        itemComprobanteCompra.getUnidadDeMedida = function() {//
            return unidadDeMedida;
        };
        
        itemComprobanteCompra.getDetalleNombreArticulo = function() {//
            return detalleNombreArticulo;
        };
        
        itemComprobanteCompra.getIdArticulo = function() {//
            return idArticulo;
        };
        
        
    /*    getUnidadDePeso = function() {
            return unidadDePeso;
        },*/
        itemComprobanteCompra.getPesoUnitario = function() {//
            return pesoUnitario;
        };
                
        itemComprobanteCompra.getPesoTotal = function() {//
            
            return pesoUnitario * cantidad;
        };
                
        itemComprobanteCompra.getProveedor = function() {
            return proveedor;
        };
                
        itemComprobanteCompra.getFecha = function() {//
            return fecha;
        };
        	
        itemComprobanteCompra.getNumeroDeComprobante = function() {//
            return numeroDeComprobante;
        };
                
        itemComprobanteCompra.getTipoDeComprobante = function() {//
            return tipoDeComprobante;
        };
        
        itemComprobanteCompra.setCantidad = function(cant) {//
            cantidad = cant;
        };
                
        itemComprobanteCompra.setUnidadDeMedida = function(mi_unidadDeMedida) {//
            unidadDeMedida = mi_unidadDeMedida;
        };
                
        itemComprobanteCompra.setPesoUnitario = function(pes) {//
            pesoUnitario = parseFloat(pes);
        };
                
        itemComprobanteCompra.setFecha = function(fcha) {//
            fecha = fcha;
        };
        
        itemComprobanteCompra.setTipoDeComprobante = function(tipoComp) {//
            tipoDeComprobante = tipoComp;
        };
        
        itemComprobanteCompra.setNumeroDeComprobante = function(nroComp) {//
            numeroDeComprobante = nroComp;
        };
        
        /**
         * Precio unitario (sin IVA) sin descuento
         * @return 
         */
        itemComprobanteCompra.getPrecioNetoGravadoSinBonificacion = function(){
            return this.getPrecioUnitario() * this.getCantidad();
        }

        /**
         * Precio unitario (sin IVA) con descuento
         * @return 
         */
        itemComprobanteCompra.getPrecioNetoGravadoConBonificacion = function() {
            return this.getPrecioUnitarioConBonificacion() * this.getCantidad();
        }
        
        /**
         * Importe total (con IVA) con descuento
         * @return 
         */
        itemComprobanteCompra.getPrecioTotal = function() {
            
            return this.getPrecioUnitarioConIVAConBonificacion() * this.getCantidad();
        }
        
        /**
        * Retorna el total de IVA aplicado sobre el precio unitario con descuentos multiplicado por la cantidad
        * @return 
        */
        itemComprobanteCompra.getTotalIVAItem = function() {
           return this.getTotalIVA() * this.getCantidad();
        }

        return itemComprobanteCompra;     
}