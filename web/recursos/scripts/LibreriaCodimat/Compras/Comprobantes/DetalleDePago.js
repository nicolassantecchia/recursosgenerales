var DetalleDePago = function (mi_id) { 
    
    var id = mi_id, //int
        detalle = "", //String
        
        getDetalle = function() {
            return detalle;
        },

        getId = function() {
            return id;
        },

        setDetalle = function(mi_detalle) {
            detalle = mi_detalle;
        },

        setId = function(mi_id) {
            id = mi_id;
        };
    
    return {        
        getDetalle : getDetalle,
        getId : getId,
        setDetalle : setDetalle,
        setId : setId
    }
}