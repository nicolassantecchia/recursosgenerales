/*
 *  -------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  -------------------------------------------
 */

var Articulo = function(mi_id, mi_nombre) {

    var id = mi_id,             //entero
        nombre = mi_nombre,     //string   
        vinculaciones = null,   //ListaDeVinculacionesArticuloProveedor
        peso = 0.0, //Double
        cantidad = 0.0, //Double
        unidadCalculo = 0.0, //Double
        precioCosto = 0.0, //Double
        precioCostoBackup = 0.0; //Double

        getId = function() {
            return id;
        },

        getNombre = function() {
            return nombre;
        },
                
        getPeso = function() {
            return peso;
        }, 
                
        setPeso = function(mi_peso) {
            peso = mi_peso;
        },
		
        getVinculaciones = function() {
            return vinculaciones;
        },

        setVinculaciones = function(vinc) {
            vinculaciones = vinc;
        },
                
        getCantidad = function() {
            return cantidad;
        }, 
                
        setCantidad = function(mi_cantidad) {
            cantidad = mi_cantidad;
        },
        
        getUnidadCalculo = function() {
            return unidadCalculo;
        },
            
        getPrecioCosto = function() {
            return precioCosto;
        },
                
        getPrecioCostoBackup = function() {
            return precioCostoBackup;
        },
        
        setUnidadCalculo = function(mi_unidadCalculo) {
            unidadCalculo = mi_unidadCalculo;
        },
            
        setPrecioCosto = function(mi_precioCosto) {
            precioCosto = mi_precioCosto;
        },
        
        setPrecioCostoBackup = function(mi_precioCosto) {
            precioCostoBackup = mi_precioCosto;
        };

    return {
        getId : getId,
        getNombre : getNombre,
        getPeso : getPeso,
        setPeso : setPeso,
        getVinculaciones : getVinculaciones,
        setVinculaciones : setVinculaciones,
        getCantidad : getCantidad,
        setCantidad : setCantidad,
        getUnidadCalculo : getUnidadCalculo,
        getPrecioCosto : getPrecioCosto,
        getPrecioCostoBackup : getPrecioCostoBackup,
        setUnidadCalculo : setUnidadCalculo,
        setPrecioCosto : setPrecioCosto,
        setPrecioCostoBackup : setPrecioCostoBackup
    };
};