/*
 *  ---------------------------------------------
 *  Fernando Prieto - Noviembre 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeVinculacionesArticuloProveedor = function () {    

    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(idProveedor, idArtProv, idArtCodimat) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(	itemActual.getArticuloProveedor().getProveedor().getId() == idProveedor &&
				itemActual.getArticuloProveedor().getId() == idArtProv &&
				itemActual.getArticuloCodimat().getId() == idArtCodimat) {
                return itemActual;
            }                
        }
        return itemActual;
    };

    return nuevaLista;
}