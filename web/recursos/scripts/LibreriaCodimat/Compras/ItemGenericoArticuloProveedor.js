var ItemGenericoArticuloProveedorCompra = function (idInternoArt) {
    
    var idInternoArticulo = idInternoArt,       //int
        proveedor = null, 			//Proveedor
        precioUnitario = 0,			//float
        bonificacion1 = 0,			//float
        bonificacion2 = 0,			//float	
        bonificacion2Calculada = false,         //boolean
        valorIva = 0,                           //float
        codigoIva = 0,				//int
        
        getIdInternoArticulo = function() {
            return idInternoArticulo;
        },
                
        getBonificacion1 = function() {
            return bonificacion1;
        },

        getBonificacion2 = function() {
            return bonificacion2;
        },
        
        /**
        * Precio sin IVA, sin descuento
        * @return 
        */    
        getPrecioUnitario = function() {
            return precioUnitario;
        },	

        /**
         * Precio sin IVA con descuento
         * @return 
         */
        getPrecioUnitarioConBonificacion = function() {
            return getPrecioUnitario() * (1 - bonificacion1/100) * (1 - bonificacion2/100);
        },
                
        /**
        * Retorna el total de IVA aplicado sobre el Precio Unitario CON descuentos
        * @return 
        */
        getTotalIVA = function() {        
            return getPrecioUnitarioConBonificacion() * getValorIvaFormatoCoeficiente();
        },

        /**
         * Precio con IVA, sin descuento
         * @return 
         */
        getPrecioUnitarioConIVASinBonificacion = function() {
            return precioUnitario * (1 + getValorIvaFormatoCoeficiente());
        },

        /**
         * Precio con bonificacion con IVA
         * @return 
         */
        getPrecioUnitarioConIVAConBonificacion = function() {
            return this.getPrecioUnitarioConBonificacion() * (1 + getValorIvaFormatoCoeficiente());
        },
                
        getValorIvaFormatoCoeficiente = function() {
            return getValorIva() / 100;
        },
                
        getValorIva = function() {
            return valorIva;
        },
                
        getCodigoIva = function() {//
            return codigoIva;
        },
                
        setProveedor = function(prov) {
            proveedor = prov;
        },        
                
        setBonificacion1 = function(b1) {
            bonificacion1 = parseFloat(b1);
        },
                
        setBonificacion2 = function(b2) {
            bonificacion2 = parseFloat(b2);
        },
                
        setPrecioUnitario = function(precioUnit) {
            precioUnitario = parseFloat(precioUnit);
        },
                
        setValorIva = function(vIva) { 
            valorIva = vIva;
        },

        setCodigoIva = function(codIva) {//
            codigoIva = codIva;
        },
                
        /**
     * Establece si la bonificacion 2 fué obtenida a partir de
     * la agrupación de otras bonificaciones o no, segun el valor de <code>calculada</code> seteando el valor correspondiente en <code>marca1</code>.
     */
    setMarcaBonificacion2Calculada = function(calculada) {
        
        bonificacion2Calculada = calculada;
    }
    
    /**
     * Retorna <code>true</code> si la bonificacion 2 fué obtenida a partir de
     * la agrupación de otras bonificaciones, <code>false</code> en caso contrario, obteniendo el valor correspondiente en <code>marca1</code>.
     * @return 
     */
    getMarcaBonificacion2Calculada = function() {
        
        return bonificacion2Calculada;
    }
    
    return {
        
        getIdInternoArticulo : getIdInternoArticulo,
        getBonificacion1 : getBonificacion1,
        getBonificacion2 : getBonificacion2,
        getPrecioUnitario : getPrecioUnitario,
        getPrecioUnitarioConBonificacion : getPrecioUnitarioConBonificacion,
        getPrecioUnitarioConIVASinBonificacion : getPrecioUnitarioConIVASinBonificacion,
        getPrecioUnitarioConIVAConBonificacion : getPrecioUnitarioConIVAConBonificacion,
        getTotalIVA : getTotalIVA,
        getValorIva : getValorIva,
        getCodigoIva : getCodigoIva,
        getValorIvaFormatoCoeficiente : getValorIvaFormatoCoeficiente,
        setProveedor : setProveedor,
        setBonificacion1 : setBonificacion1,
        setBonificacion2 : setBonificacion2,
        setPrecioUnitario : setPrecioUnitario,
        setValorIva : setValorIva,
        setCodigoIva : setCodigoIva,
        setMarcaBonificacion2Calculada : setMarcaBonificacion2Calculada,
        getMarcaBonificacion2Calculada : getMarcaBonificacion2Calculada
    }
}