/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  ---------------------------------------------
 */

var FamiliaProveedor = function (mi_id, mi_nombre, mi_proveedor) {

    //Atributos Privados
    var proveedor = mi_proveedor,      //Proveedor
        id = mi_id,             //String
        nombre = mi_nombre,         //String       
        infoAdicional = '',  //String
        rubros = new ListaDeRubrosProveedor(), //Lista

    	getId = function() {
            return id;
        },

        getInfoAdicional = function() {
            return infoAdicional;
        },

        getNombre = function() {
            return nombre;
        },

        getProveedor = function() {
            return proveedor;
        },

        getRubros = function() {
            return rubros;
        },
                
        setRubros = function(lista) {
            rubros = lista;
        },

        setInfoAdicional = function(info) {
            infoAdicional = info;
        };

    return  {                               
                getId: getId,
                getInfoAdicional: getInfoAdicional,
                getNombre: getNombre,
                getProveedor: getProveedor,
                getRubros: getRubros,
                setRubros : setRubros,
                setInfoAdicional: setInfoAdicional
            }  
}