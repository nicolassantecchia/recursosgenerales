/**
 *  REQUIERE configuracion.js que se encuentra en proyecto compras/recursos/scripts/config
 */

var OperacionesSobreServidorCompras = function() {

    var	urlParamUsuario = CONFIG.variables.sistem.sesion.parametros.usuario,            
        urlParamIP = CONFIG.variables.sistem.sesion.parametros.ip, 
        opCompras_utiles = new Utiles(), //Objeto creado para poder utlu
		opExpedi_utiles = new Utiles(),
        
        getMensajeErrorProveedor = function(idProveedor) {	
            return "Ocurri&oacute; un error al cargar los datos del proveedor <em>"+idProveedor+"</em>.";
        },
    
        getMensajeErrorFamiliasProveedor = function(idProveedor) {	
            return "Ocurri&oacute; un error al cargar las familias del proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorRubrosProveedor = function(idProveedor, idFamilia) {	
            return "Ocurri&oacute; un error al cargar los rubros del proveedor <em>"+idProveedor+"</em>, Familia <em>"+idFamilia+"</em>.";
        },

        getMensajeErrorSubrubrosProveedor = function(idProveedor, idFamilia, idRubro) {	
            return "Ocurri&oacute; un error al cargar los subrubros del proveedor <em>"+idProveedor+"</em>, Familia <em>"+idFamilia+"</em>, Rubro <em>"+idRubro+"</em>.";
        },
                
        getMensajeErrorDatosActualizacionCostos = function(idProveedor) {	
            return "Ocurri&oacute; un error al cargar los Datos de actualizacion de costos relacionados al Proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorArticulosProveedor = function(idProveedor, idFamilia, idRubro, idSubrubro) {	
            return "Ocurri&oacute; un error al cargar los art&iacute;culos del proveedor <em>"+idProveedor+"</em>, Familia <em>"+idFamilia+"</em>, Rubro <em>"+idRubro+"</em>, Subrubro <em>"+idSubrubro+"</em>.";
        },

        getMensajeErrorListaPreciosProveedor = function(idProveedor) {
            return "Ocurri&oacute; un error al cargar la &uacute;ltima lista de precios asociada al proveedor = <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorDatosUltimaCompraPorProveedor = function(idProveedor, idArticuloProv) {
            return "Ocurri&oacute; un error al cargar la informaci&oacute;n correspondiente a la &uacute;ltima compra del art&iacute;culo <em>"+idArticuloProv+"</em> para el proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorArticuloCodimat = function(idArticuloCodimat) {
            return "Ocurri&oacute; un error al cargar la informaci&oacute;n del art&iacute;culo interno <em>"+idArticuloCodimat+"</em>.";
        },
                
        getMensajeErrorArticuloFueraDeLista = function(idArticuloFueraDeLista) {
            return "Ocurri&oacute; un error al cargar la informaci&oacute;n del art&iacute;culo fuera de lista <em>"+idArticuloFueraDeLista+"</em>.";
        },

        getMensajeErrorArticuloProveedor = function(idProveedor, idArticuloProveedor) {
            return "Ocurri&oacute; un error al cargar los datos del art&iacute;culo <em>"+idArticuloProveedor+"</em> para el proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorBajaArticuloProveedor = function(idProveedor, idArticuloProveedor) {
            return "Ocurri&oacute; un error al intentar dar de baja al art&iacute;culo <em>"+idArticuloProveedor+"</em> para el proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorVinculacionesXArticuloProveedor = function(idProveedor, idArticuloProveedor) {
            return "Ocurri&oacute; un error al cargar las vinculaciones correspondientes al art&iacute;culo proveedor <em>"+idArticuloProveedor+"</em> del proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorVinculacionesXArticuloCodimat = function(idArticuloCodimat) {
            return "Ocurri&oacute; un error al cargar las vinculaciones correspondientes al art&iacute;culo interno <em>"+idArticuloCodimat+"</em>.";
        },

        getMensajeErrorEliminacionVinculacion = function(idProveedor, idArticuloProveedor, idArticuloCodimat) {
            return "Error al intentar eliminar la vinculaci&oacute;n entre el art&iacute;culo interno <em>"+idArticuloCodimat+"</em> y el art&iacute;culo proveedor <em>"+idArticuloProveedor+"</em> correspondiente al proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorVariacionesPorProveedor = function(idProveedor) {
            return "Ocurri&oacute; un error al cargar las variaciones correspondientes al proveedor <em>"+idProveedor+"</em>.";
        },
                
        getMensajeErrorEditarIdArticulo = function() {
            return "Ocurri&oacute; un error al editar el c&oacute;digo del art&iacute;culo.";
        },
                
        getMensajeErrorInsertarVinculacion = function() {
            return "Ocurri&oacute; un error al insertar la vinculaci&oacute;n.";
        },       

        /* OBSOLETO
        getMensajeErrorInsertarVariacionCosto = function(idProveedor) {
            return "Ocurri&oacute; un error al insertar la variaci&oacute;n para el proveedor <em>"+idProveedor+"</em>.";
        },*/
                
        getMensajeErrorComprobanteCompra = function(idProveedor, tipoCprb, numeroCprb) {
            return "Ocurri&oacute; un error al recuperar el comprobante de compra n&uacute;mero <em>"+numeroCprb+"</em> tipo <em>"+tipoCprb+"</em> para el proveedor <em>"+idProveedor+"</em>.";
        },

        getMensajeErrorInsertarOrdenDeCompra = function() {
            return "Ocurri&oacute; un error al insertar la orden de compra.";
        },
                
        getMensajeErrorInsertarRemitoProveedor = function() {
            return "Ocurri&oacute; un error al insertar el remito o factura del proveedor.";
        },
                
        getMensajeErrorAnularComprobanteDeCompra = function() {
            return "Ocurri&oacute; un error al anular el comprobante.";
        },

        getMensajeErrorInsertarEditarArticuloProveedor = function() {
            return  "Ocurri&oacute; un error al crear / editar el art&iacuteculo.";
        },

        getMensajeErrorInsertarFamiliaProveedor = function(excepcion, idProveedor, idFamilia, nombreFamilia, insertando) {

            if(excepcion) {
                
                if(insertando) {
                    return "Ocurri&oacute; un error al crear la familia.";
                }
                else {
                    return "Ocurri&oacute; un error al editar la familia.";
                }
            }
            else {
                var string = "La familia <em>"+nombreFamilia+"</em> con c&oacute;digo <em>"+idFamilia+"</em> para el proveedor <em>"+idProveedor+"</em> no pudo ser ";
                
                if(insertando) {
                    string = string + "creada.";
                }
                else {
                    string = string + "editada."; 
                }
                
                return string;
            }
        },
                
        getMensajeErrorDarBajaFamiliaProveedor = function(idProveedor, idFamilia) {
                        
            return "Ocurrió un error al dar de baja la familia con código " + idFamilia;
        },
        
        getMensajeErrorDarBajaRubroProveedor = function(idProveedor, idFamilia, idRubro) {
                        
            return "Ocurrió un error al dar de baja el rubro con código " + idRubro;
        },
                
        getMensajeErrorDarBajaSubrubroProveedor = function(idProveedor, idFamilia, idRubro, idSubrubro) {
            
            return "Ocurrió un error al dar de baja el subrubro con código " + idSubrubro;
        },
        
        getMensajeErrorInsertarRubroProveedor = function(excepcion, idProveedor, idFamilia, idRubro, nombreRubro, insertando) {

            if(excepcion) {
                
                if(insertando) {
                    return "Ocurri&oacute; un error al crear el rubro.";
                }
                else {
                    return "Ocurri&oacute; un error al editar el rubro.";
                }
            }
            else {
                
                var string = "El rubro <em>"+nombreRubro+"</em> con c&oacute;digo <em>"+idRubro+"</em> para la familia <em>"+idFamilia+"</em> correspondiente al proveedor <em>"+idProveedor+"</em> no pudo ser ";
                
                if(insertando) {
                    string = string + "creado.";
                }
                else {
                    string = string + "editado."; 
                }
                
                return string;
            }
        },

        getMensajeErrorInsertarSubrubroProveedor = function(excepcion, idProveedor, idFamilia, idRubro, idSubrubro, nombreSubrubro, insertando) {

            if(excepcion) {
                
                if(insertando) {
                    return "Ocurri&oacute; un error al crear el subrubro.";
                }
                else {
                    return "Ocurri&oacute; un error al editar el subrubro.";
                }
            }
            else {
                var string = "El subrubro <em>"+nombreSubrubro+"</em> con c&oacute;digo <em>"+idSubrubro+"</em> para el rubro <em>"+idRubro+"</em> de la familia <em>"+idFamilia+"</em> correspondiente al proveedor <em>"+idProveedor+"</em> no pudo ser ";
                
                if(insertando) {
                    string = string + "creado.";
                }
                else {
                    string = string + "editado."; 
                }
                
                return string;
            }
        },
                
        getMensajeErrorActualizarListaDePreciosProveedor = function(idProveedor) {
            return "Ocurrio un error al actualizar los precios de la lista de precios del proveedor <em>"+idProveedor+"</em>";
        },
                
        getMensajeErrorItemListaDePreciosPorFecha = function(idProveedor, idInternoArticulo) {
            
            return "Ocurrio un error al recuperar la entrada en la lista de precios correspondiente art&iacute;culo con id interno <em>"+idInternoArticulo+"</em> del proveedor <em>"+idProveedor+"</em>";
        },
                
        getMensajeErrorInsertarArticuloDentroLista = function() {            
            return "Ocurrio un error al insertar el art&iacute;culo";
        },
                
        getMensajeErrorRecuperarComprobantesProveedor = function() {            
            return "Ocurrio un error al recuperar los remitos de proveedor";
        },
                
        getMensajeErrorRecuperarComprobanteProveedor = function(nroComprobante) {            
            return "Ocurrio un error al recuperar el comprobante <em>"+nroComprobante+"</em>";
        },
                
        getMensajeErrorActualizarFormulasSubrubro = function(nroSubrubro) {            
            return "Ocurrio un error al actualizar las f&oacute;rmulas del subrubro <em>"+nroSubrubro+"</em>";
        },
                
        getMensajeErrorCrearInventario = function() {            
            return "Ocurrio un error al crear inventario";
        },
                
        getMensajeErrorFinalizarInventario = function() {            
            return "Ocurrio un error al finalizar inventario";
        },
               
        crearObjetoComprobante = function(xmlResponse, data) {
            
            var $comprobante = $(xmlResponse).find("comprobante");
                            
            var listaInactivos = new ListaDeItemComprobanteCompra();
            var comprobante = null;

            if($comprobante.length > 0) {  
                
                var $totales = $(xmlResponse).find("totales");
                var $articulos = $(xmlResponse).find("articuloProveedor");
                var $articulosDadosDeBaja = $(xmlResponse).find("articuloProveedorBaja");
                var $datosNroCprb = $(xmlResponse).find("datosNroCprb");
                var codBarras = $(xmlResponse).find("datosCodimatYCodBarras").attr("codBarras");
                var $transporte = $(xmlResponse).find("transporte");
                var $estado = $(xmlResponse).find("estado");

                var intTipoCprb = $comprobante.attr("tipoComprobante");

                var tipoCprb = null;
                var detalleEstadoRecepcion = $(xmlResponse).find("estadoRecepcion").attr("detalleEstado");
                
                if(intTipoCprb == CONFIG.tiposComprobante.oc.getCodigo()) {
                    
                    tipoCprb = CONFIG.tiposComprobante.oc;
                    comprobante = new OrdenDeCompra(data.proveedor, $datosNroCprb.attr("cprbNroCompleto"));
                }
                else {
                    
                    //Si es Remito Proveedor o Factura Proveedor
                    if(intTipoCprb == CONFIG.tiposComprobante.rp.getCodigo() || intTipoCprb == CONFIG.tiposComprobante.fp.getCodigo()) {
                       
                        if(intTipoCprb == CONFIG.tiposComprobante.rp.getCodigo()) {
                            tipoCprb = CONFIG.tiposComprobante.rp;
                        }
                        else {
                            tipoCprb = CONFIG.tiposComprobante.fp;
                        }
                        
                        comprobante = new RemitoProveedor(data.proveedor, $datosNroCprb.attr("cprbNroCompleto"));
                    }
                    else{
                        tipoCprb = CONFIG.tiposComprobante.oc;
                        comprobante = new OrdenDeCompra(data.proveedor, $datosNroCprb.attr("cprbNroCompleto"));
                    }
                }
                
                comprobante.setTipoComprobante(tipoCprb); //este objeto lo tengo que cambiar por le objeto creado cuando se carga el script

                comprobante.setDescuento1(parseFloat($totales.attr("bonifEnPorcentaje1"))); 
                comprobante.setDescuento2(parseFloat($totales.attr("bonifEnPorcentaje2"))); 
                comprobante.setBonificacion2Calculada(opCompras_utiles.convertirStringABoolean($totales.attr("esBonif2Calculada")));
                
                
                comprobante.setCodigoBarras(codBarras);
                
                comprobante.setFechaCreacion(new Fecha($datosNroCprb.attr("fecha"), $datosNroCprb.attr("horaCreacion")));
                comprobante.setEstadoRecepcionDetalle(detalleEstadoRecepcion);

                var listaPrecios = new ListaDePrecios();
                listaPrecios.setTipo($totales.attr("archivoAsociadoTipo"));
                listaPrecios.setPathAbsoluto($totales.attr("archivoAsociado"));

                comprobante.setListaDePreciosAsociada(listaPrecios);

                comprobante.setEstado($estado.attr('id'));

                var listaItem = comprobante.getListaDeItemComprobanteCompra();

                var itemActual = null;

                $articulos.each(function() {

                    itemActual = new ItemOrdenDeCompra($(this).attr("articuloProveedorId"), $(this).attr("articuloProveedorIdInterno"), $(this).attr("articuloProveedorNombre"), $(this).attr("cantidad"), new UnidadDeMedida(parseInt($(this).attr('unidadDeMedidaId')), ""));

                    itemActual.setProveedor(data.proveedor);
                    itemActual.setTipoDeComprobante(tipoCprb);
                    itemActual.setNumeroDeComprobante($datosNroCprb.attr("cprbNroCompleto"));
                    //itemActual.setCodigoIva();                
                    itemActual.setBonificacion1(parseFloat($(this).attr("bonif1")));                
                    itemActual.setBonificacion2(parseFloat($(this).attr("bonif2"))); 
                    itemActual.setMarcaBonificacion2Calculada(opCompras_utiles.convertirStringABoolean($(this).attr("esBonif2Calculada")));
                    itemActual.setPrecioUnitario(parseFloat($(this).attr("precioUnitario")));    
                    itemActual.setPesoUnitario(parseFloat($(this).attr("pesoUnitario")));                                    
                    itemActual.setValorIva(parseFloat($(this).attr('valorIVA')));
                    itemActual.setCodigoIva(parseFloat($(this).attr('codigoIVA')));

                    listaItem.insert(itemActual);
                });

                $articulosDadosDeBaja.each(function() {

                    itemActual = new ItemOrdenDeCompra($(this).attr("articuloProveedorId"), $(this).attr("articuloProveedorIdInterno"), $(this).attr("articuloProveedorNombre"), 0, null, "");

                    itemActual.setProveedor(data.proveedor);
                    itemActual.setTipoDeComprobante(tipoCprb);

                    listaInactivos.insert(itemActual);
                });

                var listaCondiciones = comprobante.getListaDeCondicionDeEntrega();

                if($(xmlResponse).find("transporte").length > 0)
                {
                    var transporte = new Transporte($transporte.attr("idTransporte"), $transporte.attr("nombreTransporte"));                                
                    var doc = new DocumentoIdentificador();

                    doc.setAbrev($transporte.attr("CUITTransporteAbrev"));
                    doc.setNro($transporte.attr("CUITTransporteNro"));

                    transporte.setDocumentoIdentificador(doc);
                    transporte.setTelefono1($transporte.attr("telefonoTransporte")); 

                    listaCondiciones.setTransporte(transporte);
                } 

                var $condicion = null;
                //Condiciones de entrega
                for(var i = 0 ; i < 3; i++)
                {
                    $condicion = $(xmlResponse).find("condicionDeEntrega"+i);

                    if($condicion.length > 0)
                    {
                        listaCondiciones.insert(new CondicionDeEntrega($condicion.attr("detalle")));
                    }
                }

                $condicion = null;
                listaCondiciones = comprobante.getListaDeCondicionDePago();

                //condiciones de pago
                for(i = 0 ; i < 3; i++)
                {
                    $condicion = $(xmlResponse).find("condicionDePago"+i);

                    if($condicion.length > 0)
                    {
                        listaCondiciones.insert(new CondicionDePago($condicion.attr("detalle")));
                    }
                }
            }
            
            return {comprobante : comprobante, listaItemsDadosBaja : listaInactivos};
            
        },
                
        
            
        manejoErroresSesionCompras = function(stringTipo, stringSubtipo, url) {
            
            var tipo = parseInt(stringTipo);
            var subtipo = parseInt(stringSubtipo);
            
            var respuesta = {redireccionar : false, sesion : null};
            
            if(tipo == CONFIG.variables.sistem.tipoError.sesion)
            {
                if(subtipo ==  CONFIG.variables.sistem.sesion.error.inactiva || subtipo ==  CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada || subtipo == CONFIG.variables.sistem.sesion.error.caducada)                                
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                }
                else
                {
                    if(subtipo ==  CONFIG.variables.sistem.sesion.error.permiso)
                    {
                        respuesta.sesion = {errorPermiso : true};                                    
                    }
                }
            }
            else
            {
                if(url != null)
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                    //alert(url);
                }
            }
            
            return respuesta;
        },

        recuperarProveedor = function(data) {		

            $.ajax({ 

                data: {idProveedor: data.idProveedor}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarProveedorPorId, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {
                   
                    try
                    {							
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {

                            var $rs = $(xmlResponse).find("rs");
                            var proveedor = null; 

                            if($rs.length != 0) 
                            {
                                proveedor = new Proveedor($rs.text(), $rs.attr('info'));

                                var doc = new DocumentoIdentificador($rs.attr('documentoIdentificadorId'));											
                                doc.setNro($rs.attr('documentoIdentificadorNro'));
                                
                                var condicionIVA = null;
                                
                                if( $rs.attr('condicionIVACodigo') != null && 
                                    $rs.attr('condicionIVADetalle') != null && 
                                    $rs.attr('condicionIVAAbrev') != null) {
                                        
                                    condicionIVA = new CondicionIVA($rs.attr('condicionIVACodigo'), $rs.attr('condicionIVADetalle'), $rs.attr('condicionIVAAbrev'));                                
                                }

                                var dir = new Direccion($rs.attr('direccion'), $rs.attr('ciudad'));	
                                dir.setCodigoPostal($rs.attr('codigoPostal'));

                                proveedor.setTelefono($rs.attr('telefono'));
                                proveedor.setCondicionIVA(condicionIVA);
                                proveedor.setDocumentoIdentificador(doc);
                                proveedor.setDireccion(dir);

                            }

                            //callback
                            data.onSuccess(proveedor, data.parametrosExtra);									
                        }
                        else 
                        {
                            var excep = new XMLException(getMensajeErrorProveedor(data.idProveedor), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorProveedor(data.idProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },	

        recuperarFamiliasProveedor = function(data) {

            var idProveedor = data.proveedor.getId();

            if(data.parametrosExtra)
            {
                data.parametrosExtra.proveedor = data.proveedor;
            }
            else
            {
                data.parametrosExtra = {proveedor : data.proveedor};
            }

            $.ajax({ 
                data: {idProveedor: idProveedor}, 
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarFamiliasProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                           var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                                        //callback
                                        data.onFailure(excep, data.parametrosExtra);
                            },

                success: function(xmlResponse) {

                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	
                            var $rs = $(xmlResponse).find("rs");

                            var familia = null;
                            
                            //Limpio la lista de familias que pueda tener el objeto proveedor para cargar las nuevas.
                            data.proveedor.setFamilias(new ListaDeFamiliasProveedor());
                            
                            var listaFamilias = data.proveedor.getFamilias();

                            $rs.each(function() {

                                familia = new FamiliaProveedor($(this).attr('idFamilia'), $(this).attr('nombre'), data.proveedor);
                                listaFamilias.insert(familia);
                            });	

                            //callback
                            data.onSuccess(listaFamilias, data.parametrosExtra);								
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorFamiliasProveedor(idProveedor), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorFamiliasProveedor(idProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

        recuperarRubrosProveedor = function(data) {

            var idProveedor = data.familia.getProveedor().getId();
            var idFamilia = data.familia.getId();

            if(data.parametrosExtra)
            {
                    data.parametrosExtra.familia = data.familia;
            }
            else
            {
                    data.parametrosExtra = {familia : data.familia};
            }

            $.ajax({ 
                    data: {idProveedor: idProveedor, idFamilia: idFamilia}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarRubrosProveedor, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse){

                        try 
                        {					
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var $rs = $(xmlResponse).find("rs");

                                var rubro = null;
                                
                                //Limpio la lista de rubros que pueda tener el objeto familias.
                                data.familia.setRubros(new ListaDeRubrosProveedor());
                                
                                var listaRubros = data.familia.getRubros();

                                $rs.each(function() {

                                    rubro = new RubroProveedor($(this).attr('idRubro'), $(this).attr('nombre'), data.familia);		
                                    listaRubros.insert(rubro);
                                });

                                //callback									
                                data.onSuccess(listaRubros, data.parametrosExtra);								
                            }
                            else
                            {
                                var excep = new XMLException(getMensajeErrorRubrosProveedor(idProveedor, idFamilia), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                        catch(e) 
                        {							
                            var excep = new Exception(getMensajeErrorRubrosProveedor(idProveedor, idFamilia), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    } 
            });

        },

        recuperarSubrubrosProveedor = function(data) {

            var idProveedor = data.rubro.getFamilia().getProveedor().getId();
            var idFamilia = data.rubro.getFamilia().getId();
            var idRubro = data.rubro.getId();

            if(data.parametrosExtra)
            {
                data.parametrosExtra.rubro = data.rubro;
            }
            else
            {
                data.parametrosExtra = {rubro : data.rubro};
            }

            $.ajax({

                    data: {idProveedor: idProveedor, idFamilia: idFamilia, idRubro: idRubro}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarSubRubrosProveedor, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var $rs = $(xmlResponse).find("rs");

                                var subrubro = null;
                                
                                //Limpio la lista de subrubros que pueda tener el objeto rubros.
                                data.rubro.setSubrubros(new ListaDeSubrubrosProveedor());
                                
                                var listaSubrubros = data.rubro.getSubrubros();

                                $rs.each(function() {

                                    subrubro = new SubrubroProveedor($(this).attr('subRubroId'), $(this).attr('nombre'), data.rubro);
                                    listaSubrubros.insert(subrubro);
                                });	

                                //callback
                                data.onSuccess(listaSubrubros, data.parametrosExtra);

                            }
                            else
                            {
                                var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }							
                        catch(e) 
                        {								
                            var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    } 
            });
        },

        recuperarArticulosProveedor = function(data) {

            var idProveedor = data.subrubro.getRubro().getFamilia().getProveedor().getId();
            var idFamilia = data.subrubro.getRubro().getFamilia().getId();
            var idRubro = data.subrubro.getRubro().getId();
            var idSubrubro = data.subrubro.getId();

            if(data.parametrosExtra)
            {
                data.parametrosExtra.subrubro = data.subrubro;
            }
            else
            {
                data.parametrosExtra = {subrubro : data.subrubro};
            }

            $.ajax({    
                    data: {idProveedor: idProveedor, idFamilia: idFamilia, idRubro: idRubro, idSubrubro: idSubrubro}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticulosPorSubrubro, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse){

                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var $rs = $(xmlResponse).find("rs");

                                var articuloProveedor = null;
                                
                                //Limpio la lista de arituclos que pueda tener el objeto subrubros.
                                data.subrubro.setArticulos(new ListaDeArticulosProveedor());
                                
                                var listaArticulos = data.subrubro.getArticulos();

                                $rs.each(function() {

                                    articuloProveedor = new ArticuloProveedor($(this).attr('articuloProveedorId'), $(this).attr('articuloProveedorNombre'));
                                    articuloProveedor.setIdInterno($(this).attr('idArticuloInterno'));
                                    articuloProveedor.setProveedor(data.subrubro.getRubro().getFamilia().getProveedor());
                                    articuloProveedor.setSubrubro(data.subrubro);	

                                    listaArticulos.insert(articuloProveedor);
                                });

                                //callback
                                data.onSuccess(listaArticulos, data.parametrosExtra);
                            }
                            else
                            {
                                var excep = new XMLException(getMensajeErrorArticulosProveedor(idProveedor, idFamilia, idRubro, idArticulo), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }							
                        catch(e) 
                        {								
                            var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }	
                    } 
            });
        },

        recuperarListaDePreciosProveedor = function(data) {

            $.ajax({ 

                data: {idProveedor: data.idProveedor}, 
                type: "POST",
                dataType: "xml",				
                url: CONFIG.proyectos.compras.controladores.recuperarUltimaListaDePrecios, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {

                    try 
                    {						
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	
                            var $rs = $(xmlResponse).find("archivoAsociado");
                            
                            var listaPrecios = new ListaDePrecios();
                                 
                            listaPrecios.setTipo($rs.attr('tipo'));
                            listaPrecios.setPathAbsoluto($rs.attr('archivo'));

                            //callback
                            data.onSuccess(listaPrecios, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorListaPreciosProveedor(data.idProveedor), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorListaPreciosProveedor(data.idProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

        recuperarDatosUltimaCompraPorProveedor = function(data) {

            $.ajax({ 

                data: {idProveedor: data.idProveedor, idInternoArticuloProveedor: data.idInternoArticuloProveedor}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarUltimaCompraArticuloPorProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function (xmlResponse){

                    try 
                    {					
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	
                            var $item = $(xmlResponse).find("item");

                            var itemOrdenCompra = null;

                            if($item.length != 0) 
                            {	
                                itemOrdenCompra = new ItemOrdenDeCompra($item.attr('idArticulo'), $item.attr('idInternoArticulo'), $item.attr('nombreArticulo'), $item.attr('cantidad'), new UnidadDeMedida(parseInt($item.attr('unidadMedidaId')), ""));

                                itemOrdenCompra.setBonificacion1(parseFloat($item.attr('bonificacion1')));
                                itemOrdenCompra.setBonificacion2(parseFloat($item.attr('bonificacion2')));
                                itemOrdenCompra.setMarcaBonificacion2Calculada(opCompras_utiles.convertirStringABoolean($item.attr("esBonif2Calculada")));
                                itemOrdenCompra.setPrecioUnitario(parseFloat($item.attr('precioUnitario')));
                                itemOrdenCompra.setValorIva(parseFloat($item.attr('valorIVA')));
                                itemOrdenCompra.setCodigoIva($item.attr('codigoIVA'));

                                //itemOrdenCompra.setPesoUnitario(parseFloat($item.attr('pesoUnitario'))); //El peso es el que está en el alta del artículo
                                
                                //data.parametrosExtra.pesoUnitarioAlta = parseFloat($item.attr('pesoUnitarioAlta'));
                            }/*
                            else
                            {
                                data.parametrosExtra.pesoUnitarioAlta = null;
                            }   */                                                     

                            //callback
                            data.onSuccess(itemOrdenCompra, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorDatosUltimaCompraPorProveedor(data.idProveedor, data.idArticuloProveedor), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }

                    }
                    catch(e) 
                    {	
                        var excep = new Exception(getMensajeErrorDatosUltimaCompraPorProveedor(data.idProveedor, data.idArticuloProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

        recuperarArticuloCodimat = function(data) {

            $.ajax({ 

                data: {idArticulo: data.idArticulo}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloCodimat, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                    
                },

                success: function(xmlResponse) {

                    try 
                    {						
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {									
                            var $rs = $(xmlResponse).find("rs");

                            var articuloCodimat = null;

                            if($rs.length != 0)
                            {
                                articuloCodimat = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloCodimat.setPeso($rs.attr('peso'));
                            }		

                            //callback
                            data.onSuccess(articuloCodimat, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorArticuloCodimat(data.idArticulo), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }								
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorArticuloCodimat(data.idArticulo), e);
                        
                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

        recuperarArticuloProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.proveedor.getId();
                data.parametrosExtra.idArticulo = data.idArticulo; 
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.proveedor.getId(), idArticulo : data.idArticulo};
            }

            var dataFinal = {idProveedor: data.proveedor.getId()};

            if(data.idInternoArticulo)
            {
                dataFinal.idInternoArticuloProveedor = data.idInternoArticulo;
            }
            else
            {
                dataFinal.idArticuloProveedor = data.idArticulo;
            }

            $.ajax({ 
                data : dataFinal, // get the form data
                type : "POST",
                dataType : "xml",
                url : CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloProveedor, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {	    

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success : function(xmlResponse) {

                    try
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {									
                            var $rs = $(xmlResponse).find("rs");

                            var articuloProveedor = null;

                            if($rs.length != 0)
                            {
                                articuloProveedor = new ArticuloProveedor($rs.attr('idArticulo').toUpperCase(), $rs.attr('nombreArticulo').toUpperCase());
                                
                                articuloProveedor.setProveedor(data.proveedor);
                                
                                articuloProveedor.setIdInterno($rs.attr('idArticuloInterno'));

                                articuloProveedor.setBonificacionesYRecargos($rs.attr('bonificacionesYRecargos'));
                                articuloProveedor.setCodigoIva($rs.attr('codigoIva'));
                                articuloProveedor.setValorIva($rs.attr('valorIva'));
                                articuloProveedor.setFechaVigencia($rs.attr('fechaVigencia'));
                                articuloProveedor.setPrecioCosto(parseFloat($rs.attr('precioCosto')));
                                articuloProveedor.setPeso(parseFloat($rs.attr('peso')));
                                articuloProveedor.setPrecioLista(parseFloat($rs.attr('precioLista')));
                                articuloProveedor.setTipoUnidadDeEntrega($rs.attr('tipoUnidadDeMedida'));
                                articuloProveedor.setUnidadDeFacturacion(parseFloat($rs.attr('unidadDeFacturacion')));
                            //  articuloProveedor.setUnidadDePrecio($rs.attr('unidadDePrecio'));
                                articuloProveedor.setCodigoDeBarras($rs.attr('codBarras'));
                                articuloProveedor.setInfoAdicional($rs.attr('infoAdicional'));
                                
                                articuloProveedor.setBonificacion1(parseFloat($rs.attr('bonif1')));
                                articuloProveedor.setBonificacion2(parseFloat($rs.attr('bonif2')));
                                
                                articuloProveedor.setBonificacion2Calculada(opCompras_utiles.convertirStringABoolean($rs.attr('esBonif2Calculada')));

                                data.parametrosExtra.idFamilia = $rs.attr('idFamilia');
                                data.parametrosExtra.idRubro = $rs.attr('idRubro');
                                data.parametrosExtra.idSubrubro = $rs.attr('idSubRubro');
                            }

                            //callback
                            data.onSuccess(articuloProveedor, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorArticuloProveedor(data.proveedor.getId(), data.idArticulo), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorArticuloProveedor(data.proveedor.getId(), data.idArticulo), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            }); 
        },
                
        recuperarItemListaDePreciosPorFecha = function(data) {
  
            var dataAjax = {fecha : data.fecha, idProveedor : data.idProveedor, idInternoArticulo : data.idInternoArticulo};
        
            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarPrecioArticuloProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {   
                    
                    try
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        if($mensaje.attr("estadoOperacion") == "ok")
                        {									
                            var $rs = $(xmlResponse).find("rs");

                            var itemListaDePrecios = null;

                            if($rs.length != 0)
                            {
                                itemListaDePrecios = new ItemListaDePreciosProveedor($rs.attr('idArticuloInterno'));
                                
                                itemListaDePrecios.setFechaAlta($rs.attr('fechaAlta'));                                

                                if($rs.attr('fechaActualizacion') != undefined)
                                
                                {
                                    itemListaDePrecios.setFechaActualizacion($rs.attr('fechaActualizacion'));
                                }

                                itemListaDePrecios.setBonificacion1(parseFloat($rs.attr('bonificacion1')));
                                itemListaDePrecios.setBonificacion2(parseFloat($rs.attr('bonificacion2')));
                                itemListaDePrecios.setMarcaBonificacion2Calculada(opCompras_utiles.convertirStringABoolean($rs.attr("esBonif2Calculada")));
                                
                                if($rs.attr('vigencia') ==  true)
                                {
                                    itemListaDePrecios.setVigente(true);
                                }
                                
                                itemListaDePrecios.setPrecioDeLista(parseFloat($rs.attr('precioLista')));
                                itemListaDePrecios.setTipoActualizacion($rs.attr('tipoActualizacion'));
                            }

                            //callback
                            data.onSuccess(itemListaDePrecios, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorItemListaDePreciosPorFecha(data.idProveedor, data.idInternoArticulo), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {		
                        var excep = new Exception(getMensajeErrorItemListaDePreciosPorFecha(data.proveedor, data.idArticulo), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    } 
                }
            }); 
        },

        recuperarVinculacionesArticuloProveedor = function(data) {

            var ajaxData = null;
            var articulo_Actual = null;

            if(data.articuloProveedor)
            {	
                articulo_Actual = data.articuloProveedor;				
                ajaxData = {idProveedor : data.articuloProveedor.getProveedor().getId(), idArticuloProveedor : data.articuloProveedor.getIdInterno()};

                if(data.parametrosExtra)
                {
                    data.parametrosExtra.articuloProveedor = data.articuloProveedor;
                }
                else
                {
                    data.parametrosExtra = {articuloProveedor : data.articuloProveedor};
                }
            }
            else
            {
                articulo_Actual = data.articuloCodimat;
                ajaxData = {idArticuloCodimat : data.articuloCodimat.getId()};                

                if(data.parametrosExtra)
                {
                    data.parametrosExtra.articuloCodimat = data.articuloCodimat;
                }
                else
                {
                    data.parametrosExtra = {articuloCodimat : data.articuloCodimat};
                }
            }
            
            $.ajax({ 

                data: ajaxData, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarVinculacionesArticuloProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                                var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                        },

                success: function(xmlResponse) {

                    try 
                    {	
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	
                            var $vinculaciones = $(xmlResponse).find("vinculacion");

                            var vinculacion = null;

                            var articuloVinculado = null;

                            var listaVinculaciones = new ListaDeVinculacionesArticuloProveedor();

                            $vinculaciones.each(function() {
                                
                                if(data.articuloProveedor != null)
                                {
                                    articuloVinculado = new Articulo($(this).attr('articuloCodimatId'), $(this).attr('articuloCodimatNombre')); // Articulo Codimat

                                    vinculacion = new VinculacionArticuloProveedor(articuloVinculado, articulo_Actual);
                                }
                                else
                                {
                                    articuloVinculado = new ArticuloProveedor($(this).attr('articuloProveedorId'), $(this).attr('articuloProveedorNombre')); // Articulo Proveedor
                                    articuloVinculado.setIdInterno($(this).attr('articuloProveedorIdInterno'));
                                    articuloVinculado.setProveedor(new Proveedor($(this).attr('proveedorId'), $(this).attr('proveedorNombre')));

                                    vinculacion = new VinculacionArticuloProveedor(articulo_Actual, articuloVinculado);
                                }	
                                
                                vinculacion.setFechaVigencia($(this).attr('fechaVigencia'));
                                vinculacion.setRelacionEntreArticulos($(this).attr('relacionEntreArticulos'));
                                
                                /**
                                 *  OBSOLETO - No se consideran más las variaciones como parte de una vinculación
                                 *//*
                                vinculacion.setFactorCorreccionCosto($(this).attr('factorCorreccionCosto'));

                                if($(this).attr('variacionCostoVinculado_Calificacion1') == CONFIG.selects.valueNulo || $(this).attr('variacionCostoVinculado_Porcentaje1') == '0.00') 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado1("");
                                }
                                else 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado1($(this).attr('variacionCostoVinculado_Porcentaje1'));
                                }

                                if($(this).attr('variacionCostoVinculado_Calificacion2') == CONFIG.selects.valueNulo || $(this).attr('variacionCostoVinculado_Porcentaje2') == '0.00') 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado2("");
                                }
                                else 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado2($(this).attr('variacionCostoVinculado_Porcentaje2'));
                                }

                                if($(this).attr('variacionCostoVinculado_Calificacion3') == CONFIG.selects.valueNulo || $(this).attr('variacionCostoVinculado_Porcentaje3') == '0.00') 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado3("");
                                }
                                else 
                                {
                                    vinculacion.setPorcentajeVariacionCostoVinculado3($(this).attr('variacionCostoVinculado_Porcentaje3'));
                                }

                                vinculacion.setClasificacionVariacionCostoVinculado1($(this).attr('variacionCostoVinculado_Calificacion1'));
                                vinculacion.setClasificacionVariacionCostoVinculado2($(this).attr('variacionCostoVinculado_Calificacion2'));
                                vinculacion.setClasificacionVariacionCostoVinculado3($(this).attr('variacionCostoVinculado_Calificacion3'));
                                */

                                listaVinculaciones.insert(vinculacion); 
                            });		

                            //callback
                            data.onSuccess(listaVinculaciones, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = null;

                            if(data.articuloProveedor)
                            {
                                excep = new XMLException(getMensajeErrorVinculacionesXArticuloProveedor(data.articuloProveedor.getProveedor().getId(), data.articuloProveedor.getId()), $mensaje.attr('mensaje'));
                            }
                            else
                            {
                                excep = new XMLException(getMensajeErrorVinculacionesXArticuloCodimat(data.articuloCodimat.getId()), $mensaje.attr('mensaje'));
                            }

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {	
                        var excep = null;

                        if(data.articuloProveedor)
                        {
                            excep = new Exception(getMensajeErrorVinculacionesXArticuloProveedor(articulo_Actual.getProveedor().getId(), articulo_Actual.getId()), e);
                        }
                        else
                        {	
                            excep = new Exception(getMensajeErrorVinculacionesXArticuloCodimat(articulo_Actual.getId()), e);
                        }

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            }); 
        },

        recuperarVariacionesCosto = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor};
            }

            $.ajax({ 
                data: {idProveedor : data.idProveedor}, // get the form data
                type: "POST",
                dataType: "xml",

                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarVariacionesPorProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){

                    try
                    {						
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {
                            var listaVariaciones = new Lista();

                            var variacionActual = null;

                            $(xmlResponse).find("rs").each(function() {

                                    variacionActual = new VariacionCostoVinculado($(this).attr('calificacion'), $(this).attr('porcentaje'));
                                    variacionActual.setDetalle($(this).attr('detalle'));

                                    listaVariaciones.insert(variacionActual);	
                            });

                            //callback
                            data.onSuccess(listaVariaciones, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorVariacionesPorProveedor(data.idProveedor), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorVariacionesPorProveedor(data.idProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            }); 
        },
                                
        //data -> {proveedor : {objeto}, tipoComprobante : "9", numeroCPRB : "X000100025981"}
        recuperarComprobanteCompra = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            var dataAjax = {
                usuario : data.usuario,                     
                ip : data.ip,
                urlForward : data.urlForward, 
                urlLogout : data.urlLogout, 
                idProveedor : data.proveedor.getId()
            };
            
            if(data.desdeJS) {
                dataAjax.desdeJS = data.desdeJS;
            }
            
            if(data.apartarDadosBaja) {
                dataAjax.apartarDadosBaja = data.apartarDadosBaja;
            }
            
            if(data.tipoComprobante) {
                dataAjax.tipoComprobante = data.tipoComprobante;
            }
            
            if(data.numeroCPRB) {
                dataAjax.numeroCPRB = data.numeroCPRB;
            }
            
            if(data.codigoBarras) {
                dataAjax.codigoBarras = data.codigoBarras;
            }
            
            if(data.estadoCPRB) {
                dataAjax.estadoCPRB = data.estadoCPRB;
            }
            
            $.ajax({ 
                data : dataAjax,
                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarComprobante, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){

                    try
                    {                            
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        if($mensaje.attr("estadoOperacion") == "ok")
                        { 
                            var resultado = crearObjetoComprobante(xmlResponse, data);                 
                            //callback
                            data.parametrosExtra.listaItemsDadosBaja = resultado.listaItemsDadosBaja;
                            data.onSuccess(resultado.comprobante, $mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorComprobanteCompra(data.proveedor.getId(), data.tipoComprobante, data.numeroCPRB), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorComprobanteCompra(data.proveedor.getId(), data.tipoComprobante, data.numeroCPRB), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarAsociacionesComprobanteCompra = function(data) {
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            var dataAjax = {
                usuario : data.usuario,                     
                ip : data.ip,
                urlForward : data.urlForward, 
                urlLogout : data.urlLogout, 
                idProveedor : data.idProveedor,
                tipoCPRB : data.tipoCPRB,
                nroCPRB : data.numeroCPRB,
                recuperarAsociacionesInversas : data.recuperarAsociacionesInversas,
                recuperarAsociaciones : data.recuperarAsociaciones
            };
            
            
            
            $.ajax({ 
                data : dataAjax,
                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarAsociacionComprobantes, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){

                    try
                    {                        
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        if($mensaje.attr("estadoOperacion") === "ok"){ 
                            var resultado = crearObjetosAsociacionComprobantes(xmlResponse, data);                 
                            //callback
                            //data.parametrosExtra.listaItemsDadosBaja = resultado.listaItemsDadosBaja;
                            data.onSuccess(resultado.asociacionComprobantesCompras, $mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorComprobanteCompra(data.idProveedor, data.tipoComprobante, data.numeroCPRB), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorComprobanteCompra(data.idProveedor, data.tipoComprobante, data.numeroCPRB), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        crearObjetosAsociacionComprobantes = function(xmlResponse, data) {
            var asociacionComprobantesCompras = null;
            
            var $comprobanteBase = $(xmlResponse).find("comprobanteBase");
            var $asociacionesInversas = $(xmlResponse).find("asociacionesInversas");
            var $asociaciones = $(xmlResponse).find("asociaciones");
                            
            var comprobanteBase = null;
            //var asociaciones = new ListaDeItemAsociacionComprobantes();
            //var asociacionesInversas = new ListaDeItemAsociacionComprobantes();

            if($comprobanteBase.length > 0) {  
                var intTipoCprb = $comprobanteBase.attr("tipo");
                var idProveedor = $comprobanteBase.attr("idProveedor");
                var nro = $comprobanteBase.attr("nro");
                var estadoCPRB = $comprobanteBase.attr("estadoCPRB");
                var tipoCprb = new TipoComprobante(intTipoCprb, "","","");
                var proveedor = new Proveedor(idProveedor,"");
                comprobanteBase = new ComprobanteCompra(proveedor,nro,0);
                comprobanteBase.setEstado(estadoCPRB);
                comprobanteBase.setTipoComprobante(tipoCprb); //este objeto lo tengo que cambiar por le objeto creado cuando se carga el script
                asociacionComprobantesCompras = new AsociacionComprobantesCompras(comprobanteBase);
                
                if ($asociacionesInversas !== null){
                    if ($asociacionesInversas.length > 0){
                        var $comprobantes = $asociacionesInversas.find("comprobante");
                        
                        $comprobantes.each(function() {
                            //var $comprobante = $(this).find("comprobante");

                            var intTipoCprb = $(this).attr("tipo");
                            var idProveedor = $(this).attr("idProveedor");
                            var nro = $(this).attr("nro");
                            var estadoCPRB = $(this).attr("estadoCPRB");
                            var proveedor = new Proveedor(idProveedor,"");
                            var tipoCprb = new TipoComprobante(intTipoCprb, "","","");
                            var comprobante = new ComprobanteCompra(proveedor,nro,0);
                            comprobante.setEstado(estadoCPRB);
                            comprobante.setTipoComprobante(tipoCprb); //este objeto lo tengo que cambiar por le objeto creado cuando se carga el script

                            asociacionComprobantesCompras.getListaDeItemsAsociacionInversaComprobantes().insert(comprobante);
                        });	

                    }
                }

                if ($asociaciones !== null){
                    if ($asociaciones.length > 0){
                        var $comprobantes = $asociaciones.find("comprobante");
                        console.log($comprobantes);
                        $comprobantes.each(function() {
                            //var $comprobante = $(this).find("comprobante");
                            var intTipoCprb = $(this).attr("tipo");
                            var idProveedor = $(this).attr("idProveedor");
                            var nro = $(this).attr("nro");
                            var estadoCPRB = $(this).attr("estadoCPRB");
                            var proveedor = new Proveedor(idProveedor,"");
                            var tipoCprb = new TipoComprobante(intTipoCprb, "","","");
                            var comprobante = new ComprobanteCompra(proveedor,nro,0);
                            comprobante.setEstado(estadoCPRB);
                            comprobante.setTipoComprobante(tipoCprb); //este objeto lo tengo que cambiar por le objeto creado cuando se carga el script

                            asociacionComprobantesCompras.getListaDeItemsAsociacionComprobantes().insert(comprobante);
                        });	

                    }
                }
            }
            return {asociacionComprobantesCompras : asociacionComprobantesCompras};
            
        },

        bajaVinculacion = function(data) {	
            
            $.ajax({ 
                data: {
                    usuario : $.urlParam(urlParamUsuario), 
                    ip : $.urlParam(urlParamIP),
                    idProveedor : data.idProveedor,
                    idInternoArticuloProveedor : data.idInternoArticuloProveedor,
                    idArticuloCodimat : data.idArticuloCodimat,
                    fechaVigencia : data.fechaVigencia
                },

                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.bajaVinculacionArticuloProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){

                    try
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {
                            //callback
                            data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorEliminacionVinculacion(data.idProveedor, data.idArticuloProveedor, data.idArticuloCodimat), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorEliminacionVinculacion(data.idProveedor, data.idArticuloProveedor, data.idArticuloCodimat), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

        insertarFamiliaProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
                data.parametrosExtra.nombreFamilia = data.nombreFamilia;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia, nombreFamilia : data.nombreFamilia};
            }

            $.ajax({ 

                    data: {usuario : $.urlParam(urlParamUsuario), ip : $.urlParam(urlParamIP), idProveedor : data.idProveedor, codigoFamilia : data.idFamilia, nombreFamilia : data.nombreFamilia, insertando : data.insertando }, //+"&forzarError=cualquierCosa", // get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.insertarFamiliaProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse) {     
                        
                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {	
                                //callback
                                data.onSuccess(data.parametrosExtra);
                            }
                            else
                            {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorInsertarFamiliaProveedor(false, data.idProveedor, data.idFamilia, data.nombreFamilia, data.insertando), $mensaje.attr('mensaje'), true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }                            
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorInsertarFamiliaProveedor(true, data.idProveedor, data.idFamilia, data.nombreFamilia, data.insertando), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        darBajaFamiliaProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia};
            }

            $.ajax({ 

                    data: {idUsuario : data.idUsuario, idProveedor : data.idProveedor, idFamilia : data.idFamilia, urlForward : data.urlForward, urlLogout : data.urlLogout}, // get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.darBajaFamiliaProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse) {     
                        
                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {	
                                //callback
                                data.onSuccess(data.parametrosExtra);
                            }
                            else
                            {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorDarBajaFamiliaProveedor(data.idProveedor, data.idFamilia), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }                            
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorDarBajaFamiliaProveedor(data.idProveedor, data.idFamilia), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },

        insertarRubroProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
                data.parametrosExtra.idRubro = data.idRubro;
                data.parametrosExtra.nombreRubro = data.nombreRubro;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, nombreRubro : data.nombreRubro};
            }	

            $.ajax({ 

                    data: {usuario : $.urlParam(urlParamUsuario), ip : $.urlParam(urlParamIP), idProveedor : data.idProveedor, codigoFamilia : data.idFamilia, codigoRubro : data.idRubro, nombreRubro : data.nombreRubro, insertando : data.insertando}, //+"&forzarError=cualquierCosa", // get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.insertarRubroProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                       var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                       //callback
                       data.onFailure(excep, data.parametrosExtra);
                     },

                    success: function(xmlResponse) {     

                       try 
                       {
                           var $mensaje = $(xmlResponse).find("mensaje");

                           if($mensaje.attr("estadoOperacion") == "ok") 
                           {	
                               //callback
                               data.onSuccess(data.parametrosExtra);
                           }
                           else
                           {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorInsertarRubroProveedor(false, data.idProveedor, data.idFamilia, data.idRubro, data.nombreRubro, data.insertando), $mensaje.attr('mensaje'), true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                           }
                       }
                       catch(e) {	

                           var excep = new Exception(getMensajeErrorInsertarRubroProveedor(true, data.idProveedor, data.idFamilia, data.idRubro, data.nombreRubro, data.insertando), e);

                           //callback
                           data.onFailure(excep, data.parametrosExtra);
                       }
                   }
            });
        },
                
        darBajaRubroProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
                data.parametrosExtra.idRubro = data.idRubro;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, nombreRubro : data.nombreRubro};
            }	

            $.ajax({ 

                    data: {idUsuario : data.idUsuario, idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, urlForward : data.urlForward, urlLogout : data.urlLogout}, // get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.darBajaRubroProveedor, // the file to call
                    error: function(jqXHR, textStatus, errorThrown) {

                       var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                       //callback
                       data.onFailure(excep, data.parametrosExtra);
                     },

                    success: function(xmlResponse) {     

                       try 
                       {
                           var $mensaje = $(xmlResponse).find("mensaje");

                           if($mensaje.attr("estadoOperacion") == "ok") 
                           {	
                               //callback
                               data.onSuccess(data.parametrosExtra);
                           }
                           else
                           {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorDarBajaRubroProveedor(data.idProveedor, data.idFamilia, data.idRubro, data.nombreRubro), $mensaje.attr('mensaje'), true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                           }
                       }
                       catch(e) {	

                           var excep = new Exception(getMensajeErrorDarBajaRubroProveedor(data.idProveedor, data.idFamilia, data.idRubro), e);

                           //callback
                           data.onFailure(excep, data.parametrosExtra);
                       }
                   }
            });
        },
                

        insertarSubrubroProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
                data.parametrosExtra.idRubro = data.idRubro;
                data.parametrosExtra.idSubrubro = data.idSubrubro;
                data.parametrosExtra.nombreSubrubro = data.nombreSubrubro;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, idSubrubro : data.idSubrubro, nombreSubrubro : data.nombreSubrubro};
            }		

            $.ajax({ 

                    data: {usuario : $.urlParam(urlParamUsuario), ip : $.urlParam(urlParamIP), idProveedor : data.idProveedor, codigoFamilia : data.idFamilia, codigoRubro : data.idRubro, codigoSubRubro : data.idSubrubro, nombreSubRubro : data.nombreSubrubro, insertando : data.insertando}, //+"&forzarError=cualquierCosa", // get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.insertarSubRubroProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                       var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                       //callback
                       data.onFailure(excep, data.parametrosExtra);
                     },

                    success: function(xmlResponse) {     

                       try 
                       {
                           var $mensaje = $(xmlResponse).find("mensaje");

                           if($mensaje.attr("estadoOperacion") == "ok") 
                           {	
                                   //callback
                                   data.onSuccess(data.parametrosExtra);
                           }
                           else
                           {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorInsertarSubrubroProveedor(false, data.idProveedor, data.idFamilia, data.idRubro, data.idSubrubro, data.nombreSubrubro, data.insertando), $mensaje.attr('mensaje'), true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                               }
                           }
                       }
                       catch(e) {	

                           var excep = new Exception(getMensajeErrorInsertarSubrubroProveedor(true, data.idProveedor, data.idFamilia, data.idRubro, data.idSubrubro, data.nombreSubrubro, data.insertando), e);

                           //callback
                           data.onFailure(excep, data.parametrosExtra);
                       }
                   }
            });
        },
                
        darBajaSubrubroProveedor = function(data) {

            if(data.parametrosExtra)
            {
                data.parametrosExtra.idProveedor = data.idProveedor;
                data.parametrosExtra.idFamilia = data.idFamilia;
                data.parametrosExtra.idRubro = data.idRubro;
                data.parametrosExtra.idSubrubro = data.idSubrubro;
            }
            else
            {
                data.parametrosExtra = {idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, idSubrubro : data.idSubrubro};
            }		

            $.ajax({ 

                    data: {idUsuario : data.idUsuario, idProveedor : data.idProveedor, idFamilia : data.idFamilia, idRubro : data.idRubro, idSubrubro : data.idSubrubro, urlForward : data.urlForward, urlLogout : data.urlLogout},// get the form data
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.darBajaSubrubroProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                       var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                       //callback
                       data.onFailure(excep, data.parametrosExtra);
                     },

                    success: function(xmlResponse) {     

                       try 
                       {
                           var $mensaje = $(xmlResponse).find("mensaje");

                           if($mensaje.attr("estadoOperacion") == "ok") 
                           {	
                                   //callback
                                   data.onSuccess(data.parametrosExtra);
                           }
                           else
                           {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorDarBajaSubrubroProveedor(data.idProveedor, data.idFamilia, data.idRubro, data.idSubrubro), $mensaje.attr('mensaje'), true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                               }
                           }
                       }
                       catch(e) {	

                           var excep = new Exception(getMensajeErrorDarBajaSubrubroProveedor(data.idProveedor, data.idFamilia, data.idRubro, data.idSubrubro), e);

                           //callback
                           data.onFailure(excep, data.parametrosExtra);
                       }
                   }
            });
        },

        insertarArticuloProveedor = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            var dataAjax = data.data;
            
            dataAjax = dataAjax + "&"+urlParamUsuario+"="+$.urlParam(urlParamUsuario);
            dataAjax = dataAjax + "&"+urlParamIP+"="+$.urlParam(urlParamIP);
            
            $.ajax({ 

                data: dataAjax, //+"&forzarError=cualquierCosa", // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.insertarEditarArticuloProveedor, // the file to call
                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                  },

                success: function(xmlResponse) {     
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") 
                        {				 
                            //callback
                            data.onSuccess($mensaje.attr("mensaje"), $(xmlResponse).find("nuevoId").text(), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorInsertarEditarArticuloProveedor(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) {	

                        var excep = new Exception(getMensajeErrorInsertarEditarArticuloProveedor(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        editarIdArticuloProveedor = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            $.ajax({ 
                data: {
                    usuario : $.urlParam(urlParamUsuario), ip : $.urlParam(urlParamIP), 
                    idProveedor : data.articuloProveedor.getProveedor().getId(), 
                    idInternoArticuloProveedor : data.articuloProveedor.getIdInterno(), 
                    inputNuevoIdArticulo : data.nuevoIdArticulo
                },

                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.editarIdArticuloProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {

                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") 
                        {	
                            data.articuloProveedor.setId(data.nuevoIdArticulo);
                            //callback
                            data.onSuccess(data.articuloProveedor, $mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorEditarIdArticulo(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }						
                    }	
                    catch(e) 
                    {					
                        var excep = new Exception(getMensajeErrorEditarIdArticulo(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },

/**
 * ==========================================================================================
 *  FUNCTION OBSOLETA - Se utilizaba para insertar variaciones de costo a las vinculaciones.
 * ==========================================================================================
 */
 /*
        insertarVariacionCosto = function(data) {

            if(data.parametrosExtra)
            {
                    data.parametrosExtra.idProveedor = data.idProveedor;
            }
            else
            {
                    data.parametrosExtra = {idProveedor : data.idProveedor};
            }

            $.ajax({ 
                data: {
                        usuario : $.urlParam(urlParamUsuario), 
                        ip : $.urlParam(urlParamIP), 
                        idProveedor : data.idProveedor, 
                        porcentaje : data.porcentaje, 
                        detalle : data.detalle
                },

                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.insertarVariacionCosto, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {

                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") 
                        {						
                            var nuevaVariacion = new VariacionCostoVinculado($(xmlResponse).find("rs").attr('calificacion'), $(xmlResponse).find("rs").attr('porcentaje'));
                                    nuevaVariacion.setDetalle($(xmlResponse).find("rs").attr('detalle'));

                            //callback
                            data.onSuccess(nuevaVariacion, $mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else
                        {
                            var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorInsertarVariacionCosto(data.idProveedor), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }						
                    }	
                    catch(e) 
                    {					
                        var excep = new Exception(getMensajeErrorInsertarVariacionCosto(data.idProveedor), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },*/

        insertarVinculacion = function(data) {
            
                var dataAjax = data.data;
            
                dataAjax = dataAjax + "&"+urlParamUsuario+"="+$.urlParam(urlParamUsuario);
                dataAjax = dataAjax + "&"+urlParamIP+"="+$.urlParam(urlParamIP);
            
                $.ajax({ 
                        data: dataAjax,//"usuario="+$.urlParam(urlParamUsuario)+"&ip="+$.urlParam(urlParamIP)+"&"+data.data, // get the form data
                        type: "POST",
                        dataType: "xml",
                        url: CONFIG.proyectos.compras.controladores.insertarEditarVinculacionArticuloProveedor, // the file to call

                        error: function (jqXHR, textStatus, errorThrown) {

                            var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        },

                        success: function(xmlResponse){

                            try 
                            { 
                                var $mensaje = $(xmlResponse).find("mensaje");

                                if($mensaje.attr("estadoOperacion") == "ok") 
                                {
                                    //callback
                                    data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);
                                }
                                else
                                {
                                    var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                    data.parametrosExtra.sesion = respuesta.sesion;

                                    if(!respuesta.redireccionar)
                                    {
                                        var excep = new XMLException(getMensajeErrorInsertarVinculacion(), $mensaje.attr('mensaje'));

                                        //callback
                                        data.onFailure(excep, data.parametrosExtra);
                                    }
                                }
                            }	
                            catch(e) 
                            {	
                                var excep = new Exception(getMensajeErrorInsertarVinculacion(), e);

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }			
                       }
                });

        },

        bajaArticulo = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }

            $.ajax({ 
                    data: {usuario : $.urlParam(urlParamUsuario), ip : $.urlParam(urlParamIP), idProveedor : data.idProveedor, idInternoArticuloProveedor : data.idInternoArticuloProveedor}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.bajaArticuloProveedor, // the file to call
                    error: function (jqXHR, textStatus, errorThrown) {

                            var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse){
                    
                        try
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {							
                                //callback
                                data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);
                            }
                            else
                            {                              
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {

                                    var excep = new XMLException(getMensajeErrorBajaArticuloProveedor(data.idProveedor, data.idArticuloProveedor), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }
                        }
                        catch(e) 
                        {							
                            var excep = new Exception(getMensajeErrorBajaArticuloProveedor(data.idProveedor, data.idArticuloProveedor), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                   }
            });
        },

        insertarOrdenDeCompra = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }

            $.ajax({ 

                     data: data.data, //+"&forzarError=cualquierCosa", // get the form data
                     type: "POST",
                     dataType: "xml",
                     url: CONFIG.proyectos.creadorComprobantes.creadorOrdenDeCompra, // the file to call
                     error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                      },

                     success: function(xmlResponse) {     

                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {				
                                //callback
                                data.onSuccess($(xmlResponse).find("comprobante").attr("nro"), data.parametrosExtra);
                            }
                            else
                            {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorInsertarOrdenDeCompra(), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorInsertarOrdenDeCompra(), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        //data -> {usuario: 262, idProveedor : 9999, tipoCPRB : 9, numeroCPRB : "X000100025981"}
        anularComprobanteDeCompra = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax = null;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {
                    usuario : data.usuario,
                    CO_CODIPROVE : data.idProveedor,
                    CO_TIPOCPRB : data.tipoCPRB,
                    CO_NUMECPRB : data.numeroCPRB,
                    urlForward : data.urlForward,
                    urlLogout : data.urlLogout
                }
            }
            
            $.ajax({ 

                     data: dataAjax, // get the form data
                     type: "POST",
                     dataType: "xml",
                     url: CONFIG.proyectos.compras.controladores.anularComprobanteDeCompra, // the file to call
                     error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                      },

                     success: function(xmlResponse) {     

                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {				
                                //callback
                                data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);
                            }
                            else
                            {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorAnularComprobanteDeCompra(), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorAnularComprobanteDeCompra(), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        actualizarListaDePreciosProveedor = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            $.ajax({
                    url: CONFIG.proyectos.compras.controladores.actualizarListaDePreciosPorProveedor,
                    type: 'POST',
                    data: data.data,
                    //async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                    success: function(xmlResponse) {     
                        
                        try 
                        {
                            var $mensaje = $(xmlResponse).find("mensaje");
                            var $errores = $(xmlResponse).find("errores");

                            if($mensaje.attr("estadoOperacion") == "ok") 
                            {	
                                if($errores.length == 0)
                                {
                                    //callback
                                    data.onSuccess($mensaje.attr('mensaje'), data.parametrosExtra);
                                }
                                else
                                {                                    
                                    var listaErrores = [];
                                    var errorActual = null;
                                    
                                    var $info = $(xmlResponse).find("info");
                                    
                                    var informacion = {   
                                        cantidadTotalProveedor : $info.attr("cantidadTotalProveedor"), 
                                        cantidadProcesados : $info.attr("cantidadProcesados"), 
                                        cantidadErroneos : $info.attr("cantidadErroneos")
                                    };
                                    
                                    $(xmlResponse).find("articulo").each(function(){
                                        
                                        errorActual = { 
                                            idInterno : $(this).attr("idInterno"), idArticuloProveedor : $(this).attr("idArticuloProveedor"), detalle : $(this).attr("detalle"),
                                            existe : $(this).attr("existe"), overflowB1 : $(this).attr("overflowB1"), 
                                            overflowB2 : $(this).attr("overflowB2"), excesoB1B2 : $(this).attr("excesoB1B2"), negativoPLista : $(this).attr("negativoPLista"), 
                                            excesoPLista : $(this).attr("excesoPLista")
                                        };
                                                        
                                        listaErrores.push(errorActual);
                                    });
                                    
                                    var excep = new XMLException(getMensajeErrorActualizarListaDePreciosProveedor(data.idProveedor), "La operaci&oacute;n de actualizaci&oacute;n de precios ha fallado. Corrija los errores y vuelva a intentarlo.", true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra, {informacion : informacion, listaErrores : listaErrores});
                                }
                            }
                            else
                            {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar)
                                {
                                    var excep = new XMLException(getMensajeErrorActualizarListaDePreciosProveedor(data.idProveedor), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }                                
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorActualizarListaDePreciosProveedor(data.idProveedor), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        insertarRemitoProveedor = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax = null;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {}
            }
            
            $.ajax({ 

                     data: dataAjax, // get the form data
                     type: "POST",
                     dataType: "xml",
                     url: CONFIG.proyectos.creadorComprobantes.creadorRemitoProveedor, // the file to call
                     error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                      },

                     success: function(xmlResponse) {     

                        try {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") {				
                                //callback
                                data.onSuccess($(xmlResponse).find("comprobante").attr("nro"), data.parametrosExtra);
                            }
                            else {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar) {
                                    var excep = new XMLException(getMensajeErrorInsertarRemitoProveedor(), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorInsertarRemitoProveedor(), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        insertarRemitoProveedor = function(data, serializado) {
            //console.log(CONFIG.proyectos.creadorComprobantes.creadorRemitoProveedor);
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax = null;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {}
            }
            
            $.ajax({ 

                     data: dataAjax, // get the form data
                     type: "POST",
                     dataType: "xml",
                     url: CONFIG.proyectos.creadorComprobantes.creadorRemitoProveedor, // the file to call
                     error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                      },

                     success: function(xmlResponse) {     

                        try {
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok") {				
                                //callback
                                data.onSuccess($(xmlResponse).find("comprobante").attr("nro"), data.parametrosExtra);
                            }
                            else {
                                var respuesta = manejoErroresSesionCompras($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                                data.parametrosExtra.sesion = respuesta.sesion;
                                
                                if(!respuesta.redireccionar) {
                                    var excep = new XMLException(getMensajeErrorInsertarRemitoProveedor(), $mensaje.attr('mensaje'));

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }
                        }
                        catch(e) {	

                            var excep = new Exception(getMensajeErrorInsertarRemitoProveedor(), e);

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
            });
        },
                
        recuperarArticuloInventario = function(data) {

            /*if(!data.inventario) {
                data.inventario = -1;
            }*/
            
            $.ajax({                 

                data: {idArticulo: data.idArticulo, tipoLista : data.tipoLista, terminal : data.terminal, inventario : data.inventario},
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloInventario, // the file to call
                //url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloCodimat, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                    
                },

                success: function(xmlResponse) {

                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {									
                            var $rs = $(xmlResponse).find("rs");

                            var articuloCodimat = null;

                            if($rs.length != 0)
                            {
                                articuloCodimat = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloCodimat.setPeso($rs.attr('peso'));                                
                                articuloCodimat.setUnidadCalculo($rs.attr('unidadCalculo'));
                                articuloCodimat.setPrecioCosto($rs.attr('precioCosto'));
                                articuloCodimat.setCantidad($rs.attr('totalPrecargado'));                                
                            }		

                            //callback
                            data.onSuccess(articuloCodimat, data.parametrosExtra);
                        }
                        else
                        {
                            var excep = new XMLException(getMensajeErrorArticuloCodimat(data.idArticulo), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }								
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorArticuloCodimat(data.idArticulo), e);
                        
                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        recuperarArticuloFueraDeLista = function(data) {  //utilizada en la toma de inventario

            $.ajax({ 

                data: {idArticulo: data.idArticulo, inventario: data.inventario}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloFueraDeLista, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var $rs = $(xmlResponse).find("rs");

                            var articuloFueraDeLista = null;

                            if($rs.length != 0) {
                                
                                articuloFueraDeLista = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloFueraDeLista.setCantidad($rs.attr('cantidad'));
                                articuloFueraDeLista.setPeso($rs.attr('peso'));
                                articuloFueraDeLista.setPrecioCosto($rs.attr('precio'));                                
                            }		

                            //callback
                            data.onSuccess(articuloFueraDeLista);
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorArticuloFueraDeLista(data.idArticulo), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorArticuloFueraDeLista(data.idArticulo), e);
                        
                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        insertarArticuloInventario = function(data) {  //utilizada en la toma de inventario

            var opcion = 02;
            var origen = data.origen;
            var codigo = data.codigo;
            var detalle = data.detalle;
            var cantidad = data.cantidad;
            var peso = data.peso;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var contenedor = data.contenedor;
            var posicion = data.posicion;            
            //http://192.168.0.121:8080/Inventario/inventario?opcion=02&origen=1&codigo=2990&detalle=cementoDePrueba&cantidad=1&peso=50&ipTerminal=192.168.0.117&usuario=PEPE&terminal=terminalPepe&contenedor=CAJ1&posicion=POS1'
            
            $.ajax({ 

                //data: {idProveedor: data.idProveedor, idInternoArticuloProveedor: data.idInternoArticuloProveedor}, // get the form data
                data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                //data: {origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                type: "POST",
                dataType: "xml",
                url: "../../../../Inventario/inventario", // the file to call
                //url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloFueraDeLista, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {
                            
                            /*var $rs = $(xmlResponse).find("rs");

                            var articuloFueraDeLista = null;

                            if($rs.length != 0) {
                                
                                articuloFueraDeLista = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloFueraDeLista.setCantidad($rs.attr('cantidad'));
                                articuloFueraDeLista.setPeso($rs.attr('peso'));
                                
                            }		

                            //callback*/
                            data.onSuccess('Articulo ingresado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        insertarArticuloInventarioFueraLista = function(data) {  //utilizada en la toma de inventario

            var opcion = 02;
            var origen = data.origen;
            var codigo = data.codigo;
            var detalle = data.detalle;
            var cantidad = data.cantidad;
            var peso = data.peso;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var contenedor = data.contenedor;
            var posicion = data.posicion;            
            
            $.ajax({ 

                //data: {idProveedor: data.idProveedor, idInternoArticuloProveedor: data.idInternoArticuloProveedor}, // get the form data
                data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                //data: {origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                type: "POST",
                dataType: "xml",
                url: "../../../../Inventario/inventario", // the file to call
                //url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloFueraDeLista, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {
                            
                            /*var $rs = $(xmlResponse).find("rs");

                            var articuloFueraDeLista = null;

                            if($rs.length != 0) {
                                
                                articuloFueraDeLista = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloFueraDeLista.setCantidad($rs.attr('cantidad'));
                                articuloFueraDeLista.setPeso($rs.attr('peso'));
                                
                            }		

                            //callback*/
                            data.onSuccess('Articulo ingresado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
        
        insertarArticuloCorrecionInventario = function(data) {  //utilizada en la Correción de inventario

            var opcion = 00;
            //var origen = data.origen;
            var codigo = data.codigo;
            var detalle = data.detalle;
            var cantidad = data.cantidad;
            var peso = data.peso;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var contenedor = data.contenedor;
            var posicion = data.posicion;
            var inventario = data.inventario;
            var tipoCarga = data.tipoCarga;
            var fuera = data.fuera;                        
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, codigo: codigo, descripcion : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion, inventario: inventario, tipoCarga : tipoCarga, fuera : fuera}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                //url : "http://localhost:8080/compras/inventario",
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {
                            
                            /*var $rs = $(xmlResponse).find("rs");

                            var articuloFueraDeLista = null;

                            if($rs.length != 0) {
                                
                                articuloFueraDeLista = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloFueraDeLista.setCantidad($rs.attr('cantidad'));
                                articuloFueraDeLista.setPeso($rs.attr('peso'));
                                
                            }		

                            //callback*/
                            data.onSuccess('Articulo ingresado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
        
        integrarInventario = function(data) {  //utilizada en la integracion de inventario

            var opcion = 01;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var modoIntegracion = data.modoIntegracion;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, modoIntegracion : modoIntegracion}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {
                            
                            /*var $rs = $(xmlResponse).find("rs");

                            var articuloFueraDeLista = null;

                            if($rs.length != 0) {
                                
                                articuloFueraDeLista = new Articulo($rs.attr('idArticulo'), $rs.attr('nombreArticulo'));
                                articuloFueraDeLista.setCantidad($rs.attr('cantidad'));
                                articuloFueraDeLista.setPeso($rs.attr('peso'));
                                
                            }		

                            //callback*/
                            data.onSuccess('La integración se realizó correctamente');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        recuperarListaArticulosEnStock = function(data) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }                        

            $.ajax({
                //data : {idGalpon : data.idGalpon, estadoParte: data.arregloEstadosParte, recuperarArticulos : data.recuperarArticulos, recuperarChoferes : data.recuperarChoferes, recuperarEventos : data.recuperarEventos},
                data : {ipTerminal : data.ipTerminal, usuario : data.usuario, terminal : data.terminal, inventario : data.inventario, deposito : data.deposito},
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.recursosGenerales.controladores.recuperarArticulosEnStock, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("info");
                        
                        var contenedor = null;

                        if($mensaje.text() == "true") {
                            
                            /*var listaPartes = new ListaDePartesPicking();
                            
                            var $fechaConsulta = $(xmlResponse).find("fechaConsulta");
                            listaPartes.setFechaConsulta(new Fecha($fechaConsulta.attr('fecha'), $fechaConsulta.attr('hora')));
                                                                                                                
                            $(xmlResponse).find("parte").each(function() {
                                 
                                listaPartes.insert(crearObjetoParteDesdeXML($(this)));
                            });
                            
                            //callback
                            console.log('operacionesSobreServidorCompras recuperarListaArticulosEnStock success');
                            //data.onSuccess(listaPartes, data.parametrosExtra);*/
                            
                            data.onSuccess($(xmlResponse).find("res").text(), data.parametrosExtra);
                        }
                        else
                        {
                            /*var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorAlRecuperarListaPartes(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }*/
                            data.onFailure($mensaje.text(), data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        //var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);
                        var excep = e;

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        importarStockInventario = function(data) {  //utilizada en la importacion de stock al inventario

            var opcion = 02;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var deposito = data.deposito;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, deposito : deposito}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {
                            
                            data.onSuccess('La importación del stock se realizó correctamente');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                            var excep = new XMLException('Error', $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        //var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        var excep = new Exception('Error', e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        recuperarDatosParaControlInventario = function(data) {
            
            var opcion = 03;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            //var deposito = data.deposito;
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }                        

            $.ajax({
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario}, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {                                                        
                            
                            data.onSuccess($(xmlResponse).find("res").text(), data.parametrosExtra);
                        }
                        else
                        {                            
                            data.onFailure($mensaje.text(), data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        //var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);
                        var excep = e;

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarDatosParaControlInventarioFueraLista = function(data) {
            
            var opcion = 033;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            //var deposito = data.deposito;
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }                        

            $.ajax({
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario}, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {                                                        
                            
                            data.onSuccess($(xmlResponse).find("res").text(), data.parametrosExtra);
                        }
                        else
                        {                            
                            data.onFailure($mensaje.text(), data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        //var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);
                        var excep = e;

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarDatosFueraListaParaAsignarPrecio = function(data) {
            
            var opcion = 06;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            //var deposito = data.deposito;
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }                        

            $.ajax({
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario}, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {                                                        
                            
                            data.onSuccess($(xmlResponse).find("res").text(), data.parametrosExtra);
                        }
                        else
                        {                            
                            data.onFailure($mensaje.text(), data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        //var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);
                        var excep = e;

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        generarResucarga = function(data) {
            
            var opcion = 04;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            //var deposito = data.deposito;
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            $.ajax({
                data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario}, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {
                            
                            data.onSuccess('El archivo se genero correctamente', data.parametrosExtra);
                        }
                        else
                        {                            
                            data.onFailure($mensaje.text(), data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {							
                        //var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);
                        var excep = e;

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        asignarPrecioArticuloFueraDeLista = function(data) {  //utilizada en la Asignacion de precios Articulos fuera de lista

            var opcion = 07;
            //var origen = data.origen;
            var codigo = data.codigoArticulo;
            var precio = data.precioArticulo;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var fuera = 1;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, codigo: codigo, precio : precio, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, fuera: fuera}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                //url : "http://localhost:8080/compras/inventario",
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/
                            data.onSuccess('Precio asignado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        valorizacionPreciosPostInventario = function(data) {  //utilizada en la Valorizacion de precios post inventario

            var opcion = 07;
            var codigo = data.codigo;
            var precio = data.precio;
            var precioAnterior = data.precioAnterior;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var fuera = data.fuera;
            var esPuc = data.esPuc;
            var esUltimoInventario = data.esUltimoInventario;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, codigo: codigo, precio : precio, precioAnterior : precioAnterior, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, fuera: fuera, esPuc: esPuc, esUltimoInventario: esUltimoInventario}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Precio valorizado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        actualizacionPreciosCosto = function(data) {  //utilizada en la Actualizacion de los precios de costos post inventario

            var opcion = 07;
            var codigo = data.codigo;
            var precio = data.precio;
            var tipoActualizacion = data.tipoActualizacion;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var tipoCarga = data.tipoCarga;
            var fuera = data.fuera;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, codigo: codigo, precio : precio, tipoActualizacion: tipoActualizacion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, tipoCarga: tipoCarga, fuera: fuera}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Precio valorizado');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarArticuloDentroLista(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        recuperarComprobantesProveedor = function(data) {

            $.ajax({ 

                data: { usuario : data.idUsuario,
                        ip : data.usuarioIP,
                        tipoSalida : 'pdf',
                        listaDeTiposDeComprobantes : data.listaDeTiposDeComprobantes,
                        tipoComprobante : data.tipoComprobante,
                        estadoComprobante : 1,
                        fechaInicio : data.fechaInicio,
                        fechaFin : data.fechaFin,
                        idProveedor : data.idProveedor }, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.obtenerEstadisticaComprobantes, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("respuesta");
                        
                        var listaComprobantes = new Lista();
                        
                        var encabezado, datosNroCprb;
                        var comprobante;
                        var cprbNroCompleto, fecha;
                        
                        $mensaje.find('comprobante').each(function(){
                            
                            encabezado = $(this).find('encabezado');
                            datosNroCprb = encabezado.find('datosNroCprb');                            
                            cprbNroCompleto = datosNroCprb.attr('cprbNroCompleto');
                            fecha = datosNroCprb.attr('fecha');
                            
                            comprobante = new ComprobanteCompra(null, cprbNroCompleto, null);
                            comprobante.setFechaCreacion(fecha);
                            
                            listaComprobantes.insert(comprobante);
                            
                        });
                        
                        //callback
                        data.onSuccess(listaComprobantes, data.tipoComprobante);
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
	recuperarComprobantesProveedorPendientesDeRecepcion = function(data) {

            $.ajax({ 
                async: false,
                data: { usuario : data.idUsuario,
                        ip : data.usuarioIP,
                        tipoSalida : 'pdf',
                        listaDeTiposDeComprobantes : data.listaDeTiposDeComprobantes,
                        tipoComprobante : data.tipoComprobante,
                        estadoComprobante : 1,
                        estadoRecepcion : 0,
                        fechaInicio : data.fechaInicio,
                        fechaFin : data.fechaFin,
                        idProveedor : data.idProveedor }, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.obtenerEstadisticaComprobantes, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("respuesta");
                        
                        var listaComprobantes = new Lista();
                        
                        var encabezado, datosNroCprb;
                        var comprobante;
                        var cprbNroCompleto, fecha;
                        
                        $mensaje.find('comprobante').each(function(){
                            
                            encabezado = $(this).find('encabezado');
                            datosNroCprb = encabezado.find('datosNroCprb');                            
                            cprbNroCompleto = datosNroCprb.attr('cprbNroCompleto');
                            fecha = datosNroCprb.attr('fecha');
                            
                            comprobante = new ComprobanteCompra(null, cprbNroCompleto, null);
                            comprobante.setFechaCreacion(fecha);
                            
                            listaComprobantes.insert(comprobante);
                            
                        });
                        
                        //callback
                        data.onSuccess(listaComprobantes, data.tipoComprobante);
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
            
           recuperarRemitos = function(data) {

            $.ajax({ 

                data: { usuario : data.idUsuario,
                        ip : data.usuarioIP,
                        tipoSalida : 'pdf',
                        listaDeTiposDeComprobantes : data.listaDeTiposDeComprobantes,
                        tipoComprobante : data.tipoComprobante,
                        estadoComprobante : 0,
                        fechaInicio : data.fechaInicio,
                        fechaFin : data.fechaFin,
                        urlForward : "",
                        urlLogout : "",
                        idProveedor : data.idProveedor }, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.obtenerRemitosOFacturasDeProveedor, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("respuesta");
                        
                        var listaComprobantes = new Lista();
                        
                        var encabezado, datosNroCprb;
                        var comprobante;
                        var cprbNroCompleto, fecha;
                        
                        $mensaje.find('comprobante').each(function(){
                            
                            encabezado = $(this).find('encabezado');
                            datosNroCprb = encabezado.find('datosNroCprb');                            
                            cprbNroCompleto = datosNroCprb.attr('cprbNroCompleto');
                            fecha = datosNroCprb.attr('fecha');
                            
                            comprobante = new ComprobanteCompra(null, cprbNroCompleto, null);
                            comprobante.setFechaCreacion(fecha);
                            
                            listaComprobantes.insert(comprobante);
                            
                        });
                        
                        //callback
                        data.onSuccess(listaComprobantes, data.tipoComprobante);
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        recuperarPesajesAsociadosAComprobante = function(data) {
            $.ajax({ 
                
                data: { usuario : data.usuario,
                        tipoComprobante : data.tipoComprobante,
                        nroCPRB : data.nroCPRB,
                        urlForward : "",
                        urlLogout : "",
                        idProveedor : data.idProveedor }, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarPesajesAsociadosAComprobante, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        //console.log('recuperados los pesajes ok');
                        var $mensaje = $(xmlResponse).find("respuesta");
                        
                        var listaPesajes = new Lista();
                        
                        $mensaje.find('pesaje').each(function(){
                            //console.log("ciclando sobre el pesane nro "+$(this).attr("id"));
                            var pesaje = new Pesaje(parseInt($(this).attr("id")));
                
                            pesaje.setNumero(parseInt($(this).attr("numero")));
                            pesaje.setNumeroParte(parseInt($(this).attr("numeroParte")));
                            pesaje.setPeso(parseFloat($(this).attr("peso")));
                            pesaje.setCodigoBalanza(parseInt($(this).attr("codBalanza")));
                            
                            pesaje.setActivo(opExpedi_utiles.convertirStringABoolean($(this).attr("activo")));
                            pesaje.setFechaCreacion(new Fecha($(this).attr("fechaCreacion"), $(this).attr("horaCreacion")));
                            pesaje.setUsuario(new Usuario(parseInt($(this).attr("idUsuario")), $(this).attr("nombreUsuario")));
                            pesaje.setObservacion($(this).attr("observacion"));
                            pesaje.setCodigoBarras($(this).attr("codigoBarras"));
                            
                            listaPesajes.insertFirst(pesaje);
                            
                        });
                        
                        //callback
                        //console.log("voy a hacer el callback");
                        data.onSuccess(listaPesajes);
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
		
        insertarEditarPesajesAsociadosAComprobante = function(datosDeEntrada) {
            //console.log("Datos de entrada");
            //console.log(datosDeEntrada.data);
            
            var i;
            var arregloDeIdsPesaje = new Array();
            
            
            for (i=0; i<listaDePesajesActual.size(); i++){
                var pesaje = listaDePesajesActual.getItem(i);
                
                arregloDeIdsPesaje[i] = pesaje.getNumero();
            }
            
            $.ajax({ 
                
                
                data: { usuario : datosDeEntrada.data.idUsuario,
                        tipoComprobante : datosDeEntrada.data.tipoCPRB,
                        nroCPRB : datosDeEntrada.data.numeCPRB,
                        arregloDePesajes : arregloDeIdsPesaje,
                        urlForward : datosDeEntrada.urlForward,
                        urlLogout : datosDeEntrada.urlLogout,
                        idProveedor : datosDeEntrada.data.idProveedor }, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.servletInsertarEditarPesajesAsociadosAComprobante, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    console.log("Error al al hacer el ajax: "+jqXHR.responseText+". "+errorThrown);

                    //callback
                    datosDeEntrada.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        //analizar mensaje de respuesta para ver el que hacer con el error
                        //console.log('recuperados los pesajes ok');
                        //var $mensaje = $(xmlResponse).find("respuesta");
                        
                        
                        //
//                        var listaPesajes = new Lista();
//                        
//                        $mensaje.find('pesaje').each(function(){
//                            //console.log("ciclando sobre el pesane nro "+$(this).attr("id"));
//                            var pesaje = new Pesaje(parseInt($(this).attr("id")));
//                
//                            pesaje.setNumero(parseInt($(this).attr("numero")));
//                            pesaje.setNumeroParte(parseInt($(this).attr("numeroParte")));
//                            pesaje.setPeso(parseFloat($(this).attr("peso")));
//                            pesaje.setCodigoBalanza(parseInt($(this).attr("codBalanza")));
//                            
//                            pesaje.setActivo(opExpedi_utiles.convertirStringABoolean($(this).attr("activo")));
//                            pesaje.setFechaCreacion(new Fecha($(this).attr("fechaCreacion"), $(this).attr("horaCreacion")));
//                            pesaje.setUsuario(new Usuario(parseInt($(this).attr("idUsuario")), $(this).attr("nombreUsuario")));
//                            pesaje.setObservacion($(this).attr("observacion"));
//                            pesaje.setCodigoBarras($(this).attr("codigoBarras"));
//                            
//                            listaPesajes.insertFirst(pesaje);
                            
//                        });
                        
                        //callback
                        //console.log("voy a hacer el callback");
                        datosDeEntrada.onSuccess();
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        //console.log("Excepcion al procesar la respuesta del servidor: "+e);
                        //callback
                        datosDeEntrada.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        recibirArticulos = function(datosDeEntrada) {
            //console.log("Datos de entrada");
            //console.log(datosDeEntrada);
            
            var idProveedor = datosDeEntrada.idProveedor;
            var idUsuario = datosDeEntrada.usuario;
            var idGalpon = datosDeEntrada.idGalpon;
            
            var i;
            var datosAEnviar = datosDeEntrada.data+'&idGalpon='+idGalpon;
            
            for (i=0; i<ordenesDeCompra.length; i++){
                datosAEnviar = datosAEnviar + '&ordenesDeCompra='+ordenesDeCompra[i];
            }
            
            for (i=0; i<remitosFacturasProveedor.length; i++){
                datosAEnviar = datosAEnviar + '&remitosFacturasProveedor='+remitosFacturasProveedor[i];
            }
            
            for (i=0; i<faltantes.length; i++){
                datosAEnviar = datosAEnviar + '&faltantes='+faltantes[i];
            }
            
            for (i=0; i<sobrantes.length; i++){
                datosAEnviar = datosAEnviar + '&sobrantes='+sobrantes[i];
            }
//            
//            console.log(datosAEnviar);
 
            $.ajax({ 
                data: datosAEnviar, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recibirArticulos, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    console.log("Error al al hacer el ajax: "+jqXHR.responseText+". "+errorThrown);

                    //callback
                    datosDeEntrada.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        //analizar mensaje de respuesta para ver el que hacer con el error
                        //console.log('recuperados los pesajes ok');
                        var $mensaje = $(xmlResponse).find("recepcion");
                        //console.log('$mensaje: '+$mensaje);
                        //var $datoCPRB = $mensaje.find("comprobante");
                        //console.log('$datoCPRB: '+$datoCPRB);
                        
                        var tipoCPRB = parseInt($mensaje.find("comprobante").attr("tipoNro"));
                        var detalleCPRB = $mensaje.find("comprobante").attr("tipoDeta");
                        var nroCPRB = $mensaje.find("comprobante").attr("nro");
                        
                        //pasar por variable recepcionGenerada, sacar como variable global
                        recepcionGenerada = new ComprobanteCompra(idProveedor,nroCPRB,idUsuario);
                        var tipoCPRB = new TipoComprobante(tipoCPRB, detalleCPRB, '');
                        recepcionGenerada.setTipoComprobante(tipoCPRB);
                        
//                       
                        datosDeEntrada.onSuccess();
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobantesProveedor(), e);
                        //console.log("Excepcion al procesar la respuesta del servidor: "+e);
                        //callback
                        datosDeEntrada.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
        
        recuperarArticulosComprobanteProveedor = function(data) {

            $.ajax({ 

                data: { usuario : data.idUsuario,
                        ip : data.usuarioIP,
                        idProveedor : data.idProveedor,
                        tipoComprobante : data.tipoComprobante,
                        urlForward : data.urlForward,
                        urlLogout : data.urlLogout,
                        numeroCPRB : data.numeroCPRB }, // get the form data
                type: "POST",
                async: false,
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.recuperarComprobante, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var articuloProveedor;
                            var listaDeArticulosProveedor;
                            var articuloProveedorId, articuloProveedorIdInterno, articuloProveedorNombre, articuloProveedorCantidad;

                            listaDeArticulosProveedor = new ListaDeArticulosProveedor();
                            
                            $(xmlResponse).find('comprobante > pagina > cuerpo > articuloProveedor').each(function(){
                                
                                articuloProveedorId = $(this).attr('articuloProveedorId');
                                articuloProveedorIdInterno = $(this).attr('articuloProveedorIdInterno');
                                articuloProveedorNombre = $(this).attr('articuloProveedorNombre');
                                articuloProveedorCantidad = $(this).attr('cantidad');
                                
                                articuloProveedor = new ArticuloProveedor(articuloProveedorId, articuloProveedorNombre);
                                articuloProveedor.setIdInterno(articuloProveedorIdInterno);
                                articuloProveedor.setCantidad(articuloProveedorCantidad);
                                
                                listaDeArticulosProveedor.insertarArticuloProveedor(articuloProveedor);
                                
                            });
                            
                            //callback
                            data.onSuccess(listaDeArticulosProveedor, data.numeroCPRB, data.tipoComprobante);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorRecuperarComprobanteProveedor(data.numeroCPRB), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep, data);
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarComprobanteProveedor(data.numeroCPRB), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
	},
                
        importarDatosDesdeXml = function(data) {  //utilizada en la importacion de datos desde archivos XML

            //var opcion = 10;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            var tipoCarga = data.tipoCarga;
            var dataForm1 = data.data1;
            var dataForm2 = data.data2;
            
            $.ajax({ 
                
                //data: {opcion : opcion, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario, tipoCarga: tipoCarga, archivoDentro: dataForm1, archivoFuera: dataForm2}, // get the form data
                data: dataForm2, // get the form data
                type: "POST",
                //dataType: "xml",
                cache: false,
                contentType: false,
                processData: false,
                //enctype : "multipart/form-data",
                //url : "http://192.168.0.6:8080/compras/inventarios/controladores/subirArchivoImportacion.jsp",
                url: CONFIG.proyectos.compras.controladores.subirArchivosImportacionDatos,
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('La importación de los datos se realizó correctamente');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException('Error1: ', $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception('Error2', e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
        
        recuperarDatosActualizacionCostos = function(data) {

            /*var idProveedor = data.rubro.getFamilia().getProveedor().getId();
            var idFamilia = data.rubro.getFamilia().getId();
            var idRubro = data.rubro.getId();*/
            var idInventario = data.idInventario;
            var idProveedor = data.idProveedor;

            /*if(data.parametrosExtra)
            {
                data.parametrosExtra.rubro = data.rubro;
            }
            else
            {
                data.parametrosExtra = {rubro : data.rubro};
            }*/

            $.ajax({

                    data: {idInventario: idInventario, idProveedor: idProveedor}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarDatosActualizacionCostos, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var datosActualizacionCostos = null;
                                var listaDatosActualizacionCostos = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                //Limpio la lista de subrubros que pueda tener el objeto rubros.
                                //data.rubro.setSubrubros(new ListaDeSubrubrosProveedor());
                                
                                //var listaSubrubros = data.rubro.getSubrubros();                                
                                $rs.each(function() {

                                    datosActualizacionCostos = new DatosActualizacionCostos();
                                    datosActualizacionCostos.setCodigoProveedor($(this).attr('codigoProveedor'));
                                    datosActualizacionCostos.setCodigoArticuloPropio($(this).attr('codigoArticulo'));
                                    datosActualizacionCostos.setCodigoFamilia($(this).attr('codigoFamilia'));
                                    datosActualizacionCostos.setNombreFamilia($(this).attr('nombreFamilia'));
                                    datosActualizacionCostos.setCodigoRubro($(this).attr('codigoRubro'));
                                    datosActualizacionCostos.setNombreRubro($(this).attr('nombreRubro'));
                                    datosActualizacionCostos.setCodigoSubrubro($(this).attr('subRubroCodigo'));
                                    datosActualizacionCostos.setNombreSubrubro($(this).attr('subrubroNombre'));
                                    datosActualizacionCostos.setSubrubroFormula1($(this).attr('subrubroFormula1'));
                                    datosActualizacionCostos.setSubrubroFormula2($(this).attr('subrubroFormula2'));
                                    datosActualizacionCostos.setSubrubroFormula3($(this).attr('subrubroFormula3'));
                                    datosActualizacionCostos.setSubrubroFormula4($(this).attr('subrubroFormula4'));
                                    datosActualizacionCostos.setSubrubroFormula5($(this).attr('subrubroFormula5'));
                                    datosActualizacionCostos.setSubrubroFormula6($(this).attr('subrubroFormula6'));
                                    datosActualizacionCostos.setCoeficienteDolarSubrubro($(this).attr('coeficienteDolarSubrubro'));
                                    datosActualizacionCostos.setTipoModificacionPrecioCosto($(this).attr('tipoModificacionPrecioCosto'));
                                    datosActualizacionCostos.setDescripcionModificacionPrecioCosto($(this).attr('descripcionModificacionPrecioCosto'));
                                    
                                    listaDatosActualizacionCostos.insert(datosActualizacionCostos);
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                data.onSuccess(listaDatosActualizacionCostos);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar datos', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar datos', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
                
        actualizarValorFormulaSubrubro = function(data) {  //utilizada en la Actualizacion de los precios de costos post inventario

            var opcion = 8;
            var codigoRubro = data.codigoRubro;
            var codigoSubrubro = data.codigoSubrubro;
            var baseDeCalculo = data.baseDeCalculo;
            var flete = data.flete;
            var hayPuc = data.hayPuc;
            var for1Anterior = data.for1Anterior;
            var for2Anterior = data.for2Anterior;
            var for3Anterior = data.for3Anterior;
            var for4Anterior = data.for4Anterior;
            var for5Anterior = data.for5Anterior;
            var for6Anterior = data.for6Anterior;
            var coeficienteDolarSubrubro = data.coeficienteDolarSubrubro;
            var formula1 = data.formula1;
            var formula2 = data.formula2;
            var formula3 = data.formula3;
            var formula4 = data.formula4;
            var formula5 = data.formula5;
            var formula6 = data.formula6;
            var formula1ParaRecalculo = data.formula1ParaRecalculo;
            var formula2ParaRecalculo = data.formula2ParaRecalculo;
            var formula3ParaRecalculo = data.formula3ParaRecalculo;
            var formula4ParaRecalculo = data.formula4ParaRecalculo;
            var formula5ParaRecalculo = data.formula5ParaRecalculo;
            var formula6ParaRecalculo = data.formula6ParaRecalculo;
            var ipTerminal = data.ipTerminal;
            var usuario = data.usuario;
            var terminal = data.terminal;
            var inventario = data.inventario;
            
            $.ajax({ 

                //data: {opcion : opcion , origen: origen, codigo: codigo, detalle : detalle, cantidad : cantidad, peso : peso, ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, contenedor : contenedor, posicion : posicion}, // get the form data
                data: {opcion : opcion, codigoRubro: codigoRubro, codigoSubrubro: codigoSubrubro, baseDeCalculo: baseDeCalculo, flete: flete, hayPuc: hayPuc, for1Anterior: for1Anterior, for2Anterior: for2Anterior, 
                       for3Anterior:for3Anterior, for4Anterior:for4Anterior, for5Anterior:for5Anterior, for6Anterior:for6Anterior, coeficienteDolarSubrubro: coeficienteDolarSubrubro,
                       formula1 : formula1, formula2: formula2, formula3: formula3, formula4: formula4, formula5: formula5, formula6: formula6, formula1ParaRecalculo: formula1ParaRecalculo,
                       formula2ParaRecalculo: formula2ParaRecalculo, formula3ParaRecalculo: formula3ParaRecalculo, formula4ParaRecalculo: formula4ParaRecalculo,
                       formula5ParaRecalculo: formula5ParaRecalculo, formula6ParaRecalculo: formula6ParaRecalculo,
                       ipTerminal : ipTerminal, usuario : usuario, terminal : terminal, inventario: inventario}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Fórmulas del subrubro ' + codigoSubrubro + ' actualizadas con éxito');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorActualizarFormulasSubrubro(codigoSubrubro), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorActualizarFormulasSubrubro(codigoSubrubro), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        creacionInventario = function(data) {

            var opcion = 15;
            var numeroEjercicio = data.numeroEjercicio;
            var descripcion = data.descripcion;
            var fechaInicio = data.fechaInicio;
            var horaInicio = data.horaInicio;
            var tipoInventario = data.tipoInventario;            
            
            $.ajax({ 

                data: {opcion: opcion, numeroEjercicio: numeroEjercicio, descripcion: descripcion, fechaInicio: fechaInicio, horaInicio: horaInicio, tipoInventario: tipoInventario}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('El Inventario se creó exitosamente');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorCrearInventario(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorCrearInventario(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        finalizacionInventario = function(data) {

            var opcion = 16;
            var idInventario = data.idInventario;
            var fechaFin = data.fechaFin;
            var horaFin = data.horaFin;
            
            $.ajax({ 

                data: {opcion: opcion, idInventario: idInventario, fechaFin: fechaFin, horaFin: horaFin}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        //if($mensaje.attr("estadoOperacion") == "ok") {
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('El Inventario se finalizó exitosamente');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException(getMensajeErrorFinalizarInventario(), $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorFinalizarInventario(), e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        recuperarListaDeTerminalesDetalleInventario = function(data) {
           
            var opcion = 17;
            var idInventario = data.idInventario;

            $.ajax({

                    //data: {opcion: opcion, idInventario: idInventario}, 
                    data: {idInventario: idInventario},
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarTerminalesDetalleInventario, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var detalleArticuloInventario = null;
                                var listaDeDetalleArticulosInventario = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                //Limpio la lista de subrubros que pueda tener el objeto rubros.
                                //data.rubro.setSubrubros(new ListaDeSubrubrosProveedor());
                                
                                //var listaSubrubros = data.rubro.getSubrubros();                                
                                $rs.each(function() {

                                    /*detalleArticuloInventario = new DetalleArticuloInventario();
                                    
                                    detalleArticuloInventario.setTerminal($(this).attr('terminal'));*/
                                    
                                    /*datosActualizacionCostos.setCodigoProveedor($(this).attr('codigoProveedor'));
                                    datosActualizacionCostos.setCodigoArticuloPropio($(this).attr('codigoArticulo'));
                                    datosActualizacionCostos.setCodigoFamilia($(this).attr('codigoFamilia'));
                                    datosActualizacionCostos.setNombreFamilia($(this).attr('nombreFamilia'));
                                    datosActualizacionCostos.setCodigoRubro($(this).attr('codigoRubro'));
                                    datosActualizacionCostos.setNombreRubro($(this).attr('nombreRubro'));
                                    datosActualizacionCostos.setCodigoSubrubro($(this).attr('subRubroCodigo'));
                                    datosActualizacionCostos.setNombreSubrubro($(this).attr('subrubroNombre'));
                                    datosActualizacionCostos.setSubrubroFormula1($(this).attr('subrubroFormula1'));
                                    datosActualizacionCostos.setSubrubroFormula2($(this).attr('subrubroFormula2'));
                                    datosActualizacionCostos.setSubrubroFormula3($(this).attr('subrubroFormula3'));
                                    datosActualizacionCostos.setSubrubroFormula4($(this).attr('subrubroFormula4'));
                                    datosActualizacionCostos.setSubrubroFormula5($(this).attr('subrubroFormula5'));
                                    datosActualizacionCostos.setSubrubroFormula6($(this).attr('subrubroFormula6'));*/
                                    
                                    //listaDeDetalleArticulosInventario.insert(detalleArticuloInventario);
                                    listaDeDetalleArticulosInventario.insert($(this).attr('terminal'));
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                data.onSuccess(listaDeDetalleArticulosInventario);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar terminales', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar terminales', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
        
        recuperarListaDeArticulosSegunRubroSubrubro = function(data) {
            
            var idInventario = data.inventario;
            var codigoRubro = data.codigoRubro;
            var codigoSubrubro = data.codigoSubrubro;          

            $.ajax({

                    data: {idInventario: idInventario, codigoRubro: codigoRubro, codigoSubrubro: codigoSubrubro}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticulosSegunRubroSubrubro, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var articulo = null;
                                var listaArticulos = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                //Limpio la lista de subrubros que pueda tener el objeto rubros.
                                //data.rubro.setSubrubros(new ListaDeSubrubrosProveedor());
                                
                                //var listaSubrubros = data.rubro.getSubrubros();                                
                                $rs.each(function() {

                                    articulo = new Articulo($(this).attr('codigoArticulo'), $(this).attr('descripcionArticulo'));
                                    articulo.setUnidadCalculo($(this).attr('unidadCalculo'));
                                    articulo.setPrecioCosto($(this).attr('precioCosto'));
                                    articulo.setPrecioCostoBackup($(this).attr('precioCostoBackup'));
                                    
                                    listaArticulos.insert(articulo);
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                data.onSuccess(listaArticulos);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar artículos', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar artículos', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
                
        recuperarArticuloConPUC = function(data) {
            
            var idInventario = data.inventario;
            var codigoProveedor = data.codigoProveedor;
            var codigoArticulo = data.codigoArticulo;          
            //var lista = data.lista;
            
            $.ajax({

                    data: {idInventario: idInventario, codigoProveedor: codigoProveedor, codigoArticulo: codigoArticulo}, 
                    //data: {idInventario: idInventario, codigoProveedor: codigoProveedor, lista: lista}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarArticuloConPrecioUltimaCompra, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var articulo = null;
                                var listaArticulos = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                                     
                                $rs.each(function() {

                                    articulo = new Articulo($(this).attr('codigoArticulo'), $(this).attr('descripcionArticulo'));
                                    articulo.setPrecioCosto($(this).attr('precioUltimaCompra'));                                    
                                    
                                    listaArticulos.insert(articulo);
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                //data.onSuccess(listaArticulos);
                                data.onSuccess(articulo);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar artículo PUC', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar artículo PUC', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
                
        recuperarDatosTotalesModificacionCostos = function(data) {
            
            var idInventario = data.inventario;
            var codigoSubrubro = data.codigoSubrubro;
            var tipoModificacion = data.tipoModificacion;
            //var lista = data.lista;
            
            $.ajax({

                    data: {idInventario: idInventario, codigoSubrubro: codigoSubrubro, tipoModificacion: tipoModificacion}, 
                    //data: {idInventario: idInventario, codigoProveedor: codigoProveedor, lista: lista}, 
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.recursosGenerales.controladores.recuperarDatosTotalesModificacionCostosPorSubrubro, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("mensaje");

                            if($mensaje.attr("estadoOperacion") == "ok")
                            {	
                                var datosTotales = null;
                                var articulo = null;
                                var listaArticulos = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                                     
                                $rs.each(function() {
                                    
                                    datosTotales = new DatosTotalesModificacionCostos();
                                    datosTotales.setCodigoSubrubro($(this).attr('codigoSubrubro'));
                                    datosTotales.setTotalRecalculado($(this).attr('totalRecalculado'));        
                                    datosTotales.setTotalOriginal($(this).attr('totalOriginal'));
                                    datosTotales.setDiferencia($(this).attr('diferencia'));
                                    datosTotales.setPorcentajeDiferencia($(this).attr('porcentajeDiferencia'));
                                    datosTotales.setFechaModificacion($(this).attr('fechaModificacion'));
                                    datosTotales.setHoraModificacion($(this).attr('horaModificacion'));
                                    datosTotales.setIdInventario($(this).attr('idInventario'));
                                    datosTotales.setEstado($(this).attr('estado'));
                                    
                                    //articulo = new Articulo($(this).attr('codigoArticulo'), $(this).attr('descripcionArticulo'));
                                    //articulo.setPrecioCosto($(this).attr('precioUltimaCompra'));                                    
                                    
                                    //listaArticulos.insert(articulo);
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                //data.onSuccess(listaArticulos);
                                //data.onSuccess(articulo);
                                data.onSuccess(datosTotales);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar totales modificación', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar totales modificación', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
                
        actualizarPrecioCostoAlCierreEjercicio = function(data) {  //utilizada en la Actualizacion costos
            
            var opcion = 17;
            var inventario = data.inventario;
            var codigoSubrubro = data.codigoSubrubro;
            var listaArticulos = data.listaArticulos;
            
            $.ajax({ 
                
                data: {opcion: opcion, inventario: inventario, codigoSubrubro: codigoSubrubro, listaArticulos: listaArticulos}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Precios actualizados');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException('Actualizar costo cierre', $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception('Actualizar costo cierre', e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        }, 
                
        actualizarPrecioCostoPorPUC = function(data) {  //utilizada en la Actualizacion costos
            
            var opcion = 18;
            var inventario = data.inventario;
            var codigoSubrubro = data.codigoSubrubro;
            var listaArticulos = data.listaArticulos;
            
            $.ajax({ 
                
                data: {opcion: opcion, inventario: inventario, codigoSubrubro: codigoSubrubro, listaArticulos: listaArticulos}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Precios actualizados');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException('Actualizar costo PUC', $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception('Actualizar costo PUC', e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        },
                
        recuperarTotalesInventario = function(data) {
           
            var opcion = 19;
            var inventario = data.inventario;

            $.ajax({

                    //data: {opcion: opcion, idInventario: idInventario}, 
                    data: {opcion: opcion, inventario: inventario},
                    type: "POST",
                    dataType: "xml",
                    url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call

                    error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        //data.onFailure(excep, data.parametrosExtra);
                        data.onFailure(excep);
                    },

                    success: function(xmlResponse){

                        try 
                        {						
                            var $mensaje = $(xmlResponse).find("info");

                            if($mensaje.text() == "true")
                            {	
                                var detalleArticuloInventario = null;
                                var lista = new Lista();
                                
                                var $rs = $(xmlResponse).find("rs");                                
                                
                                $rs.each(function() {
                                
                                    lista.insert($(this).attr('totalRecalculado'));
                                    lista.insert($(this).attr('totalOriginal'));
                                    lista.insert($(this).attr('diferencia'));
                                    lista.insert($(this).attr('porcentajeDiferencia'));
                                });
                                

                                //callback
                                //data.onSuccess(listaSubrubros, data.parametrosExtra);
                                data.onSuccess(lista);

                            }
                            else
                            {
                                //var excep = new XMLException(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), $mensaje.attr('mensaje'));
                                //var excep = new XMLException(getMensajeErrorDatosActualizacionCostos(idProveedor), $mensaje.attr('mensaje'));
                                var excep = new XMLException('Recuperar totales', $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data);
                            }
                        }							
                        catch(e) 
                        {								
                            //var excep = new Exception(getMensajeErrorSubrubrosProveedor(idProveedor, idFamilia, idRubro), e);
                            //var excep = new Exception(getMensajeErrorDatosActualizacionCostos(idProveedor), e);
                            var excep = new Exception('Recuperar totales', e);

                            //callback
                            data.onFailure(excep, data);
                        }
                    } 
            });
        },
                
        blanqueoFormulasSubrubros = function(data) {  //utilizada en la Actualizacion costos
            
            var opcion = 20;
            var inventario = data.inventario;
            var idFormulaBlanquear = data.idFormulaBlanquear;
            
            $.ajax({ 
                
                data: {opcion: opcion, inventario: inventario, idFormulaBlanquear: idFormulaBlanquear}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.compras.controladores.operacionesInventario, // the file to call
                
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("info");
                        
                        if($mensaje.text() == "true") {

                            //callback*/                            
                            data.onSuccess('Se blanqueó la fórmula y los subrubros fueron recalculados');
                        }
                        else {
                            
                            //var excep = new XMLException(getMensajeErrorInsertarArticuloDentroLista(), $mensaje.attr('mensaje'));
                            var excep = new XMLException('Blanqueo fórmulas', $mensaje.text());
                                    
                            //callback
                            data.onFailure(excep, data);
                        }								
                    }
                    catch(e) {
                        
                        var excep = new Exception('Blanqueo fórmulas', e);
                        
                        //callback
                        data.onFailure(excep, data);
                    }
                }                                
            });
        };
		
	return {
	
            recuperarProveedor : recuperarProveedor,
            recuperarFamiliasProveedor : recuperarFamiliasProveedor,
            recuperarRubrosProveedor : recuperarRubrosProveedor,
            recuperarSubrubrosProveedor : recuperarSubrubrosProveedor,
            recuperarArticulosProveedor : recuperarArticulosProveedor,
            recuperarListaDePreciosProveedor : recuperarListaDePreciosProveedor,
            recuperarDatosUltimaCompraPorProveedor : recuperarDatosUltimaCompraPorProveedor,
            recuperarArticuloCodimat : recuperarArticuloCodimat,            
            recuperarArticuloProveedor : recuperarArticuloProveedor,
            recuperarVinculacionesArticuloProveedor : recuperarVinculacionesArticuloProveedor,
            recuperarVariacionesCosto : recuperarVariacionesCosto,
            recuperarComprobanteCompra : recuperarComprobanteCompra,
            recuperarAsociacionesComprobanteCompra : recuperarAsociacionesComprobanteCompra,
            recuperarItemListaDePreciosPorFecha : recuperarItemListaDePreciosPorFecha,
            recuperarArticuloInventario : recuperarArticuloInventario,
            recuperarArticuloFueraDeLista : recuperarArticuloFueraDeLista,
            recuperarDatosActualizacionCostos : recuperarDatosActualizacionCostos,
            recuperarListaDeArticulosSegunRubroSubrubro : recuperarListaDeArticulosSegunRubroSubrubro,
            recuperarArticuloConPUC : recuperarArticuloConPUC,
            recuperarDatosTotalesModificacionCostos : recuperarDatosTotalesModificacionCostos,
            recuperarTotalesInventario : recuperarTotalesInventario,

            insertarArticuloInventario : insertarArticuloInventario,
            insertarArticuloInventarioFueraLista : insertarArticuloInventarioFueraLista,
            insertarArticuloCorrecionInventario : insertarArticuloCorrecionInventario,
            integrarInventario : integrarInventario,
            recuperarListaArticulosEnStock : recuperarListaArticulosEnStock,
            recuperarDatosParaControlInventario : recuperarDatosParaControlInventario,
            recuperarDatosParaControlInventarioFueraLista : recuperarDatosParaControlInventarioFueraLista,
            importarStockInventario : importarStockInventario,
            recuperarDatosFueraListaParaAsignarPrecio : recuperarDatosFueraListaParaAsignarPrecio,
            recuperarListaDeTerminalesDetalleInventario : recuperarListaDeTerminalesDetalleInventario,
            asignarPrecioArticuloFueraDeLista : asignarPrecioArticuloFueraDeLista,
            valorizacionPreciosPostInventario : valorizacionPreciosPostInventario,
            actualizacionPreciosCosto : actualizacionPreciosCosto,
            importarDatosDesdeXml : importarDatosDesdeXml,
            actualizarValorFormulaSubrubro : actualizarValorFormulaSubrubro,
            actualizarPrecioCostoAlCierreEjercicio : actualizarPrecioCostoAlCierreEjercicio,
            actualizarPrecioCostoPorPUC : actualizarPrecioCostoPorPUC,
            creacionInventario : creacionInventario,
            finalizacionInventario : finalizacionInventario,
            generarResucarga : generarResucarga,
            recuperarRemitos : recuperarRemitos,
            recuperarPesajesAsociadosAComprobante:recuperarPesajesAsociadosAComprobante,
            insertarEditarPesajesAsociadosAComprobante:insertarEditarPesajesAsociadosAComprobante,
            recibirArticulos : recibirArticulos,
            recuperarComprobantesProveedor : recuperarComprobantesProveedor,
            recuperarComprobantesProveedorPendientesDeRecepcion : recuperarComprobantesProveedorPendientesDeRecepcion,
            recuperarArticulosComprobanteProveedor : recuperarArticulosComprobanteProveedor,
            
            blanqueoFormulasSubrubros : blanqueoFormulasSubrubros,
            
            insertarOrdenDeCompra : insertarOrdenDeCompra,	
            anularComprobanteDeCompra : anularComprobanteDeCompra,
            insertarFamiliaProveedor : insertarFamiliaProveedor,
            darBajaFamiliaProveedor : darBajaFamiliaProveedor,
            insertarRubroProveedor : insertarRubroProveedor,	
            darBajaRubroProveedor : darBajaRubroProveedor,
            insertarSubrubroProveedor : insertarSubrubroProveedor,
            darBajaSubrubroProveedor : darBajaSubrubroProveedor,
            insertarArticuloProveedor : insertarArticuloProveedor,
            editarIdArticuloProveedor : editarIdArticuloProveedor,
            insertarVinculacion : insertarVinculacion,
            //insertarVariacionCosto : insertarVariacionCosto, //OBSOLETO
            
            insertarRemitoProveedor : insertarRemitoProveedor,
            
            actualizarListaDePreciosProveedor : actualizarListaDePreciosProveedor,

            bajaArticulo : bajaArticulo,
            bajaVinculacion : bajaVinculacion,
            
            getMensajeErrorBajaArticuloProveedor : getMensajeErrorBajaArticuloProveedor
	};						
 }

