/*
 *  ---------------------------------------------
 *  Fernando Prieto - Noviembre 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeArticulosProveedor = function () {    

    var nuevaLista = Object.create(new Lista());
    
    nuevaLista.insertarArticuloProveedor = function(articuloProveedor) {

        this.insert(articuloProveedor);
        
    },
            
    nuevaLista.recuperarArticuloProveedor = function(posicion) {

        var articuloProveedor;

        articuloProveedor = this.getItem(posicion);
        
        return articuloProveedor;
        
    },

    nuevaLista.searchItem = function(id_articulo) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_articulo) {
                return itemActual;
            }                
        }
        return itemActual;
    };

    return nuevaLista;
}