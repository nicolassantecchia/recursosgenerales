/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeSubrubrosProveedor = function () {    

    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(id_subrubro) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_subrubro) {
                return itemActual;
            }                
        }
        return itemActual;
    };

    return nuevaLista;
}