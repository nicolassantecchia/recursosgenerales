/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  ---------------------------------------------
 */

var SubrubroProveedor = function (mi_id, mi_nombre, mi_rubro) {

    //Atributos Privados
    var rubro = mi_rubro,          //RubroProveedor
        id = mi_id,                //String
        nombre = mi_nombre,        //String
        infoAdicional = '',        //String
        articulos = new ListaDeArticulosProveedor(),   

        getId = function() {
            return id;
        },

        getInfoAdicional = function() {
            return infoAdicional;
        },

        getNombre = function() {
            return nombre;
        },

        getRubro = function() {
            return rubro;
        },

        getArticulos = function() {
            return articulos;
        },
                
        setArticulos = function(lista) {
            articulos = lista;
        },

        setInfoAdicional = function(info) {
            infoAdicional = info;
        };

    return  {                               
                getId: getId,
                getInfoAdicional: getInfoAdicional,
                getNombre: getNombre,
                getRubro: getRubro,
                getArticulos: getArticulos,
                setArticulos : setArticulos,
                setInfoAdicional: setInfoAdicional
            }    
}