/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeFamiliasProveedor = function () {    

    var nuevaLista = Object.create(new Lista())

    nuevaLista.searchItem = function(id_familia) {

        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_familia) {
                return itemActual;
            }                
        }
        return itemActual;
    };
    
    nuevaLista.deleteFamilia = function(id_familia) {
        
        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_familia) {
                
                this.deleteItem(i);
                break;
            }                
        }
        
    };

    return nuevaLista;
}