/*
 *  ---------------------------------------------
 *              Alejandro Battista
 *  ---------------------------------------------
 */

var DatosActualizacionCostos = function () {

    //Atributos Privados
    var proveedor = -1,
        articuloPropio = -1,
        codigoFamilia = -1,
        nombreFamilia = '',
        codigoRubro = -1,
        nombreRubro = '',
        codigoSubrubro = -1,
        nombreSubrubro = '',
        subrubroFormula1 = '',
        subrubroFormula2 = '',
        subrubroFormula3 = '',
        subrubroFormula4 = '',
        subrubroFormula5 = '',
        subrubroFormula6 = '',
        coeficienteDolarSubrubro = -1,
        tipoModificacionPrecioCosto = -1,
        descripcionModificacionPrecioCosto = '',
        
        getCodigoProveedor = function() {
            return proveedor;
        },
                
        getCodigoArticuloPropio = function() {
            return articuloPropio;
        },

        getCodigoFamilia = function() {
            return codigoFamilia;
        },

        getNombreFamilia = function() {
            return nombreFamilia;
        },
        
        getCodigoRubro = function() {
            return codigoRubro;
        },

        getNombreRubro = function() {
            return nombreRubro;
        },
                
        getCodigoSubrubro = function() {
             return codigoSubrubro;
        },

        getNombreSubrubro = function() {
            return nombreSubrubro;
        },

        getSubrubroFormula1 = function() {
            return subrubroFormula1;
        };

        getSubrubroFormula2 = function() {
            return subrubroFormula2;
        };

        getSubrubroFormula3 = function() {
            return subrubroFormula3;
        };

        getSubrubroFormula4 = function() {
            return subrubroFormula4;
        };

        getSubrubroFormula5 = function() {
            return subrubroFormula5;
        };

        getSubrubroFormula6 = function() {
            return subrubroFormula6;
        };
        
        getCoeficienteDolarSubrubro = function() {
            return coeficienteDolarSubrubro;
        };
        
        getTipoModificacionPrecioCosto = function() {
            return tipoModificacionPrecioCosto;
        };
        
        getDescripcionModificacionPrecioCosto = function() {
            return descripcionModificacionPrecioCosto;
        };
        
        setCodigoProveedor = function(pr) {
            proveedor = pr;
        },
                
        setCodigoArticuloPropio = function(ap) {
            articuloPropio = ap;
        },

        setCodigoFamilia = function(cf) {
            codigoFamilia = cf;
        },

        setNombreFamilia = function(nf) {
            nombreFamilia = nf;
        },
        
        setCodigoRubro = function(cr) {
            codigoRubro = cr;
        },

        setNombreRubro = function(nr) {
            nombreRubro = nr;
        },
                
        setCodigoSubrubro = function(cs) {
             codigoSubrubro = cs;
        },

        setNombreSubrubro = function(ns) {
            nombreSubrubro = ns;
        },

        setSubrubroFormula1 = function(f1) {
            subrubroFormula1 = f1;
        };

        setSubrubroFormula2 = function(f2) {
            subrubroFormula2 = f2;
        };

        setSubrubroFormula3 = function(f3) {
            subrubroFormula3 = f3;
        };

        setSubrubroFormula4 = function(f4) {
            subrubroFormula4 = f4;
        };

        setSubrubroFormula5 = function(f5) {
            subrubroFormula5 = f5;
        };

        setSubrubroFormula6 = function(f6) {
            subrubroFormula6 = f6;
        };
        
        setCoeficienteDolarSubrubro = function(coeficiente) {
            coeficienteDolarSubrubro = coeficiente;
        };
        
        setTipoModificacionPrecioCosto = function(tipoModificacion) {
            tipoModificacionPrecioCosto = tipoModificacion;
        };
        
        setDescripcionModificacionPrecioCosto = function(descripcion) {
            descripcionModificacionPrecioCosto = descripcion;
        };

    return  {                               
                getCodigoProveedor: getCodigoProveedor,
                getCodigoArticuloPropio: getCodigoArticuloPropio,
                getCodigoFamilia: getCodigoFamilia,
                getNombreFamilia: getNombreFamilia,
                getCodigoRubro: getCodigoRubro,
                getNombreRubro: getNombreRubro,
                getCodigoSubrubro: getCodigoSubrubro,
                getNombreSubrubro: getNombreSubrubro,                
                getSubrubroFormula1: getSubrubroFormula1,
                getSubrubroFormula2: getSubrubroFormula2,
                getSubrubroFormula3: getSubrubroFormula3,
                getSubrubroFormula4: getSubrubroFormula4,
                getSubrubroFormula5: getSubrubroFormula5,
                getSubrubroFormula6: getSubrubroFormula6,
                getCoeficienteDolarSubrubro: getCoeficienteDolarSubrubro,
                getTipoModificacionPrecioCosto: getTipoModificacionPrecioCosto,
                getDescripcionModificacionPrecioCosto: getDescripcionModificacionPrecioCosto,
                setCodigoProveedor: setCodigoProveedor,
                setCodigoArticuloPropio: setCodigoArticuloPropio,
                setCodigoFamilia: setCodigoFamilia,
                setNombreFamilia: setNombreFamilia,
                setCodigoRubro: setCodigoRubro,
                setNombreRubro: setNombreRubro,
                setCodigoSubrubro: setCodigoSubrubro,
                setNombreSubrubro: setNombreSubrubro,
                setSubrubroFormula1: setSubrubroFormula1,
                setSubrubroFormula2: setSubrubroFormula2,
                setSubrubroFormula3: setSubrubroFormula3,
                setSubrubroFormula4: setSubrubroFormula4,
                setSubrubroFormula5: setSubrubroFormula5,
                setSubrubroFormula6: setSubrubroFormula6,                
                setCoeficienteDolarSubrubro: setCoeficienteDolarSubrubro,
                setTipoModificacionPrecioCosto: setTipoModificacionPrecioCosto,
                setDescripcionModificacionPrecioCosto: setDescripcionModificacionPrecioCosto
            }    
}