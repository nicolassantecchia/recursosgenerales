/*
 *  ---------------------------------------------
 *  Alejandro Battista
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeDetalleArticulosInventario = function () {    

    var nuevaLista = Object.create(new Lista());
    
    nuevaLista.insertarDetalleArticuloInventario = function(detalleArticuloInventario) {

        this.insert(detalleArticuloInventario);
        
    },
            
    nuevaLista.recuperarDetalleArticuloInventario = function(posicion) {

        var detalleArticuloInventario;

        detalleArticuloInventario = this.getItem(posicion);
        
        return detalleArticuloInventario;
        
    };

    /*nuevaLista.searchItem = function(id_articulo) {

        var itemActual = null;
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_articulo) {
                return itemActual;
            }                
        }
        return itemActual;
    };*/

    return nuevaLista;
}