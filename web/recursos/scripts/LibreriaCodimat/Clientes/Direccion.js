/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Octubre 2014
 *  ---------------------------------------------
 */

var Direccion = function (mi_dom, mi_ciu) { 

	//Atributos Privados
	var domicilio = mi_dom, //String
		ciudad = mi_ciu,//String
		codigoPostal,//String
		
    getCiudad = function() {
        return ciudad;
    },

    getDomicilio = function() {
        return domicilio;
    },

    getCodigoPostal = function() {
        return codigoPostal;
    },

    setCodigoPostal = function(cPost) {
        codigoPostal = cPost;
    }; 

    return  {  
				getCiudad: getCiudad,
				getDomicilio: getDomicilio,
				getCodigoPostal: getCodigoPostal,
				setCodigoPostal: setCodigoPostal
			}
}