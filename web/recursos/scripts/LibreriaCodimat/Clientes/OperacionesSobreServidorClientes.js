/**
 *  REQUIERE configuracion.js que se encuentra en proyecto clientes/recursos/scripts/config
 */

var OperacionesSobreServidorClientes = function() {

    var	urlParamUsuario = CONFIG.variables.sistem.sesion.parametros.usuario,            
        urlParamIP = CONFIG.variables.sistem.sesion.parametros.ip, 
        opCompras_utiles = new Utiles(), //Objeto creado para poder utlu
        
        getMensajeErrorCliente= function(idCliente) {	
            return "Ocurri&oacute; un error al cargar los datos del Cliente<em>"+idCliente+"</em>.";
        },


        recuperarCliente= function(data) {		

            $.ajax({ 

                data: {idCliente: data.idCliente}, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarClientePorId, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {
                   
                    try
                    {							
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {

                            var $rs = $(xmlResponse).find("rs");
                            var cliente = null; 

                            if($rs.length != 0) 
                            {
                                
                                cliente = new Cliente($rs.text(), $rs.attr('info'));                                
                                var doc = new DocumentoIdentificador($rs.attr('documentoIdentificadorId'));											
                                doc.setNro($rs.attr('documentoIdentificadorNro'));
                                
                                /*var condicionIVA = null;
                                
                                if( $rs.attr('condicionIVACodigo') != null && 
                                    $rs.attr('condicionIVADetalle') != null && 
                                    $rs.attr('condicionIVAAbrev') != null) {
                                        
                                    condicionIVA = new CondicionIVA($rs.attr('condicionIVACodigo'), $rs.attr('condicionIVADetalle'), $rs.attr('condicionIVAAbrev'));                                
                                }

                                var dir = new Direccion($rs.attr('direccion'), $rs.attr('ciudad'));	
                                dir.setCodigoPostal($rs.attr('codigoPostal'));

                                proveedor.setTelefono($rs.attr('telefono'));
                                proveedor.setCondicionIVA(condicionIVA);
                                proveedor.setDocumentoIdentificador(doc);
                                proveedor.setDireccion(dir);
                                */
                            }

                            //callback
                            data.onSuccess(cliente, data.parametrosExtra);
                            
                        }
                        else 
                        {
                            var excep = new XMLException(getMensajeErrorCliente(data.idCliente), $mensaje.attr('mensaje'));

                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorCliente(data.idCliente), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        };
		
	return {	
            recuperarCliente : recuperarCliente
            /*recuperarFamiliasProveedor : recuperarFamiliasProveedor,
            recuperarRubrosProveedor : recuperarRubrosProveedor,
            recuperarSubrubrosProveedor : recuperarSubrubrosProveedor,
            recuperarArticulosProveedor : recuperarArticulosProveedor,
            recuperarListaDePreciosProveedor : recuperarListaDePreciosProveedor,
            recuperarDatosUltimaCompraPorProveedor : recuperarDatosUltimaCompraPorProveedor,
            recuperarArticuloCodimat : recuperarArticuloCodimat,            
            recuperarArticuloProveedor : recuperarArticuloProveedor,
            recuperarVinculacionesArticuloProveedor : recuperarVinculacionesArticuloProveedor,
            recuperarVariacionesCosto : recuperarVariacionesCosto,
            recuperarComprobanteCompra : recuperarComprobanteCompra,
            recuperarItemListaDePreciosPorFecha : recuperarItemListaDePreciosPorFecha,
            recuperarArticuloInventario : recuperarArticuloInventario,
            recuperarArticuloFueraDeLista : recuperarArticuloFueraDeLista,
            recuperarDatosActualizacionCostos : recuperarDatosActualizacionCostos,

            insertarArticuloInventario : insertarArticuloInventario,
            insertarArticuloInventarioFueraLista : insertarArticuloInventarioFueraLista,
            insertarArticuloCorrecionInventario : insertarArticuloCorrecionInventario,
            integrarInventario : integrarInventario,
            recuperarListaArticulosEnStock : recuperarListaArticulosEnStock,
            recuperarDatosParaControlInventario : recuperarDatosParaControlInventario,
            recuperarDatosParaControlInventarioFueraLista : recuperarDatosParaControlInventarioFueraLista,
            importarStockInventario : importarStockInventario,
            recuperarDatosFueraListaParaAsignarPrecio : recuperarDatosFueraListaParaAsignarPrecio,
            asignarPrecioArticuloFueraDeLista : asignarPrecioArticuloFueraDeLista,
            valorizacionPreciosPostInventario : valorizacionPreciosPostInventario,
            actualizacionPreciosCosto : actualizacionPreciosCosto,
            importarDatosDesdeXml : importarDatosDesdeXml,
            generarResucarga : generarResucarga,
            recuperarComprobantesProveedor : recuperarComprobantesProveedor,
            recuperarArticulosComprobanteProveedor : recuperarArticulosComprobanteProveedor,
            
            insertarOrdenDeCompra : insertarOrdenDeCompra,	
            anularComprobanteDeCompra : anularComprobanteDeCompra,
            insertarFamiliaProveedor : insertarFamiliaProveedor,
            darBajaFamiliaProveedor : darBajaFamiliaProveedor,
            insertarRubroProveedor : insertarRubroProveedor,	
            darBajaRubroProveedor : darBajaRubroProveedor,
            insertarSubrubroProveedor : insertarSubrubroProveedor,
            darBajaSubrubroProveedor : darBajaSubrubroProveedor,
            insertarArticuloProveedor : insertarArticuloProveedor,
            editarIdArticuloProveedor : editarIdArticuloProveedor,
            insertarVinculacion : insertarVinculacion,
            //insertarVariacionCosto : insertarVariacionCosto, //OBSOLETO
            
            insertarRemitoProveedor : insertarRemitoProveedor,
            
            actualizarListaDePreciosProveedor : actualizarListaDePreciosProveedor,

            bajaArticulo : bajaArticulo,
            bajaVinculacion : bajaVinculacion,
            
            getMensajeErrorBajaArticuloProveedor : getMensajeErrorBajaArticuloProveedor*/
	};						
 }

