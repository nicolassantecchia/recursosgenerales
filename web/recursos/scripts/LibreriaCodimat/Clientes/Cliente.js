/*
 *  ---------------------------------------------
 *  Nicolas Santecchia - Febrero 2018
 *  ---------------------------------------------
 */

var Cliente = function (mi_id, mi_nombre) { 

    //Atributos Privados
    var id = mi_id,                        //String
        nombre = mi_nombre,               //String
        direccion,
        documentoIdentificador,
        telefono = '',
        condicionIVA,  //CondicionIVA

        getDireccion = function() {
            return direccion;
        },
		
        getDocumentoIdentificador = function() {
            return documentoIdentificador;
        },

        getId = function() {
            return id;
        },

        getNombre = function() {
            return nombre;
        },

        getFamilias = function() {
            return familias;
        },
                
        setFamilias = function(lista) {
            familias = lista;
        },
		
        getTelefono = function() {
            return telefono;
        },
                
        getCondicionIVA = function() {
            return condicionIVA;
        },
                
        setCondicionIVA = function(cond) {
            condicionIVA = cond;
        },
        
        setTelefono = function(tel) {
            telefono = tel;
        }

        setDireccion = function(dir) {
            direccion = dir;
        },
		
        setDocumentoIdentificador = function(doc) {
            documentoIdentificador = doc;
        }; 

    return  {                               
                getDireccion : getDireccion,
                getDocumentoIdentificador : getDocumentoIdentificador,
                getId : getId,
                getNombre : getNombre,
                getFamilias : getFamilias,
                getTelefono : getTelefono,
                getCondicionIVA : getCondicionIVA,
                
                setFamilias : setFamilias,
                setCondicionIVA : setCondicionIVA,
                setTelefono : setTelefono,
                setDireccion: setDireccion,
                setDocumentoIdentificador : setDocumentoIdentificador
            }
}