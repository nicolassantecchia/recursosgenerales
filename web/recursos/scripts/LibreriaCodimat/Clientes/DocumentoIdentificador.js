/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Octubre 2014
 *  ---------------------------------------------
 */

var DocumentoIdentificador = function (mi_id) { 

    //Atributos Privados
    var id = mi_id, //int
        detalle,	//String
        abrev,		//String
        nro, 		//String
		
    getAbrev = function() {
        return abrev;
    },

    getDetalle = function() {
        return detalle;
    },

    getId = function() {
        return id;
    },
	
    getNro = function() {
        return nro;
    },

    setAbrev = function(abr) {
        abrev = abr;
    },

    setDetalle = function(det) {
        detalle = det;
    },
	
    setNro = function(num) {
        nro = num;
    }; 

    return  {
        
        getAbrev: getAbrev,
        getDetalle: getDetalle,
        getId: getId,
        getNro: getNro,
        setAbrev: setAbrev,
        setDetalle: setDetalle,
        setNro: setNro
    }
}