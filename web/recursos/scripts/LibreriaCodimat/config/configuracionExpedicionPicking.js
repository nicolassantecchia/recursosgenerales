/**
 * REQUIERE configuracionExpedicion.js
 * REQUIERE OperacionesSobreServidorPicking.js
 */

CONFIG.variables.expedicion.picking = {
    estadosParte : {},
    monitorPartes : {},
    tipoPosicion : {}
};

CONFIG.ventanas.expedicion.picking = {
    contenedores : {ancho : 1000, alto : 850},
    posiciones : {ancho : 1000, alto : 850},
    partes : {ancho : 1000, alto : 850}
};

CONFIG.variables.expedicion.picking.milisegundosActualizacionEstadoPartes = 30000;
CONFIG.variables.expedicion.picking.maximaCantidadDiasEstadisticaPartes = 30;


//Pre-inicializacion variables MONITOR PARTES
CONFIG.variables.expedicion.picking.monitorPartes.milisegundosActualizacionPantallaClientes = 5000;

CONFIG.variables.expedicion.picking.monitorPartes.cantidadPaginasAMostrar = 2;
CONFIG.variables.expedicion.picking.monitorPartes.cantidadFilasPorPagina = 12;
CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPaginaPpal = 10000;
CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPaginasSecundarias = 10000;

CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPartesEntregados = 120000;

//Pre-inicializacion de estados
CONFIG.variables.expedicion.picking.estadosParte.estadoPartePendienteDePreparacion =          0;
CONFIG.variables.expedicion.picking.estadosParte.estadoPartePreparandose =                    1;
CONFIG.variables.expedicion.picking.estadosParte.estadoPartePreparadoSinEntregar =            2;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteEnBufferEsperaControl =           3;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteEnControl =                       4;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoACliente =               5;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoAClienteParcialmente =   6;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteControladoEnSalida =              7;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteSuspendido =                      8;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteAnulado =                         9;
CONFIG.variables.expedicion.picking.estadosParte.estadoParteRetrasado =                      10;

CONFIG.variables.expedicion.picking.tipoPosicion.celdaStock = -1;
CONFIG.variables.expedicion.picking.tipoPosicion.celdaPicking = -1;



/**
 * CARGO MEDIANTE AJAX LA CONFIGURACION DESDE EL SERVER
 */
    
var confExpedicionPicking_operacionesPicking = new OperacionesSobreServidorPicking();

confExpedicionPicking_operacionesPicking.obtenerDatosConfiguracion({        
    onSuccess : confExpedicionPicking_handleSuccessObtenerDatosConfiguracion, 
    onFailure : confExpedicionPicking_handleFailureObtenerDatosConfiguracion
});    

function confExpedicionPicking_handleSuccessObtenerDatosConfiguracion(configuracion) {    
    
    CONFIG.variables.expedicion.picking.estadosParte.estadoPartePendienteDePreparacion = configuracion.estadosParte.estadoPartePendienteDePreparacion;
    CONFIG.variables.expedicion.picking.estadosParte.estadoPartePreparandose = configuracion.estadosParte.estadoPartePreparandose;
    CONFIG.variables.expedicion.picking.estadosParte.estadoPartePreparadoSinEntregar = configuracion.estadosParte.estadoPartePreparadoSinEntregar;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteEnBufferEsperaControl = configuracion.estadosParte.estadoParteEnBufferEsperaControl;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteEnControl = configuracion.estadosParte.estadoParteEnControl;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoACliente =  configuracion.estadosParte.estadoParteEntregadoACliente;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoAClienteParcialmente = configuracion.estadosParte.estadoParteEntregadoAClienteParcialmente;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteControladoEnSalida = configuracion.estadosParte.estadoParteControladoEnSalida;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteSuspendido = configuracion.estadosParte.estadoParteSuspendido;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteAnulado = configuracion.estadosParte.estadoParteAnulado;
    CONFIG.variables.expedicion.picking.estadosParte.estadoParteRetrasado =  configuracion.estadosParte.estadoParteRetrasado;  
        
    CONFIG.variables.expedicion.picking.monitorPartes.milisegundosActualizacionPantallaClientes = configuracion.monitorPartes.milisegundosActualizacionPantallaClientes;
    CONFIG.variables.expedicion.picking.monitorPartes.cantidadPaginasAMostrar = configuracion.monitorPartes.cantidadPaginasAMostrar;
    CONFIG.variables.expedicion.picking.monitorPartes.cantidadFilasPorPagina = configuracion.monitorPartes.cantidadFilasPorPagina;
    CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPaginaPpal = configuracion.monitorPartes.milisegundosVisibilidadPaginaPpal;
    CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPaginasSecundarias = configuracion.monitorPartes.milisegundosVisibilidadPaginasSecundarias;
    CONFIG.variables.expedicion.picking.monitorPartes.milisegundosVisibilidadPartesEntregados = configuracion.monitorPartes.milisegundosVisibilidadPartesEntregados;
    
    CONFIG.variables.expedicion.picking.tipoPosicion.celdaStock = configuracion.posiciones.tipoPosicion.celdaStock;
    CONFIG.variables.expedicion.picking.tipoPosicion.celdaPicking = configuracion.posiciones.tipoPosicion.celdaPicking;
}

function confExpedicionPicking_handleFailureObtenerDatosConfiguracion(exception) {
    mostrarModalError(exception.getMensaje());
}

CONFIG.variables.expedicion.picking.admiteEtiquetaControlEntrega = function(idEstado) {
    
    if( idEstado == CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoACliente ||
        idEstado == CONFIG.variables.expedicion.picking.estadosParte.estadoParteEntregadoAClienteParcialmente) {
        return true;
    }
    else {
        return false;
    }
}