CONFIG.variables.compras = {
    
    articuloProveedor : {    
        id : {}, //codigo proveedor
        nombre : {},
        codBarras : {},
        bonifYRec : {},
        bonificaciones : {
            porcent : {}
        },
        observacion : {}
    },
    
    familiaProveedor : {    
        id : {},
        nombre : {}
    },
    
    rubroProveedor : {    
        id : {},
        nombre : {}   
    },
    
    subrubroProveedor : {    
        id : {},
        nombre : {}   
    },
    
    
    vinculacion : {    
        relacionEntreArt : {}
    },
    
    //Items lista de precios
    itemListaDePreciosProveedor : {    
        tipo_actualizacion : {}
    },
    
    tipoListaPrecios : {},
    
    proveedoresEspeciales : {  
    
        ACINDAR : {id : 1450}
    },
    
    mensajes : {},
    cantidadDecimales : 4

};
    
CONFIG.variables.compras.articuloProveedor.id.length = 30; //codigo proveedor
CONFIG.variables.compras.articuloProveedor.nombre.minLength = 2;
CONFIG.variables.compras.articuloProveedor.nombre.maxLength = 80;
CONFIG.variables.compras.articuloProveedor.codBarras.length = 20;
CONFIG.variables.compras.articuloProveedor.bonifYRec.length = 30;
CONFIG.variables.compras.articuloProveedor.observacion.length = 30;
CONFIG.variables.compras.articuloProveedor.observacion.length = 30;

CONFIG.variables.compras.articuloProveedor.bonificaciones.porcent.min = 0.00;
CONFIG.variables.compras.articuloProveedor.bonificaciones.porcent.max = 100.00;
        

CONFIG.variables.compras.articuloProveedor.mensajeErrorIDNoDef = "El c&oacute;digo especificado se encuentra reservado para uso interno del sistema y no pueden realizarse b&uacute;squedas a partir del mismo.</br></br>Si el art&iacute;culo que desea ingresar posee un c&oacute;digo indefinido, deber&aacute; buscarlo por su nombre.";
CONFIG.variables.compras.articuloProveedor.noDefinido = "INDEF";
CONFIG.variables.compras.articuloProveedor.noDefinidoBD = '-1';

CONFIG.variables.compras.familiaProveedor.id.length = 10;
CONFIG.variables.compras.familiaProveedor.nombre.length = 40;

CONFIG.variables.compras.rubroProveedor.id.length = 10;
CONFIG.variables.compras.rubroProveedor.nombre.length = 40;

CONFIG.variables.compras.subrubroProveedor.id.length = 10;
CONFIG.variables.compras.subrubroProveedor.nombre.length = 40;

CONFIG.variables.compras.vinculacion.relacionEntreArt.parteEntera = 8;
CONFIG.variables.compras.vinculacion.relacionEntreArt.decimales = 8;

CONFIG.variables.compras.itemListaDePreciosProveedor.tipo_actualizacion.manual = 0;
CONFIG.variables.compras.itemListaDePreciosProveedor.tipo_actualizacion.automatica = 1;

CONFIG.variables.compras.tipoListaPrecios.vacia = -1;
CONFIG.variables.compras.tipoListaPrecios.anotacion = 0;
CONFIG.variables.compras.tipoListaPrecios.documento = 1;
CONFIG.variables.compras.tipoListaPrecios.longitudAnotacion = 60;

CONFIG.variables.compras.mensajes.condicionIVADesconocida = "Condición de IVA desconocida. Comuníquese con el Área Compras para solicitar la actualización de la condición fiscal del proveedor de acuerdo al padrón de AFIP."

/*
console.log("1- "+CONFIG.variables.compras.cantidadDecimales);
console.log("2- "+CONFIG.cantidadDecimales.compras);*/

/*
console.log("1- "+CONFIG.variables.compras.proveedoresEspeciales.ACINDAR.id);
console.log("2- "+CONFIG.proveedoresEspeciales.ACINDAR.id);*/

/*
console.log("1- "+CONFIG.variables.compras.tipoListaPrecios.vacia);
console.log("2- "+CONFIG.tipoListaPrecios.vacia);
console.log("3- "+CONFIG.variables.compras.tipoListaPrecios.anotacion);
console.log("4- "+CONFIG.tipoListaPrecios.anotacion);
console.log("5- "+CONFIG.variables.compras.tipoListaPrecios.documento);
console.log("6- "+CONFIG.tipoListaPrecios.documento);
console.log("7- "+CONFIG.variables.compras.tipoListaPrecios.longitudAnotacion);
console.log("8- "+CONFIG.tipoListaPrecios.longitudAnotacion);*/

/*
console.log("1- "+CONFIG.variables.compras.itemListaDePreciosProveedor.tipo_actualizacion.manual);
console.log("2- "+CONFIG.itemListaDePreciosProveedor.tipo_actualizacion.manual);
console.log("3- "+CONFIG.variables.compras.itemListaDePreciosProveedor.tipo_actualizacion.automatica);
console.log("4- "+CONFIG.itemListaDePreciosProveedor.tipo_actualizacion.automatica);*/

/*
console.log("1- "+CONFIG.variables.compras.vinculacion.relacionEntreArt.parteEntera);
console.log("2- "+CONFIG.vinculacion.relacionEntreArt.parteEntera);
console.log("3- "+CONFIG.variables.compras.vinculacion.relacionEntreArt.decimales);
console.log("4- "+CONFIG.vinculacion.relacionEntreArt.decimales);
*/
/*
console.log("1- "+CONFIG.variables.compras.subrubroProveedor.id.length);
console.log("2- "+CONFIG.subrubroProveedor.id.length);
console.log("3- "+CONFIG.variables.compras.subrubroProveedor.nombre.length);
console.log("4- "+CONFIG.subrubroProveedor.nombre.length);*/

/*
console.log("1- "+CONFIG.variables.compras.rubroProveedor.id.length);
//console.log("2- "+CONFIG.rubroProveedor.id.length);
console.log("3- "+CONFIG.variables.compras.rubroProveedor.nombre.length);
//console.log("4- "+CONFIG.rubroProveedor.nombre.length);
*/
/*
console.log("1- "+CONFIG.variables.compras.familiaProveedor.id.length);
//console.log("2- "+CONFIG.familiaProveedor.id.length);
console.log("3- "+CONFIG.variables.compras.familiaProveedor.nombre.length);
//console.log("4- "+CONFIG.familiaProveedor.nombre.length);
*/

/*

console.log("1- "+CONFIG.variables.compras.articuloProveedor.id.length); //codigo proveedor
//console.log("1- "+CONFIG.articuloProveedor.id.length);
console.log("2- "+CONFIG.variables.compras.articuloProveedor.nombre.minLength);
//console.log("2- "+CONFIG.articuloProveedor.nombre.minLength);
console.log("3- "+CONFIG.variables.compras.articuloProveedor.nombre.maxLength);
//console.log("3- "+CONFIG.articuloProveedor.nombre.maxLength);
console.log("4- "+CONFIG.variables.compras.articuloProveedor.codBarras.length);
//console.log("4- "+CONFIG.articuloProveedor.codBarras.length);
console.log("5- "+CONFIG.variables.compras.articuloProveedor.bonifYRec.length);
//console.log("5- "+CONFIG.articuloProveedor.bonifYRec.length);
console.log("6- "+CONFIG.variables.compras.articuloProveedor.observacion.length);
//console.log("6- "+CONFIG.articuloProveedor.observacion.length);

console.log("7- "+CONFIG.variables.compras.articuloProveedor.mensajeErrorIDNoDef);
//console.log("7- "+CONFIG.articuloProveedor.mensajeErrorIDNoDef);
console.log("8- "+CONFIG.variables.compras.articuloProveedor.noDefinido);
//console.log("8- "+CONFIG.articuloProveedor.noDefinido);
console.log("9- "+CONFIG.variables.compras.articuloProveedor.noDefinidoBD);
//console.log("9- "+CONFIG.articuloProveedor.noDefinidoBD);
*/

