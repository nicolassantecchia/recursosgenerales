
/*
 ***************************************
	VARIABLES DE CONFIGURACION
 ***************************************	
*/

var CONFIG = {
    
    servidores : {
        
        tomcat : "http://"+IP_SERVER+":"+PUERTO_TOMCAT,
        apache : "http://"+IP_SERVER+":80",

        //tomcat6 : "http://192.168.0.6:"+PUERTO_TOMCAT, //Utilizada como parche para los proyectos que corren en .12 y deben referenciar visucomp en .6 
        tomcatIntraServer : "http://192.168.0.5"
    },
    
    variables : {},

    dateTimePicker :{
                        fecha :     {
                                        min : '01/01/2014',	// MM/DD/YYYY	
                                        formatoInterno : 'MM/DD/YYYY',
                                        formatoServer : 'YYYY-MM-DD',
                                        formatoVisible : 'DD-MM-YYYY'
                                    }
                    },

    selects :   {
                    valueAgregar : 'agregar', //valor utilizado en los selects para incluir una opcion que permite agregar nuevas opciones en los selects. Ejemplo: "Agregar Familia".
                    valueNulo : '@',  //valor nulo utilizado en selects para incluir una opcion que ejecute una determinada accion. Ejemplo "No existen Familias".
                    frmNoDefinido : "0"  //valor no definido para id de Familias, Rubros y Subrubros.
                },

    ventanas :  {
                    compras :   {
                                    altaArticulo : {ancho : 1000, alto : 700},
                                    vinculacion : {ancho : 1000, alto : 850}
                                },

                    pdf : 	{
                                portrait : {ancho : 825, alto : 700},
                                landscape : {ancho : 1150, alto : 800}
                            }
                },

    ctrlInput : {},

    //separadores : {decimal : '.', miles: ','},
    //tipoError : {},

    comprobante : {

        numero : { longPrefijo : 4, longSufijo : 8},
        estado : { 
                    oc : { 
                        pendienteRecepcion : 1,
                        anulado : 0
                    }
                }
    },

    tiposComprobante : {  
        
        codigo : {
            oc : 19, //Orden de compra
            rp : 20, //Remito Proveedor
            fe : 21, //Faltante
            so : 22, //Sobrante
            fp : 23  //Factura Proveedor
        },
        
        letra : {oc : "X"},
    }
};

CONFIG.proyectos = {
    cocoon : CONFIG.servidores.tomcat + '/cocoon2.2/plantillas',
    //cocoon6 : 'http://192.168.0.6:8080/cocoon2.2/plantillas', //Utilizada como parche para los proyectos que corren en .12 y debe referenciar visucomp en .6 
    generadorPDFOnline : CONFIG.servidores.tomcat + '/generadorPDF/',
    filemanager : CONFIG.servidores.apache + '/FileManager'
};

CONFIG.recursosExternos = {
    recursosIntraServer : CONFIG.servidores.tomcatIntraServer + '/recursos'
};