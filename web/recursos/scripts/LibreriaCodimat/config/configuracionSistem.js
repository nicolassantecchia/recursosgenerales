CONFIG.variables.sistem = {
    
    //Tipos de Error
    tipoError : {},
    
    sesion : {  
        
        error : {},                 //Subtipos de errores de sesion 
        tipoMetodo : {},            //Comunicacion con el jsp de login (loginUsuario.jsp)
        metodoAutenticacion : {},   //parametros que se pasan a todos los jsp. usuario + ip        
        parametros :  {}  
    },
    
    separador : {},
    
    dispositivosDeEntrada : {
        tarjetaMagnetica : {},
        pickeadora : {},
        scanner : {}
    },     
    
    generadorPDFOnline : {}
};

CONFIG.variables.sistem.generadorPDFOnline.separadorGeneral = "__";
CONFIG.variables.sistem.generadorPDFOnline.separadorParametros = "_";
CONFIG.variables.sistem.generadorPDFOnline.separadorParametroNombreValor = "=";
CONFIG.variables.sistem.generadorPDFOnline.nombreParametroXML = "XML";
CONFIG.variables.sistem.generadorPDFOnline.nombreParametroXSL = "XSL";
CONFIG.variables.sistem.generadorPDFOnline.nombreParametroNombrePDF = "NOMBRE";

CONFIG.variables.sistem.tipoError.sesion = 2;

CONFIG.variables.sistem.separador.decimal = ".";
CONFIG.variables.sistem.separador.miles = ",";

CONFIG.variables.sistem.sesion.error.excepcion = 5;
CONFIG.variables.sistem.sesion.error.excepcionMensaje = "Ha ocurrido error. Comun&iacute;quese con el &Aacute;rea Sistemas.";

CONFIG.variables.sistem.sesion.error.inactiva = 0;
CONFIG.variables.sistem.sesion.error.inactivaMensaje = "Debe loguearse.";

//caso en que la sesion esta inactiva por que fue cerrada desde otra pestaña o caducó en otra pestaña
CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada = 4;
CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerradaMensaje = "Usted ha cerrado sesi&oacute;n desde otra ventana o bien la sesi&oacute;n ha caducado. Debe loguearse para continuar.";

CONFIG.variables.sistem.sesion.error.caducada = 2;
CONFIG.variables.sistem.sesion.error.caducadaMensaje = "La sesi&oacute;n ha caducado, debe loguearse nuevamente.";

CONFIG.variables.sistem.sesion.error.permiso = 3;
CONFIG.variables.sistem.sesion.error.permisoMensaje = "Usted no cuenta con los permisos necesarios para acceder a esta funcionalidad.";
                                
CONFIG.variables.sistem.sesion.tipoMetodo.login = 0;
CONFIG.variables.sistem.sesion.tipoMetodo.logout = 1;
CONFIG.variables.sistem.sesion.tipoMetodo.sesionActiva = 2;
CONFIG.variables.sistem.sesion.tipoMetodo.sesionActivaYPermiso = 3;
CONFIG.variables.sistem.sesion.tipoMetodo.autenticar = 4;
CONFIG.variables.sistem.sesion.tipoMetodo.autenticarYPermiso = 5;

CONFIG.variables.sistem.sesion.metodoAutenticacion.userPassword = 0;
CONFIG.variables.sistem.sesion.metodoAutenticacion.tarjetaMagnetica = 1;

CONFIG.variables.sistem.sesion.metodoAutenticacion.mensajeTarjetaInvalida = "Credencial inv&aacute;lida.";
                    
//Solo a nivel de vistas                    
CONFIG.variables.sistem.sesion.parametros.usuario = 'usuario';
CONFIG.variables.sistem.sesion.parametros.ip = 'ip';

CONFIG.variables.sistem.dispositivosDeEntrada.tarjetaMagnetica.millisEspera = 60;
CONFIG.variables.sistem.dispositivosDeEntrada.scanner.millisEspera = 60;

/*
console.log("0- "+CONFIG.variables.sistem.tipoError.sesion); //Variable ahora 
//console.log("0- "+CONFIG.tipoError.sesion);//variable antes

console.log("1- "+CONFIG.variables.sistem.sesion.error.excepcion);
//console.log("1- "+CONFIG.sesion.error.excepcion);
console.log("2- "+CONFIG.variables.sistem.sesion.error.excepcionMensaje);
//console.log("2- "+CONFIG.sesion.error.excepcionMensaje);

console.log("3- "+CONFIG.variables.sistem.sesion.error.inactiva);
//console.log("3- "+CONFIG.sesion.error.inactiva);
console.log("4- "+CONFIG.variables.sistem.sesion.error.inactivaMensaje);
//console.log("4- "+CONFIG.sesion.error.inactivaMensaje);

//caso en que la sesion esta inactiva por que fue cerrada desde otra pestaña o caducó en otra pestaña
console.log("5- "+CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada);
//console.log("5- "+CONFIG.sesion.error.inactivaCaducadaCerrada);
console.log("6- "+CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerradaMensaje);
//console.log("6- "+CONFIG.sesion.error.inactivaCaducadaCerradaMensaje);

console.log("7- "+CONFIG.variables.sistem.sesion.error.caducada);
//console.log("7- "+CONFIG.sesion.error.caducada);
console.log("8- "+CONFIG.variables.sistem.sesion.error.caducadaMensaje);
//console.log("8- "+CONFIG.sesion.error.caducadaMensaje);

console.log("9- "+CONFIG.variables.sistem.sesion.error.permiso);
//console.log("9- "+CONFIG.sesion.error.permiso);
console.log("10- "+CONFIG.variables.sistem.sesion.error.permisoMensaje);
//console.log("10- "+CONFIG.sesion.error.permisoMensaje);
                                
console.log("11- "+CONFIG.variables.sistem.sesion.tipoMetodo.login);
//console.log("11- "+CONFIG.sesion.tipoMetodo.login);
console.log("12- "+CONFIG.variables.sistem.sesion.tipoMetodo.logout);
//console.log("12- "+CONFIG.sesion.tipoMetodo.logout);
console.log("13- "+CONFIG.variables.sistem.sesion.tipoMetodo.sesionActiva);
//console.log("13- "+CONFIG.sesion.tipoMetodo.sesionActiva);
console.log("14- "+CONFIG.variables.sistem.sesion.tipoMetodo.sesionActivaYPermiso);
//console.log("14- "+CONFIG.sesion.tipoMetodo.sesionActivaYPermiso);

console.log("15- "+CONFIG.variables.sistem.sesion.metodoAutenticacion.userPassword);
//console.log("15- "+CONFIG.sesion.metodoAutenticacion.userPassword);
console.log("16- "+CONFIG.variables.sistem.sesion.metodoAutenticacion.tarjetaMagnetica);
//console.log("16- "+CONFIG.sesion.metodoAutenticacion.tarjetaMagnetica);
                    
//Solo a nivel de vistas                    
console.log("17- "+CONFIG.variables.sistem.sesion.parametros.usuario);
//console.log("17- "+CONFIG.sesion.parametros.usuario);
console.log("18- "+CONFIG.variables.sistem.sesion.parametros.ip);
//console.log("18- "+CONFIG.sesion.parametros.ip);

//En config general
console.log("19- "+CONFIG.variables.sistem.separador.decimal);
//console.log("19- "+CONFIG.separadores.decimal);
console.log("20- "+CONFIG.variables.sistem.separador.miles);
//console.log("20- "+CONFIG.separadores.miles);
*/