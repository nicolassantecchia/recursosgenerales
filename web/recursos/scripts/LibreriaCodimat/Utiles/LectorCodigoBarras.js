/**
 * handler : function que será llamada una vez que se haya efectuado una lectura.
 */
var LectorCodigoBarras = function(handler, milisegundos) {    
    
    var bufferLecturaScanner = "",
        efectuarLectura = true,
        tiempoPresionAnterior = 0,
        tiempoPresionActual = 0,
        primeraPresion = true;

        var milisegundosEspera = CONFIG.variables.sistem.dispositivosDeEntrada.tarjetaMagnetica.millisEspera;  
    
        if(milisegundos != null && milisegundos != "undefined") {
            milisegundosEspera = milisegundos;  
        }
    
        $('body').on('keypress', function(event) {

            if(efectuarLectura) {

                var charcode = obtenerKeyCode(event); 
                var caracterActual = String.fromCharCode(charcode); 

                tiempoPresionActual = new Date().getTime();

                if(primeraPresion) {
                    tiempoPresionAnterior = tiempoPresionActual;
                    primeraPresion = false;
                }

                //Si el tiempo entre presiones de teclado es <= a milisegundosEspera -> sigo procesando
                if(tiempoPresionActual - tiempoPresionAnterior <= milisegundosEspera) {

                    tiempoPresionAnterior = tiempoPresionActual;                

                    if(charcode == 13 && bufferLecturaScanner != "") { //fin de linea    

                        handler(bufferLecturaScanner);
                        inicializarVariables();                    
                    }
                    else {                        
                        bufferLecturaScanner = bufferLecturaScanner + caracterActual;
                    }
                }
                else { //tiempo agotado, reinicializo variables
                    inicializarVariables();
                }
            }        
        });
    
    var inicializarVariables = function () {
    
            tiempoPresionAnterior = 0;
            tiempoPresionActual = 0;
            bufferLecturaScanner = "";
            primeraPresion = true;
        },
                
        setHabilitarLectura = function(permitirLectura) {            
            efectuarLectura = permitirLectura;
        };
    
    return {
        setHabilitarLectura : setHabilitarLectura
    };
};