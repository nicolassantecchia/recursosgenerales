/*
 * Fernando Prieto 21/12/2015
 */

var TarjetaMagnetica = function(track1, track2, track3) {
     
    var t1 = track1,
        t2 = track2,
        t3 = track3,
        
        getTrack1 = function() {
            return t1;
        },
        
        getTrack2 = function() {
            return t2;
        },
        
        getTrack3 = function() {
            return t3;
        };
        
    return {
        getTrack1 : getTrack1,
        getTrack2 : getTrack2,
        getTrack3 : getTrack3
    };              
};
 
 /**
  * milisegundos : cantidad de milisegundos aceptables entre presion de teclas.
  * handler : function que será llamada una vez que la TarjetaMagnetica haya sido formada o que se haya acabado el tiempo de espera.
  */
 var TarjetaMagneticaListener = function(handler, milisegundos) { 
         
    var milisegundosEspera = CONFIG.variables.sistem.dispositivosDeEntrada.tarjetaMagnetica.millisEspera;  
    
    if(milisegundos != null && milisegundos != "undefined") {
        milisegundosEspera = milisegundos;  
    }
    
    var bufferLectura;
    
    var lectura = {track1 : "", track2 : "", track3 : ""};
    
    var track;
    var timer;
    
    var ignorarPresiones = false;
    
    var primeraPresion;
    var ultimaPresionFueEnter;
    
    var tiempoPresionAnterior;
    var tiempoPresionActual;
    
    var inicializarVariablesGlobales = function() {
       //console.log("Inicializando.............");
       
        //Reinicializo para que se pueda leer una próxima tarjeta                
        track = 0;
        timer = null;  
        primeraPresion = true;
        ultimaPresionFueEnter = false;
        tiempoPresionAnterior = 0;
        tiempoPresionActual = 0;
        bufferLectura = "";
        lectura.track1 = "";
        lectura.track2 = "";
        lectura.track3 = "";
    };
    
     inicializarVariablesGlobales();
     
    $('body').on('keypress', function(event) {
        
        if(!ignorarPresiones) {
        
            var charcode = obtenerKeyCode(event); 
            var caracterActual = String.fromCharCode(charcode);   

            if(track < 3) {
//console.log("track ");
                if(timer != null) {
                    clearTimeout(timer); //Cancelo el timer
                }
                
                tiempoPresionActual = new Date().getTime();
                
                if(primeraPresion) {
                    //console.log("PRIMERA PRESION ");
                    tiempoPresionAnterior = tiempoPresionActual;
                    primeraPresion = false;
                }
                
                
                //Si el tiempo entre presiones de teclado es <= a milisegundosEspera -> sigo procesando                
                if(tiempoPresionActual - tiempoPresionAnterior <= milisegundosEspera) {
                    
                    tiempoPresionAnterior = tiempoPresionActual;                

                    if(charcode == 13) { //fin de linea 

                        if(track == 0) {
                            lectura.track1 = bufferLectura;
                            bufferLectura = "";
                            track = track + 1;
                        }
                        else {

                            if(track == 1) {
                                lectura.track2 = bufferLectura;
                                bufferLectura = "";
                                track = track + 1;
                            }
                            else {

                                if(track == 2) {
                                    lectura.track3 = bufferLectura;
                                    bufferLectura = "";
                                    track = track + 1;                            
                                }
                            }
                        }
                        
                        if(lectura.track1.length > 0 || lectura.track2.length > 0 || lectura.track3.length > 0) {
                            timer = setTimeout(validarTracks, milisegundosEspera);//Tiempo entre presiones de tecla
                        }
                    }
                    else {                        
                        bufferLectura = bufferLectura + caracterActual;
                    }
                }
                else { //tiempo agotado, reinicializo variables
                    inicializarVariablesGlobales();
                }
            }
        }
    });
    
    var validarTracks = function() {
        //console.log("############# SE DISPARÓ ############");
        
        if(timer != null){
            clearTimeout(timer);
        }

        var tarjetaValida = true;

        /*
         * Si track vacio -> es válido
         * Si track !vacio y tiene mas de dos caracteres -> se asume que el primero y el útimo son centinelas.
         */
        if(lectura.track1 == "" || lectura.track1.length > 2) {//Hay algo en el track y además está bien formado 

            if(lectura.track1.length > 2) {
               lectura.track1 = lectura.track1.substr(1, lectura.track1.length - 2); //Le quito los dos caracteres centinela 
            }            
        }
        else {
            tarjetaValida = false;
        }

        if(lectura.track2 == "" || lectura.track2.length > 2) {            
            if(lectura.track2.length > 2) {
               lectura.track2 = lectura.track2.substr(1, lectura.track2.length - 2); //Le quito los dos caracteres centinela 
            } 
        }
        else {
            tarjetaValida = false;
        }

        if(lectura.track3 == "" || lectura.track3.length > 2) {  

            if(lectura.track3.length > 2) {
               lectura.track3 = lectura.track3.substr(1, lectura.track3.length - 2); //Le quito los dos caracteres centinela 
            } 
        }
        else {
            tarjetaValida = false;
        }        

        if(tarjetaValida) {
            handler(new TarjetaMagnetica(lectura.track1, lectura.track2, lectura.track3));
        }
        else {
            handler(null);
        }
        
        inicializarVariablesGlobales();
    };
    
    var setIgnorarKeyPressed = function(ignorar) {
            
            ignorarPresiones = ignorar;
        },
                
        getignorarKeyPressed = function() {
            return ignorarPresiones;
        };
        
    return {
        setIgnorarKeyPressed : setIgnorarKeyPressed,
        getignorarKeyPressed : getignorarKeyPressed
    }
}