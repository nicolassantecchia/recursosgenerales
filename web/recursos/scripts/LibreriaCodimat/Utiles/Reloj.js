var Reloj = function(handler, intervaloActualizacion) {
    
    var timeZoneBuenosAires = CONFIG.variables.recursosGenerales.timeZoneBSAS; //-3h = -180min
    
    var horaActual = null;
    
    var intervalFunction = null;
    var intervalo = 1000; // 1 segundo    
    
    if(intervaloActualizacion != null && intervaloActualizacion != 'undefined') {
        intervalo = intervaloActualizacion;
    }
    
    var getDiferenciaMillisRespectoTimeZoneDeBSAS = function(objetoDate) {
            
            var timeZoneAsociado = objetoDate.getTimezoneOffset();
            
            return (timeZoneAsociado - timeZoneBuenosAires) * 60 * 1000;
        },
    
    iniciar = function() {
        
        if(intervalFunction == null) {
            intervalFunction =  setInterval(function() {
        
                var date = new Date();
                date.setTime(date.getTime() + getDiferenciaMillisRespectoTimeZoneDeBSAS(date));
                
                horaActual = date.toTimeString();
                // horaActual is "20:32:01 GMT+0530 (India Standard Time)"
                // Split with ' ' and we get: ["20:32:01", "GMT+0530", "(India", "Standard", "Time)"]
                // Take the first value from array :)
                horaActual = horaActual.split(' ')[0];
                
                handler();

            }, intervalo);
        }
        
    },
            
    detener = function() {
        clearInterval(intervalFunction);
        intervalFunction = null;
    },
            
    getHoraMinutosSegundos = function() {
        
        if(horaActual != null) {
            return horaActual;
        }
        else {
            return "--:--:--";
        }
    },
            
    getHoraMinutos = function() {
        if(horaActual != null) {
            return getHoraMinutosSegundos().substr(0,5);
        }
        else {
            return "--:--";
        }
    };
    
    return {
        iniciar : iniciar,
        detener : detener,
        getHoraMinutos : getHoraMinutos,
        getHoraMinutosSegundos : getHoraMinutosSegundos
    }
}