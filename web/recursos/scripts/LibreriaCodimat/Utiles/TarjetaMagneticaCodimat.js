/* 
 * Fernando Prieto 21/12/2015
 */

var TarjetaMagneticaCodimat = function(track1, track2, track3) { 
    
    var nroTarjeta;      
    
    if(/^\d+$/.test(track1)){ //verifico que el track1 tenga un numero entero -> nroTarjeta
        nroTarjeta = parseInt(track1);
    }
    else {
   //     console.log("La tarjeta se encuentra mal formada.");
        throw CONFIG.variables.sistem.sesion.metodoAutenticacion.mensajeTarjetaInvalida;
    }

    var tarjeta = Object.create(new TarjetaMagnetica(track1, track2, track3));
    
    tarjeta.getNroTarjeta = function() {
        return nroTarjeta;
    }
    
    return tarjeta;
}