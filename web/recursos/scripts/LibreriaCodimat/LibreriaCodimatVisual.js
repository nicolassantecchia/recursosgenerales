/*!
 * ======================================================================================================================
 * Archivo: 		LibreriaCodimatVisual.js
 * Fecha: 			06/01/2014
 * Autor: 			Fernando Prieto
 * Editado por: 	-
 *
 * Descripcion: 	Provee funciones generales para la creacioón de elementos en el DOM, su correcta visualización y
 					funciones de validación.
 * ======================================================================================================================
 */

var loadingBar = new Array();  // usado para guardar el ancho y alto de la loading bar para poder centrarla en pantalla
var loadingBarFunction;
var clase_error_input = 'has-error-input';

$('document').ready(function() {

	crearModalMensajes();

	$(window).resize(function(){
		centrarModal('modal-utils-error');
                centrarModal('modal-utils-advertencia');
	}); 
});


var LibreriaCodimatVisual = function() {
    
    var crearPopoverInformacion = function(container, titulo, content, placement, esParaModal) {

            $('#'+container).popover({
                    container: 'body',
                    animation: false,
                    title: '<div ><div style="text-align:left; display:inline-block;"><span>' + titulo + '</span></div><div style="float:right"><a id="popover-close-'+container+'" rel="popover-close-link" data-toggle="#" class="popover-close">&times;</a></div></div>',
                    content: "<div class='container' style='width: 210px;'><div style='padding-left:0px; padding-right:0px;'>"+content+"</div></div>",
                    placement: placement,
                    trigger: "manual",
                    html: true
            });

            $('#'+container).popover('show');  
/*
            if(esParaModal) {
                    $('#popover-body-'+container).closest('.popover').addClass('popover-for-modal');		
            }

            //Calculo para el centrado del icono de error / warning

            var valor = $('#popover-body-'+container).actual('height') - $("#popover-icon-"+container+"-img").actual('height');

            if(valor > 0 ) {
                    $('#popover-icon-'+container).css("paddingTop", valor/2);
            }
            else {
                    $('#popover-icon-'+container).css("paddingTop", 0);
            }*/

            $('#popover-close-'+container).click(function(event) {
                    event.preventDefault(); 
                    eliminarPopoverWarningError(container);	
            });
    }
    
    return {
        crearLoadingBarFullScreen : crearLoadingBarFullScreen,
        cambiarTextoLoadingBarFullScreen : cambiarTextoLoadingBarFullScreen,
        mostrarLoadingBarFullScreen : mostrarLoadingBarFullScreen,
        ocultarLoadingBarFullScreen :ocultarLoadingBarFullScreen,
        centrarLoadingBarFullScreen : centrarLoadingBarFullScreen,
        habilitarSelect : habilitarSelect,
        inicializacionModals : inicializacionModals,
        crearModalMensajes : crearModalMensajes,
        mostrarModalError : mostrarModalError,
        mostrarModalAdvertencia : mostrarModalAdvertencia,
        centrarModal : centrarModal,
        startLoadingText : startLoadingText,
        stopLoadingText : stopLoadingText,
        crearPopoverWarningError : crearPopoverWarningError,
        crearPopoverInformacion : crearPopoverInformacion,
        eliminarPopoverWarningError : eliminarPopoverWarningError,
        crearErrorModal : crearErrorModal,
        ocultarError : ocultarError,
        ocultarErrores : ocultarErrores,
        mostrarLoadingBarAjax : mostrarLoadingBarAjax,
        ocultarLoadingBarAjax : ocultarLoadingBarAjax
    }
}




/*
 * ----------------------------------------------------------------------------------------------------------------------
 * LOADING BAR
 * ----------------------------------------------------------------------------------------------------------------------
 */

/*
 *	Genera una Loading Bar en pantalla completa.
 *	Parámetros: {container: contentedor, type: tipo-barra-bootstrap, text: texto-barra}
 * 	Ejemplo invocación: crearLoadingBarFullScreen({container: 'modalsAndBar', type: 'info', text: 'Loading...'});
 */
function crearLoadingBarFullScreen(arguments) {
	var tipo = "";

	if(arguments.type) {
		tipo = 'progress-bar-'+arguments.type;
	}

	var code =	'<div id="contenedor-loadingBar">'+
					'<div id="full-screen-bar-body" class="progress progress-striped active loadingBar">'+
						'<div class="progress-bar '+tipo+'" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
							'<span id="loadingText-fullScreenBar" class="loading-bar-text">'+arguments.text+'</span>'+
						'</div>'+
						
					'</div>'+'<div id="backdrop" class="modal-backdrop fade in"></div>'+
				'</div>';

	$(arguments.container).append(code);
	
	loadingBar['width'] = $('#full-screen-bar-body').css('width');
	loadingBar['height'] = $('#full-screen-bar-body').css('height');
	
	loadingBarTextFunction = startLoadingText('fullScreenBar');
	ocultarLoadingBarFullScreen();
}

function cambiarTextoLoadingBarFullScreen(text) {
	$('#loadingText-fullScreenBar').html(text);
}

function mostrarLoadingBarFullScreen(){
	centrarLoadingBarFullScreen();
	loadingBarTextFunction = startLoadingText('fullScreenBar');
	$('#contenedor-loadingBar').show();
}

function ocultarLoadingBarFullScreen() {
	$('#contenedor-loadingBar').hide();
	stopLoadingText(loadingBarTextFunction, 'fullScreenBar');
}

function centrarLoadingBarFullScreen(){
	var widthBody  = $('body').css('width').split("px"); 
	widthBody = parseFloat(widthBody[0]);
	
	var width  = loadingBar['width'].split("px"); 
	width = parseFloat(width[0]);
	
	var left = (widthBody - width) / 2;
	
	$('#full-screen-bar-body').css('left',left);
	
	var height = loadingBar['height'].split("px");
	height = parseFloat(height[0]);

	$('#full-screen-bar-body').css('top',(window.innerHeight - height) / 2);
}


/*
 * ----------------------------------------------------------------------------------------------------------------------
 * SELECTS
 * ----------------------------------------------------------------------------------------------------------------------
 */

/* 
 *	Habilita o deshabilita el select con id == idSelect 
 */

function habilitarSelect(idSelect, habilitar) {

	//if(browserIE8) {//	es Internet Explorer 8 - variable seteada en header.jsp

		$('#'+idSelect).attr('disabled', !habilitar);
	/*}
	else {
		var current_hab = $('#'+idSelect).prop('disabled');
	 	var hab = true;

	 	if(habilitar) {
	 		hab = false;
	 	}

	 	if(hab != current_hab)
	 	{
	 		$('#'+idSelect).prop('disabled', hab);
			$('#'+idSelect).selectpicker('refresh'); 
	 	}
	}*/
 }

 /*
 * ----------------------------------------------------------------------------------------------------------------------
 * MODALS
 * ----------------------------------------------------------------------------------------------------------------------
 */

 function inicializacionModals() {

	$('[rel="modals"], .modal').modal({
            show: false,
            backdrop: 'static',
            keyboard: false
	});

	$('[rel="modals"], .modal').on('hidden.bs.modal', function (e) {
            $('[rel="modals-inputs"], .modals-inputs').val("");
	});
}

function crearModalMensajes() {

	var modal = 
			'<div id="modal-utils-error" data-tipo="error" class="modal fade" rel="modals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
				'<div class="modal-dialog modal-dialog-error">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<div class="modal-title-label">'+
								'<label class="modal-title">ERROR</label>'+
							'</div>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div id="alert-error-error" class="alert alert-modal alert-danger" style="margin-top: 0px;"></div>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<button id="modal-utils-cancel-error" type="button" class="btn btn-touch btn-default" data-dismiss="modal">Aceptar</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';

	$('#div-marco-body').append(modal);
        
        modal = 
			'<div id="modal-utils-advertencia" data-tipo="advertencia" class="modal fade" rel="modals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
				'<div class="modal-dialog modal-dialog-error">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<div class="modal-title-label">'+
								'<label class="modal-title">ATENCI&Oacute;N!</label>'+
							'</div>'+
						'</div>'+
						'<div class="modal-body">'+
							'<div id="alert-utils-advertencia" class="alert alert-modal alert-warning" style="margin-top: 0px;"></div>'+
						'</div>'+
						'<div class="modal-footer">'+
							'<button id="modal-utils-cancel-advertencia" type="button" class="btn btn-touch btn-default" data-dismiss="modal">Aceptar</button>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';

	$('#div-marco-body').append(modal);

	$("#modal-utils-error, #modal-utils-advertencia").modal({
		show: false,
		backdrop: 'static',
		keyboard: true
	});
	
	$('#modal-utils-error, #modal-utils-advertencia').keydown(function(event) {
	
		var charCode = obtenerKeyCode(event);
		
		if(charCode == 13){ //enter
			event.preventDefault();
			$('#modal-utils-cancel-'+$(this).data('tipo')).click();
		}
		else {
			if(charCode == 9 && $('#modal-utils-cancel-'+$(this).data('tipo')).is(':focus')) {
				event.preventDefault();
			}
		}	
	});
}

function mostrarModalError(mensaje) {

	if(mensaje != "") {
		$('#alert-error-error').html('<strong>ERROR!</strong> '+mensaje);
	}

	centrarModal('modal-utils-error');

	$('#modal-utils-error').modal('show');
}

function mostrarModalAdvertencia(mensaje) {

	if(mensaje != "") {
		$('#alert-utils-advertencia').html('<strong>ATENCI&Oacute;N!</strong> '+mensaje);
	}

	centrarModal('modal-utils-advertencia');

	$('#modal-utils-advertencia').modal('show');
}

function centrarModal(idModal) {

	heightModal = $('#'+idModal+' > .modal-dialog').actual( 'height'); 
	var valor = $(window).innerHeight() - heightModal;
	if(valor > 0 ) {
		$('#'+idModal+' > .modal-dialog').css("marginTop",(valor) / 2);
	}
	else {
		$('#'+idModal+' > .modal-dialog').css("marginTop", 0);
	}
}

/*
 * ----------------------------------------------------------------------------------------------------------------------
 * FUNCIONES VARIAS
 * ----------------------------------------------------------------------------------------------------------------------
 */

function startLoadingText(idBar) {

	var originalText = $('#loadingText-'+idBar).html();
	$('#loadingText-'+idBar).html(originalText + ' ');

    var i = 0;
	var funct = setInterval(function() {
		    $('#loadingText-'+idBar).html($('#loadingText-'+idBar).html() + '.');
		    i++;
		    if(i == 4){
		        $('#loadingText-'+idBar).html(originalText + ' ');
		        i = 0;
		    }

		}, 400);

	var respuesta = new Array();
	respuesta['funcion'] = funct;
	respuesta['texto'] = originalText; 

	return respuesta;

}

function stopLoadingText(funct, idBar) {
    
    if(funct != null) {
	clearInterval(funct['funcion']);
	$('#loadingText-'+idBar).html(funct['texto']);
    }
}

/*
 * ----------------------------------------------------------------------------------------------------------------------
 * ERRORES
 * ----------------------------------------------------------------------------------------------------------------------
 */

function crearPopoverWarningError(container, icon, content, placement, esParaModal) {

	$('#'+container).popover({
		container: 'body',
		animation: false,
		title: '<div class="popover-title-content" ><a id="popover-close-'+container+'" rel="popover-close-link" data-toggle="#" class="popover-close">&times;</a></div>',
		content: "<div class='container' style='width: 210px;'><div class='row'>"+
					"<div id='popover-icon-"+container+"' class='col-md-1' style='padding-left:0px; padding-right:0px;'>"+
						"<div id='popover-icon-"+container+"-img' class='icon icon-"+icon+"'/>"+
					"</div>"+
					"<div id='popover-body-"+container+"' class='col-md-11' style='padding-left:0px; padding-right:0px;'>"+content+"</div>"+
					"<div class='clear-both'></div></div></div>",
		placement: placement,
		trigger: "manual",
		html: true
	});

	$('#'+container).popover('show');  

	if(esParaModal) {
		$('#popover-body-'+container).closest('.popover').addClass('popover-for-modal');		
	}

	//Calculo para el centrado del icono de error / warning

	var valor = $('#popover-body-'+container).actual('height') - $("#popover-icon-"+container+"-img").actual('height');
        
	if(valor > 0 ) {
		$('#popover-icon-'+container).css("paddingTop", valor/2);
	}
	else {
		$('#popover-icon-'+container).css("paddingTop", 0);
	}

	$('#popover-close-'+container).click(function(event) {
		event.preventDefault(); 
		eliminarPopoverWarningError(container);	
	});
}


function eliminarPopoverWarningError(container) {
	$('#'+container).popover('destroy'); 	
}

function crearErrorModal(modal, texto, localizacion) {

 	crearPopoverWarningError('modal-body-'+modal, 'error', texto, localizacion, true);

	$('#modal-'+modal).on('hide.bs.modal', function (e) {
		ocultarError('modal-body-'+modal, '[rel="modal-input-'+modal+'"]');
	});
}

function ocultarError(container, input) { 
 	eliminarPopoverWarningError(container);
 	$(input).removeClass(clase_error_input);
}

function ocultarErrores() {
    

  	$('.has-error-input').each(function() {
  		$(this).removeClass(clase_error_input);
  	});

  	$('.popover').each(function() {
  		$('[rel="popover-close-link"]').click(); 
  	});
}

function mostrarLoadingBarAjax() {
    $('#div-marco-body-loading-bar').css('visibility', 'visible');
}

function ocultarLoadingBarAjax() {
    $('#div-marco-body-loading-bar').css('visibility', 'hidden'); 
}