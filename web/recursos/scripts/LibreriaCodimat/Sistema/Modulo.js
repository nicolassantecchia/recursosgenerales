/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Modulo = function (mi_numero, mi_nombre) {
    
    var numero = mi_numero,
        nombre = mi_nombre,
        enlace,
        
        getNumero = function() {
                return numero;
        },
        
        getNombre = function() {
                return nombre;
        },
        
        getEnlace = function() {
                return enlace;
        },
        
        setEnlace = function(mi_enlace) {
                enlace = mi_enlace;
        };
        
        return {
                getNumero : getNumero,
                getNombre : getNombre,
                getEnlace : getEnlace,
                setEnlace : setEnlace
        }
        
}
