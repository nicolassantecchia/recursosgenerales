/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Tarjeta = function(mi_codigo, mi_tipo){
    
    var codigo = mi_codigo, // int
        tipo = mi_tipo, // int 0: proximidad, 1: magnetica

        getCodigo = function() {
            return codigo;
        }
        
        getTipo = function() {
            return tipo;
        }
        
        return {
                getCodigo : getCodigo,
                getTipo : getTipo,
        }
        
}

