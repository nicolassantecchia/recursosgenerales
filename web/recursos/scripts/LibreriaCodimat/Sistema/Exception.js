CONFIG.exception = {
    
    general : {codigo : 0, nombre : "Excepci&oacute;n"}, //codigo proveedor
    xml : {codigo : 1, nombre : "XML"},
    ajax : {codigo : 2, nombre : "Ajax"}
}

var     EXCEP_CODIGO = CONFIG.exception.general.codigo,
	EXCEP_NOMBRE_CODIGO = CONFIG.exception.general.nombre,  //excepciones ocurridas en el codigo del js.
	
	EXCEP_XML_ERROR = CONFIG.exception.xml.codigo,
	EXCEP_NOMBRE_XML = CONFIG.exception.xml.nombre,
	
	EXCEP_AJAX_ERROR = CONFIG.exception.ajax.codigo,
	EXCEP_NOMBRE_AJAX = CONFIG.exception.ajax.nombre;


var Exception = function(mi_mensaje_amigable, mi_mensaje, soloMensaje) {

	var tipo = EXCEP_CODIGO,
            descripcionTipo = EXCEP_NOMBRE_CODIGO,
            mensajeAmigable = mi_mensaje_amigable,
            mensaje = mi_mensaje,


            getTipo = function() {
                    return tipo;
            },

            getDescipcionTipo = function() {
                    return "<u>Tipo de Error</u>: "+descripcionTipo;
            },

            //retorna el mensaje compuesto y formateado listo para mostrar que incluye el mensaje amigable y el mensaje correspondiente a la generación del error.
            getMensaje = function() {

                if(!soloMensaje)
                {
                    var tipoE = getDescipcionTipo();
                    return mensajeAmigable+"</br></br>"+tipoE+".</br><u>Mensaje</u>: "+mensaje; //"+"</br></br>Por favor, <strong>no cierre este cuadro</strong><em> y comun&iacute;quese con el &aacute;rea de sistemas.</em>";
                }
                else
                {
                    var mensajeFinal = mensajeAmigable;

                    if(mensaje != "")
                    {
                        mensajeFinal = mensajeFinal +"</br></br>"+mensaje;
                    }

                    return mensajeFinal;
                }
            },

            setTipo = function(tip) {
                    tipo = tip;
            },

            setDescripcionTipo = function(descipcion) {
                    descripcionTipo = descipcion;
            },
                 
            //Retorna el mensaje amigable.
            getMensajeAmigable = function() {
                return mensajeAmigable;
            },
            
            //Retorna el mensaje original del error, debería ser el que viene desde el servidor.
            getMensajeOriginal = function() {
                return mensaje;
            };

            return {
                    getTipo : getTipo,
                    getDescipcionTipo : getDescipcionTipo,
                    getMensaje : getMensaje,
                    getMensajeAmigable : getMensajeAmigable,
                    getMensajeOriginal : getMensajeOriginal,

                    setTipo : setTipo,
                    setDescripcionTipo : setDescripcionTipo
            }
}

var AjaxException = function(texto, status, error) {

	var excep = Object.create(new Exception("", texto+"</br>Status: "+status+"</br>Error: "+error, false));
	
	excep.setTipo(EXCEP_AJAX_ERROR);
	excep.setDescripcionTipo(EXCEP_NOMBRE_AJAX);
	
	return excep;
}

//modela una excepcion dada por el atributo estadoOperacion del tag <mensaje>
var XMLException = function(mensajeAmigable, mensajeError, mostrarSoloMsj) {

	var excep = Object.create(new Exception(mensajeAmigable, mensajeError, true));
	
	excep.setTipo(EXCEP_XML_ERROR);
	excep.setDescripcionTipo(EXCEP_NOMBRE_XML);
	
	return excep;
}

