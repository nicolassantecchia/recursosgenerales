var Equipo = function(idEquipo){
 
    var id = idEquipo, //int
        ip , //String
        mac, //String
        activo = false,//boolean
        fechaAlta,//Fecha
        marca, //String
        modelo, //String
        tipoEquipo = 0, //int - 0: indefinido. 1: piqueadora. 2: unidadContenedora.

        getIp = function() {

            if (ip === null){
                return "valor desconocido";
            }

            return ip;
        },

        setIp = function(mi_ip) {
            ip = mi_ip;
        },

        getMac = function() {

            if (mac === null){
                return "valor desconocido";
            }
            
            return mac;
        },

    setMac = function(mi_mac) {
        mac = mi_mac;
    },

    getId = function() {
        return id;
    },

    isActivo = function() {
        return activo;
    },

    setActivo = function(act) {
        activo = act;
    },

    getFechaAlta = function() {
        return fechaAlta;
    },

    setFechaAlta = function(fecha) {
        fechaAlta = fecha;
    },

    getMarca = function() {
       
        if (marca === null){
            return "valor desconocido";
        }
        
        return marca;
    },

    setMarca = function(mi_marca) {
        marca = mi_marca;
    },

    getModelo = function() {
        
        if (modelo === null){
            return "valor desconocido";
        }
        
        return modelo;
    },

    setModelo = function(mi_modelo) {
        modelo = mi_modelo;
    },

    getTipoEquipo = function() {
        return tipoEquipo;
    },

    setTipoEquipo = function(tipo) {
        tipoEquipo = tipo;
    },

    setId = function(mi_id) {
        id = mi_id;
    };
    
    return {
        getIp : getIp,
        setIp : setIp,
        getMac : getMac,
        setMac : setMac,
        getId : getId,
        isActivo : isActivo,
        setActivo : setActivo,
        getFechaAlta : getFechaAlta,
        setFechaAlta : setFechaAlta,
        getMarca : getMarca,
        setMarca : setMarca,
        getModelo : getModelo,
        setModelo : setModelo,
        getTipoEquipo : getTipoEquipo,
        setTipoEquipo : setTipoEquipo,
        setId : setId
    };
};