/*
 *  ---------------------------------------------
 *  Fernando Prieto - Febrero 2015
 *  ---------------------------------------------
 */
 
 
var OperacionesSobreServidorSistema = function() {
    
    var manejoErroresSesion = function(stringTipo, stringSubtipo, url) {
            
            var tipo = parseInt(stringTipo);
            var subtipo = parseInt(stringSubtipo);
            
            var respuesta = {redireccionar : false, sesion : null};
            
            if(tipo == CONFIG.variables.sistem.tipoError.sesion)
            {
                if(subtipo ==  CONFIG.variables.sistem.sesion.error.inactiva || subtipo ==  CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada || subtipo == CONFIG.variables.sistem.sesion.error.caducada)                                
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                }
                else
                {
                    if(subtipo ==  CONFIG.variables.sistem.sesion.error.permiso)
                    {
                        respuesta.sesion = {errorPermiso : true};                                    
                    }
                }
            }
            else
            {
                if(url != null)
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                }
            }
            
            return respuesta;
        },

    	getMensajeErrorTiposComprobante = function() {	
            return "Ocurri&oacute; una excepci&oacute;n al cargar los tipos de comprobantes.";
        },
                
        getMensajeErrorLogin = function() {           
            return "Ocurri&oacute; una excepci&oacute;n al iniciar cesi&oacute;n</br></br>Contacte al administrador.";
        },
        
        getMensajeErrorLogout = function() {
            return "Ocurri&oacute; una excepci&oacute;n al cerrar cesi&oacute;n</br></br>Contacte al administrador.";
        },   
                
        getMensajeErrorSesionActiva = function() {
            return "Ocurri&oacute; una excepci&oacute;n al controlar sesion activa</br></br>Contacte al administrador.";
        },

        getMensajeAutenticarYVerificarPermisoAplicacion = function() {
            return "Ocurri&oacute; una excepci&oacute;n al efectuar la autenticación y control de permisos.</br></br>Contacte al administrador.";
        },
                
        getMensajeErrorUsuario = function() {	
            return "Ocurri&oacute; una excepci&oacute;n al cargar el nombre del usuario.";
        },
                
        getMensajeErrorInsertarPermisosUsuario = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al actualizar los permisos del usuario <em>"+idUsuario+"</em>.";      
        },
        
        getMensajeErrorRecuperarTarjeta = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al recuperar la tarjeta del usuario <em>"+idUsuario+"</em>.";      
        },
                
        getMensajeErrorAltaTarjeta = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al dar de alta la tarjeta del usuario <em>"+idUsuario+"</em>.";      
        },
                
        getMensajeErrorBajaTarjeta = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al dar de baja la tarjeta del usuario <em>"+idUsuario+"</em>.";      
        },
        
        getMensajeErrorActualizarNickYPassUsuario = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al actualizar el nick y el password del usuario <em>"+idUsuario+"</em>.";      
        },
        
        getMensajeErrorDatosUsuario = function(idUsuario) {
            return "Ocurri&oacute; una excepci&oacute;n al recuperar algunos datos del usuario <em>"+idUsuario+"</em>.";      
        },
        
        //METODO SINCRONICO 
        recuperarTiposComprobante = function(data) {		

            $.ajax({ 
                data : {algo : 'para que no falle en el .6'},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarTiposDeComprobante, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse) {

                    try
                    {							
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {
                            var listaDeTipoDeComprobante = new ListaDeTipoComprobante();
                            
                            $(xmlResponse).find("tipo").each(function() {                                    
                                listaDeTipoDeComprobante.insert(new TipoComprobante($(this).attr("codigo"), $(this).attr("detalle"), $(this).attr("abbr")));   
                            });
                            
                            //callback
                            data.onSuccess(listaDeTipoDeComprobante, $mensaje.attr("mensaje"), data.parametrosExtra);									
                        }
                        else 
                        {
                            var excep = new XMLException(getMensajeErrorTiposComprobante(), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorTiposComprobante(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        sesionLogin = function(data) {
            
            if(data.parametrosExtra == null) {
                data.parametrosExtra = {};
            }

            $.ajax({                
                                        
                data: {metodo : CONFIG.variables.sistem.sesion.tipoMetodo.login, metodoAuth : data.metodoAuth, usuario : data.usuario, password : data.password}, 
                
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.sistem.controladores.loginUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        if($mensaje.attr("estadoOperacion") == "ok") 
                        {
                            var $info = $(xmlResponse).find("info");
                            
                            if(data.urlForward != null)
                            {
                                //callback
                                data.onSuccess(data.urlForward+'?usuario='+$info.attr("usuarioID")+'&ip='+$info.attr("ip"), $mensaje.attr("mensaje"), data.parametrosExtra);
                            }
                            else
                            {
                                window.location.replace(CONFIG.proyectos.sistem.vistas.login);
                            }
                        }
                        else 
                        {
                            if($(xmlResponse).find("urlForward").length != 0) {
                                
                                data.parametrosExtra.errorGral = true;
                                data.onFailure(excep, data.parametrosExtra);
                            }
                            else {
                                //callback
                                data.onFailure(new XMLException($mensaje.attr('mensaje'), "", true), data.parametrosExtra);
                            }
                        }	 
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorLogin(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        }
        
        sesionLogout = function(data) {
                                        
            $.ajax({ 

                data: { metodo: CONFIG.variables.sistem.sesion.tipoMetodo.logout, 
                        usuario : $.urlParam(CONFIG.variables.sistem.sesion.parametros.usuario), 
                        ip: $.urlParam(CONFIG.variables.sistem.sesion.parametros.ip),
                        urlLogout : data.urlLogout
                    }, 
                    
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.sistem.controladores.loginUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                    
                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                    //callback
                    data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){
                    try 
                    {
                        $(xmlResponse).find("mensaje").each(function() {

                            if($(this).attr("estadoOperacion") == "ok") 
                            {
                                window.location.replace($(xmlResponse).find("urlForward").attr("url"));
                            }
                            else 
                            {
                                var urlFwd = $(xmlResponse).find("urlForward");
                                
                                if(urlFwd.length != 0) {
                                    window.location.replace(urlFwd.attr("url"));
                                }
                                else 
                                {
                                    var excep = new XMLException("Ocurri&oacute; una excepci&oacute;n al cerrar cesi&oacute;n (JSP) "+$(this).attr("mensaje")+"</br></br>Contacte al administrador.", "", true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }	        	
                        });
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorLogout(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        sesionActiva = function(data) { 
                
            var ip = $.urlParam(CONFIG.variables.sistem.sesion.parametros.ip);
            
            if(data.ip) {
                ip = data.ip;
            }
                                
            $.ajax({ 
 		
                data: { metodo: CONFIG.variables.sistem.sesion.tipoMetodo.sesionActiva, 
                        usuario : $.urlParam(CONFIG.variables.sistem.sesion.parametros.usuario), 
                        ip: ip,
                        urlForward : data.urlForward,
                        urlLogout : data.urlLogout
                    }, 
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.sistem.controladores.loginUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                success: function(xmlResponse) {
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        $mensaje.each(function() {
                            
                            var subtipo = parseInt($mensaje.attr("subtipo"));

                            if($(this).attr("estadoOperacion") == "ok") 
                            {
                                if(subtipo ==  CONFIG.variables.sistem.sesion.error.inactiva || subtipo ==  CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada || subtipo == CONFIG.variables.sistem.sesion.error.caducada)                                
                                {
                                   window.location.replace($(xmlResponse).find("urlForward").attr("url"));
                                }
                                else
                                {
                                    //callback
                                    data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);  
                                }
                            }
                            else 
                            {
                                var urlFwd = $(xmlResponse).find("urlForward");
                                
                                if(urlFwd.length != 0){
                                    
                                    window.location.replace(urlFwd.attr("url"));
                                }
                                else 
                                {
                                    var excep = new XMLException("Ocurri&oacute; una excepci&oacute;n al controlar sesi&oacute;n activa (JSP) </br></br>Contacte al administrador.", "", true);

                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }
                            }        	
                        });
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorSesionActiva(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });	
        };
        
        sesionActivaYPermiso = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            $.ajax({ 
 		
                data: {
                        metodo: CONFIG.variables.sistem.sesion.tipoMetodo.sesionActivaYPermiso, 
                        usuario : $.urlParam(CONFIG.variables.sistem.sesion.parametros.usuario), 
                        ip: $.urlParam(CONFIG.variables.sistem.sesion.parametros.ip), 
                        urlForward : data.urlForward,
                        urlLogout : data.urlLogout,
                        idAplicacion : data.idAplicacion
                }, 
                
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.sistem.controladores.loginUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        $mensaje.each(function() {
                            
                            var subtipo = parseInt($mensaje.attr("subtipo"));

                            if($(this).attr("estadoOperacion") == "ok") 
                            {                                
                                if(subtipo == CONFIG.variables.sistem.sesion.error.inactiva || subtipo == CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada || subtipo == CONFIG.variables.sistem.sesion.error.caducada)                                
                                {                                    
                                    window.location.replace($(xmlResponse).find("urlForward").attr("url"));
                                }
                                else
                                {
                                    if(subtipo ==  CONFIG.variables.sistem.sesion.error.permiso)
                                    {
                                        data.parametrosExtra.sesion = {errorPermiso : true};                                    
                                    }
                                    
                                    //callback
                                    data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);                                          
                                }
                            }
                            else 
                            {
                                var urlFwd = $(xmlResponse).find("urlForward");
                                
                                if(urlFwd.length != 0) {                                    
                                    window.location.replace(urlFwd.attr("url"));
                                }
                                else 
                                {
                                    var excep = new XMLException("Ocurri&oacute; una excepci&oacute;n al controlar sesion activa (JSP) </br></br>Contacte al administrador.", "", true);
                                    //callback
                                    data.onFailure(excep, data.parametrosExtra);
                                }

                                
                            }        	
                        });
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeErrorSesionActiva(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });	
        },
                
        autenticarYVerificarPermisoAplicacion = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }

            $.ajax({ 
 		
                data: {
                    metodo: CONFIG.variables.sistem.sesion.tipoMetodo.autenticarYPermiso, 
                    metodoAuth : data.metodoAuth, 
                    usuario : data.usuario, 
                    password : data.password, 
                    idAplicacion : data.idAplicacion
                }, 
                
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.sistem.controladores.loginUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    },

                success: function(xmlResponse) {
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");                            
                        var subtipo = parseInt($mensaje.attr("subtipo"));

                        if($mensaje.attr("estadoOperacion") == "ok") 
                        {   
                            var mensaje = "";

                            if(subtipo == CONFIG.variables.sistem.sesion.error.permiso)
                            {
                                data.parametrosExtra.sesion = {errorPermiso : true};
                                mensaje = CONFIG.variables.sistem.sesion.error.permisoMensaje;
                            }
                            else {
                                var $info = $(xmlResponse).find("info");
                                
                                data.parametrosExtra.idUsuario = $info.attr("usuarioID");
                                data.parametrosExtra.nombreUsuario = $info.attr("usuarioNombre");                                
                            }

                            //callback
                            data.onSuccess(mensaje, data.parametrosExtra);    
                        }
                        else 
                        {                            
                            var excep = new XMLException($mensaje.attr('mensaje'), "", true);
                            data.onFailure(excep, data.parametrosExtra);
                        }
                    }
                    catch(e) 
                    {
                        var excep = new Exception(getMensajeAutenticarYVerificarPermisoAplicacion(), e);
                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });	
        },
                
        recuperarUsuario = function(data) {		

            $.ajax({
                
                data : {idUsuario : data.idUsuario, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarUsuarioPorId,

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var usuario, datosUsuario;
                            
                            datosUsuario = $(xmlResponse).find("usuario");
                            
                            if(datosUsuario.length != 0) {
                                
                                usuario = new Usuario(datosUsuario.attr("codigo"), datosUsuario.attr("nombre"));
                                
                            }                                
                            
                            //callback
                            data.onSuccess(usuario);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorUsuario(), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorUsuario(), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        recuperarPermisosUsuario = function(data) {		

            $.ajax({
                
                data : {idUsuario : data.idUsuario, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarPermisosUsuario, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep); 
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var listaDePermisos = new ListaDePermisos();
                            
                            var listaDeTarjetas = new ListaDeTarjetas();
                            
                            var usuario, datosUsuario, menorCodTarjMag;
                            
                            datosUsuario = $(xmlResponse).find("usuario");
                            
                            if(datosUsuario.length != 0) {
                                
                                usuario = new Usuario(datosUsuario.attr("id"), datosUsuario.attr("nombre"));
                                usuario.setNick(datosUsuario.attr("nick"));
                                usuario.setPass(datosUsuario.attr("pass"));
                                
                            }
                            
                            $(xmlResponse).find("tarjeta").each(function() {
                                
                                listaDeTarjetas.insert(new Tarjeta($(this).attr("cod"), $(this).attr("tipo")));
                                
                            });
                            
                            $(xmlResponse).find("tarjetaMag").each(function() {
                                
                                menorCodTarjMag = $(this).attr("id");
                                
                            });
                            
                            $(xmlResponse).find("permiso").each(function() {
                                
                                listaDePermisos.insert(new Permiso($(this).attr("sistema"), $(this).attr("subsistema"), $(this).attr("modulo")));
                                
                            });
                            
                            //callback
                            data.onSuccess(usuario, listaDeTarjetas, menorCodTarjMag, listaDePermisos);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorDatosUsuario(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorDatosUsuario(data.idUsuario), e);

                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        insertarPermisosUsuario = function(data) {		

            $.ajax({
                
                data : {idUsuario : data.idUsuario, permisos : data.permisosUsuario, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.sistem.controladores.insertarPermisos,

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var mensajeModal = $mensaje.attr("mensaje") + " " + $('#input-usuario-nombre').val() + ".";
                            
                            //callback
                            data.onSuccess(mensajeModal);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorInsertarPermisosUsuario(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorInsertarPermisosUsuario(data.idUsuario), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        recuperarTarjetasUsuario = function(data) {		

            $.ajax({
                
                data : {idUsuario : data.idUsuario, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.recursosGenerales.controladores.recuperarTarjetaPorIdUsuario,
                
                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var listaDeTarjetas = new ListaDeTarjetas();
                            
                            $(xmlResponse).find("tarjeta").each(function() {
                                
                                listaDeTarjetas.insert(new Tarjeta($(this).attr("cod"), $(this).attr("tipo")));
                                
                            });
                            
                            //callback
                            data.onSuccess(listaDeTarjetas);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorRecuperarTarjeta(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorRecuperarTarjeta(data.idUsuario), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        },
                
        darAltaTarjeta = function(data) {
            
            var menorCodTarjeta = -1;

            $.ajax({
                
                data : {idUsuario : data.idUsuario, idTarjeta : data.idTarjeta, tipoTarjeta : data.tipoTarj, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.sistem.controladores.darAltaTarjeta,
                
                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep, menorCodTarjeta);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var mensajeModal = $mensaje.attr("mensaje") + " " + $('#input-usuario-nombre').val() + ".";
                            var nuevaTarjeta;
                            
                            $(xmlResponse).find("tarjeta").each(function() {
                                
                                nuevaTarjeta = new Tarjeta($(this).attr("codTarjeta"), $(this).attr("tipoTarjeta"));
                                
                            });
                            
                            $(xmlResponse).find("tarjetaMag").each(function() {
                                
                                menorCodTarjeta = $(this).attr("codTarjeta");
                                
                            });                            
                            
                            //callback
                            data.onSuccess(mensajeModal, nuevaTarjeta, menorCodTarjeta);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorAltaTarjeta(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            $(xmlResponse).find("tarjetaMag").each(function() {
                                
                                menorCodTarjeta = $(this).attr("codTarjeta");
                                
                            }); 
                            
                            //callback
                            data.onFailure(excep, menorCodTarjeta);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorAltaTarjeta(data.idUsuario), e);
                        
                        //callback
                        data.onFailure(excep, menorCodTarjeta);
                        
                    }
                    
                }
                
            });
            
        },
                
        darBajaTarjeta = function(data) {
            
            var menorCodTarjeta = -1;

            $.ajax({
                
                data : {idUsuario : data.idUsuario, idTarjeta : data.idTarjeta, tipoTarjeta : data.tipoTarj, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.sistem.controladores.darBajaTarjeta,
                
                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep, menorCodTarjeta);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var mensajeModal = $mensaje.attr("mensaje") + " " + $('#input-usuario-nombre').val() + ".";
                            var nuevaTarjeta;
                            
                            $(xmlResponse).find("tarjeta").each(function() {
                                
                                nuevaTarjeta = new Tarjeta($(this).attr("codTarjeta"), $(this).attr("tipoTarjeta"));
                                
                            });
                            
                            $(xmlResponse).find("tarjetaMag").each(function() {
                                
                                menorCodTarjeta = $(this).attr("codTarjeta");
                                
                            });
                            
                            //callback
                            data.onSuccess(mensajeModal, nuevaTarjeta, menorCodTarjeta);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorBajaTarjeta(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            $(xmlResponse).find("tarjetaMag").each(function() {
                                
                                menorCodTarjeta = $(this).attr("codTarjeta");
                                
                            }); 
                            
                            //callback
                            data.onFailure(excep, menorCodTarjeta);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorBajaTarjeta(data.idUsuario), e);
                        
                        //callback
                        data.onFailure(excep, menorCodTarjeta);
                        
                    }
                    
                }
                
            });
            
        },
                
        actualizarNickYPassUsuario = function(data) {		

            $.ajax({
                
                data : {idUsuario : data.idUsuario, nick : data.nickUsuario, pass : data.passUsuario, idUsuarioLog : data.idUsuarioLog},
                type: "POST",
                dataType: "xml",
                async: false,
                url: CONFIG.proyectos.sistem.controladores.actualizarNickYPass,

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);
                    
                    //callback
                    data.onFailure(excep);
                    
                },

                success: function(xmlResponse) {

                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            
                            var mensajeModal = $mensaje.attr("mensaje") + " " + $('#input-usuario-nombre').val() + ".";
                            
                            //callback
                            data.onSuccess(mensajeModal);
                            
                        }
                        else {
                            
                            var excep = new XMLException(getMensajeErrorActualizarNickYPassUsuario(data.idUsuario), $mensaje.attr('mensaje'));
                            
                            //callback
                            data.onFailure(excep);
                            
                        }
                        
                    }
                    catch(e) {
                        
                        var excep = new Exception(getMensajeErrorActualizarNickYPassUsuario(data.idUsuario), e);
                        
                        //callback
                        data.onFailure(excep);
                        
                    }
                    
                }
                
            });
            
        };
		
	return {
            manejoErroresSesion: manejoErroresSesion,
            sesionLogin : sesionLogin,
            sesionLogout : sesionLogout,
            sesionActiva : sesionActiva,
            sesionActivaYPermiso : sesionActivaYPermiso,
            autenticarYVerificarPermisoAplicacion : autenticarYVerificarPermisoAplicacion,
            recuperarTiposComprobante : recuperarTiposComprobante,
            recuperarUsuario : recuperarUsuario,
            recuperarPermisosUsuario : recuperarPermisosUsuario,
            insertarPermisosUsuario : insertarPermisosUsuario,
            recuperarTarjetasUsuario : recuperarTarjetasUsuario,
            darAltaTarjeta : darAltaTarjeta,
            darBajaTarjeta : darBajaTarjeta,
            actualizarNickYPassUsuario : actualizarNickYPassUsuario
        };						
 };