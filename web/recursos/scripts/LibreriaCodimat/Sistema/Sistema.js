/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Sistema = function(mi_numero, mi_nombre){
    
    var numero = mi_numero,
        nombre = mi_nombre,
        prefijo,
        listaDeSubsistemas = new listaDeSubsistemas(),

        getNumero = function() {
            return numero;
        }

        getNombre = function() {
            return nombre;
        }

        getPrefijo = function() {
            return prefijo;
        }

        setPrefijo = function(mi_prefijo) {
            prefijo = mi_prefijo;
        }

        getListaDeSubsistemas = function() {
            return listaDeSubsistemas;
        }
        
        return {
                getNumero : getNumero,
                getNombre : getNombre,
                getPrefijo : getPrefijo,
                setPrefijo : setPrefijo,
                getListaDeSubsistemas : getListaDeSubsistemas
        }
        
}
