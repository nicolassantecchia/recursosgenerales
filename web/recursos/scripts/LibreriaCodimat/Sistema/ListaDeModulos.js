/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ListaDeModulos = function () {    

    var nuevaLista = Object.create(new Lista())

    nuevaLista.searchItem = function(id_modulo) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_modulo) {
                return itemActual;
            }                
        }
        return itemActual;
    };

    return nuevaLista;
}
