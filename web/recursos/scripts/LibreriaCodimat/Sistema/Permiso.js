/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Permiso = function(mi_sistema, mi_subSistema, mi_modulo){
    
    var sistema = mi_sistema,
        subSistema = mi_subSistema,
        modulo = mi_modulo,

        getSistema = function() {
            return sistema;
        }

        getSubSistema = function() {
            return subSistema;
        }

        getModulo = function() {
            return modulo;
        }
        
        return {
                getSistema : getSistema,
                getSubSistema : getSubSistema,
                getModulo : getModulo
        }
        
}
