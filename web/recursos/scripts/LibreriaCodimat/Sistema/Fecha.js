/*
 * Esta clase almacena solo strings (fecha y hora en formato string)
 * Requiere Utils.js
 * Requiere Moment.js
 * 
 * mi_dia viene en formato dia/mes/año (el separador no importa)
 */
var Fecha = function(mi_dia, mi_hora) {    
    
    var separador = "/",
        dateAsociado = null;

    /**
         * Asume fecha en formato dia/mes/año o dia-mes-año
         * @returns {String}
         */
    var inicializarFechaDesdeString = function(dia, hora) {        
            
            var diaComp = "01/01/1900";
            var horaComp = "00:00:00";
            
            if(dia != "") {
                
                var d = dia.substr(0, 2);
                var m = dia.substr(3, 2);
                var y = dia.substr(6, 4);
                
                if(parseInt(m) > 12 || parseInt(m) < 0) {
                    throw Exception("Mes inv&aacute;lido", "", true);
                }
                
                if(parseInt(d) > 31 || parseInt(d) < 0) {
                    throw Exception("D&iacute;a  inv&aacute;lido", "", true);
                }
                
                diaComp = m + "/" + d + "/" + y;
            }
            
            if(hora != "") {
                horaComp = hora;
            }
            
            dateAsociado = new Date(Date.parse(diaComp + " " + horaComp));
        },
    
        /**
         * Retorna el string correspondiente a la fecha
         * @returns {String|Number|mi_separador}
         */
        getDia = function() {
            
            var dd = dateAsociado.getDate();
            var mm = dateAsociado.getMonth() + 1; //January is 0!

            var yyyy = dateAsociado.getFullYear();

            if(dd<10){
                dd = '0' + dd;
            } 
            if(mm<10){
                mm = '0' + mm;
            } 
            
            return dd + separador + mm + separador + yyyy;
        },
                
        getHora = function() {            
            return dateAsociado.toTimeString().split(' ')[0];
        },
                
        getHoraMinutos = function() {
            return getHora().substr(0,5);
        },
        
        setDia = function(mi_dia) {            
            inicializarFechaDesdeString(mi_dia, getHora());
        },
        
        setHora = function(mi_hora) {            
            inicializarFechaDesdeString(getDia(), mi_hora);
        },
        
        setHoy = function(mi_separador) {
            
            separador = mi_separador;
            dateAsociado = new Date();
        },
                
        /**
         * Retorna -1 si la fecha hora de este objeto es menor a la pasada por parámetro; 1 en caso contrario y 0 si son iguales.
         * (este criterio es siempre asi en el lenguaje). Arroja Excepción si las fechas son ivalidas.
         * @param {type} fecha2
         * @returns {undefined}
         */
        compararFecha = function(fecha2) {  
            
            var date1 = getMilisegundos();
            var date2 = fecha2.getMilisegundos();
            
            if(date1 < date2) {
                return -1
            }
            else {
                if(date1 > date2) {
                    return 1;
                }
                else {
                    if(date1 == date2) {
                        return 0;
                    }
                    else {
                        throw Exception("Alguna de las fechas es inv&aacute;lida. No es posible realizar la comparaci&oacute;n.", "", true);
                    }
                }
            }
        },
        
        //retorna la cantidad de milisegundos transcurridos desde 1970 a esta fecha
        getMilisegundos = function() {            
            return Date.UTC(dateAsociado.getYear(), dateAsociado.getMonth(), dateAsociado.getDate(), dateAsociado.getHours(), dateAsociado.getMinutes(), dateAsociado.getSeconds());
        };
        
    setHoy(separador);
    
    if(mi_dia != null) {
        setDia(mi_dia);
    }    

    if(mi_hora != null) {
        setHora(mi_hora);
    }

    return {
        getDia : getDia,
        getHora : getHora,
        setDia : setDia,
        setHora : setHora,
        setHoy : setHoy,
        getHoraMinutos : getHoraMinutos,
        compararFecha : compararFecha,
        getMilisegundos : getMilisegundos
    };     
};