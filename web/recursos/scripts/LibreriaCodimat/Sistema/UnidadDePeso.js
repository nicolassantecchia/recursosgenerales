var UnidadDePeso = function (mi_nombreDetalle){
    
	var idEnTabla = -1,
		nombreDetalle = mi_nombreDetalle,
		nombreAbreviacion = "",
		unidadMinima = -1,

		getIdEnTabla = function() {
			return idEnTabla;
		},

		getNombreAbreviacion = function() {
			return nombreAbreviacion;
		},

		getNombreDetalle = function() {
			return nombreDetalle;
		},

		getUnidadMinima = function() {
			return unidadMinima;
		},

		setIdEnTabla = function(mi_idEnTabla) {
			idEnTabla = mi_idEnTabla;
		},

		setNombreAbreviacion = function(mi_nombreAbreviacion) {
			nombreAbreviacion = mi_nombreAbreviacion;
		},

		setUnidadMinima = function(mi_unidadMinima) {
			unidadMinima = mi_unidadMinima;
		};
	
	return {
	
		getIdEnTabla : getIdEnTabla,
		getNombreAbreviacion : getNombreAbreviacion,
		getNombreDetalle : getNombreDetalle,
		getUnidadMinima : getUnidadMinima,

		setIdEnTabla : setIdEnTabla,
		setNombreAbreviacion : setNombreAbreviacion,
		setUnidadMinima : setUnidadMinima
	}
}