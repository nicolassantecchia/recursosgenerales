var TipoEvento = function(mi_codigo) {
    
    var codigo = mi_codigo, //int
        detalle = null, //String

    getCodigo = function() {
        return codigo;
    },

    getDetalle = function() {
        return detalle;
    },

    setDetalle = function(det) {
        detalle = det;
    };
    
    return {
        getCodigo : getCodigo,
        getDetalle : getDetalle,
        setDetalle : setDetalle
    };
}