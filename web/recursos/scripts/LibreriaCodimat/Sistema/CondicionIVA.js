var CondicionIVA = function (cod, det, abr) { 

    //Atributos Privados
    var codigo = cod,
        detalle = det,
        abrev = abr,

        getCodigo = function() {
            return codigo;
        },
		
        getDetalle = function() {
            return detalle;
        },

        getAbrev = function() {
            return abrev;
        }; 

    return  {                               
                getCodigo : getCodigo,
                getDetalle : getDetalle,
                getAbrev : getAbrev
            }
}
