/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var SubSistema = function(mi_numero, mi_nombre){
    
    var numero = mi_numero,
        nombre = mi_nombre,
        arc,
        ord,
        listaDeModulos = new ListaDeModulos(),

        getArc = function() {
            return arc;
        },

        setArc = function(mi_arc) {
            arc = mi_arc;
        },

        getOrd = function() {
            return ord;
        }

        setOrd = function(mi_ord) {
            ord = mi_ord;
        }

        getNumero = function() {
            return numero;
        }

        getNombre = function() {
            return nombre;
        }

        getListaDeModulos = function() {
            return listaDeModulos;
        };
        
        return {
                getArc : getArc,
                setArc : setArc,
                getOrd : getOrd,
                setOrd : setOrd,
                getNumero : getNumero,
                getNombre : getNombre,
                getListaDeModulos : getListaDeModulos
        }
        
}
