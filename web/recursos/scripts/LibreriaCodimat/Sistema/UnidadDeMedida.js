var UnidadDeMedida = function (mi_idEnTabla, mi_nombreDetalle) { 

    var idEnTabla = mi_idEnTabla, //int
		nombreDetalle = mi_nombreDetalle,	//string
		nombreAbreviacion = "", //string
		unidadMinima = -1,	//double

    getIdEnTabla = function() {
        return idEnTabla;
    },

    getNombreAbreviacion = function() {
        return nombreAbreviacion;
    },

    getNombreDetalle = function() {
        return nombreDetalle;
    },

    getUnidadMinima = function() {
        return unidadMinima;
    },

    setIdEnTabla = function(mi_idEnTabla) {
        idEnTabla = mi_idEnTabla;
    },
	
    setNombreAbreviacion = function(mi_nombreAbreviacion) {
        nombreAbreviacion = mi_nombreAbreviacion;
    },

    setUnidadMinima = function(mi_unidadMinima) {
        unidadMinima = mi_unidadMinima;
    };	
	
	return {
		getIdEnTabla : getIdEnTabla,
		getNombreAbreviacion : getNombreAbreviacion,
		getNombreDetalle : getNombreDetalle,
		getUnidadMinima : getUnidadMinima,

		setIdEnTabla : setIdEnTabla,
		setNombreAbreviacion : setNombreAbreviacion,
		setUnidadMinima : setUnidadMinima
	}
}