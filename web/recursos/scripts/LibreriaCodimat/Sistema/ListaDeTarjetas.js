/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ListaDeTarjetas = function () {    

    var nuevaLista = Object.create(new Lista());
    
    nuevaLista.getTarjeta = function(mi_posicion) {

        var itemActual = null;
        
        itemActual = nuevaLista.getItem(mi_posicion);
        
        return itemActual;
        
    },
    
    nuevaLista.eliminarTarjeta = function(mi_posicion) {
        
        nuevaLista.deleteItem(mi_posicion);
        
    },
            
    nuevaLista.insertarTarjeta = function(mi_tarjeta) {
        
        nuevaLista.insert(mi_tarjeta);
        
    };            

    return nuevaLista;
    
}