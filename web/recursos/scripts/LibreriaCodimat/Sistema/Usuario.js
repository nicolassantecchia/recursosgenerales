var Usuario = function(idUser, nombreUser) {
    
    var nombre = "";                    //String
    
    if(nombre != null) {
        nombre = nombreUser;                         
    }
    
    var idUsuario = idUser,                 //int
        pass = null,                        //String
        passEncriptado = null,              //String
        nick,                               //String
        rangoEspecialAntesDeHorario = 0,    //int
        rangoEspecialDespuesDeHorario = 0,  //int
        abrevNombre = "",                   //String
        idUsuarioJoomla = -1,               //int

    getIdUsuario = function() {
        return idUsuario;
    },

    getNombre = function() {
        return nombre.trim();
    },

    getNick = function() {
        return nick;
    },

    getRangoEspecialAntesDeHorario = function() {
        return rangoEspecialAntesDeHorario;
    },

    getRangoEspecialDespuesDeHorario = function() {
        return rangoEspecialDespuesDeHorario;
    },

    setNick = function(mi_nick) {
        nick = mi_nick;
    },
    
    setPass = function(mi_pass) {
        pass = mi_pass;
    },
            
    getPass = function() {
        return pass;
    },

    setPassEncriptado = function(pass) {
        passEncriptado = pass;
    },
    
    getPassEncriptado = function() {
        return passEncriptado;
    },

    marcaConHorarioExtendido = function(){
        return rangoEspecialAntesDeHorario != 0;
    },

    getAbrevNombre = function() {
        return abrevNombre;
    },

    setAbrevNombre = function(abrev) {
        abrevNombre = abrev;
    };
    
    return {
        getIdUsuario : getIdUsuario,
        getNombre : getNombre,
        getNick : getNick,
        getRangoEspecialAntesDeHorario : getRangoEspecialAntesDeHorario,
        getRangoEspecialDespuesDeHorario : getRangoEspecialDespuesDeHorario,
        setNick : setNick,    
        setPass : setPass,            
        getPass : getPass,
        setPassEncriptado : setPassEncriptado,    
        getPassEncriptado : getPassEncriptado,
        marcaConHorarioExtendido : marcaConHorarioExtendido,
        getAbrevNombre : getAbrevNombre,
        setAbrevNombre : setAbrevNombre
    }
}
