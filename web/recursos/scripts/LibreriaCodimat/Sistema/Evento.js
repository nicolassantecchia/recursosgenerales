/**
 * Requiere LibreriaCodimat/Sistema/TipoEvento,js
 */
var Evento = function(mi_id, mi_tipo) {
    
    var id = mi_id,
        tipoEvento = mi_tipo,
        usuarioAsociado = null, //Usuario
        fecha = null, //Fecha
        comentario = "", //String
        
    getTipoEvento = function() {
        return tipoEvento;
    },

    setTipoEvento = function(tipoEvt) {
        tipoEvento = tipoEvt;
    },

    getId = function() {
        return id;
    },

    setId = function(mi_id) {
        id = mi_id;
    },

    getUsuarioAsociado = function() {
        return usuarioAsociado;
    },

    setUsuarioAsociado = function(usuario) {
        usuarioAsociado = usuario;
    },

    getFecha = function() {
        return fecha;
    },

    setFecha = function(fec) {
        fecha = fec;
    },

    getComentario = function() {
        return comentario;
    },

    setComentario = function(coment) {
        comentario = coment;
    };
    
    return {
        getId : getId,
        setId : setId,
        getUsuarioAsociado : getUsuarioAsociado,
        setUsuarioAsociado : setUsuarioAsociado,
        getFecha : getFecha,
        setFecha : setFecha,
        getComentario : getComentario,
        setComentario : setComentario,
        getTipoEvento : getTipoEvento,
        setTipoEvento : setTipoEvento
    }
}

