/*
 *  ---------------------------------------------
 *  Fernando Prieto - Febrero 2015
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeTipoComprobante = function() {    

    var nuevaLista = Object.create(new Lista());
        
        nuevaLista.searchItem = function(codigo_cprb) {

        var itemActual = null;
        
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getCodigo() == codigo_cprb) {
                return itemActual;
            }                
        }
        
        return itemActual;
    };
        
    return nuevaLista;
}