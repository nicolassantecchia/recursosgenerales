/* Buscar definicion de objeto CONFIG.tiposComprobante en configuracionGeneral.js del proyecto RecursosGenerales
 * No quedo otra que ponerlo ahí para evitar que el ajax de $('document').ready se ejecute cuando no es necesario.
 */


$('document').ready(function() {
    
    var operacionesSistema_tipoCprb = new OperacionesSobreServidorSistema();
    
    operacionesSistema_tipoCprb.recuperarTiposComprobante({        
        onSuccess : handleSuccessRecupTiposComprobante_tipoCprb, 
        onFailure : handleFailureRecupTiposComprobante_tipoCprb
    });
    
});

function handleSuccessRecupTiposComprobante_tipoCprb(lista) {    
    
    CONFIG.tiposComprobante.oc = lista.searchItem(CONFIG.tiposComprobante.codigo.oc); //Orden de Compra 
    CONFIG.tiposComprobante.rp = lista.searchItem(CONFIG.tiposComprobante.codigo.rp); //Remito Proveedor
    CONFIG.tiposComprobante.fe = lista.searchItem(CONFIG.tiposComprobante.codigo.fe); //Faltante
    CONFIG.tiposComprobante.so = lista.searchItem(CONFIG.tiposComprobante.codigo.so); //Sobrante
    CONFIG.tiposComprobante.fp = lista.searchItem(CONFIG.tiposComprobante.codigo.fp); //Factura Proveedor
}

function handleFailureRecupTiposComprobante_tipoCprb(exception) {
    mostrarModalError(exception.getMensaje());
}

var TipoComprobante = function (mi_codigo, mi_detalle, mi_abbr, mi_letra) { 
    
    var codigo = mi_codigo,
        detalle = mi_detalle,
        abreviacion = mi_abbr,
        letra = mi_letra,
        
        getAbreviacion = function() {
            return abreviacion;
        },

        getCodigo = function() {
            return codigo;
        },

        getDetalle = function() {
            return detalle;
        },
        
        getLetra = function() {
            return letra;
        };
    
    return {
        getAbreviacion : getAbreviacion,
        getCodigo : getCodigo,
        getDetalle : getDetalle,
        getLetra : getLetra
    }
}