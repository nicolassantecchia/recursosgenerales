var Bulto = function(nro) {
    
    var numero = nro, //int
        codigoBarras,

    getNumero = function() {
        return numero;
    },
            
    getCodigoBarras = function() {
        return codigoBarras;
    },

    setCodigoBarras = function(codBarras) {
        codigoBarras = codBarras;
    };
    
    return {
        getNumero : getNumero,
        getCodigoBarras : getCodigoBarras,
        setCodigoBarras : setCodigoBarras
    }
}