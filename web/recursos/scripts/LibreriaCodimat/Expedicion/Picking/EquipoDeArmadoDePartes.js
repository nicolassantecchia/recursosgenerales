/**
 *  Requiere:
    LibreriaCodimat/Expedicion/Picking/ListaDeUnidadesContenedoras.js
 */
var EquipoDeArmadoDePartes = function(idEquipo) {
    
    var piqueadora, //Pickeadora
        listaDeUnidadesContenedoras = new ListaDeUnidadesContenedoras(),
        nroParteActual, //int
        estado, //int
        usuarioActual; //UsuarioPiqueadora
          
    var nuevoEquipo = Object.create(new Equipo(idEquipo));


    nuevoEquipo.getNroParteActual = function () {
        return nroParteActual;
    };

    nuevoEquipo.setNroParteActual = function(nroParte){
        nroParteActual = nroParte;
    };

    nuevoEquipo.getEstado = function() {
        return estado;
    };

    nuevoEquipo.setEstado = function(est) {
        estado = est;
    };

    nuevoEquipo.getUsuarioActual = function() {
        return usuarioActual;
    };

    nuevoEquipo.setUsuarioActual = function(usuario) {
        usuarioActual = usuario;
    };

    nuevoEquipo.getPiqueadora = function() {
        return piqueadora;
    };

    nuevoEquipo.setPiqueadora = function(piq) {
        piqueadora = piq;
    };

    nuevoEquipo.getListaDeUnidadesContenedoras = function() {
        return listaDeUnidadesContenedoras;
    };

    nuevoEquipo.setListaDeUnidadesContenedoras = function(lista) {
        listaDeUnidadesContenedoras = lista;
    };
    
    return nuevoEquipo;
};