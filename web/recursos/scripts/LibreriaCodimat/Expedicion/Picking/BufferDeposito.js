var BufferDeposito = function(idBuf) {
    
    var idBuffer = idBuf,
        posicion,
        codigoBarras,
        capacidad,
        tipoBuffer,
        detalle,
        activo = false,

    getIdBuffer = function() {
        return idBuffer;
    },
    setIdBuffer = function(idBuf) {
        idBuffer = idBuf;
    },
    getPosicion = function() {
        return posicion;
    },
    setPosicion = function(pos) {
        posicion = pos;
    },
    getCodigoBarras = function() {
        return codigoBarras;
    },
    setCodigoBarras = function(codBarras) {
        codigoBarras = codBarras;
    },
    getCapacidad = function() {
        return capacidad;
    },
    setCapacidad = function(capac) {
        capacidad = capac;
    },
    getTipoBuffer = function() {
        return tipoBuffer;
    },
    setTipoBuffer = function(tipoBuf) {
        tipoBuffer = tipoBuf;
    },
    getDetalle = function() {
        return detalle;
    },
    setDetalle = function(det) {
        this.detalle = det;
    },
    isActivo = function() {
        return activo;
    },
    setActivo = function(act) {
        activo = act;
    };
    
    return {
        getIdBuffer : getIdBuffer,
        setIdBuffer : setIdBuffer,
        getPosicion : getPosicion,
        setPosicion : setPosicion,
        getCodigoBarras : getCodigoBarras,
        setCodigoBarras : setCodigoBarras,
        getCapacidad : getCapacidad,
        setCapacidad : setCapacidad,
        getTipoBuffer : getTipoBuffer,
        setTipoBuffer : setTipoBuffer,
        getDetalle : getDetalle,
        setDetalle : setDetalle,
        isActivo : isActivo,
        setActivo : setActivo
    };
}