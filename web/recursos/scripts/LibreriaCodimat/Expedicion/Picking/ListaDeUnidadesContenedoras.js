/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/UnidadContenedora.js
 */
var ListaDeUnidadesContenedoras = function() {
    
    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(id_uc) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() === id_uc) {
                return itemActual;
            } 
            else {
                itemActual = null;
            }
        }
        return itemActual;
    };

    return nuevaLista;
};