/**
 *  Requiere:
    LibreriaCodimat/Expedicion/Picking/PosicionPicking.js
    LibreriaCodimat/Expedicion/Picking/TipoContenedor.js
 */
var Contenedor = function(idContenedor) {
    
    var id = idContenedor,
        ancho = 0.0, //double
        alto = 0.0, //double
        tipoContenedor = null, //TipoContenedor
        articulo = null, //Articulo
        cantidadUnidadesInicial = 0.0, //double
        cantidadUnidadesActual = 0.0, //double - lo que se va descontando
        idPosicionPicking = -1, //int
        posicionPicking = null,
        pesoInicial = 0.0, //double - tendria que ser la multiplicacion del peso unitario por cantidad
        pesoActual = 0.0, //double - lo que se vaya descontando
        fechaAlta = null, //Fecha
        fechaBaja = null, //Fecha
        fechaUltimaActualizacion = null, //Fecha
        usuarioAlta = null, //Usuario
        usuarioBaja = null, //Usuario
        usuarioUltimaActualizacion = null, //Usuario
        activo = true, //boolean
        estado = 0, //int
        usuarioActual = null, //Usuario - para hacer altas, bajas, o modificaciones, verifico que el contenedor
        codigoDeBarras = "", //String
        
    getId = function() {
        return id;
    },

    getAncho = function() {
        return ancho;
    },

    setAncho = function(mi_ancho) {
        ancho = mi_ancho;
    },

    getAlto = function() {
        return alto;
    },

    setAlto = function(mi_alto) {
        alto = mi_alto;
    },

    setTipoContenedor = function(mi_tipo) {
        tipoContenedor = mi_tipo;
    },

    getArticulo = function() {
        return articulo;
    },

    setArticulo = function(mi_articulo) {
        articulo = mi_articulo;
    },

    getCantidadUnidadesInicial = function() {
        return cantidadUnidadesInicial;
    },

    setCantidadUnidadesInicial = function(mi_cantidadUnidadesInicial) {
        cantidadUnidadesInicial = mi_cantidadUnidadesInicial;
    },

    getCantidadUnidadesActual = function() {
        return cantidadUnidadesActual;
    },

    setCantidadUnidadesActual = function(mi_cantidadUnidadesActual) {
        cantidadUnidadesActual = mi_cantidadUnidadesActual;
    },

    getIdPosicionPicking = function() {
        return idPosicionPicking;
    },

    setIdPosicionPicking = function(mi_idPosicionPicking) {
        idPosicionPicking = mi_idPosicionPicking;
    },
            
    getPosicionPicking = function() {
        return posicionPicking;
    },
    
    setPosicionPicking = function(posicion) {
        posicionPicking = posicion;
    }
    
    getPesoInicial = function() {
        return pesoInicial;
    },

    setPesoInicial = function(mi_pesoInicial) {
        pesoInicial = mi_pesoInicial;
    },

    getPesoActual = function() {
        return pesoActual;
    },

    setPesoActual = function(mi_pesoActual) {
        pesoActual = mi_pesoActual;
    },

    getFechaAlta = function() {
        return fechaAlta;
    },

    setFechaAlta = function(mi_fechaAlta) {
        fechaAlta = mi_fechaAlta;
    },

    getFechaBaja = function() {
        return fechaBaja;
    },

    setFechaBaja = function(mi_fechaBaja) {
        fechaBaja = mi_fechaBaja;
    },

    getFechaUltimaActualizacion = function() {
        return fechaUltimaActualizacion;
    },

    setFechaUltimaActualizacion = function(mi_fechaUltimaActualizacion) {
        fechaUltimaActualizacion = mi_fechaUltimaActualizacion;
    },

    getUsuarioAlta = function() {
        return usuarioAlta;
    },

    setUsuarioAlta = function(mi_usuarioAlta) {
        usuarioAlta = mi_usuarioAlta;
    },

    getUsuarioBaja = function() {
        return usuarioBaja;
    },

    setUsuarioBaja = function(mi_usuarioBaja) {
        usuarioBaja = mi_usuarioBaja;
    },

    getUsuarioUltimaActualizacion = function() {
        return usuarioUltimaActualizacion;
    },

    setUsuarioUltimaActualizacion = function(mi_usuarioUltimaActualizacion) {
        usuarioUltimaActualizacion = mi_usuarioUltimaActualizacion;
    },

    getActivo = function() {
        return activo;
    },

    setActivo = function(mi_activo) {
        
        activo = mi_activo;
    },

    getEstado = function() {
        return estado;
    },

    setEstado = function(mi_estado) {
        estado = mi_estado;
    },

    getUsuarioActual = function() {
        return usuarioActual;
    },

    setUsuarioActual = function(mi_usuarioActual) {
        usuarioActual = mi_usuarioActual;
    },

    getCodigoDeBarras = function() {
        return codigoDeBarras;
    },

    setCodigoDeBarras = function(mi_codigoDeBarras) {
        codigoDeBarras = mi_codigoDeBarras;
    },
    
    getTipoContenedor = function() {
        return tipoContenedor;
    };
    
    return {
        getId : getId,
        getAncho : getAncho,
        setAncho : setAncho,
        getAlto : getAlto,
        setAlto : setAlto,
        getArticulo : getArticulo,
        setArticulo : setArticulo,
        getCantidadUnidadesInicial : getCantidadUnidadesInicial,
        setCantidadUnidadesInicial : setCantidadUnidadesInicial,
        getCantidadUnidadesActual : getCantidadUnidadesActual,
        setCantidadUnidadesActual : setCantidadUnidadesActual,
        getIdPosicionPicking : getIdPosicionPicking,
        setIdPosicionPicking : setIdPosicionPicking,  
        getPosicionPicking : getPosicionPicking,
        setPosicionPicking : setPosicionPicking,  
        getPesoInicial : getPesoInicial,
        setPesoInicial : setPesoInicial,
        getPesoActual : getPesoActual,
        setPesoActual : setPesoActual,
        getFechaAlta : getFechaAlta,
        setFechaAlta : setFechaAlta,
        getFechaBaja : getFechaBaja,
        setFechaBaja : setFechaBaja,
        getFechaUltimaActualizacion : getFechaUltimaActualizacion,
        setFechaUltimaActualizacion : setFechaUltimaActualizacion,
        getUsuarioAlta : getUsuarioAlta,
        setUsuarioAlta : setUsuarioAlta,
        getUsuarioBaja : getUsuarioBaja,
        setUsuarioBaja : setUsuarioBaja,
        getUsuarioUltimaActualizacion : getUsuarioUltimaActualizacion,
        setUsuarioUltimaActualizacion : setUsuarioUltimaActualizacion,
        getActivo : getActivo,
        setActivo : setActivo,
        getEstado : getEstado,
        setEstado : setEstado,
        getUsuarioActual : getUsuarioActual,
        setUsuarioActual : setUsuarioActual,
        getCodigoDeBarras : getCodigoDeBarras,
        setCodigoDeBarras : setCodigoDeBarras,
        getTipoContenedor : getTipoContenedor,
        setTipoContenedor : setTipoContenedor
    };
};