var UnidadContenedora = function(idEquipo) {    
    
    var ocupada = false,                //boolean
        usuarioActual,          //usuario    
        sectorAsignado,         //int
        medidas,                //String
        capacidad,              //double
        tipoUnidadContenedora,  //int
        nombre,                 //String
        codigoBarra;            //String
        
    var nuevoEquipo = Object.create(new Equipo(idEquipo));
    
    nuevoEquipo.setTipoEquipo(2);

    nuevoEquipo.isOcupada = function () {
        return ocupada;
    };

    nuevoEquipo.setOcupada = function (ocup) {
        ocupada = ocup;
    };

    nuevoEquipo.getUsuarioActual = function () {
        return usuarioActual;
    };

    nuevoEquipo.setUsuarioActual = function (usuario) {
        usuarioActual = usuario;
    };

    nuevoEquipo.getSectorAsignado = function () {
        return sectorAsignado;
    };

    nuevoEquipo.setSectorAsignado = function (sector) {
        sectorAsignado = sector;
    };

    nuevoEquipo.getMedidas = function () {
        return medidas;
    };

    nuevoEquipo.setMedidas = function (medida) {
        medidas = medida;
    };

    nuevoEquipo.getCapacidad = function() {
        return capacidad;
    };

    nuevoEquipo.setCapacidad = function(cap) {
        capacidad = cap;
    };

    nuevoEquipo.getTipoUnidadContenedora = function() {
        return tipoUnidadContenedora;
    };

    nuevoEquipo.setTipoUnidadContenedora = function(tipo) {
        tipoUnidadContenedora = tipo;
    };

    nuevoEquipo.getNombre = function () {
        return nombre;
    };

    nuevoEquipo.setNombre = function(nom) {
        nombre = nom;
    };

    nuevoEquipo.getCodigoBarra = function () {
        return codigoBarra;
    };

    nuevoEquipo.setCodigoBarra = function (codigoBar) {
        codigoBarra = codigoBar;
    };
    
    return nuevoEquipo;
};