var TipoDeEntrega = function(idTipoEntr) {

    var idTipoEntrega = idTipoEntr,
        numeroTipoEntrega,
        detalleTipoEntrega,
        bufferAsociado,

        getNumeroTipoEntrega = function(){
            return numeroTipoEntrega;
        },

        setNumeroTipoEntrega = function(numero) {
            numeroTipoEntrega = numero;
        },

        getDetalleTipoEntrega = function() {
            return detalleTipoEntrega;
        },

        setDetalleTipoEntrega = function(detalleTipoEntr) {
            detalleTipoEntrega = detalleTipoEntr;
        },

        getBufferAsociado = function() {
            return bufferAsociado;
        },

        setBufferAsociado = function(bufferAsoc) {
            bufferAsociado = bufferAsoc;
        },

        getIdTipoEntrega = function() {
            return idTipoEntrega;
        };
        
    return {
        getNumeroTipoEntrega : getNumeroTipoEntrega, 
        setNumeroTipoEntrega : setNumeroTipoEntrega,
        getDetalleTipoEntrega : getDetalleTipoEntrega,
        setDetalleTipoEntrega : setDetalleTipoEntrega,
        getBufferAsociado : getBufferAsociado,
        setBufferAsociado : setBufferAsociado,
        getIdTipoEntrega : getIdTipoEntrega
    };
}