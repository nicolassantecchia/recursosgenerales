var EstadoParte = function(mi_id, mi_nombre) {
        
    var numeroEstado = mi_id,
        nombre = mi_nombre,
        detalle,
        detalleAcotado,
    
    getNumeroEstado = function() {
        return numeroEstado;
    },

    getNombre = function() {
        return nombre;
    },

    getDetalle = function() {
        return detalle;
    },

    setDetalle = function(det) {
        detalle = det;
    },

    getDetalleAcotado = function() {
        return detalleAcotado;
    },

    setDetalleAcotado = function(det) {
        detalleAcotado = det;
    };
    
    return {
        getNumeroEstado : getNumeroEstado,
        getNombre : getNombre,
        getDetalle : getDetalle,
        setDetalle : setDetalle,
        getDetalleAcotado : getDetalleAcotado,
        setDetalleAcotado : setDetalleAcotado,
    };
}