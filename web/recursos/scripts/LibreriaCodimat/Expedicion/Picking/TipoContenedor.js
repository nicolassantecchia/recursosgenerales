var TipoContenedor = function(mi_id, mi_detalle) {
    
    var id = mi_id, //int 
        detalle = mi_detalle, //String
        pesoEstandar = 0, //double
        anchoEstandar = 0, //double
        altoEstandar = 0, //double
        abreviacion = "", //String
        prefijoCodigoBarras = "", //String
        habilitado = true, //boolean    
    
    getId = function() {
        return id;
    },

    getDetalle = function() {        
        return detalle;
    },

    getAnchoEstandar = function() {
        return anchoEstandar;
    },

    setAnchoEstandar = function(ancho) {
        anchoEstandar = ancho;
    },

    getAltoEstandar = function() {
        return altoEstandar;
    },

    setAltoEstandar = function(alto) {
        altoEstandar = alto;
    },

    getPesoEstandar = function() {
        return pesoEstandar;
    },

    setPesoEstandar = function(peso) {
        pesoEstandar = peso;
    },

    getAbreviacion = function() {
        return abreviacion;
    },

    setAbreviacion = function(abbr) {
        abreviacion = abbr;
    },

    getPrefijoCodigoBarras = function() {
        return prefijoCodigoBarras;
    },

    setPrefijoCodigoBarras = function(prefijo) {
        prefijoCodigoBarras = prefijo;
    },

    getHabilitado = function() {        
        return habilitado;
    },

    setDetalle = function(det) {
        detalle = det;
    },

    setHabilitado = function(hab) {
        
        habilitado = hab;
    };
    
    return {
        getId : getId,
        getDetalle : getDetalle,
        getAnchoEstandar : getAnchoEstandar,
        setAnchoEstandar : setAnchoEstandar,
        getAltoEstandar : getAltoEstandar,
        setAltoEstandar : setAltoEstandar,
        getPesoEstandar : getPesoEstandar,
        setPesoEstandar : setPesoEstandar,
        getAbreviacion : getAbreviacion,
        setAbreviacion : setAbreviacion,
        getPrefijoCodigoBarras : getPrefijoCodigoBarras,
        setPrefijoCodigoBarras : setPrefijoCodigoBarras,
        getHabilitado : getHabilitado,
        setDetalle : setDetalle,
        setHabilitado : setHabilitado
    }
}