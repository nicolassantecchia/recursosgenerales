var ArticuloPartePicking = function(idArt) {
    
    var idArticulo = idArt,        //int
        detalleArticulo = null,      //String
        cantidadRequerida = 0.0,   //double
        cantidadPiqueada = 0.0,    //double
        cantidadControlada = 0.0,  //double
        posicion = null,             //String
        fechaPiqueo = null,        //Fecha
        estadoPiqueo = 0,          //int
        codigoBarrasPosicion = null, //String
        codigoBarrasArticulo = null, //String
        pesoAproximado = 0.0,      //double
        pesoPosicion = 0,          //int

        getDetalleArticulo = function() {
            return detalleArticulo;
        },

        setDetalleArticulo = function(detalle) {
            detalleArticulo = detalle;
        },

        getCantidadRequerida = function() {
            return cantidadRequerida;
        },

        getPosicion = function() {
            return posicion;
        },

        getFechaPiqueo = function() {
            return fechaPiqueo;
        },

        getIdArticulo = function() {
            return idArticulo;
        },

        getEstadoPiqueo = function() {
            return estadoPiqueo;
        },

        getCodigoBarrasPosicion = function() {
            return codigoBarrasPosicion;
        },

        setCodigoBarrasPosicion = function(codigoBarras) {
            codigoBarrasPosicion = codigoBarras;
        },

        getCodigoBarrasArticulo = function() {
            return codigoBarrasArticulo;
        },

        setCodigoBarrasArticulo = function(codigoBarras) {
            codigoBarrasArticulo = codigoBarras;
        },

        setEstadoPiqueo = function(estado) {
            estadoPiqueo = estado;
        },

        setFechaPiqueo = function(fecha) {
            fechaPiqueo = fecha;
        },

        setPosicion = function(posicion) {
            posicion = posicion;
        },

        setCantidadRequerida = function(cantidad) {
            cantidadRequerida = cantidad;
        },

        getCantidadPiqueada = function() {
            return cantidadPiqueada;
        },

        setCantidadPiqueada = function(cantidad) {
            cantidadPiqueada = cantidad;
        },

        getCantidadControlada = function() {
            return cantidadControlada;
        },

        setCantidadControlada = function(cantidad) {
            cantidadControlada = cantidad;
        },

        getPesoAproximado = function() {
            return pesoAproximado;
        },

        setPesoAproximado = function(peso) {
            pesoAproximado = peso;
        },

        getPesoPosicion = function() {
            return pesoPosicion;
        },

        setPesoPosicion = function(peso) {
            pesoPosicion = peso;
        };

        return {
            getDetalleArticulo : getDetalleArticulo,
            setDetalleArticulo : setDetalleArticulo,
            getCantidadRequerida : getCantidadRequerida,
            getPosicion : getPosicion,
            getFechaPiqueo : getFechaPiqueo,
            getIdArticulo : getIdArticulo,
            getEstadoPiqueo : getEstadoPiqueo,
            getCodigoBarrasPosicion : getCodigoBarrasPosicion,
            setCodigoBarrasPosicion : setCodigoBarrasPosicion,
            getCodigoBarrasArticulo : getCodigoBarrasArticulo,
            setCodigoBarrasArticulo : setCodigoBarrasArticulo,
            setEstadoPiqueo : setEstadoPiqueo,
            setFechaPiqueo : setFechaPiqueo,
            setPosicion : setPosicion,
            setCantidadRequerida : setCantidadRequerida,
            getCantidadPiqueada : getCantidadPiqueada,
            setCantidadPiqueada : setCantidadPiqueada,
            getCantidadControlada : getCantidadControlada,
            setCantidadControlada : setCantidadControlada,
            getPesoAproximado : getPesoAproximado,
            setPesoAproximado : setPesoAproximado,
            getPesoPosicion : getPesoPosicion,
            setPesoPosicion : setPesoPosicion
        }
}
