/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/Contenedor.js
 */
var ListaDeContenedores = function() {
    
    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(id_cont) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() === id_cont) {
                return itemActual;
            }  
            else {
                itemActual = null;
            }
        }
        return itemActual;
    };
    
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordenarPorID = function(mayorAmenor) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(mayorAmenor == true) {
            
            //De mayor a menor
            this.ordenar(function(contenedor1, contenedor2) {    
                
                if(contenedor1.getId() < contenedor2.getId()) {
                    return 1;
                }
                else {
                    if(contenedor1.getId() > contenedor2.getId()) {
                        return -1;
                    }
                    else { //iguales
                        return 0;
                    }
                }                
            }); 
        }
        else { 
                       
            this.ordenar(function(contenedor1, contenedor2) {  
                
                if(contenedor1.getId() < contenedor2.getId()) {
                    return -1;
                }
                else {
                    if(contenedor1.getId() > contenedor2.getId()) {
                        return 1;
                    }
                    else { //iguales
                        return 0;
                    }
                }  
            }); 
        }      
    };

    return nuevaLista;
};