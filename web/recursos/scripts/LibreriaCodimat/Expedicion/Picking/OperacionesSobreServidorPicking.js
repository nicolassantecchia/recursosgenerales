/*
 *  ---------------------------------------------
 *  Fernando Prieto
 *  REQUIERE configuracion.js que se encuentra en proyecto expedicion/recursos/scripts/config
 *  ---------------------------------------------
 */
OperacionesSobreServidorPicking = function() {
    
        var opSistem = new OperacionesSobreServidorSistema(), //Objeto creado para poder utilizar el metodo manejoErroresSesion
            opPicking_utiles = new Utiles(), //Objeto creado para poder utlu
	                
        getMensajeErrorasignarParteAPartirDeCodigoBarrasUC = function(idUC) {	
            return "Ocurri&oacute; un error al recuperar el parte asociado a la unidad transportadora <em>"+idUC+"</em>.";
        },
                
        getMensajeErrorRecuperarPartePickingPorNumero = function(numeroParte) {
            return "Ocurri&oacute; un error al recuperar el parte n&uacute;mero <em>"+numeroParte+"</em>.";
        },
                
        getMensajeErrorControlParte = function() {	
            return "Ocurri&oacute; un error al controlar el parte.";
        },
                
        getMensajeErroraCrearContenedor = function() {
            return "Ocurri&oacute; un error al crear el contenedor.";
        },
                
        getMensajeErrorAlRecuperarListaPartes = function() {
            return "Ocurri&oacute; un error al recuperar la lista de partes.";
        },
                
        getMensajeErrorAlRecuperarDatosRapidosParte = function() {
            return "Ocurri&oacute; un error al recuperar el estado de las colas.";
        },
                
        getMensajeErrorAlRecuperarLeyendaEntregaParte = function() {
            return "Ocurri&oacute; un error al recuperar las leyendas de entrega del parte.";
        },
                
        getMensajeErrorAsignarParteAUsuario = function() {
            return "Ocurri&oacute; un error al asignar el parte.";
        },
                
        getMensajeErrorRetrasarParte = function() {
            return "Ocurri&oacute; un error al retrasar el parte.";
        },
                
        getMensajeErrorAnularParte = function() {
            return "Ocurri&oacute; un error al anular el parte.";
        },
                
        getMensajeErrorSetearPartePendientePreparacion = function() {
            return "Ocurri&oacute; un error al establece el parte como pendiente de preparación.";
        },
                
        getMensajeErrorAlRecuperarContenedoresPorArticulo = function() {
            return "Ocurri&oacute; un error al recuperar los contenedores asociados al art&iacute;culo.";
        },
                
        getMensajeErrorAlRecuperarElContenedor = function() {
            return "Ocurri&oacute; un error al recuperar el contenedor.";
        },
                
        getMensajeErrorAlRecuperarPosicion = function() {
            return "Ocurri&oacute; un error al recuperar la posici&oacute;n.";
        },
                
        getMensajeErrorAlRecuperarDatosConfiguracion = function() {
            return "Ocurri&oacute; un error al recuperar los datos de Configuración.";
        },
                        
        crearObjetoParteDesdeXML = function($parte) {
            
            var parteDepositoPicking = new ParteDeDepositoPicking(parseInt($parte.attr('numeroParte')));
            
            parteDepositoPicking.setIdAutoincremental(parseInt($parte.attr('registroEnBD')));
            parteDepositoPicking.setNaveOGalpon(parseInt($parte.attr('idGalpon')));
            
            parteDepositoPicking.setTipoParte(parseInt($parte.attr('tipoParte')));
            
            parteDepositoPicking.setNroCPRBAsociado($parte.attr('numeroCprbAsociado'));
            parteDepositoPicking.setTipoDeCPRBAsociado(parseInt($parte.attr('tipoCprbAsociado')));
            
            parteDepositoPicking.setNroCPRBAsociadoParaMostrar($parte.attr('numeroCprbAsociadoParaMostrar'));
            parteDepositoPicking.setTipoDeCPRBAsociadoParaMostrar(parseInt($parte.attr('tipoCprbAsociadoParaMostrar')));
            
            parteDepositoPicking.setNombreCliente($parte.attr('nombreCliente'));
            parteDepositoPicking.setCodigoBarrasCPRBAsociado($parte.attr('codBarrasCprbAsociado'));

            parteDepositoPicking.setPosicionEnLista(parseInt($parte.attr('posicionEnLista')));
                        
            var estadoParte = new EstadoParte(parseInt($parte.attr('estado')), "");
            
            estadoParte.setDetalle($parte.attr('estadoDetalle'));
            estadoParte.setDetalleAcotado($parte.attr('estadoDetalleAcotado'));
            
            parteDepositoPicking.setEstado(estadoParte);
            
            parteDepositoPicking.setTieneUsuarioAsignadoEspecialmente(opPicking_utiles.convertirStringABoolean($parte.attr('tieneUsuarioAsignadoEspecialmente')));
            
            if($parte.attr('idUsuarioAsignadoEspecialmente') != null) { 
                parteDepositoPicking.setUsuarioAsignadoEspecialmente(new Usuario(parseInt($parte.attr('idUsuarioAsignadoEspecialmente')), $parte.attr('nombreUsuarioAsignadoEspecialmente')));
            }
            
            if($parte.attr('idUsuarioCreador') != null) {
                parteDepositoPicking.setUsuarioCreador(new Usuario(parseInt($parte.attr('idUsuarioCreador')), $parte.attr('nombreUsuarioCreador')));
            }
            parteDepositoPicking.setFechaCreacion(new Fecha($parte.attr('fechaCreacionParte'), $parte.attr('horaCreacionParte')));
            
            if($parte.attr('idUsuarioPreparador') != null) {
                parteDepositoPicking.setUsuarioPrepardor(new Usuario(parseInt($parte.attr('idUsuarioPreparador')), $parte.attr('nombreUsuarioPreparador')));
            }
            parteDepositoPicking.setFechaInicioPreparacion(new Fecha($parte.attr('fechaInicioPreparacion'), $parte.attr('horaInicioPreparacion')));
            parteDepositoPicking.setFechaFinPreparacion(new Fecha($parte.attr('fechaFinPreparacion'), $parte.attr('horaFinPreparacion')));
            
            if($parte.attr('idUsuarioControl') != null) {
                parteDepositoPicking.setUsuarioControlador(new Usuario(parseInt($parte.attr('idUsuarioControl')), $parte.attr('nombreUsuarioControl')));
            }
            parteDepositoPicking.setFechaInicioControl(new Fecha($parte.attr('fechaInicioControl'), $parte.attr('horaInicioControl')));
            parteDepositoPicking.setFechaFinControl(new Fecha($parte.attr('fechaFinControl'), $parte.attr('horaFinControl')));
            
            if($parte.attr('idUsuarioControlSalida') != null) {
                parteDepositoPicking.setUsuarioSalida(new Usuario(parseInt($parte.attr('idUsuarioControlSalida')), $parte.attr('nombreUsuarioControlSalida')));
            }
            parteDepositoPicking.setFechaControlSalida(new Fecha($parte.attr('fechaControlSalida'), $parte.attr('horaControlSalida')));
            
            if($parte.attr('idUsuarioSuspension') != null) {
                parteDepositoPicking.setUsuarioSuspende(new Usuario(parseInt($parte.attr('idUsuarioSuspension')), $parte.attr('nombreUsuarioSuspension')));
            }
            
            parteDepositoPicking.setFechaSuspension(new Fecha($parte.attr('fechaSuspension'), $parte.attr('horaSuspension')));
            
            parteDepositoPicking.setFechaUltimoUpdate(new Fecha($parte.attr('fechaUltActualizacion'), $parte.attr('horaUltActualizacion')));
            
            var tipoEntrega = new TipoDeEntrega(parseInt($parte.attr('idTipoEntrega')));
            tipoEntrega.setNumeroTipoEntrega($parte.attr('numeroTipoEntrega'));
            tipoEntrega.setDetalleTipoEntrega($parte.attr('detalleTipoEntrega'));
            
            parteDepositoPicking.setTipoDeEntrega(tipoEntrega);
            
            var $bultos = $parte.find("bultos");
            
            if($bultos.length != 0) {
                
                var listaBultos = parteDepositoPicking.getListaDeBulto();   
                var bulto = null;
                
                $bultos.find("bulto").each(function(){  
                    
                    bulto = new Bulto(parseInt($(this).attr('numero')));
                    bulto.setCodigoBarras($(this).attr('codigoBarras'));
                    
                    listaBultos.insert(bulto);                                                                
                });
            }
            
            var $chofer = $parte.find("chofer");
            
            if($chofer.length != 0) {
                
                var chofer = new Chofer($chofer.attr("nombre"));
                chofer.setId($(this).attr("idChofer"));
                
                var $transporte = $chofer.find("transporteAsociado");
                
                if($transporte.length != 0) {
                    
                    var transporte = new Transporte($transporte.attr("id"), $transporte.attr("nombre"));

                    var docIdent = new DocumentoIdentificador(0);
                    docIdent.setNro($transporte.attr("CUIT"));

                    transporte.setDocumentoIdentificador(docIdent);								
                    transporte.setTelefono1($transporte.attr("telefono1"));
                    
                    chofer.setTransporteAsociado(transporte);
                }
                
                parteDepositoPicking.setChofer(chofer);
            }
            
            var $leyendas = $parte.find("leyendas");
            
            if($leyendas.length != 0) {
                
                var listaLeyendas = new Lista();   
                
                $leyendas.find("leyenda").each(function(){
                    listaLeyendas.insert($(this).attr('texto'));                                                                
                });
                
                parteDepositoPicking.setListaDeLeyendas(listaLeyendas);
            }
            
            var $equipoDeArmado = $parte.find("equipoDeTrabajo");
    
            if($equipoDeArmado.length != 0) {

                var equipoDeArmado = new EquipoDeArmadoDePartes(parseInt($equipoDeArmado.attr('id')));

                //poner piqueadora

                var unidadContenedora = null;

                $equipoDeArmado.find("unidadContenedora").each(function(){

                    unidadContenedora = new UnidadContenedora(parseInt($(this).attr('id')));
                    unidadContenedora.setNombre($(this).attr('nombre'));

                    equipoDeArmado.getListaDeUnidadesContenedoras().insert(unidadContenedora);
                }); 

                parteDepositoPicking.setEquipoDeArmadoDePartes(equipoDeArmado);
            }

            var articulo = null;

            $parte.find("articulo").each(function(){

                articulo = new ArticuloPartePicking(parseInt($(this).attr("id")));

                articulo.setDetalleArticulo($(this).attr("detalle"));
                articulo.setCantidadRequerida(parseFloat($(this).attr("cantidadRequerida")));
                articulo.setCantidadPiqueada(parseFloat($(this).attr("cantidadPiqueada")));
                articulo.setCantidadControlada(parseFloat($(this).attr("cantidadControlada")));
                
                parteDepositoPicking.getListaDeArticulos().insert(articulo);
            });
            
            var listaEventos = parteDepositoPicking.getListaDeEventosParte();
            
            $parte.find("evento").each(function() {
                
                var evento = new EventoParte($(this).attr("id"), new TipoEvento($(this).attr("tipo"), $(this).attr("detalleTipo")), parteDepositoPicking.getNumeroDeParte(), parteDepositoPicking.getNaveOGalpon(), usuario);
                
                var usuario = null; 
                
                if($(this).attr('idUsuarioAsociado') != null) {
                    usuario = new Usuario(parseInt($(this).attr('idUsuarioAsociado')), $(this).attr('nombreUsuarioAsociado'));
                }
                
                evento.setUsuarioAsociado(usuario);
                
                evento.setFecha(new Fecha($(this).attr('fecha'), $(this).attr('hora')));
                evento.setDetalleEvento($(this).attr('detalle'));
                evento.setComentario($(this).attr('comentario'));
                                        
                var estadoAnterior = new EstadoParte(parseInt($(this).attr('idEstadoAnterior')), "");            
                estadoAnterior.setDetalle($(this).attr('detalleEstadoAnterior'));
                estadoAnterior.setDetalleAcotado($(this).attr('detalleAcotadoEstadoAnterior'));
                
                evento.setEstadoAnteriorAlEvento(estadoAnterior);
                
                var estadoPosterior = new EstadoParte(parseInt($(this).attr('idEstadoPosterior')), "");            
                estadoPosterior.setDetalle($(this).attr('detalleEstadoPosterior'));
                estadoPosterior.setDetalleAcotado($(this).attr('detalleAcotadoEstadoPosterior'));
                
                evento.setEstadoPosteriorAlEvento(estadoPosterior);
                
                evento.setFechaAuxiliar(new Fecha($(this).attr('fechaAuxiliar'), $(this).attr('horaAuxiliar')));
                evento.setEnteroAuxiliar(parseInt($(this).attr('enteroAuxiliar')));
                evento.setStringAuxiliar($(this).attr('stringAuxiliar'));
                
                listaEventos.insert(evento);
            });
            
            return parteDepositoPicking;
        },
                
        crearObjetoContenedorDesdeXML = function($contenedor) {
                                   
            var tipoContenedor = new TipoContenedor(parseInt($contenedor.attr('tipoId')), $contenedor.attr('tipoDetalle'));
                tipoContenedor.setAnchoEstandar(parseFloat($contenedor.attr('anchoEstandar')));
                tipoContenedor.setAltoEstandar(parseFloat($contenedor.attr('altoEstandar')));
                tipoContenedor.setPesoEstandar(parseFloat($contenedor.attr('pesoEstandar')));
                tipoContenedor.setAbreviacion($contenedor.attr('tipoAbreviacion'));
                tipoContenedor.setPrefijoCodigoBarras($contenedor.attr('prefijoCodBarras'));
                tipoContenedor.setDetalle($contenedor.attr('tipoDetalle'));
                tipoContenedor.setHabilitado(opPicking_utiles.convertirStringABoolean($contenedor.attr('habilitado')));
                        
            var contenedor = new Contenedor(parseInt($contenedor.attr('id')));
            
                contenedor.setTipoContenedor(tipoContenedor);
                
                contenedor.setCodigoDeBarras($contenedor.attr('codigoBarras'));
                
                contenedor.setCantidadUnidadesInicial(parseFloat($contenedor.attr('cantidadInicial')));
                contenedor.setCantidadUnidadesActual(parseFloat($contenedor.attr('cantidadActual')));
                
                contenedor.setPesoInicial(parseFloat($contenedor.attr('pesoInicial')));
                contenedor.setPesoActual(parseFloat($contenedor.attr('pesoActual')));
                contenedor.setAncho(parseFloat($contenedor.attr('ancho')));
                contenedor.setAlto(parseFloat($contenedor.attr('alto')));
                
                contenedor.setActivo(opPicking_utiles.convertirStringABoolean($contenedor.attr('activo'))); 
                contenedor.setEstado($contenedor.attr('estado'));  
                
                contenedor.setIdPosicionPicking(parseInt($contenedor.attr('idPosicionPicking')));
                
                contenedor.setArticulo(new Articulo(parseInt($contenedor.attr('codigoArticulo')), $contenedor.attr('detalleArticulo')));      
                
                if($contenedor.attr('usuarioActualId') != null) { 
                    contenedor.setUsuarioActual(new Usuario(parseInt($contenedor.attr('usuarioActualId')), $contenedor.attr('usuarioActualNombre')));
                }
                
                if($contenedor.attr('usuarioAltaId') != null) {                
                    contenedor.setUsuarioAlta(new Usuario(parseInt($contenedor.attr('usuarioAltaId')), $contenedor.attr('usuarioAltaNombre')));
                }
                
                if($contenedor.attr('usuarioBajaId') != null) {                
                    contenedor.setUsuarioBaja(new Usuario(parseInt($contenedor.attr('usuarioBajaId')), $contenedor.attr('usuarioBajaNombre')));
                }
                                
                if($contenedor.attr('usuarioUltActId') != null) {                
                    contenedor.setUsuarioUltimaActualizacion(new Usuario(parseInt($contenedor.attr('usuarioUltActId')), $contenedor.attr('usuarioUltActNombre')));
                }
                
                if($contenedor.attr('fechaAlta') != null && $contenedor.attr('horaAlta') != null) {
                    contenedor.setFechaAlta(new Fecha($contenedor.attr('fechaAlta'), $contenedor.attr('horaAlta')));
                }
                
                if($contenedor.attr('fechaBaja') != null && $contenedor.attr('horaBaja') != null) {
                    contenedor.setFechaBaja(new Fecha($contenedor.attr('fechaBaja'), $contenedor.attr('horaBaja')));   
                }
                
                if($contenedor.attr('fechaUltAct') != null && $contenedor.attr('horaUltAct') != null) {
                    contenedor.setFechaUltimaActualizacion(new Fecha($contenedor.attr('fechaUltAct'), $contenedor.attr('horaUltAct')));
                }
            
            return contenedor;
        },
                
        crearObjetoPosicionDesdeXML = function($posicion) {
            
            var posicion = null;
            
            if($posicion.length > 0) {
            
                var posicion = new PosicionPicking(parseInt($posicion.attr('id')));
                    
                if($posicion.attr('usuarioActualId') != null) { 
                    posicion.setUsuarioActual(new Usuario(parseInt($posicion.attr('usuarioActualId')), $posicion.attr('usuarioActualNombre')));
                }
                posicion.setNave(parseInt($posicion.attr('nave')));
                posicion.setCalle(parseInt($posicion.attr('calle')));
                posicion.setColumna(parseInt($posicion.attr('columna')));
                posicion.setFila(parseInt($posicion.attr('fila')));
                posicion.setAncho(parseFloat($posicion.attr('ancho')));
                posicion.setAlto(parseFloat($posicion.attr('alto')));
                posicion.setActiva(opPicking_utiles.convertirStringABoolean($posicion.attr('activa')));
                posicion.setPesoMax(parseFloat($posicion.attr('pesoMaximo')));

                posicion.setEstadoId(parseInt($posicion.attr('estadoId')));
                posicion.setEstadoDetalle($posicion.attr('estadoDetalle'));

                posicion.setPesoRecorrido(parseFloat($posicion.attr('pesoRecorrido')));
                posicion.setFechaAlta(new Fecha($posicion.attr('fechaAlta'), $posicion.attr('horaAlta')));
                
                if($posicion.attr('fechaUltAct') != null) { 
                    posicion.setFechaUltimaActualizacion(new Fecha($posicion.attr('fechaUltAct'), $posicion.attr('horaUltAct')));
                }
                
                // FALTA EL SETEO DE LOS CONTENEDORES DE ESTA POSICION - AUN NO HA SIDO NECESARIO.
                //posicion.setListaDeContenedores();
                posicion.setCodigoDeBarras($posicion.attr('codigoBarras'));
                posicion.setTipoPosicion(parseInt($posicion.attr('tipoId')));
                posicion.setDetalleTipoPosicion($posicion.attr('tipoDetalle'));                
            }
            
            return posicion;
        },
                
        /**
         * ************************************
         * PICKING
         * ************************************
         */
                
        asignarParteAPartirDeCodigoBarrasUC = function(data, serializado) {	
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            } 
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, nombreUsuario : data.nombreUsuario, codigoBarrasUC : data.codigoBarras, recuperarLeyendas : data.recuperarLeyendas, idGalpon: data.idGalpon, urlForward : data.urlForward, urlLogout : data.urlLogout}
            }

            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",                
                url: CONFIG.proyectos.expedicion.controladores.asignarParteAPartirDeCodigoBarrasUC, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                               
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var parteDepositoPicking = null;

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	                            
                            
                            var $parte = $(xmlResponse).find("parte");
                            
                            if($parte.length != 0) {
                                
                                parteDepositoPicking = crearObjetoParteDesdeXML($parte);
                                
                            }
                            //callback
                            data.onSuccess(parteDepositoPicking, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorasignarParteAPartirDeCodigoBarrasUC(data.codigoBarras), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorasignarParteAPartirDeCodigoBarrasUC(data.codigoBarras), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        /**
         * Hace la recuperacion buscando tanto entre los partes que se encuentran en transito como entre los que se encuentran archivados.
         * Parametros a especificar en el objeto data:
         * 
         *  idUsuario,
         *  numeroParte,
         *  estadoParte (opcional),
         *  idGalpon,
         *  urlForward, 
         *  urlLogout, 
         *  busquedaGeneral (opcional)
         *  
         * @param {type} data
         * @param {type} serializado
         * @returns {undefined}
         */
        recuperarPartePickingPorNumero = function(data, serializado) {	
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            } 
            
            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, numeroParte : data.numeroParte, estadoParte : data.estadoParte, recuperarEquipoDeTrabajo : data.recuperarEquipoDeTrabajo, 
                            recuperarChoferes : data.recuperarChoferes, recuperarLeyendas : data.recuperarLeyendas, recuperarArticulos : data.recuperarArticulos, idGalpon: data.idGalpon, urlForward : data.urlForward, urlLogout : data.urlLogout};
                                        
                if(data.busquedaGeneral) {
                   dataAjax.busquedaGeneral = data.busquedaGeneral;
                }
            }

            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",                
                url: CONFIG.proyectos.expedicion.controladores.recuperarPartePickingPorNumero, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var parteDepositoPicking = null;

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	                            
                            
                            var $parte = $(xmlResponse).find("parte");
                            
                            if($parte.length != 0) {
                                
                                parteDepositoPicking = crearObjetoParteDesdeXML($parte);
                                
                            }
                            //callback
                            data.onSuccess(parteDepositoPicking, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorRecuperarPartePickingPorNumero(data.numeroParte), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorRecuperarPartePickingPorNumero(data.numeroParte), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        controlarParte = function(data, serializado) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }  
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            
            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.controlarParte, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            //callback
                            data.onSuccess($mensaje, data.parametrosExtra);
                        }
                        else {
                            
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorControlParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorControlParte(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        cancelarControlParte = function(data, serializado) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }    
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {
                    idUsuario : data.idUsuario,
                    numeroParte : data.numeroParte,
                    idGalpon : data.idGalpon,
                    urlForward : data.urlForward, 
                    urlLogout : data.urlLogout
                }
            }
            
            $.ajax({ 

                data: dataAjax,
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.cancelarControlParte, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {   
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            //callback
                            data.onSuccess($mensaje.attr('mensaje'), data.parametrosExtra);
                        }
                        else {
                            
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorControlParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorControlParte(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        retrasarParte = function(data, serializado) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }  
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idGalpon : data.idGalpon, numeroParte : data.numeroParte, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }
            
            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.retrasarParte, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            //callback
                            data.onSuccess($mensaje, data.parametrosExtra);
                        }
                        else {
                            
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorRetrasarParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorRetrasarParte(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        anularParte = function(data, serializado) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }  
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idGalpon : data.idGalpon, numeroParte : data.numeroParte, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }
            
            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.anularParte, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            //callback
                            data.onSuccess($mensaje, data.parametrosExtra);
                        }
                        else {
                            
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorAnularParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAnularParte(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        setearPartePendientePreparacion = function(data, serializado) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }  
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idGalpon : data.idGalpon, numeroParte : data.numeroParte, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }
            
            $.ajax({ 

                data: dataAjax, // get the form data
                type: "POST",
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.setearPartePendientePreparacion, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {
                            //callback
                            data.onSuccess($mensaje, data.parametrosExtra);
                        }
                        else {
                            
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorSetearPartePendientePreparacion(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorSetearPartePendientePreparacion(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
        
        crearContenedor = function(data, serializado) {
            
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = { idUsuario : data.idUsuario, nombreUsuario : data.nombreUsuario, tipoContenedor : data.tipoContenedor, idArticulo : data.idArticulo,  detalleArticulo : data.detalleArticulo, cantidadUnidadesArticuloContenedor : data.cantidadUnidadesArticuloContenedor, peso : data.peso, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.crearContenedor, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var contenedor = null;

                        if($mensaje.attr("estadoOperacion") == "ok")
                        {	
                            var $contenedor = $(xmlResponse).find("contenedor");
                            
                            if($contenedor.length != 0) {                                
                                contenedor = crearObjetoContenedorDesdeXML($contenedor);
                            }  
                            
                            //callback
                            data.onSuccess(contenedor, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErroraCrearContenedor(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErroraCrearContenedor(), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
          
        /**
         * Recupera la lista de partes que se corresponde con los siguientes parámetros:
         * {idUsuario : , estadoParte: , recuperarArticulos : , recuperarChoferes : }
         * Alternativamente estos datos podrían ser provistos mediante un String serializado.
         * 
         * @param {type} data dato a enviar al servidor
         * @param {type} serializado <code>true</code> los datos a enviar vienen en un string serializado. Ej: algo=asd&algo1=ased. <code>false</code> los datos a enviar vienen en un formato de objetos.
         * @returns {undefined}
         */
        recuperarListaDePartes = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idGalpon : data.idGalpon, estadoParte: data.arregloEstadosParte, recuperarArticulos : data.recuperarArticulos, recuperarChoferes : data.recuperarChoferes, recuperarEventos : data.recuperarEventos};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarListaDePartes, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var contenedor = null;

                        if($mensaje.attr("estadoOperacion") == "ok") {	
                            
                            var listaPartes = new ListaDePartesPicking();
                            
                            var $fechaConsulta = $(xmlResponse).find("fechaConsulta");
                            listaPartes.setFechaConsulta(new Fecha($fechaConsulta.attr('fecha'), $fechaConsulta.attr('hora')));
                                                                                                                
                            $(xmlResponse).find("parte").each(function() {
                                 
                                listaPartes.insert(crearObjetoParteDesdeXML($(this)));
                            });
                            
                            //callback
                            console.log('operacionesSobreServidorPicking recuperarListaDePartes success');
                            data.onSuccess(listaPartes, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorAlRecuperarListaPartes(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarListaPartes(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarDatosRapidosParte = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idGalpon : data.idGalpon};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarDatosRapidosParte, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    
                    try 
                    {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var contadoresPartes = null;

                        if($mensaje.attr("estadoOperacion") == "ok") {	
                            
                            var $contadoresPartes = $(xmlResponse).find("contadoresDepartes");
                                                        
                            contadoresPartes = new ContadoresPartes();
                            
                            var listaEstadoColas = contadoresPartes.getListaEstadoColas();
                                                        
                            $contadoresPartes.find("estado").each(function(){
                             
                                var estado = new EstadoColasPicking(parseInt($(this).attr("id")));
                                
                                estado.setNombreCola($(this).attr("nombre"));
                                estado.setDetalleCola($(this).attr("detalle"));
                                estado.setCantidadPartes(parseInt($(this).attr("cantidad")));
                                
                                listaEstadoColas.insert(estado);                                                                
                            });                            
                            
                            //callback
                            data.onSuccess(listaEstadoColas, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorAlRecuperarDatosRapidosParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarDatosRapidosParte(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarLeyendaEntregaParte  = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {tipoCPRB : data.tipoCPRB, numeroCPRB : data.numeroCPRB};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarLeyendaEntregaParte, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    
                    try {
                        var $mensaje = $(xmlResponse).find("mensaje");
                        
                        var listaLeyendas = new Lista();

                        if($mensaje.attr("estadoOperacion") == "ok") {	
                            $(xmlResponse).find("leyenda").each(function(){
                                listaLeyendas.insert($(this).attr('texto'));                                                                
                            });                            
                            
                            //callback
                            data.onSuccess(listaLeyendas, data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorAlRecuperarLeyendaEntregaParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarLeyendaEntregaParte(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        reordenarPartes = function(data, serializado) {
            
            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }
            
            var dataAjax;
            
            if(serializado) {
                dataAjax = data.data;
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.reordenarPartes, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    
                    try {
                        
                        var $mensaje = $(xmlResponse).find("mensaje");
                                                

                        if($mensaje.attr("estadoOperacion") == "ok") {	                        
                            
                            //callback
                            data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorReordenarParte(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorReordenarParte(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            })
        },
                    
        asignarParteAUsuario = function(data, serializado) {

            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idGalpon : data.idGalpon, numeroParte : data.numeroParte, idUsuarioEspecial : data.idUsuarioEspecial, nombreUsuarioEspecial : data.nombreUsuarioEspecial, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.asignarParteAUsuario, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {

                    try {

                        var $mensaje = $(xmlResponse).find("mensaje");


                        if($mensaje.attr("estadoOperacion") == "ok") {	                        

                            //callback
                            data.onSuccess($mensaje.attr("mensaje"), data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorAsignarParteAUsuario(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAsignarParteAUsuario(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarContenedoresPorArticulo = function(data, serializado) {

            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idArticulo : data.idArticulo, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarContenedoresPorArticulo, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
                    
                    try { 
      
                        var $mensaje = $(xmlResponse).find("mensaje");


                        if($mensaje.attr("estadoOperacion") == "ok") {	 
                                                                                    
                            var lista = new ListaDeContenedores();
                            
                            $(xmlResponse).find("contenedor").each(function() {
                                
                                var contenedor = crearObjetoContenedorDesdeXML($(this));
                                
                                var $posicion = $(this).find('posicion');
                                
                                if($posicion.length > 0) {
                                    
                                    var posicion = crearObjetoPosicionDesdeXML($posicion);
                                    contenedor.setPosicionPicking(posicion);
                                }                                
                                
                                lista.insert(contenedor);
                            });   

                            //callback
                            data.onSuccess(lista, data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorAlRecuperarContenedoresPorArticulo(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarContenedoresPorArticulo(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarContenedoresPorID = function(data, serializado) {

            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idContenedor : data.idContenedor, idArticulo : data.idArticulo, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarContenedoresPorID, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
              
                    try { 
      
                        var $mensaje = $(xmlResponse).find("mensaje");


                        if($mensaje.attr("estadoOperacion") == "ok") {	 
                                                                                    
                            var contenedor = null;
                            
                            var $contenedor = $(xmlResponse).find("contenedor");
                            
                            if($contenedor.length != 0) {
                                contenedor = crearObjetoContenedorDesdeXML($contenedor);
                            } 

                            //callback
                            data.onSuccess(contenedor, data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorAlRecuperarElContenedor(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarElContenedor(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        recuperarPosicion = function(data, serializado) {

            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {idUsuario : data.idUsuario, idGalpon : data.idGalpon, calle : data.calle, fila : data.fila, columna : data.columna, urlForward : data.urlForward, urlLogout : data.urlLogout};
            }

            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.recuperarPosicion, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },

                success: function(xmlResponse) {
              
                    try {
      
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {	 
                                                                                    
                            var posicion = null;
                            
                            var $posicion = $(xmlResponse).find("posicion");
                            
                            if($posicion.length > 0) {
                                
                                posicion = crearObjetoPosicionDesdeXML($posicion);
                                
                                var listaContenedores  = posicion.getListaDeContenedores();
                                
                                $(xmlResponse).find("contenedor").each(function() {
                                
                                    var contenedor = crearObjetoContenedorDesdeXML($(this));
                                    
                                    listaContenedores.insert(contenedor);
                                });
                            }

                            //callback
                            data.onSuccess(posicion, data.parametrosExtra);								
                        }
                        else {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar) {
                                var excep = new XMLException(getMensajeErrorAlRecuperarPosicion(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarPosicion(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        },
                
        obtenerDatosConfiguracion = function(data, serializado) {

            if(!data.parametrosExtra) {
                data.parametrosExtra = {};
            }

            var dataAjax;

            if(serializado) {
                dataAjax = data.data;
            }
            else {
                dataAjax = {};
            }
            
            $.ajax({
                data : dataAjax, // get the form data
                type : "POST",
                async : false,
                dataType : "xml",                
                url : CONFIG.proyectos.expedicion.controladores.obtenerDatosConfiguracionPicking, // the file to call

                error : function (jqXHR, textStatus, errorThrown) {

                    var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                    //callback
                    data.onFailure(excep);
                },
                
                success: function(xmlResponse) {
                    
                    try 
                    {
                        var $xmlResponse = $(xmlResponse);
                        
                        var $mensaje = $xmlResponse.find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {

                            
                            var $estadosPartes = $xmlResponse.find("estadosPartes");
                            var $monitorPartes = $xmlResponse.find("monitorPartes");
                            var $tipoPosicion = $xmlResponse.find("tipoPosicion");

                            var respuestaConfiguracion = {
                                estadosParte : {},
                                monitorPartes : {},
                                posiciones : {tipoPosicion : {}}
                            };

                            respuestaConfiguracion.estadosParte.estadoPartePendienteDePreparacion = parseInt($estadosPartes.find("estadoPartePendienteDePreparacion").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoPartePreparandose = parseInt($estadosPartes.find("estadoPartePreparandose").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoPartePreparadoSinEntregar = parseInt($estadosPartes.find("estadoPartePreparadoSinEntregar").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteEnBufferEsperaControl = parseInt($estadosPartes.find("estadoParteEnBufferEsperaControl").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteEnControl = parseInt($estadosPartes.find("estadoParteEnControl").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteEntregadoACliente = parseInt($estadosPartes.find("estadoParteEntregadoACliente").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteEntregadoAClienteParcialmente = parseInt($estadosPartes.find("estadoParteEntregadoAClienteParcialmente").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteControladoEnSalida = parseInt($estadosPartes.find("estadoParteControladoEnSalida").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteSuspendido = parseInt($estadosPartes.find("estadoParteSuspendido").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteAnulado = parseInt($estadosPartes.find("estadoParteAnulado").attr('id'));
                            respuestaConfiguracion.estadosParte.estadoParteRetrasado = parseInt($estadosPartes.find("estadoParteRetrasado").attr('id'));    
                            
                            respuestaConfiguracion.monitorPartes.milisegundosActualizacionPantallaClientes = parseInt($monitorPartes.find("milisegundosActualizacionPantallaClientes").attr('valor'));
                            respuestaConfiguracion.monitorPartes.cantidadPaginasAMostrar = parseInt($monitorPartes.find("cantidadPaginasAMostrar").attr('valor'));
                            respuestaConfiguracion.monitorPartes.cantidadFilasPorPagina = parseInt($monitorPartes.find("cantidadFilasPorPagina").attr('valor'));
                            respuestaConfiguracion.monitorPartes.milisegundosVisibilidadPaginaPpal = parseInt($monitorPartes.find("milisegundosVisibilidadPaginaPpal").attr('valor'));
                            respuestaConfiguracion.monitorPartes.milisegundosVisibilidadPaginasSecundarias = parseInt($monitorPartes.find("milisegundosVisibilidadPaginasSecundarias").attr('valor'));
                            respuestaConfiguracion.monitorPartes.milisegundosVisibilidadPartesEntregados = parseInt($monitorPartes.find("milisegundosVisibilidadPartesEntregados").attr('valor'));
                             		 
                            respuestaConfiguracion.posiciones.tipoPosicion.celdaStock = parseInt($tipoPosicion.find("tipoCeldaStock").attr('valor'));
                            respuestaConfiguracion.posiciones.tipoPosicion.celdaPicking = parseInt($tipoPosicion.find("tipoCeldaPicking").attr('valor'));
                            
                            //callback
                            data.onSuccess(respuestaConfiguracion, data.parametrosExtra);								
                        }
                        else
                        {
                            var respuesta = opSistem.manejoErroresSesion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));

                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorAlRecuperarDatosConfiguracion(), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorAlRecuperarDatosConfiguracion(), e);                        

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });
        };      
    

        return {            
            obtenerDatosConfiguracion : obtenerDatosConfiguracion,               
            asignarParteAPartirDeCodigoBarrasUC : asignarParteAPartirDeCodigoBarrasUC,
            controlarParte : controlarParte,
            retrasarParte : retrasarParte,
            anularParte : anularParte,
            setearPartePendientePreparacion : setearPartePendientePreparacion,
            cancelarControlParte : cancelarControlParte,
            crearContenedor: crearContenedor,            
            recuperarListaDePartes : recuperarListaDePartes,
            recuperarPartePickingPorNumero : recuperarPartePickingPorNumero,
            recuperarDatosRapidosParte : recuperarDatosRapidosParte,
            recuperarLeyendaEntregaParte : recuperarLeyendaEntregaParte,
            reordenarPartes : reordenarPartes,
            recuperarContenedoresPorArticulo : recuperarContenedoresPorArticulo,
            recuperarContenedoresPorID : recuperarContenedoresPorID,
            recuperarPosicion : recuperarPosicion,
            asignarParteAUsuario : asignarParteAUsuario
        };
}
