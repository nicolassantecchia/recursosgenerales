/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/ArticuloPartePicking.js
 */
var ListaDeArticulosPartePicking = function () {    

    var nuevaLista = Object.create(new Lista())

    nuevaLista.searchItem = function(id_articulo) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);
            
            if(itemActual.getIdArticulo() === id_articulo) {
                return itemActual;
            } 
            else {
                itemActual = null;
            }
        }
        
        return itemActual;
    };

    return nuevaLista;
};