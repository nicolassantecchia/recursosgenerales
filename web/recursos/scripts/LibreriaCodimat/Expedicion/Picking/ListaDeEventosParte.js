/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/EventosParte.js
 */
var ListaDeEventosParte = function() {
        
    var nuevaLista = Object.create(new Lista());
    
    var fechaConsulta = null;

    nuevaLista.searchItem = function(id_evento) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() === id_evento) {
                return itemActual;
            } 
            else {
                itemActual = null;
            }
        }
        
        return itemActual;
    };    
            
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordernarPorFecha = function(masRecientePrimero) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(masRecientePrimero == true) {
            
            //De más reciente a más antigua
            this.ordenar(function(evento1, evento2) {            
                return evento1.getFecha().compararFecha(evento2.getFecha()) * -1; //Invierto el orden natural
            }); 
        }
        else { 
            
            //De más antigua a más reciente
            this.ordenar(function(evento1, evento2) {            
                return evento1.getFecha().compararFecha(evento2.getFecha());
            }); 
        }      
    };

    return nuevaLista;
};