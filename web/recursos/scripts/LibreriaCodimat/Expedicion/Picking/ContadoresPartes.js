/**
 * Requiere TDA.js
 * @returns {ContadoresPartes.Anonym$0}
 */
var ContadoresPartes = function() {
    
    var listaEstadoColas = new Lista(),
        
        getListaEstadoColas = function() {
            return listaEstadoColas;
        };
        
    return {
        getListaEstadoColas : getListaEstadoColas
    }
}

