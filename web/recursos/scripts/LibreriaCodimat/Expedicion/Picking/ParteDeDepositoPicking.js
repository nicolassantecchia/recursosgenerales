/**
 *  Requiere:
    LibreriaCodimat/Sistema/Fecha.js
    LibreriaCodimat/Sistema/Usuario.js
    LibreriaCodimat/Sistema/Equipo.js
    LibreriaCodimat/Expedicion/Picking/ArticuloPartePicking.js
    LibreriaCodimat/Expedicion/Picking/UnidadContenedora.js
    LibreriaCodimat/Expedicion/Picking/EquipoDeArmadoDePartes.js
    LibreriaCodimat/Expedicion/Picking/ParteDeDepositoPicking.js
    LibreriaCodimat/Expedicion/Picking/OperacionesSobreServidorPicking.js
    LibreriaCodimat/Expedicion/Picking/TipoDeEntrega.js
    LibreriaCodimat/Expedicion/Picking/BufferDeposito.js

    LibreriaCodimat/Expedicion/Picking/ListaDeArticulosPartePicking.js
    LibreriaCodimat/Expedicion/Picking/ListaDeEventosParte.js
    LibreriaCodimat/Expedicion/Picking/EstadoParte.js
    LibreriaCodimat/Expedicion/Picking/EventoParte.js

    LibreriaCodimat/Expedicion/ListaDeBulto.js
    LibreriaCodimat/Expedicion/Bulto.js

 * @param {type} nroDeParte
 * @returns {ParteDeDepositoPicking.Anonym$0}
 */
var ParteDeDepositoPicking = function(nroDeParte) {
    
    var estado = null, //PendienteDeArmado - Preparandose - Pendiente de verificacion - Verificandose - Entregado al cliente - Entregado
        listaDeArticulos = new ListaDeArticulosPartePicking(), //ListaDeArticulosPartePicking
        listaDeEventosParte = new ListaDeEventosParte(), //ListaDeEventosParte
        listaDeBulto = new ListaDeBulto(), //ListaDeBulto
        numeroDeParte = nroDeParte,                //int
        puestoDeImpresion = null,                  //PuestoDeImpresion
        tipoParte = 0,                             //int
        idAutoincremental = 0,                     //int
        nroCPRBAsociado = null,                    //String
        nroCPRBAsociadoParaMostrar = null,          //String  
        codigoBarrasCPRBAsociado = null,           //String
        tipoDeCPRBAsociado = 0,                    //int
        tipoDeCPRBAsociadoParaMostrar = 0,         //int
        fechaCreacion = null,                      //FechaV2
        fechaInicioPreparacion = null,             //FechaV2
        fechaFinPreparacion = null,                //FechaV2
        fechaInicioControl = null,                 //FechaV2
        fechaFinControl = null,                    //FechaV2
        fechaControlSalida = null,                 //FechaV2
        fechaSuspension = null,                    //FechaV2
        fechaUltimoUpdate = null,                  //FechaV2
        usuarioAsignadoEspecialmente = null,       //Usuario
        usuarioCreador = null,                     //Usuario
        usuarioPrepardor = null,                   //Usuario
        usuarioControlador = null,                 //Usuario
        usuarioSalida = null,                      //Usuario
        usuarioSuspende = null,                    //Usuario
        usuarioSupervisor = null,                  //Usuario
        imprimible = false,                        //boolean
        posicionEnLista = 0,                       //int
        equipoDeArmadoDePartes = null,             //EquipoDeArmadoDePartes
        tieneUsuarioAsignadoEspecialmente,         //boolean
        bufferDeEsperaPorControl,                  //int
        nombreCliente = null,                      //String
        pesoAproximado = 0.0,                      //double
        tipoEntrega = null,
        chofer = null,
        listaDeLeyendas = null,
        naveOGalpon = -1, //int

    getEstado = function() {
        return estado;
    },

    getNumeroDeParte = function() {
        return numeroDeParte;
    },

    getPuestoDeImpresion = function() {
        return puestoDeImpresion;
    },

    getTipoParte = function() {
        return tipoParte;
    },

    getIdAutoincremental = function() {
        return idAutoincremental;
    },

    getNroCPRBAsociado = function() {
        return nroCPRBAsociado;
    },
    
    getNroCPRBAsociadoParaMostrar = function() {        
        return nroCPRBAsociadoParaMostrar
    },

    getTipoDeCPRBAsociado = function() {
        return tipoDeCPRBAsociado;
    },

    getTipoDeCPRBAsociadoParaMostrar = function() {
        return tipoDeCPRBAsociadoParaMostrar;
    },

    getFechaCreacion = function() {
        return fechaCreacion;
    },
    
    getUsuarioCreador = function() {
        return usuarioCreador;
    },

    isImprimible = function() {
        return imprimible;
    },

    setImprimible = function(impr) {
        imprimible = impr;
    },

    getPosicionEnLista = function() {
        return posicionEnLista;
    },

    setPosicionEnLista = function(posicion) {
        posicionEnLista = posicion;
    },

    getEquipoDeArmadoDePartes = function() {
        return equipoDeArmadoDePartes;
    },

    setEquipoDeArmadoDePartes = function(equipo) {
        equipoDeArmadoDePartes = equipo;
    },

    setUsuarioCreador = function(usuario) {
        usuarioCreador = usuario;
    },

    getUsuarioPrepardor = function() {
        return usuarioPrepardor;
    },

    setUsuarioPrepardor = function(usuario) {
        usuarioPrepardor = usuario;
    },

    getUsuarioControlador = function() {
        return usuarioControlador;
    },

    setUsuarioControlador = function(usuario) {
        usuarioControlador = usuario;
    },

    getUsuarioSalida = function() {
        return usuarioSalida;
    },

    setUsuarioSalida = function(usuario) {
        usuarioSalida = usuario;
    },

    setFechaCreacion = function(fecha) {
        fechaCreacion = fecha;
    },

    setTipoDeCPRBAsociado = function(tipoDeCPRBA) {
        tipoDeCPRBAsociado = tipoDeCPRBA;
    },
            
    setTipoDeCPRBAsociadoParaMostrar = function(tipoDeCPRBA) {
        tipoDeCPRBAsociadoParaMostrar = tipoDeCPRBA;
    },

    setNroCPRBAsociado = function(nroCPRBA) {
        nroCPRBAsociado = nroCPRBA;
    },

    setNroCPRBAsociadoParaMostrar = function(nroCPRBA) {
        nroCPRBAsociadoParaMostrar = nroCPRBA;
    },

    setIdAutoincremental = function(idAutoinc) {
        idAutoincremental = idAutoinc;
    },

    setTipoParte = function(tipo) {
        tipoParte = tipo;
    },

    setPuestoDeImpresion = function(puestoDeImp) {
        puestoDeImpresion = puestoDeImp;
    },

    setEstado = function(est) {
        estado = est;
    },
    
    getArticuloPartePicking = function(pos){        
        return listaDeArticulos.getArticuloPartePicking(pos);
    },
    
    agregarArticuloPartePicking = function(articuloPartePicking){
        listaDeArticulos.insertarArticuloPartePicking(articuloPartePicking);
    },
    
    cantidadArticuloPartePicking = function(){
        return listaDeArticulos.size();
    },

    isTieneUsuarioAsignadoEspecialmente = function() {
        return tieneUsuarioAsignadoEspecialmente;
    },

    setTieneUsuarioAsignadoEspecialmente = function(tieneUsuarioAsignado) {
        tieneUsuarioAsignadoEspecialmente = tieneUsuarioAsignado;
    },
            
    getUsuarioAsignadoEspecialmente = function() {
        return usuarioAsignadoEspecialmente;
    },
            
    setUsuarioAsignadoEspecialmente = function(usuarioEspecial) {
        return usuarioAsignadoEspecialmente = usuarioEspecial;
    },

    getListaDeArticulos = function() {
        return listaDeArticulos;
    },

    setListaDeArticulos = function(listaDeArt) {
        listaDeArticulos = listaDeArt;
    },

    getBufferDeEsperaPorControl = function() {
        return bufferDeEsperaPorControl;
    },

    setBufferDeEsperaPorControl = function(bufferDeEsperaParaControl) {
        bufferDeEsperaPorControl = bufferDeEsperaParaControl;
    },

    getNombreCliente = function() {
        return nombreCliente;
    },

    setNombreCliente = function(nombre) {
        nombreCliente = nombre;
    },

    getFechaInicioPreparacion = function() {
        return fechaInicioPreparacion;
    },

    setFechaInicioPreparacion = function(fecha) {
        fechaInicioPreparacion = fecha;
    },

    getFechaFinPreparacion = function() {
        return fechaFinPreparacion;
    },

    setFechaFinPreparacion = function(fecha) {
        fechaFinPreparacion = fecha;
    },

    getFechaInicioControl = function() {
        return fechaInicioControl;
    },

    setFechaInicioControl = function(fecha) {
        fechaInicioControl = fecha;
    },

    getFechaFinControl = function() {
        return fechaFinControl;
    },

    setFechaFinControl = function(fecha) {
        fechaFinControl = fecha;
    },

    getFechaControlSalida = function() {
        return fechaControlSalida;
    },

    setFechaControlSalida = function(fecha) {
        fechaControlSalida = fecha;
    },

    getPesoAproximado = function() {
        
        var peso = 0;
        
        for(var i = 0; i < listaDeArticulos.size(); i++){
            peso = peso + getArticuloPartePicking(i).getPesoAproximado();
        }
        
        return peso;
    },

//    public void setPesoAproximado(double pesoAproximado) {
//        //IMPLEMENTAR EFICIENTEMENTE -> si el peso esta en 0, recorrer la lista
//        // de articulos y sumar. Caso contrario hacer solamente el return
//        this.pesoAproximado = pesoAproximado;
//    }

    getFechaSuspension = function() {
        return fechaSuspension;
    },

    setFechaSuspension = function(fecha) {
        fechaSuspension = fecha;
    },

    getUsuarioSuspende = function() {
        return usuarioSuspende;
    },

    setUsuarioSuspende = function(usuario) {
        usuarioSuspende = usuario;
    },

    getFechaUltimoUpdate = function() {
        return fechaUltimoUpdate;
    },

    setFechaUltimoUpdate = function(fecha) {
        fechaUltimoUpdate = fecha;
    },

    getUsuarioSupervisor = function() {
        return usuarioSupervisor;
    },

    setUsuarioSupervisor = function(usuario) {
        usuarioSupervisor = usuario;
    },

    getCodigoBarrasCPRBAsociado = function() {
        return codigoBarrasCPRBAsociado;
    },

    setCodigoBarrasCPRBAsociado = function(codigoBarras) {
        codigoBarrasCPRBAsociado = codigoBarras;
    },
            
    getTipoDeEntrega = function() {
        return tipoEntrega;
    },
    
    setTipoDeEntrega = function(tipoEntr) {
        tipoEntrega = tipoEntr;
    },
            
    getChofer = function() {
        return chofer;
    },

    setChofer = function(chof) {
        chofer = chof;
    },
            
    getListaDeLeyendas = function() {
        return listaDeLeyendas;
    },
    
    setListaDeLeyendas = function(listaLeyenda) {
        listaDeLeyendas = listaLeyenda;
    },
            
    getListaDeEventosParte = function() {
        return listaDeEventosParte;
    },

    setListaDeEventosParte = function(listaDeEvt) {
        listaDeEventosParte = listaDeEvt;
    },
            
    getListaDeBulto = function() {
        return listaDeBulto;
    },
            
    setListaDeBulto = function(lista) {
        listaDeBulto = lista;
    },
        
    getNaveOGalpon = function() {
        return naveOGalpon;
    },
            
    setNaveOGalpon = function(nave) {
      naveOGalpon = nave;  
    };
    
    return {
        getEstado : getEstado,    
        getNumeroDeParte : getNumeroDeParte,
        getPuestoDeImpresion : getPuestoDeImpresion,
        getTipoParte : getTipoParte,
        getTipoDeEntrega : getTipoDeEntrega,
        getIdAutoincremental : getIdAutoincremental,
        getNroCPRBAsociado : getNroCPRBAsociado,
        getTipoDeCPRBAsociado : getTipoDeCPRBAsociado,
        getNroCPRBAsociadoParaMostrar : getNroCPRBAsociadoParaMostrar,
        getTipoDeCPRBAsociadoParaMostrar : getTipoDeCPRBAsociadoParaMostrar,
        getFechaCreacion : getFechaCreacion,    
        getUsuarioCreador : getUsuarioCreador,
        isImprimible : isImprimible,
        setImprimible : setImprimible,
        getPosicionEnLista : getPosicionEnLista,
        setPosicionEnLista : setPosicionEnLista,
        getEquipoDeArmadoDePartes : getEquipoDeArmadoDePartes,
        setEquipoDeArmadoDePartes : setEquipoDeArmadoDePartes,
        setUsuarioCreador : setUsuarioCreador,
        getUsuarioPrepardor : getUsuarioPrepardor,
        setUsuarioPrepardor : setUsuarioPrepardor,
        getUsuarioControlador : getUsuarioControlador,
        setUsuarioControlador : setUsuarioControlador,
        getUsuarioSalida : getUsuarioSalida,
        setUsuarioSalida : setUsuarioSalida,
        setFechaCreacion : setFechaCreacion,
        setTipoDeCPRBAsociado : setTipoDeCPRBAsociado,
        setTipoDeCPRBAsociadoParaMostrar : setTipoDeCPRBAsociadoParaMostrar,
        setNroCPRBAsociado : setNroCPRBAsociado,        
        setNroCPRBAsociadoParaMostrar : setNroCPRBAsociadoParaMostrar,
        setIdAutoincremental : setIdAutoincremental,
        setTipoParte : setTipoParte,
        setTipoDeEntrega : setTipoDeEntrega,
        setPuestoDeImpresion : setPuestoDeImpresion,
        setEstado : setEstado,    
        getArticuloPartePicking : getArticuloPartePicking,    
        agregarArticuloPartePicking : agregarArticuloPartePicking,    
        cantidadArticuloPartePicking : cantidadArticuloPartePicking,
        isTieneUsuarioAsignadoEspecialmente : isTieneUsuarioAsignadoEspecialmente,
        setTieneUsuarioAsignadoEspecialmente : setTieneUsuarioAsignadoEspecialmente,
        getUsuarioAsignadoEspecialmente : getUsuarioAsignadoEspecialmente,
        setUsuarioAsignadoEspecialmente : setUsuarioAsignadoEspecialmente,
        getListaDeArticulos : getListaDeArticulos,
        setListaDeArticulos : setListaDeArticulos,
        getBufferDeEsperaPorControl : getBufferDeEsperaPorControl,
        setBufferDeEsperaPorControl : setBufferDeEsperaPorControl,
        getNombreCliente : getNombreCliente,
        setNombreCliente : setNombreCliente,
        getFechaInicioPreparacion : getFechaInicioPreparacion,
        setFechaInicioPreparacion : setFechaInicioPreparacion,
        getFechaFinPreparacion : getFechaFinPreparacion,
        setFechaFinPreparacion : setFechaFinPreparacion,
        getFechaInicioControl : getFechaInicioControl,
        setFechaInicioControl : setFechaInicioControl,
        getFechaFinControl : getFechaFinControl,
        setFechaFinControl : setFechaFinControl,
        getFechaControlSalida : getFechaControlSalida,
        setFechaControlSalida : setFechaControlSalida,
        getPesoAproximado : getPesoAproximado,
        getFechaSuspension : getFechaSuspension,
        setFechaSuspension : setFechaSuspension,
        getUsuarioSuspende : getUsuarioSuspende,
        setUsuarioSuspende : setUsuarioSuspende,
        getFechaUltimoUpdate : getFechaUltimoUpdate,
        setFechaUltimoUpdate : setFechaUltimoUpdate,
        getUsuarioSupervisor : getUsuarioSupervisor,
        setUsuarioSupervisor : setUsuarioSupervisor,
        getCodigoBarrasCPRBAsociado : getCodigoBarrasCPRBAsociado,
        setCodigoBarrasCPRBAsociado : setCodigoBarrasCPRBAsociado,
        getChofer : getChofer,
        setChofer : setChofer,
        getListaDeEventosParte : getListaDeEventosParte,
        setListaDeEventosParte : setListaDeEventosParte,
        getNaveOGalpon : getNaveOGalpon,
        setNaveOGalpon : setNaveOGalpon,
        getListaDeLeyendas : getListaDeLeyendas,
        setListaDeLeyendas : setListaDeLeyendas,
        getListaDeBulto : getListaDeBulto,
        setListaDeBulto : setListaDeBulto
    }
};