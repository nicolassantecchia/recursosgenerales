var EstadoColasPicking = function(id) {
    
    var idCola = id,
        nombre = "",
        detalle = "",
        cantidadPartes = 0,
        
        getIdCola = function() {
            return idCola;
        },
        getNombreCola = function() {
            return nombre;
        },
        getDetalleCola = function() {
            return detalle;
        },
        getCantidadPartes = function() {
            return cantidadPartes;
        },
        setNombreCola = function(nom) {
            nombre = nom;
        },
        setDetalleCola = function(det) {
            detalle = det;
        },
        setCantidadPartes = function(cantidad) {
            cantidadPartes = cantidad
        }

    return {
        getIdCola : getIdCola,
        getNombreCola : getNombreCola,
        getDetalleCola : getDetalleCola,
        getCantidadPartes : getCantidadPartes,
        setNombreCola : setNombreCola,
        setDetalleCola : setDetalleCola,
        setCantidadPartes : setCantidadPartes
    } 
}