/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/PosicionPicking.js
 */
var ListaDePosicionesPicking = function() {
    
    var nuevaLista = Object.create(new Lista());

    nuevaLista.searchItem = function(id_pos) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getIdPosicionPicking() === id_pos) {
                return itemActual;
            }  
            else {
                itemActual = null;
            }
        }
        return itemActual;
    };
    
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordenarPorID = function(mayorAmenor) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(mayorAmenor == true) {
            
            //De mayor a menor
            this.ordenar(function(posicion1, posicion2) {    
                
                if(posicion1.getId() < posicion2.getId()) {
                    return 1;
                }
                else {
                    if(posicion1.getId() > posicion2.getId()) {
                        return -1;
                    }
                    else { //iguales
                        return 0;
                    }
                }                
            }); 
        }
        else { 
                       
            this.ordenar(function(posicion1, posicion2) {  
                
                if(posicion1.getId() < posicion2.getId()) {
                    return -1;
                }
                else {
                    if(posicion1.getId() > posicion2.getId()) {
                        return 1;
                    }
                    else { //iguales
                        return 0;
                    }
                }  
            }); 
        }      
    };

    return nuevaLista;
};