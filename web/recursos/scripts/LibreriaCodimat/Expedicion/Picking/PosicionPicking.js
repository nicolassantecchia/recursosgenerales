/**
 *  Requiere:
 *  LibreriaCodimat/Expedicion/Picking/Contenedor.js
 *  LibreriaCodimat/Expedicion/Picking/ListaDeContenedores.js
 */
var PosicionPicking = function(idPosicion) {

    var idPosicionPicking = idPosicion, // int
    nave = -1, // int
    calle = -1, // int
    columna = -1,// int
    fila = -1, // int
    ancho = 0, //double
    alto = 0, //double
    activa = true, //boolean
    pesoMax = 0, //double
    estadoId = 0, //int 0- libre, 1- parcialmente ocupada, 2-ocupada
    estadoDetalle = "", //String
    fechaAlta = null, //Fecha
    fechaUltimaActualizacion = null, //Fecha
    pesoRecorrido = 0, // int
    listaDeContenedores = new ListaDeContenedores(), //ListaDeContenedores
    codigoDeBarras = "", //String
    tipoPosicion = 0, // int  
    detalleTipoPosicion = "NO DEFINIDA",
    usuarioActual = null, //Usuario
        
    getUsuarioActual = function() {
        return usuarioActual;
    },

    setUsuarioActual = function(usuario) {
        usuarioActual = usuario;
    },

    getIdPosicionPicking = function() {
        return idPosicionPicking;
    },

    setIdPosicionPicking = function(idPosicion) {
        idPosicionPicking = idPosicion;
    },

    getNave = function() {
        return nave;
    },

    setNave = function(nav) {
        nave = nav;
    },

    getCalle = function() {
        return calle;
    },

    setCalle = function(mi_calle) {
        calle = mi_calle;
    },

    getColumna = function() {
        return columna;
    },

    setColumna = function(col) {
        columna = col;
    },

    getFila = function() {
        return fila;
    },

    setFila = function(mi_fila) {
        fila = mi_fila;
    },

    getAncho = function() {
        return ancho;
    },

    setAncho = function(mi_ancho) {
        ancho = mi_ancho;
    },

    getAlto = function() {
        return alto;
    },

    setAlto = function(mi_alto) {
        alto = mi_alto;
    },

    getActiva = function() {
        return activa;
    },

    setActiva = function(isActiva) {
        activa = isActiva;
    },

    getPesoMax = function() {
        return pesoMax;
    },

    setPesoMax = function(peso) {
        pesoMax = peso;
    },

    getEstadoId = function() {
        return estadoId;
    },

    setEstadoId = function(estado) {
        estadoId = estado;
    },
            
    getEstadoDetalle = function() {
        return estadoDetalle;
    },

    setEstadoDetalle = function(estado) {
        estadoDetalle = estado;
    },

    getFechaAlta = function() {
        return fechaAlta;
    },

    setFechaAlta = function(fecha) {
        fechaAlta = fecha;
    },

    getPesoRecorrido = function() {
        return pesoRecorrido;
    },

    setPesoRecorrido = function(peso) {
        pesoRecorrido = peso;
    },

    getFechaUltimaActualizacion = function() {
        return fechaUltimaActualizacion;
    },

    setFechaUltimaActualizacion = function(fecha) {
        fechaUltimaActualizacion = fecha;
    },

    getListaDeContenedores = function() {
        return listaDeContenedores;
    },

    setListaDeContenedores = function(lista) {
        listaDeContenedores = lista;
    },

    getCodigoDeBarras = function() {
        return codigoDeBarras;
    },

    setCodigoDeBarras = function(codigoBarras) {
        codigoDeBarras = codigoBarras;
    },
    
    getAnchoTotalOcupadoPorContenedores = function() {
        
        var respuesta = 0;
        
        if(listaDeContenedores != null) {
        
            for (var i = 0; i < listaDeContenedores.size(); i++){
                respuesta = respuesta + listaDeContenedores.getItem(i).getAncho();
            }
        }
        
        return respuesta;
    },
    
    /**
     * Devuelve el peso estimado actual en la posicion, que es la suma de los pesos actuales de los contenedores
     * @return 
     */
    getPesoTotalOcupadoPorContenedores = function(){
        
        var respuesta = 0;
        
        if(listaDeContenedores != null) {
            for (var i = 0; i < listaDeContenedores.size(); i++){
                respuesta = respuesta + listaDeContenedores.getItem(i).getPesoActual();
            }
        }
        
        return respuesta;
    },

    getTipoPosicion = function() {
        return tipoPosicion;
    },

    setTipoPosicion = function(tipo) {
        tipoPosicion = tipo;
    },
    
    getDetalleTipoPosicion = function() {
        return detalleTipoPosicion;
    },
    
    setDetalleTipoPosicion = function(detalle) {
        detalleTipoPosicion = detalle;
    };
    
    return {
        getUsuarioActual : getUsuarioActual,
        setUsuarioActual : setUsuarioActual,
        getIdPosicionPicking : getIdPosicionPicking,
        setIdPosicionPicking : setIdPosicionPicking,
        getNave : getNave,
        setNave : setNave,
        getCalle : getCalle,
        setCalle : setCalle,
        getColumna : getColumna,
        setColumna : setColumna,
        getFila : getFila,
        setFila : setFila,
        getAncho : getAncho,
        setAncho : setAncho,
        getAlto : getAlto,
        setAlto : setAlto,
        getActiva : getActiva,
        setActiva : setActiva,
        getPesoMax : getPesoMax,
        setPesoMax : setPesoMax,
        getEstadoId : getEstadoId,
        setEstadoId : setEstadoId,
        getEstadoDetalle : getEstadoDetalle,
        setEstadoDetalle : setEstadoDetalle,
        getFechaAlta : getFechaAlta,
        setFechaAlta : setFechaAlta,
        getPesoRecorrido : getPesoRecorrido,
        setPesoRecorrido : setPesoRecorrido,
        getFechaUltimaActualizacion : getFechaUltimaActualizacion,
        setFechaUltimaActualizacion : setFechaUltimaActualizacion,
        getListaDeContenedores : getListaDeContenedores,
        setListaDeContenedores : setListaDeContenedores,
        getCodigoDeBarras : getCodigoDeBarras,
        setCodigoDeBarras : setCodigoDeBarras,    
        getAnchoTotalOcupadoPorContenedores : getAnchoTotalOcupadoPorContenedores,
        getPesoTotalOcupadoPorContenedores : getPesoTotalOcupadoPorContenedores,
        getTipoPosicion : getTipoPosicion,
        setTipoPosicion : setTipoPosicion,    
        getDetalleTipoPosicion : getDetalleTipoPosicion,    
        setDetalleTipoPosicion : setDetalleTipoPosicion
    }
}
