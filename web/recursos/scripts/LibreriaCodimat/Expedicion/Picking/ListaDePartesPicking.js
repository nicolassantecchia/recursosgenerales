/**
 *  Requiere:
    LibreriaCodimat/TDA.js
    LibreriaCodimat/Expedicion/Picking/ParteDeDepositoPicking.js
 */
var ListaDePartesPicking = function() {
    
    var nuevaLista = Object.create(new Lista());
    
    var fechaConsulta = null;

    nuevaLista.searchItem = function(nro_parte) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getNumeroDeParte() === nro_parte) {
                return itemActual;
            } 
            else {
                itemActual = null;
            }
        }
        
        return itemActual;
    };
    
    nuevaLista.setFechaConsulta = function(fecha) {
        fechaConsulta = fecha;
    };
            
    nuevaLista.getFechaConsulta = function() {
        return fechaConsulta;
    };
            
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordernarPorFechaInicioPreparacion = function(masRecientePrimero) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(masRecientePrimero == true) {
            
            //De más reciente a más antigua
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaInicioPreparacion().compararFecha(parte2.getFechaInicioPreparacion()) * -1; //Invierto el orden natural
            }); 
        }
        else { 
            
            //De más antigua a más reciente
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaInicioPreparacion().compararFecha(parte2.getFechaInicioPreparacion());
            }); 
        }      
    };
            
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordernarPorFechaCreacion = function(masRecientePrimero) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(masRecientePrimero == true) {
            
            //De más reciente a más antigua
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaCreacion().compararFecha(parte2.getFechaCreacion()) * -1; //Invierto el orden natural
            }); 
        }
        else { 
            
            //De más antigua a más reciente
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaCreacion().compararFecha(parte2.getFechaCreacion());
            }); 
        }               
    };
            
    /**
     * Si <code>masRecientePrimero</code> = true -> ordena de más reciente a más antiguo. Si el parámetro no está definido
     * o está seteado en falso, ordena por defecto de más antiguo a más reciente.
     */
    nuevaLista.ordernarPorFechaUltimoUpdate = function(masRecientePrimero) {
        
        //ver documentacion del metodo ordenar en TDA.js
        
        if(masRecientePrimero == true) {
            
            //De más reciente a más antigua
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaUltimoUpdate().compararFecha(parte2.getFechaUltimoUpdate()) * -1; //Invierto el orden natural
            }); 
        }
        else { 
            
            //De más antigua a más reciente
            this.ordenar(function(parte1, parte2) {            
                return parte1.getFechaUltimoUpdate().compararFecha(parte2.getFechaUltimoUpdate());
            }); 
        }               
    };
    

    return nuevaLista;
};