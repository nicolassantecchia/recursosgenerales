/**
 *  Requiere:
    LibreriaCodimat/Sistema/Evento.js
    LibreriaCodimat/Expedicion/Picking/EstadoParte.js
 */
var EventoParte = function(mi_id, mi_tipo, mi_numeroPartePicking, mi_idGalpon, mi_usuario) {
    
    var nuevoEvento = Object.create(new Evento(mi_id, mi_tipo));
    
    nuevoEvento.setUsuarioAsociado(mi_usuario);
    
    var detalleEvento = "", //String
        numeroPartePicking = mi_numeroPartePicking, //int
        estadoAnteriorAlEvento = null, //EstadoParte
        estadoPosteriorAlEvento = null, //EstadoParte
        idGalpon = mi_idGalpon, //int
        fechaAuxiliar = null, //Fecha
        enteroAuxiliar = -1, //int
        stringAuxiliar = ""; //String
    
    nuevoEvento.getDetalleEvento = function() {
        return detalleEvento;
    }

    nuevoEvento.setDetalleEvento = function(det) {
        detalleEvento = det;
    }

    nuevoEvento.getNumeroPartePicking = function() {
        return numeroPartePicking;
    }
    
    nuevoEvento.getEstadoAnteriorAlEvento = function() {
        return estadoAnteriorAlEvento;
    }

    nuevoEvento.setEstadoAnteriorAlEvento = function(estadoAnterior) {
        estadoAnteriorAlEvento = estadoAnterior;
    }

    nuevoEvento.getEstadoPosteriorAlEvento = function() {
        return estadoPosteriorAlEvento;
    }

    nuevoEvento.setEstadoPosteriorAlEvento = function(estadoPosterior) {
        estadoPosteriorAlEvento = estadoPosterior;
    }

    nuevoEvento.getIdGalpon = function() {
        return idGalpon;
    }

    nuevoEvento.getFechaAuxiliar = function() {
        return fechaAuxiliar;
    }

    nuevoEvento.setFechaAuxiliar = function(fecha) {
        fechaAuxiliar = fecha;
    }

    nuevoEvento.getEnteroAuxiliar = function() {
        return enteroAuxiliar;
    }

    nuevoEvento.setEnteroAuxiliar = function(entero) {
        enteroAuxiliar = entero;
    }

    nuevoEvento.getStringAuxiliar = function() {
        return stringAuxiliar;
    }

    nuevoEvento.setStringAuxiliar = function(string) {
        stringAuxiliar = string;
    }
    
    return nuevoEvento;
}
