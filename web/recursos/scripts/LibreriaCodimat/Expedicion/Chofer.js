/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Octubre 2014
 *  ---------------------------------------------
 */

var Chofer = function (mi_nombreYApellido) { 

    var nombreYApellido = mi_nombreYApellido,
		id,
		idTransporte,
		documentoIdentificador, //DocumentoIdentificador
		direccion,
		telefono1,
                transporteAsociado = null,
		
		getNombreYApellido = function() {
			return nombreYApellido;
		},

		getDocumentoIdentificador = function() {
			return documentoIdentificador;
		},

		getDireccion = function() {
			return direccion;
		},

		getTelefono1 = function() {
			return telefono1;
		},

		setTelefono1 = function(mi_telefono1) {
			telefono1 = mi_telefono1;
		},

		setDireccion = function(mi_direccion) {
			direccion = mi_direccion;
		},

		setDocumentoIdentificador = function(mi_documentoIdentificador) {
			documentoIdentificador = mi_documentoIdentificador;
		},
		
		setId = function(mi_id) {
			id = mi_id;
		},

		setIdTransporte = function(mi_idTransporte) {
                    idTransporte = mi_idTransporte;
		},
                        
                getTransporteAsociado = function() {
                    return transporteAsociado;
                },
                        
                        
                setTransporteAsociado = function(transporteAsoc) {
                    transporteAsociado = transporteAsoc;
                },

		getId = function() {
			return id;
		},

		getIdTransporte = function() {
			return idTransporte;
		},
		
		print = function() {
		
			console.log('===== CHOFER =====');
			console.log('  ID= '+id);
			console.log('  nombreYApellido= '+nombreYApellido);
			console.log('  documentoIdentificador= '+documentoIdentificador);
			console.log('  direccion= '+direccion);
			console.log('  telefono1= '+telefono1);
		};

		return {
			print : print,
			getNombreYApellido : getNombreYApellido,
			getDocumentoIdentificador : getDocumentoIdentificador,
			getDireccion : getDireccion,
			getTelefono1 : getTelefono1,
			setTelefono1 : setTelefono1,
			setDireccion : setDireccion,
			setDocumentoIdentificador : setDocumentoIdentificador,	
                        getTransporteAsociado : getTransporteAsociado,
                        setTransporteAsociado : setTransporteAsociado,
			setId : setId,
			setIdTransporte : setIdTransporte,
			getId : getId,
			getIdTransporte : getIdTransporte
		}
}
