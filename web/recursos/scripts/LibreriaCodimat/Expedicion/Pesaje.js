var Pesaje = function(id) {
    
    var idPesaje = id, //int
        numeroPesaje = 0, //int    
        numeroParte = 0,
        codigoBarras = "",
        peso = 0.0, //double  
        codigoBalanza = 0, //int
        activo, //boolean
        fechaCreacion, //Fecha
        usuario, //Usuario
        observacion,
                
        getId = function() {
            return idPesaje;
        },

        getNumero = function() {
            return numeroPesaje;
        },

        setNumero = function(nroPesaje) {
            numeroPesaje = nroPesaje;
        },

        getNumeroParte = function() {
            return numeroParte;
        },

        setNumeroParte = function(nroParte) {
            numeroParte = nroParte;
        },

        getPeso = function() {
            return peso;
        },

        setPeso = function(pes) {
            peso = pes;
        },

        getCodigoBalanza = function() {
            return codigoBalanza;
        },

        setCodigoBalanza = function(codBalanza) {
            codigoBalanza = codBalanza;
        },

        getActivo = function() {
            return activo;
        },

        setActivo = function(act) {
            activo = act;
        },

        getFechaCreacion = function() {
            return fechaCreacion;
        },

        setFechaCreacion = function(fecha) {
            fechaCreacion = fecha;
        },

        getUsuario = function() {
            return usuario;
        },

        setUsuario = function(user) {
            usuario = user;
        },
                
        getObservacion = function() {
            return observacion;
        },
                
        setObservacion = function(obs) {
            observacion = obs;
        },
                
        getCodigoBarras = function() {
            return codigoBarras;
        },
                
        setCodigoBarras = function(codBarras) {
            codigoBarras = codBarras;
        };
    
    return {
        getId : getId,
        getNumero : getNumero,
        setNumero : setNumero,
        getNumeroParte : getNumeroParte,
        setNumeroParte : setNumeroParte,
        getPeso : getPeso,
        setPeso : setPeso,
        getCodigoBalanza : getCodigoBalanza,
        setCodigoBalanza : setCodigoBalanza,
        getActivo : getActivo,
        setActivo : setActivo,
        getFechaCreacion : getFechaCreacion,
        setFechaCreacion : setFechaCreacion,
        getUsuario : getUsuario,
        setUsuario : setUsuario,
        getObservacion : getObservacion,                
        setObservacion : setObservacion,
        getCodigoBarras : getCodigoBarras,
        setCodigoBarras : setCodigoBarras
    }
}

