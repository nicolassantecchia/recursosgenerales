/*
 *  ---------------------------------------------
 *  Fernando Prieto
 *  REQUIERE configuracion.js que se encuentra en proyecto expedicion/recursos/scripts/config
 *  ---------------------------------------------
 */
OperacionesSobreServidorExpedi = function() {
    
        var opSistem = new OperacionesSobreServidorSistema(), //Objeto creado para poder utilizar el metodo manejoErroresSesion
            opExpedi_utiles = new Utiles(), //Objeto creado para poder utlu

	/*	
	 *	Requiere Exception.js
	 *	Requiere Transporte.js
	 *	data : {
	 *			idTransporte :
	 *			onSuccess : 
	 *			onFailure :
	 *		   }
	 
	 *	Crea, completa y retorna el objeto Transporte.
	 */	

	getMensajeErrorTransporte = function(idTransporte) {	
            return "Ocurri&oacute; una excepci&oacute;n al cargar los datos del transporte con c&oacute;digo = "+idTransporte+".";
        },
                
        getMensajeErrorPesaje = function(nroPesaje) {
            return "Ocurri&oacute; un error al cargar los datos del pesaje número " + nroPesaje;
        },
            
        manejoErroresSesionExpedicion = function(stringTipo, stringSubtipo, url) {
            
            var tipo = parseInt(stringTipo);
            var subtipo = parseInt(stringSubtipo);
            
            var respuesta = {redireccionar : false, sesion : null};
            
            if(tipo == CONFIG.variables.sistem.tipoError.sesion)
            {
                if(subtipo ==  CONFIG.variables.sistem.sesion.error.inactiva || subtipo ==  CONFIG.variables.sistem.sesion.error.inactivaCaducadaCerrada || subtipo == CONFIG.variables.sistem.sesion.error.caducada)                                
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                }
                else
                {
                    if(subtipo ==  CONFIG.variables.sistem.sesion.error.permiso)
                    {
                        respuesta.sesion = {errorPermiso : true};                                    
                    }
                }
            }
            else
            {
                if(url != null)
                {
                    respuesta.redireccionar = true;
                    window.location.replace(url);
                    //alert(url);
                }
            }
            
            return respuesta;
        },
                
        crearObjetoPesaje = function(xmlResponse, data) {
            
            console.log(xmlResponse);
            
            var pesaje = null;

            var $pesaje = $(xmlResponse).find("pesaje");

            if($pesaje.length > 0) {  
                
                pesaje = new Pesaje(parseInt($pesaje.attr("id")));
                
                pesaje.setNumero(parseInt($pesaje.attr("numero")));
                pesaje.setNumeroParte(parseInt($pesaje.attr("numeroParte")));
                pesaje.setPeso(parseFloat($pesaje.attr("peso")));
                pesaje.setCodigoBalanza(parseInt($pesaje.attr("codBalanza")));
                pesaje.setActivo(opExpedi_utiles.convertirStringABoolean($pesaje.attr("activo")));
                pesaje.setFechaCreacion(new Fecha($pesaje.attr("fechaCreacion"), $pesaje.attr("horaCreacion")));
                pesaje.setUsuario(new Usuario(parseInt($pesaje.attr("idUsuario")), $pesaje.attr("nombreUsuario")));
                pesaje.setObservacion($pesaje.attr("observacion"));
                pesaje.setCodigoBarras($pesaje.attr("codigoBarras"));
            }
            
            return {pesaje : pesaje};
            
        },

        recuperarTransporte = function(data) {		

                $.ajax({ 

                                 data: {idTransporte: data.idTransporte}, // get the form data
                                 type: "POST",
                                 dataType: "xml",
                                 url: CONFIG.proyectos.recursosGenerales.controladores.recuperarTransporte, // the file to call

                                 error: function (jqXHR, textStatus, errorThrown) {

                                                var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);

                                                //callback
                                                data.onFailure(excep);
                                        },

                                 success: function(xmlResponse) {

                                        //console.log(xmlResponse);

                                        try
                                                {

                                                        var $mensaje = $(xmlResponse).find("mensaje");

                                                        if($mensaje.attr("estadoOperacion") == "ok") {

                                                                var $transporte = $(xmlResponse).find("transporte");

                                                                var transporte = null; 

                                                                if($transporte.length != 0) 
                                                                {

                                                                        transporte = new Transporte($transporte.attr("idTransporte"), $transporte.attr("nombreTransporte"));

                                                                        var docIdent = new DocumentoIdentificador(0);
                                                                        docIdent.setNro($transporte.attr("cuit"));

                                                                        transporte.setDocumentoIdentificador(docIdent);								
                                                                        transporte.setTelefono1($transporte.attr("telefono"));				

                                                                        var chof = null;

                                                                        $(xmlResponse).find("chofer").each(function() {

                                                                                chof = new Chofer($(this).attr("nombreChofer"));

                                                                                docIdent = new DocumentoIdentificador(0);
                                                                                docIdent.setNro($(this).attr("cuit"));

                                                                                chof.setDocumentoIdentificador(docIdent);
                                                                                chof.setId($(this).attr("idChofer"));

                                                                                transporte.getListaDeChoferes().insert(chof);
                                                                        });		
                                                                }
                                                                //callback
                                                                data.onSuccess(transporte);
                                                        }
                                                        else
                                                        {
                                                                var excep = new XMLException(getMensajeErrorTransporte(data.idProveedor), $mensaje.attr('mensaje'));

                                                                //callback
                                                                data.onFailure(excep);
                                                        }
                                                }
                                                catch(e) {

                                                        var excep = new Exception(getMensajeErrorTransporte(data.idTransporte), e);

                                                        //callback
                                                        data.onFailure(excep);
                                                }
                                        }
                });
        },
        
        /*
         * formato data 
         * {
                usuario    : , 
                urlForward : , 
                urlLogout  : , 
                nroPesaje  : 
                nroParte   :
            }
         */
        recuperarPesaje = function(data) {
            
            if(!data.parametrosExtra)
            {
                data.parametrosExtra = {};
            }
            
            var dataAjax = {
                usuario : data.usuario, 
                urlForward : data.urlForward, 
                urlLogout : data.urlLogout, 
            };
            
            if(data.numeroPesaje) {
                dataAjax.nroPesaje = data.numeroPesaje;
            }
            
            if(data.numeroParte) {
                dataAjax.nroParte = data.numeroParte;
            }
            
            if(data.codigoBarras) {
                dataAjax.codBarras = data.codigoBarras;
            }
            
            console.log("------------");
            console.log(dataAjax);
            
            console.log(CONFIG.proyectos.expedicion.controladores.recuperarPesaje);
            
            $.ajax({ 
                data : dataAjax,
                type: "POST",	         
                dataType: "xml",
                url: CONFIG.proyectos.expedicion.controladores.recuperarPesaje, // the file to call

                error: function (jqXHR, textStatus, errorThrown) {
                        var excep = new AjaxException(jqXHR.responseText, textStatus, errorThrown);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                },

                success: function(xmlResponse){
console.log(xmlResponse);
                    try
                    {                            
                        var $mensaje = $(xmlResponse).find("mensaje");

                        if($mensaje.attr("estadoOperacion") == "ok") {             
                            
                            var resultado = crearObjetoPesaje(xmlResponse, data);
                                                        
                                                        console.log(resultado);
                                                        
                            //callback
                            data.onSuccess(resultado.pesaje, $mensaje.attr("mensaje"), data.parametrosExtra);
                        }
                        else {
                            var respuesta = manejoErroresSesionExpedicion($mensaje.attr("tipo"), $mensaje.attr("subtipo"), $(xmlResponse).find("urlForward").attr("url"));
                                
                            data.parametrosExtra.sesion = respuesta.sesion;

                            if(!respuesta.redireccionar)
                            {
                                var excep = new XMLException(getMensajeErrorPesaje(data.numeroPesaje), $mensaje.attr('mensaje'));

                                //callback
                                data.onFailure(excep, data.parametrosExtra);
                            }
                        }
                    }
                    catch(e) 
                    {							
                        var excep = new Exception(getMensajeErrorPesaje(data.numeroPesaje), e);

                        //callback
                        data.onFailure(excep, data.parametrosExtra);
                    }
                }
            });    
        },                
        
        recuperarComprobanteEnCot = function(data) {
            
            //alert(data.idComprobante);
        
            var idComprobante = data.idComprobante;
            //alert('OperacionesSobreServidorExpedi: ' + comprobante);
            
                $.ajax({
                        data : {idComprobante: idComprobante}, // get the form data
                        type: "POST",
                        //type: "GET",
                        dataType: "xml",
                        //url: "http://localhost:8080/recursosGenerales/recuperarComprobanteEnCot?idComprobante=91 R000100188590",
                        //url: "http://localhost:8080/recursosGenerales/recuperarComprobanteEnCot",
                        url: CONFIG.proyectos.recursosGenerales.controladores.recuperarComprobanteEnCot, // the file to call
                        
                        error: function (jqXHR, textStatus, errorThrown) {

                                       var excep = new AjaxException(jqXHR.responseText,textStatus,errorThrown);
                                       //var excep = new XMLException(jqXHR.responseText, errorThrown);

                                       //alert('Error1: ' + jqXHR.responseText);
                                       //alert('Error2: ' + textStatus);
                                       //alert('Error3: ' + errorThrown);
                                       //callback
                                       data.onFailure(excep);
                               },

                        success: function(xmlResponse) {

                            //console.log(xmlResponse);

                            try
                                {
                                    var $mensaje = $(xmlResponse).find("mensaje");

                                    if($mensaje.attr("estadoOperacion") == "ok") {

                                            var $cot = $(xmlResponse).find("cot");

                                            var cot = null; 

                                            if($cot.length != 0)
                                            {
                                                cot = new Cot($cot.attr("tipoParte"), $cot.attr("numeroParte"));

                                                cot.setFechaSolicitud($cot.attr("fechaSolicitud"))
                                                cot.setHoraSolicitud($cot.attr("horaSolicitud"))
                                                cot.setNumeroCot($cot.attr("numeroCot"))
                                                cot.setCodigoIntegracion($cot.attr("codigoIntegridad"))
                                                cot.setNumeroComprobante($cot.attr("numeroComprobante"))
                                                cot.setCantidadRemitos($cot.attr("cantidadRemitos"))
                                                
                                                data.onSuccess(cot);
                                            }
                                            else
                                                {
                                                    var excep = new XMLException('Error', 'No se encontraron datos para el comprobante ingresado');

                                                    data.onFailure(excep);
                                                }
                                                
                                            //callback
                                            //data.onSuccess(cot);
                                    }
                                    else
                                    {
                                            var excep = new XMLException('Error', $mensaje.attr('mensaje'));

                                            //callback
                                            data.onFailure(excep);
                                    }
                                }
                                catch(e){

                                    var excep = new Exception('Error', e);

                                    //callback
                                    data.onFailure(excep);
                                }
                            }
                });
        };  

        return {            
            recuperarTransporte : recuperarTransporte,
            recuperarPesaje : recuperarPesaje,
            recuperarComprobanteEnCot : recuperarComprobanteEnCot
        };
};