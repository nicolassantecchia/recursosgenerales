/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Octubre 2014
 *  ---------------------------------------------
 */

var Transporte = function (mi_id, mi_nombre) { 

	//Atributos Privados
	var id = mi_id,
		nombre = mi_nombre, //String   
		listaDeChoferes = new ListaDeChoferes(), //Lista  
		listaDeMoviles = new ListaDeMovil(), //Lista  
		documentoIdentificador, //DocumentoIdentificador
		direccion, //Direccion
		telefono1, //String
	
		getDocumentoIdentificador = function() {
			return documentoIdentificador;
		},

		getId = function() {
			return id;
		},

		getListaDeChoferes = function() {
			return listaDeChoferes;
		},

		getListaDeMoviles = function() {
			return listaDeMoviles;
		},

		getDireccion = function() {
			return direccion;
		},

		getTelefono1 = function() {
			var respuesta = null;
			
			if (telefono1 != null) {
				respuesta = telefono1.trim();
			}
			return respuesta;
		},

		setTelefono1 = function(mi_telefono1) {
			telefono1 = mi_telefono1;
		},

		setDireccion = function(mi_direccion) {
			direccion = mi_direccion;
		},

		getNombre = function() {
			var respuesta = null;
			if (nombre!=null){
				respuesta = nombre.trim();
			}
			return respuesta;
		},

		setDocumentoIdentificador = function(mi_documentoIdentificador) {
			documentoIdentificador = mi_documentoIdentificador;
		},

		setId = function(mi_id) {
			id = mi_id;
		},

		setListaDeChoferes = function(mi_listaDeChoferes) {
			listaDeChoferes = mi_listaDeChoferes;
		},

		setListaDeMoviles = function(mi_listaDeMoviles) {
			listaDeMoviles = mi_listaDeMoviles;
		},

		setNombre = function(mi_nombre) {
			nombre = mi_nombre;
		},
		
		print = function() {
		
			console.log('===== TRANSPORTE =====');
			console.log('  ID= '+id);
			console.log('  nombre= '+nombre);
			console.log('  documentoIdentificador= '+documentoIdentificador);
			console.log('  direccion= '+direccion);
			console.log('  telefono1= '+telefono1);
		};
		
		return {
			print : print,
			getDocumentoIdentificador : getDocumentoIdentificador,
			getId : getId,
			getListaDeChoferes : getListaDeChoferes,
			getListaDeMoviles : getListaDeMoviles,
			getDireccion : getDireccion,
			getTelefono1 : getTelefono1,
			setTelefono1 : setTelefono1,
			setDireccion : setDireccion,
			getNombre : getNombre,
			setDocumentoIdentificador : setDocumentoIdentificador,
			setId : setId,
			setListaDeChoferes : setListaDeChoferes,
			setListaDeMoviles : setListaDeMoviles,
			setNombre : setNombre
		}   
}