/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var Cot = function(tipoParte, nroParte) {
    
    var co_tipoParte = tipoParte,
        co_nroParte = nroParte,
        co_fechsolic = "",
        co_horasolic = "",
        co_nroCot = 0,
        co_codintegr = "",
        co_nroCprb = "",
        co_cantRemi = 0,
                
        getTipoParte = function() {
            return co_tipoParte;
        },
        
        setTipoParte = function(tipoParte) {
            co_tipoParte = tipoParte;
        },

        getNumeroParte = function() {
            return co_nroParte;
        },

        setNumeroParte = function(nroParte) {
            co_nroParte = nroParte;
        },       

        getFechaSolicitud = function() {
            return co_fechsolic;
        },

        setFechaSolicitud = function(fechaSolicitud) {
            co_fechsolic = fechaSolicitud;
        },

        getHoraSolicitud = function() {
            return co_horasolic;
        },

        setHoraSolicitud = function(horaSolicitud) {
            co_horasolic = horaSolicitud;
        },

        getNumeroCot = function() {
            return co_nroCot;
        },

        setNumeroCot = function(numeroCot) {
            co_nroCot = numeroCot;
        },

        getCodigoIntegracion = function() {
            return co_codintegr;
        },

        setCodigoIntegracion = function(codigoIntegracion) {
            co_codintegr = codigoIntegracion;
        },

        getNumeroComprobante = function() {
            return co_nroCprb;
        },

        setNumeroComprobante = function(numeroComprobante) {
            co_nroCprb = numeroComprobante;
        },
                
        getCantidadRemitos = function() {
            return co_cantRemi;
        },
                
        setCantidadRemitos = function(cantidadRemitos) {
            co_cantRemi = cantidadRemitos;
        };
    
    return {
        getTipoParte : getTipoParte,
        setTipoParte : setTipoParte,
        getNumeroParte : getNumeroParte,
        setNumeroParte : setNumeroParte,
        getFechaSolicitud : getFechaSolicitud,
        setFechaSolicitud : setFechaSolicitud,
        getHoraSolicitud : getHoraSolicitud,
        setHoraSolicitud : setHoraSolicitud,
        getNumeroCot : getNumeroCot,
        setNumeroCot : setNumeroCot,
        getCodigoIntegracion : getCodigoIntegracion,
        setCodigoIntegracion : setCodigoIntegracion,
        getNumeroComprobante : getNumeroComprobante,
        setNumeroComprobante : setNumeroComprobante,
        getCantidadRemitos : getCantidadRemitos,
        setCantidadRemitos : setCantidadRemitos
    }
}

