/*
 *	Fernando Prieto - Marco Iglesias
 */
 
var MovilV3 = function(nroAsignado, mapa) {   
	
	var id = nroAsignado;
	var idGPS = 0;
	var posicion = new google.maps.LatLng(-38.71759016077481, -62.29836656223083);// Codimat S.A
	var map = mapa;
	var patente = "";
	var chofer = "";
	var velocidad = 0.0;
	var deriva = 0;
	var motorEncendido = false;
	var habilitadoParaMostrar = false;

	var posicionLabel = new google.maps.Point(5, 33)

	var geocoder = new google.maps.Geocoder();

	var DIREC_DESCONOCIDA = "Direcci&oacute;n Desconocida";
	var direccion_postal = DIREC_DESCONOCIDA;
	var infoOpened = false;

	var marker = new google.maps.Marker({
                                                position: posicion,
                                                map: map,
                                                draggable: false,
                                                raiseOnDrag: true,
                                                labelContent: id,
                                                title: 'Movil '+id, 
                                                labelAnchor: posicionLabel,
                                                //labelClass: "markerLabel", // the CSS class for the label
                                                labelInBackground: false
                                            });
                                
        //marcador para que la infowindow quede ubicada a buena altura.
        var markerForInfoW = new google.maps.Marker({ position: posicion, map: map });                                    
                                           
	markerForInfoW.setVisible(false);
        
	var markerWL = new MarkerWithLabel(marker); 
	markerWL.setVisible(false);

	var infoWindow = new google.maps.InfoWindow();

	google.maps.event.addListener(markerWL, 'click', function() {

		getDireccionPostal();
	});

	//Metodos privados

	function setContenidoInfoWindow() {

		generarInfoWindowContent();
		
                infoWindow.open(map, markerForInfoW);

		infoOpened = true;
	}

	function generarInfoWindowContent() {

		var contentString = '<div style="text-align: left; width: 360px;">'+
		     '<div style="display: table;">'+
		     	'<div style="display: table-row;">'+
		     		'<div style="display: table-cell; font-weight: bold;">Dominio</div>'+
		     		'<div style="display: table-cell;">'+patente+'</div>'+
		     	'</div>'+
			     '<div style="display: table-row;">'+
		     		'<div style="display: table-cell; font-weight: bold;">Chofer</div>'+
		     		'<div style="display: table-cell;">'+chofer+'</div>'+
		     	'</div>'+
		     	'<div style="display: table-row;">'+
		     		'<div style="display: table-cell; font-weight: bold;">N&deg; Asignado</div>'+
		     		'<div style="display: table-cell;">'+id+'</div>'+
		     	'</div>'+
		     	'<div style="display: table-row;">'+
		     		'<div style="display: table-cell; font-weight: bold;">N&deg; GPS</div>'+
		     		'<div style="display: table-cell;">'+idGPS+'</div>'+
		     	'</div>'+
		     	'<div style="display: table-row;">'+
		     		'<div style="display: table-cell; width: 110px; font-weight: bold;">Posici&oacute;n Actual</div>'+
		     		'<div style="display: table-cell;">'+direccion_postal+'</div>'+
		     	'</div>'+
		     '</div>'+
	    '</div>';

	    infoWindow.setContent(contentString);
	}

	function getDireccionPostal() {
		
		geocoder.geocode({'latLng': posicion}, function(results, status) {

		    if (status == google.maps.GeocoderStatus.OK) 
		    {
				if (results[0]) 
				{
					direccion_postal = parseDireccionPostal(results[0].formatted_address);
					setContenidoInfoWindow();
				} 
				else 
				{
					direccion_postal = DIREC_DESCONOCIDA; 
					setContenidoInfoWindow();
				}
			} 
			else 
			{
				direccion_postal =  DIREC_DESCONOCIDA; 
				setContenidoInfoWindow();
			}
		});
	}

	/*
	 *	Formatea una direccion postal quitando el país
	 */
	function parseDireccionPostal(dir) {
		
		var array = dir.split(", ");

		var resultado = "";

		for(var i = 0; i < array.length - 1; i++) 
		{
			resultado = resultado + array[i];

			if(i < array.length - 2) {
				resultado = resultado + ", ";
			}
		}

		return resultado;
	}


	//Metodos públicos	

    var setIdGPS = function(id) {
    	idGPS = id;
    },

    setChofer = function(chof) {
    	chofer = chof;
    },

    setPatente = function(pat) {
    	patente = pat;
    },

    setEnMovimiento = function() {
  
    	markerWL.setIcon(	new google.maps.MarkerImage(CONFIG.recursosExternos.recursosIntraServer + '/expedi/imagenes/ex18_seg/mapas/movimiento.gif',
							new google.maps.Size(40, 80),
							new google.maps.Point(0, 0),
							new google.maps.Point(21, 60)));
	},

    setDetenido = function() {

    	markerWL.setIcon(	new google.maps.MarkerImage(CONFIG.recursosExternos.recursosIntraServer + '/expedi/imagenes/ex18_seg/mapas/detenido.gif',
							new google.maps.Size(40, 80),
							new google.maps.Point(0, 0),
							new google.maps.Point(21, 60)));
    },

    setMotorParado = function() {  
  
    	markerWL.setIcon(	new google.maps.MarkerImage(CONFIG.recursosExternos.recursosIntraServer + '/expedi/imagenes/ex18_seg/mapas/motorApagado.gif',
							new google.maps.Size(40, 80),
							new google.maps.Point(0, 0),
							new google.maps.Point(21, 60)));
    },

    setPosicion = function (pos) {	
		
		if ((posicion.lat() != pos.lat()) || (posicion.lng() != pos.lng())) {

			infoWindow.close();
			posicion = pos;
			markerWL.setPosition(posicion);	
                      
                        markerForInfoW.setPosition(posicion);
 
 
			if(infoOpened) {
				getDireccionPostal();
				infoOpened = false;
			}
		}
    },

    setVelocidad = function(vel) {

    	if(vel != velocidad)
    	{
	    	velocidad = vel;
	    	definirEstadoMovil();
	    }
    },

    setDeriva = function(deri) {
    	deriva = deri;
    },

    setMotorEncendido = function(encendido) {

    	if(encendido != motorEncendido)
    	{
	    	motorEncendido = encendido;
	    	definirEstadoMovil();
	    }
    },

    setHabilitadoParaMostrar = function(habilitado) {

    	var aux = false;

    	if(habilitado == "true")
    	{
    		aux = true;
    	}


    	if(aux != habilitadoParaMostrar) {

	    	habilitadoParaMostrar = aux;

	    	if(habilitadoParaMostrar) {
	    		markerWL.setVisible(true);
	    	}
	    	else {
	    		markerWL.setVisible(false);
	    	}
	    }
    },

    definirEstadoMovil = function() {

	    if(velocidad > 0) {
			setEnMovimiento();
		}
		else {
			if(motorEncendido === "true") {
				setDetenido();
			}
			else{
				setMotorParado();
			}
		}
	},

    getId = function() {
        return id;
    },

    getIdGPS = function() {
    	return idGPS;
    },

    getChofer = function() {
    	return chofer;
    }

    getPatente = function() {
    	return patente;
    },

    getPosicion = function() {	
		return posicion;
    },

    getVelocidad = function() {
    	return velocidad;
    },

    getDeriva = function() {
    	return deriva;
    },

    getMotorEncendido = function() {
    	return motorEncendido;
    },

    getHabilitadoParaMostrar = function() {
    	return habilitadoParaMostrar;
    };
	
	return  {                      
                setIdGPS : setIdGPS,
                setChofer : setChofer,
                setPatente : setPatente,
                setEnMovimiento : setEnMovimiento,
                setDetenido : setDetenido,
                setMotorParado : setMotorParado,
                setPosicion : setPosicion,
                setVelocidad : setVelocidad,
                setDeriva : setDeriva,
                setMotorEncendido : setMotorEncendido,
                setHabilitadoParaMostrar : setHabilitadoParaMostrar,

                getId : getId,
                getIdGPS : getIdGPS,
                getChofer : getChofer,
                getPatente : getPatente,
                getPosicion : getPosicion,
                getVelocidad : getVelocidad,
                getDeriva : getDeriva,
                getMotorEncendido : getMotorEncendido,
                getHabilitadoParaMostrar : getHabilitadoParaMostrar
	}
}