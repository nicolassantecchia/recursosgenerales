/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Octubre 2014
 *  Requiere TDA.js
 *  ---------------------------------------------
 */
 
var ListaDeChoferes = function () {    

    var nuevaLista = Object.create(new Lista())

    nuevaLista.searchItem = function(id_chofer) {

        var itemActual = null;
		
        for(var i = 0; i < this.size(); i++) {

            itemActual = this.getItem(i);

            if(itemActual.getId() == id_chofer) {
                return itemActual;
            }                
        }
        return itemActual;
    };

    return nuevaLista;
}