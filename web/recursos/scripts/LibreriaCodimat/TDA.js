/*
 *  ---------------------------------------------
 *  Marco Iglesias - Fernando Prieto - Julio 2014
 *  ---------------------------------------------
 */

var Lista = function () {

    //Private attributes
    var lista = new Array(),
   //     length = 0,	
    	
        getItem = function(indice) {       
            if(indice >= 0 && indice < lista.length) { 
                return(lista[indice]);
            }

            return false;
    	},	
    	
        size = function() { 
            return lista.length; 
    	},	
    	
        insert = function(objeto) { 
            //lista[length] = objeto;
            lista.push(objeto);
           // length ++;
    	},

        replace = function(objeto, posicion){
            lista[posicion] = objeto;
        };

        insertFirst = function(objeto) {
            for(var i = lista.length ; i > 0 ; i--) {
                lista[i] = lista[i-1];
            }

            lista[0] = objeto;
            //length ++;
        },
        
        deleteAllItems = function() {//para liberar memoria
            for (var j = 0; j < lista.length; j++) {
                delete(lista[j]);
            }
            
            delete(lista);
            lista = new Array();

          //  length = 0;
    	},
        
        deleteItem = function(indice) {
            
            lista.splice(indice, 1);
            
            /*
            if(indice >= 0 && indice < length) {         

                for (var j = indice + 1; j < length; j++) {
                    lista[j - 1] = lista[j];
                }
                delete(lista[length - 1]);
                length --;
            }*/
        },
                
        /*  Recibe como parámetro una funcion que implementa un comparador que debe cumplir:
         *  function(a, b){
         *  
         *    ordenamiento de mayor a menor
         *    return a > b ? -1 : a < b ? 1 : 0;
         *    
         *    ordenamiento de menor a mayor
         *    return a > b ? 1 : a < b ? -1 : 0;
         *  }
        */        
        ordenar = function(comparador) {
            lista = lista.sort(comparador);
        };        

        return  { 
            
            getItem: getItem,
            size: size,
            insert: insert,
            replace : replace,
            insertFirst : insertFirst,
            deleteAllItems: deleteAllItems,
            deleteItem: deleteItem,
            ordenar : ordenar
        }    
}




/*
var lista1 = new Lista();
var lista2 = new Lista();

lista1.insert("a");

lista2.insert("0");
lista2.insert("1");
lista2.insert("2");
lista2.insert("3");
lista2.insertFirst("-1");

console.log("Arreglo 1 # "+lista1.size());

for(var p = 0; p < lista1.size(); p++) {
    
    console.log("["+p+"] = "+lista1.getItem(p));
    
}


lista1.deleteItem(0);
console.log("Elimino a... # "+lista1.size());

for( p = 0; p < lista1.size(); p++) {
    
    console.log("["+p+"] = "+lista1.getItem(p));
    
}

console.log("Arreglo 2 # "+lista2.size());

for( p2 = 0; p2 < lista2.size(); p2++) {
    
    console.log("["+p2+"] = "+lista2.getItem(p2));
    
}


lista2.deleteItem(2);

console.log("Elimino la posicion 2 que contiene 1... # "+lista2.size());

for( p2 = 0; p2 < lista2.size(); p2++) {
    
    console.log("["+p2+"] = "+lista2.getItem(p2));
    
}


lista2.deleteItem(3);
console.log("Elimino la posicion 3 que contiene 3...# "+lista2.size());

for( p2 = 0; p2 < lista2.size(); p2++) {
    
    console.log("["+p2+"] = "+lista2.getItem(p2));
    
}

lista2.deleteAllItems();
console.log("Elimino .. # "+lista2.size());

for( p2 = 0; p2 < lista2.size(); p2++) {
    
    console.log("["+p2+"] = "+lista2.getItem(p2));
    
}

lista2.insertFirst("5");
console.log(lista2.getItem(0));
console.log(lista2.getItem(1)); */