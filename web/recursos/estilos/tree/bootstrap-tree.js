/*
 * Creado por alguien, editado por Fernando Prieto
 */
 
var BootstrapTree = function(args) {

	var cantidadNodosMostrados = 50;
	
	if(args.cantidadNodosMostrados)
	{
		cantidadNodosMostrados = args.cantidadNodosMostrados;
	}

	var textoContraer = 'Contraer esta rama';
	var textoExpandir = 'Expandir esta rama';
	
	if(args != null && args.textoContraer)
	{
		textoContraer = args.textoContraer;
	}
	
	if(args != null && args.textoExpandir)
	{
		textoExpandir = args.textoExpandir;
	}
        
        var idModal = null;
        
        if(args != null && args.modal)
        {
            idModal = args.modal;
        }
	
	var getCantidadNodosMostrados = function() {
			return cantidadNodosMostrados;
		},
	
		inicializarVisualizacionRaiz = function (id_raiz) {
			  $('#'+id_raiz).attr('role', 'tree');
		},

		inicializarVisualizacionSubarbol = function (id_span, esHoja, titulo) {
			
			$('#'+id_span).find(' > ul').attr('role', 'group');

			$('#'+id_span).parent('li:has(ul)').addClass('parent_li').attr('role', 'treeitem');

                        if(!esHoja)
                        {
                            $('#'+id_span).attr('title', textoExpandir);
                        }
                        else {
                            $('#'+id_span).attr('title', titulo);
                        }

			$('#'+id_span).click(function (e) {
                            
				var children = $(this).parent('li.parent_li').find(' > ul > li');  

				if (children.is(':visible')) {
					children.hide('fast'); 

					var icon = $(this).attr('title', textoExpandir).find(' > i');
					if(icon.hasClass('glyphicon')) {
						icon.addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
					}
				}
				else {

					children.show('fast');

					var icon = $(this).attr('title', textoContraer).find(' > i');
					if(icon.hasClass('glyphicon')) {
						icon.addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
					}
				}

				e.stopPropagation();
			});
		},

		manejarNodoSinHijos = function (id_span) { 

			$('#'+id_span).attr('title', '');
			$('#'+id_span).find('> i').removeClass('glyphicon').removeClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign').removeClass('loading-spinner');
		},

		cargandoSubArbol = function (id_span) {
			$('#'+id_span).find('> i').removeClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign').addClass('loading-spinner');
		},

		finalizarCargaSubArbol = function (id_span) {
			$('#'+id_span).find('> i').addClass('glyphicon-minus-sign').removeClass('loading-spinner');
		},

		eliminarArbol = function (idContenedor) {

			$('#'+idContenedor+' > ul').remove();
		},

		/*
		 *  Crea las raíces (primer nodo de un arbol) cuya información está contenida en arreglo
		 *
		 *  arreglo: array con los siguentes campos -> arreglo['id_raiz'], arreglo['nombre_raiz']
		 */
		crearRaiz = function (idContenedor, id_raiz, nombre_raiz) {
                    
			var codigo =    '<ul id="ul-'+id_raiz+'">'+
								'<li>'+
									'<span id="'+id_raiz+'" class="tree-raiz"><i class="glyphicon glyphicon-plus-sign"></i> '+nombre_raiz+'</span>'+
									'<ul></ul>'+
								'</li>'+
							'</ul>';
			
			$('#'+idContenedor).append(codigo);

			$('#'+idContenedor).show();   

			inicializarVisualizacionRaiz('ul-'+id_raiz);
			inicializarVisualizacionSubarbol(id_raiz);
                        
                        if(idModal != null)
                        {
                            centrarModal(idModal);
                        }
		},

		crearNodo = function (id_padre, id_nodo, nombre_nodo, id_li) {

			var codigo =    '<li id="'+id_li+'">'+
								'<span id="'+id_nodo+'" class="tree-nodo"><i class="glyphicon glyphicon-plus-sign"></i> '+nombre_nodo+'</span>'+
								'<ul></ul>'+
							'</li>';

			$('#'+id_padre).next().append(codigo);

			inicializarVisualizacionSubarbol(id_nodo);
                        
                        if(idModal != null)
                        {
                            centrarModal(idModal);
                        }
		},

		crearHoja = function (id_padre, id_hoja, nombre_hoja, titulo) {

			var codigo =    '<li>'+
                                                '<span id="'+id_hoja+'" class="tree-hoja">'+nombre_hoja+'</span>'+
                                        '</li>';

			$('#'+id_padre).next().append(codigo);

			inicializarVisualizacionSubarbol(id_hoja, true, titulo);
                         
                        if(idModal != null)
                        {
                            centrarModal(idModal);
                        }
		},
                        
                contraerNodo = function(nodo) {
                    
                    var children;
                    
                    children = nodo.parent('.parent_li').find(' > ul > li');
                    
                    children.hide('fast'); 

                    var icon = nodo.attr('title', textoExpandir).find(' > i');
                    
                    if(icon.hasClass('glyphicon')) {
                        icon.addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
                    }
                    
                },
                        
                expandirNodo = function(nodo) {
                    
                    var children;
                    
                    children = nodo.parent('.parent_li').find(' > ul > li');
                    children.show('fast');

                    var icon = nodo.attr('title', textoContraer).find(' > i');
                    
                    if(icon.hasClass('glyphicon')) {
                        icon.addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                    }
                    
                }
		
		return {
				getCantidadNodosMostrados : getCantidadNodosMostrados,
				inicializarVisualizacionRaiz : inicializarVisualizacionRaiz,
				inicializarVisualizacionSubarbol : inicializarVisualizacionSubarbol,
				manejarNodoSinHijos : manejarNodoSinHijos,
				cargandoSubArbol : cargandoSubArbol,
				finalizarCargaSubArbol  : finalizarCargaSubArbol,
				eliminarArbol  : eliminarArbol,
				crearRaiz : crearRaiz,
				crearNodo : crearNodo,
				crearHoja : crearHoja,
                                contraerNodo : contraerNodo,
                                expandirNodo : expandirNodo
		};
}