/*
 * **************************
 * TECLADO NUMERICO REDUCIDO
 * **************************
 * 
 * REQUIERE: 1)../scripts/caret/jquery.caret.js
 *           2)   tecladoVirtual.js
 * 
 * Crea un modal con un teclado numerico 
 * @param id - ID que tendrá el modal al ser creado.
 * @param $elementoPadre - elemento al cual se le hará append del modal creado
 */
var TecladoNumericoReducido = function(id, $elementoPadre) {    
    
    var idModal = id,
        $parent = $elementoPadre,
        $modal = null,        
        $input = null,
        $teclasNumericas = null;     

    var generarHTML = function() {
        
        $modal = $('<div id="'+idModal+'" class="modal" tabindex="-1" role="dialog" data-backdrop="static"></div>');

        $parent.append($modal);

        $modal.append(

                '<div class="modal-dialog modal-teclado-numerico">'+
                  '<div class="modal-content">'+
                      '<div class="modal-header">'+
                          '<div class="modal-title-label">'+
                              '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                              '<label class="modal-title">Teclado en Pantalla</label>'+
                          '</div>'+
                      '</div>'+
                      '<div class="modal-body modal-body-teclado">'+
                          '<input type="text" class="form-control input-lg input-numpad input-virtualkeyboard" />'+
                          '<div class="teclado">'+   
                              '<div class="pull-left">'+

                                  '<div class="fila-numpad-funciones">'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num btn-numpad-num-texto" data-value="clear">Limpiar</button>'+
                                      '<button type="button" class="btn btn-default btn-numpad-separacion-right btn-numpad-num btn-numpad-horizontal-doble btn-numpad-num-texto" data-value="backspace">&larr;&nbsp;&nbsp;Retroceso</button>'+
                                  '</div>'+ 

                                  '<div>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num btn-numpad-num-texto btn-numpad-disp-none" data-value="bloqNum" disabled>Bloq<br/>Num</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num btn-numpad-disp-none" data-value="/" disabled>/</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num btn-numpad-disp-none" data-value="*" disabled>*</button>'+                        
                                  '</div>'+

                                  '<div class="fila-numpad-interna">'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="7">7</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="8">8</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num" data-value="9">9</button>'+
                                  '</div>'+

                                  '<div class="fila-numpad-interna">'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="4">4</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="5">5</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num" data-value="6">6</button>'+
                                  '</div>'+

                                  '<div class="fila-numpad-interna">'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="1">1</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num" data-value="2">2</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num" data-value="3">3</button>'+
                                  '</div>'+

                                  '<div>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-separacion-right btn-numpad-num btn-numpad-horizontal-doble" data-value="0">0</button>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num btn-separadorDecimal" data-value=".">.</button>'+
                                  '</div>'+
                              '</div>'+
                              '<div class="pull-right">'+
                                  '<div class="fila-numpad-funciones">'+
                                      '<button type="button" class="btn btn-default btn-numpad-num btn-numpad-hide"></button>'+
                                  '</div>'+ 
                                  '<div>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num btn-numpad-disp-none" data-value="-" disabled>-</button>'+
                                  '</div>'+
                                  '<div class="fila-numpad-interna">'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num btn-numpad-vertical-doble btn-numpad-hide" data-value="+" disabled>+</button>'+
                                  '</div>'+
                                  '<div>'+
                                      '<button type="button" tabindex="-1" class="btn btn-default btn-numpad-num btn-numpad-num-texto btn-numpad-vertical-doble" data-value="enter">Enter</button>'+
                                  '</div>'+
                              '</div>'+
                              '<div class="clear-both"></div>'+
                          '</div>'+
                      '</div>'+
                  '</div>'+
                '</div>');
        
        $input = $("#"+idModal+" .input-numpad.input-virtualkeyboard");
        $teclasNumericas = $("#"+idModal+" .btn-numpad-num");
    };
    
    //GENERO EL HTML
    generarHTML();
    
    var bloqNum = "bloqNum",
        slash = "/",
        asterisco = "*",
        barraMedia = "-",
        mas = "+",
        enter = "enter",
        separadorDecimal = ".",
        clear = "clear",
        backspace = "backspace",
    
        elementoAsociado = null;
    
    $modal.on('shown.bs.modal', function(event) {
        $input.focus();
    });
    
    //Pongo el foco en el input siempre que se haga click en cualquier lado del modal menos en los botones y en el propio input
    $modal.click(function(event){
        if($(event.target).is("#"+idModal+"  *:not(.btn-numpad-num, .input-virtualkeyboard)")) {
            $input.focus();
        }
    });    
    
    $input.keypress(function(event) { 
        event.preventDefault();
    });
    
    $input.on('input', function(event) { 
        anularSeparadorDecimal();
    });
    
    $teclasNumericas.mouseup(function(event){ 
        
        if(event.which != 1) {
            event.preventDefault();
        }
        else {
            
            var tecla = $(this).data("value");

            if(!esTeclaFuncion(tecla) || esTeclaBorradora(tecla)) {
                
                switch(tecla) {
                    
                    case clear:     efectuarClear($input);
                                    break;
                                    
                    case backspace: efectuarBackspace($input);
                                    break;
                                    
                    default:        efectuarEscrituraTecla($input, tecla);
                }                
                
                anularSeparadorDecimal();
            }
            else {
                if(tecla === enter) {
                    
                    lanzarEventoEnter();
                    $modal.modal("hide");
                    $input.val("");
                }
            }
        }
        
        $input.focus();
    });
    
    $teclasNumericas.mousedown(function(event) {  
        if(event.which != 1) {
            event.preventDefault();
        }
    });
    
    var efectuarBackspace = function($input) {
        
            var posicionCursor = $input.caret();
            var value = $input.val();
                        
            if(posicionCursor !== 0) {//si el cursor no está al principio          

                if(value.length == posicionCursor) {
                    $input.val(value.substring(0, posicionCursor - 1));
                }
                else {
                    $input.val(value.substring(0, posicionCursor - 1) + value.substring(posicionCursor, value.length));
                }

                $input.caret(posicionCursor - 1);
            }   
        },
                
        efectuarClear = function($input) {
            $input.val("");
        },
                
        efectuarEscrituraTecla = function($input, valorAEscribir) {
            
            var posicionCursor = $input.caret();
            var value = $input.val();
            var valueFinal = "";

            if(posicionCursor === 0) {//cursor al principio
                valueFinal = valorAEscribir + value;
            }
            else {                                                    
                if(value.length === posicionCursor) { //escribiendo al final
                    valueFinal = value + valorAEscribir;
                }
                else { //esta en una posicion del medio                                
                    valueFinal = value.substring(0, posicionCursor) + valorAEscribir + value.substring(posicionCursor, value.lenght) 
                }
            }

            $input.val(valueFinal).caret(posicionCursor + 1);
        },
    
        esTeclaFuncion = function(data) {        
            return data === bloqNum || data === slash || data === asterisco || data === barraMedia  || data === mas || data === enter;
        },
        
        esTeclaBorradora = function(data) {
            return data === backspace || data === clear;
        },
        
        anularSeparadorDecimal = function() {
        
            var $teclaSeparadora = $("#"+idModal+" .btn-separadorDecimal");

            if($input.val().indexOf(separadorDecimal) != -1) {
                $teclaSeparadora.attr('disabled', true);
            }
            else {
                $teclaSeparadora.attr('disabled', false);
            }
        },
                
        lanzarEventoEnter = function() {
            $modal.trigger('teclaEnterPresionada', {elementoAsociado : elementoAsociado, textoInput : $input.val()});
        };
        
    $modal.setTexto = function(text) {
        $input.val(text);
    };    
        
    $modal.setElementoAsociado = function($elemento) {
        elementoAsociado = $elemento;
    };
    
    $modal.getElementoAsociado = function() {
        return elementoAsociado;
    };  
    
    return $modal;
};