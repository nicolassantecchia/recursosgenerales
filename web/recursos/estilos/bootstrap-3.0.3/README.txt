/*
 * Fernando Prieto 31/12/2013
*/

Framework de CSS (Twitter Bootstrap)
Version 3.0.3 - http://getbootstrap.com/



*********************************************
 Ejemplo de uso de un submenú en un dropdown
*********************************************


    <ul class="dropdown-menu multi-level">
        <li class="dropdown-submenu">
            <a href="#" class="dropdown-toggle puto" data-toggle="dropdown">Submenu</a>
            <ul class="dropdown-menu">
                <li><a href="#">Submenu item 1</a></li>
                <li><a href="#">Submenu item 2</a></li>
                <li><a href="#">Submenu item 3</a></li>
                <li><a href="#">Submenu item 4</a></li>
                <li><a href="#">Submenu item 5</a></li>
            </ul>
        </li>
    </ul>