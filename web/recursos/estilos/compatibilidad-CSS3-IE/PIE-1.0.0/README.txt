Agrega a Internet Explorer compatibilidad con ciertas propiedades de CSS3

Ej  border-radius
    box-shadow
    linear-gradient

La ruta de la css class a PIE.htc debe ser relativa al archivo HTML que utiliza la clase y no 
a la ubicación de la stylesheet. Con lo cual esta clase se define en el archivo html.

behavior: url(/recursos/estilos/compatibilidad-CSS3-IE/PIE-1.0.0/PIE.htc);

http://css3pie.com/