<%@ page language="java" session="false" contentType="text/html; charset=windows-1252"
    import="java.net.URLDecoder"  
    import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
    
    <%
        String page_title = request.getParameter("page_title");
    %>
    <c:import url="headerGeneral.jsp">
        <c:param name="page_title" value="<%= page_title %>" />
    </c:import>
 
    <div class="div-container">

        <% 
            String mostrarLeyendaNavegadores = request.getParameter("TEXTO_BROWSERS");  

            if(mostrarLeyendaNavegadores == null || Boolean.parseBoolean(mostrarLeyendaNavegadores)) {
        %>
                <div style="font-size: 11.5pt; margin-bottom: 40px; ">
                    Para visualizar correctamente este sitio debe utilizar <strong>Google Chrome</strong>, <strong>Mozilla Firefox</strong> o <strong>Internet Explorer 11</strong>
                </div>
        <%  
            }
        %>

        <ol id="breadcrumb-nav" class="breadcrumb breadcrumb-nav"></ol>

        <div class="div-marco"> 
            <div id="div-marco-titulo" class="div-marco-titulo compatibilidad-IE">
                <span><% out.println(request.getParameter("form_title")); %></span>           
            </div>
            <div id="div-marco-body" class="div-marco-body compatibilidad-IE">  

                <% 
                    String nombre_usuario = request.getParameter("USER_NAME");  
                    String url_logout = request.getParameter("URL_LOGOUT");  


                    int user_id = -1;
                    String id = request.getParameter("USER_ID");

                    if(id != null)
                    {    
                        user_id = Integer.parseInt(id);
                    }                        

                    //controlo si hay un logueo - configuracion.js se encarga de hacer el deslogueo cuando se hace click en link-logout
                    if(nombre_usuario != null && user_id != -1 && url_logout != null)
                    { 
                        out.println("<div class=\"div-sesion\">"
                                        + "<label class=\"label-sesion\">Sesi&oacute;n iniciada como:&nbsp;&nbsp;&nbsp;&nbsp;"+nombre_usuario+"</label>"
                                        + "<a id=\"link-logout\" href=\""+url_logout+"\" class=\"btn btn-xs btn-link link-sesion\"> "
                                            + "<span class=\"glyphicon glyphicon-log-out\"> </span>"
                                            + "<span class=\"texto-logout\"> Cerrar Sesi&oacute;n</span>"
                                        + "</a>"
                                        + "<div class=\"clear-both\"></div>"
                                + "</div>");
                    }                    
                %>
                <div id="div-marco-body-loading-bar" class="loading-bar"></div>
                <div class="clear-both"></div>