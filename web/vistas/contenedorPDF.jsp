<%-- 
    Document   : contenedorPDF
    Created on : 18/12/2014, 11:52:40
    Author     : Fernando Prieto -  Comentario de prueba
--%>

<%@page import="java.nio.charset.Charset"%>
<%@page session="false" contentType="text/html" pageEncoding="windows-1252"%>

<%
    String title = request.getParameter("nombreVentana");
    String urlPDF = request.getParameter("url");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title><%= title %></title>
        <link type="text/css" href="../recursos/estilos/bootstrap-3.0.3/css/personalizados/CodimatEstilos.css" rel="stylesheet" />
        <style>
            .iframe {
                width: 100%;
                display: none;                
            }
            
            body {
                margin: 0px auto;
                padding: 0px;
            }
        </style>
        
        <script charset="utf-8" type="text/javascript" src="../recursos/scripts/lib/jquery-1.11.0.min.js"></script>
        <script charset="utf-8" type="text/javascript" src="../recursos/scripts/lib/jquery.actual.min.js"></script>
        
        <script>
            
            $(document).ready(function(){
                
                $(window).trigger('resize'); 
                
                var $iframe = $('#iframe');
                
                $iframe.attr('src', encodeURI($('#url').val()));
                
                $iframe.load(function(){
                    
                    $('#div-iframe-loading').hide();
                    
                }).show();                
              
            }); 
            
            $(window).resize(function(){
        
                var altura = $(window).innerHeight() - 4;
                var ancho = $(document).innerWidth();
                
                //console.log("altura "+altura+" altura2 "+$(window).height()+" ancho "+ancho);
                
                var $divIframe = $('#div-iframe-loading');
                
                $divIframe.addClass('loading-sol');
                
                $divIframe.css({
                        top: (altura - 4 - $('#div-iframe-loading').height()) / 2, 
                        left: (ancho - $('#div-iframe-loading').width()) / 2, 
                        position:'absolute'
                    });
                    
                $('#iframe').height(altura);
            });

        </script>
    </head>
    <body>
        <input id="url" type="hidden" value="<%= urlPDF %>">
        
        <iframe id="iframe" frameBorder="0" class="iframe" src=""></iframe>      
        <div id="div-iframe-loading"></div>
        
    </body>
</html>
