<%@ page language="java" session="false" contentType="text/html; charset=windows-1252" import="java.net.InetAddress"
import="LibreriaCodimat.Utiles.Cadenas"
import="java.util.ArrayList"
import="LibreriaCodimat.Sistema.SesionCodimat"
import="LibreriaCodimat.Sistema.OperacionesSobreBDSistema"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- Fernando Prieto - 30/12/2013 -->

<% 
    String evitarCacheo = Cadenas.getCadenaParaEvitarCacheoScriptsCss();
%>

<c:set var="IP_SERVER" value="${pageContext.request.serverName}" /> 
<c:set var="PUERTO" value="${pageContext.request.serverPort}" /> 

<!DOCTYPE html>
<html lang="es">
    <head>
            <title><% out.println(request.getParameter("page_title"));%></title>

            <c:set var="scripts" value="http://${IP_SERVER}:${PUERTO}/recursosGenerales/recursos/scripts"/>
            <c:set var="estilos" value="http://${IP_SERVER}:${PUERTO}/recursosGenerales/recursos/estilos" />
    
            <!-- Este tag permite asegurar el mejor rendering posible en cada version soportada de Internet Explorer. -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
            
            <!-- Bootstrap -->
            <link href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/bootstrap.css" rel="stylesheet" />
            <link href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/bootstrap-theme.css" rel="stylesheet" />
            <link href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/normalize.css" rel="stylesheet" />

            <link href="<c:out value="${estilos}"/>/autocomplete/jquery.autocomplete.css" rel="stylesheet" />

            <link type="text/css" href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/personalizados/CodimatEstilos.css<%= evitarCacheo %>" rel="stylesheet" />
	
			
            <script type="text/javascript">         
                
                    var IP_SERVER = "<c:out value="${IP_SERVER}"/>"; //Ip del servidor para ser utilizada por los scripts
                    var PUERTO_TOMCAT = "<c:out value="${PUERTO}"/>"; //Puerto utilizado para la comunicacion con el servidor
                    
                    var browserIE8 = false; 
                    
            </script>
            

            <!--[if IE 9]>

                <link type="text/css" href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/personalizados/CodimatEstilos-IE9.css" rel="stylesheet" />

            <![endif]-->

            <!--[if IE 8]>
                <style type="text/css">   

                    .compatibilidad-IE {
                            behavior: url(<c:out value="${estilos}"/>/compatibilidad-CSS3-IE/PIE-1.0.0/PIE.htc);
                        }

                </style>

                <link type="text/css" href="<c:out value="${estilos}"/>/bootstrap-3.0.3/css/personalizados/CodimatEstilos-IE8.css" rel="stylesheet" />

                <script type="text/javascript">
                    browserIE8 = true;  
                </script>

            <![endif]-->
            
            <!--[if lt IE 10]>
                <style type="text/css">

                    fieldset {
                        position:relative;
                        padding-top: 8px;
                        margin-bottom: 17px !Important;             
                    }

                    fieldset > legend {
                        display: inline-block; 
                        position: absolute; 
                        top: -9px;
                        background-color: #FFFFFF;
                    }

                    fieldset > div {
                        margin-top: 6px;
                    }

                    .fieldset > div.con-textarea {
                        margin-bottom: 16px !Important;
                    }

                    .button-submit {
                        margin-top: 0px;
                    }
                </style>

            <![endif]-->

            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/lib/jquery-1.11.0.min.js"></script>

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

            <!--[if lt IE 9]>
                <script charset="utf-8" src="<c:out value="${estilos}"/>/bootstrap-3.0.3/js/html5shiv.js"></script>
                <script charset="utf-8" src="<c:out value="${estilos}"/>/bootstrap-3.0.3/js/respond.min.js"></script>
            <![endif]-->

            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/lib/jquery.cookie.js"></script>    
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/lib/jquery.actual.min.js"></script> 
            
            <script type="text/javascript" charset="utf-8" src="<c:out value="${estilos}"/>/bootstrap-3.0.3/js/bootstrap.js"></script>

            <script type="text/javascript" charset="utf-8" src="<c:out value="${estilos}"/>/autocomplete/jquery.autocomplete.js"></script>
            
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/config/configuracionGeneral.js<%= evitarCacheo %>"></script>
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/config/configuracionSistem.js<%= evitarCacheo %>"></script>
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/config/configuracionRecursosGenerales.js<%= evitarCacheo %>"></script>
            
            <script type="text/javascript" charset="utf-8" src="http://${IP_SERVER}:${PUERTO}/sistem/recursos/scripts/config/configuracion.js<%= evitarCacheo %>"></script>
            
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/Sistema/OperacionesSobreServidorSistema.js<%= evitarCacheo %>"></script>
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/Sistema/Exception.js<%= evitarCacheo %>"></script>
            
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/config/configuracion.js<%= evitarCacheo %>"></script>

            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/Utils.js<%= evitarCacheo %>"></script>
            <script type="text/javascript" charset="utf-8" src="<c:out value="${scripts}"/>/LibreriaCodimat/LibreriaCodimatVisual.js<%= evitarCacheo %>"></script>
    </head>
    <body>