/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.ListaDeDatosActualizacionCostos;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario407
 */
public class recuperarDatosActualizacionCostos extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarDatosActualizacionCostos.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        response.setContentType("application/xml;charset=UTF-8");
        
        int idInventario = -1;
        int idProveedor = -1;
        int i = 0;
        int proveedor = -1;
        int articuloPropio = -1;
        int codigoFamilia = -1;
        String nombreFamilia = "";
        int codigoRubro = -1;
        String nombreRubro = "";
        int codigoSubrubro = -1;
        String nombreSubrubro = "";
        String subrubroFormula1 = "";
        String subrubroFormula2 = "";
        String subrubroFormula3 = "";
        String subrubroFormula4 = "";
        String subrubroFormula5 = "";
        String subrubroFormula6 = "";
        int coeficienteDolarSubrubro = -1;
        int tipoModificacionPrecioCosto = -1;
        String descripcionModificacionPrecioCosto = "";
        ListaDeDatosActualizacionCostos lista = null;
        
        /*int idProveedor;
        String idRubro;
        String idFamilia;*/
        out.println("<results>");

        try{ //Bloque 100
            idInventario = Integer.parseInt(request.getParameter("idInventario"));
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            /*idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idRubro = request.getParameter("idRubro").trim();
            idFamilia = request.getParameter("idFamilia").trim();*/

            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    //ListaDeSubrubrosProveedor lista = BD.getListaDeSubRubrosProveedor(idProveedor,idFamilia,idRubro);
                    lista = BD.getDatosActualizacionCostos(idProveedor, idInventario);
                    
                    if ((lista != null) && (lista.size() > 0))
                    {
                        for(i=0; i<lista.size(); i++)
                        {
                            proveedor = lista.getDatosActualizacionCostos(i).getCodigoProveedor();
                            articuloPropio = lista.getDatosActualizacionCostos(i).getCodigoArticuloPropio();
                            codigoFamilia = lista.getDatosActualizacionCostos(i).getCodigoFamilia();
                            nombreFamilia = lista.getDatosActualizacionCostos(i).getNombreFamilia();
                            codigoRubro = lista.getDatosActualizacionCostos(i).getCodigoRubro();
                            nombreRubro = lista.getDatosActualizacionCostos(i).getNombreRubro();
                            codigoSubrubro = lista.getDatosActualizacionCostos(i).getCodigoSububro();
                            nombreSubrubro = lista.getDatosActualizacionCostos(i).getNombreSubrubro();
                            subrubroFormula1 = lista.getDatosActualizacionCostos(i).getSubrubroFormula1();
                            subrubroFormula2 = lista.getDatosActualizacionCostos(i).getSubrubroFormula2();
                            subrubroFormula3 = lista.getDatosActualizacionCostos(i).getSubrubroFormula3();
                            subrubroFormula4 = lista.getDatosActualizacionCostos(i).getSubrubroFormula4();
                            subrubroFormula5 = lista.getDatosActualizacionCostos(i).getSubrubroFormula5();
                            subrubroFormula6 = lista.getDatosActualizacionCostos(i).getSubrubroFormula6();
                            coeficienteDolarSubrubro = lista.getDatosActualizacionCostos(i).getCoeficienteDolarSubrubro();
                            tipoModificacionPrecioCosto = lista.getDatosActualizacionCostos(i).getTipoModificacionPrecioCosto();
                            descripcionModificacionPrecioCosto = lista.getDatosActualizacionCostos(i).getDescripcionModificacionPrecioCosto();
                            out.println("<rs codigoProveedor=\""+proveedor+"\" codigoArticulo=\""+articuloPropio+"\" codigoFamilia=\""+codigoFamilia+"\" nombreFamilia=\""+nombreFamilia+"\" codigoRubro=\""+codigoRubro+"\" nombreRubro=\""+nombreRubro+"\" subRubroCodigo=\""+codigoSubrubro+"\" subrubroNombre=\""+nombreSubrubro+"\" subrubroFormula1=\""+subrubroFormula1+"\" subrubroFormula2=\""+subrubroFormula2+"\" subrubroFormula3=\""+subrubroFormula3+"\" subrubroFormula4=\""+subrubroFormula4+"\" subrubroFormula5=\""+subrubroFormula5+"\" subrubroFormula6=\""+subrubroFormula6+"\" coeficienteDolarSubrubro=\""+coeficienteDolarSubrubro+"\" tipoModificacionPrecioCosto=\""+tipoModificacionPrecioCosto+"\" descripcionModificacionPrecioCosto=\""+descripcionModificacionPrecioCosto+"\" ></rs>");
                        }
                    }
                    else
                        out.println("<rs codigoProveedor=\""+0+"\" codigoArticulo=\""+0+"\" codigoFamilia=\""+0+"\" nombreFamilia=\"\" codigoRubro=\""+0+"\" nombreRubro=\"\" subRubroCodigo=\""+0+"\" subrubroNombre=\"\" subrubroFormula1=\"\" subrubroFormula2=\"\" subrubroFormula3=\"\" subrubroFormula4=\"\" subrubroFormula5=\"\" subrubroFormula6=\"\" coeficienteDolarSubrubro=\"\" tipoModificacionPrecioCosto=\""+0+"\" descripcionModificacionPrecioCosto=\"\" ></rs>");
                    
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Datos recuperados, pero no se pudo cerrar la conexion con la BD: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar los datos. Exception: "+hi.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
