/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.Articulo;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alejandro Battista
 */
public class recuperarArticuloInventario extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarArticuloInventario.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        //int idArticulo = -1;
        String idArticulo = "";
        int tLista = -1, idInventario = -1;
        String term = "";
        boolean esValorizacion = false;
        
        out.println("<results>");
        try{ //Bloque 100	          
            //idArticulo = Integer.parseInt(request.getParameter("idArticulo"));
            idArticulo = request.getParameter("idArticulo").trim();
            tLista = Integer.parseInt(request.getParameter("tipoLista"));
            term = request.getParameter("terminal");
            Articulo articulo;
            //idInventario = Integer.parseInt(request.getParameter("idInventario"));
            try{ //Bloque 101            
                  OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    //Articulo articulo = BD.recuperarArticuloInventario(idArticulo, tLista, term, idInventario);
                    if (request.getParameter("inventario") == null)
                        articulo = BD.recuperarArticuloInventario(idArticulo, tLista, term);
                    else
                        {
                            esValorizacion = true;
                            articulo = BD.recuperarArticuloDentroListaParaValorizacionInventario(Integer.parseInt(idArticulo), Integer.parseInt(request.getParameter("inventario")));
                        }
                    
                    if(articulo != null){
                        out.println("<rs "+
                                " idArticulo=\""+articulo.getCodigo()+"\"  "+
                                " nombreArticulo=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getIdentificacion().trim())+"\" "+
                                " peso=\""+articulo.getPeso()+"\"  "+
                                " unidadCalculo=\""+articulo.getUnidadCalculada()+"\"  "+
                                " precioCosto=\""+articulo.getPrecioCosto()+"\"  "+
                                " totalPrecargado=\""+articulo.getCantidad()+"\"  "+
                                " ></rs>");
                    }
                    try{ //Bloque 103	
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");     
                    }
                    catch (Exception g){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Se recupero el articulo, pero no se pudo cerrar la conexion con la Base de Datos: "+g.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Se recupero el articulo, pero no se pudo cerrar la conexion con la Base de Datos: "+g.getMessage()+"\"  />");
                    }
                }
                catch(Exception d){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar el articulo. Exception. "+d.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar el articulo. Exception. "+d.getMessage()+"\"  />");
                }
            }
            catch(Exception f){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo conectar con la Base de Datos. Exception. "+f.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo conectar con la Base de Datos. Exception. "+f.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception. "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception. "+e.getMessage()+"\"  />");
        }	
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
