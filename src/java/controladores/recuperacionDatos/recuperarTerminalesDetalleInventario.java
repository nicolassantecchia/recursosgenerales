/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.ListaDeDatosActualizacionCostos;
import LibreriaCodimat.Compras.ListaDeDetalleArticuloInventario;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario407
 */
public class recuperarTerminalesDetalleInventario extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarTerminalesDetalleInventario.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        response.setContentType("application/xml;charset=UTF-8");
        
        int idInventario = -1;        
        /*String codigoArticulo = "";
        String descripcionArticulo = "";
        double cantidadIngresada = 0;
        double pesoIngresado = 0;
        Date fechaInsercion = null;
        String horaInsercion = null;
        String usuario = "";*/
        String terminal = "";
        /*String contenedor = "";
        String posicion = "";
        int fueraLista = -1;
        int tipoCarga = -1;
        int estado = -1;
        int indiceDetalleInventario = -1;
        int indiceCabeceraInventario = -1;
        int indicePadre = -1;
        int indiceRegistro = -1;*/
        
        ListaDeDetalleArticuloInventario lista = null;
        int i=0;
                
        out.println("<results>");

        try{ //Bloque 100
            idInventario = Integer.parseInt(request.getParameter("idInventario"));

            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    lista = BD.recuperarListaDeTerminalesDetalleInventario(idInventario);
                    
                    if ((lista != null) && (lista.size() > 0))
                    {
                        for(i=0; i<lista.size(); i++)
                        {
                            /*codigoArticulo = lista.getDetalleArticuloInventario(i).getCodigoArticulo();
                            descripcionArticulo = lista.getDetalleArticuloInventario(i).getCodigoArticulo();
                            cantidadIngresada = lista.getDetalleArticuloInventario(i).getCantidadIngresada();
                            pesoIngresado = lista.getDetalleArticuloInventario(i).getPesoIngresado();
                            fechaInsercion = lista.getDetalleArticuloInventario(i).getFechaInsercion();
                            horaInsercion = lista.getDetalleArticuloInventario(i).getCodigoArticulo();
                            usuario = lista.getDetalleArticuloInventario(i).getUsuario();*/
                            terminal = lista.getDetalleArticuloInventario(i).getTerminal();
                            /*contenedor = lista.getDetalleArticuloInventario(i).getContenedor();
                            posicion = lista.getDetalleArticuloInventario(i).getPosicion();
                            tipoCarga = lista.getDetalleArticuloInventario(i).getTipoCarga();
                            estado = lista.getDetalleArticuloInventario(i).getEstado();
                            indiceDetalleInventario = lista.getDetalleArticuloInventario(i).getIndiceDetalleInventario();
                            indiceCabeceraInventario = lista.getDetalleArticuloInventario(i).getIndiceCabeceraInventario();
                            indicePadre = lista.getDetalleArticuloInventario(i).getIndicePadre();
                            indiceRegistro = lista.getDetalleArticuloInventario(i).getIndiceRegistro();*/
                            //out.println("<rs codigoArticulo=\""+codigoArticulo+"\" descripcionArticulo=\""+descripcionArticulo+"\" cantidadIngresada=\""+cantidadIngresada+"\" pesoIngresado=\""+pesoIngresado+"\" fechaInsercion=\""+fechaInsercion+"\" horaInsercion=\""+horaInsercion+"\" usuario=\""+usuario+"\" terminal=\""+terminal+"\" contenedor=\""+contenedor+"\" posicion=\""+posicion+"\" tipoCarga=\""+tipoCarga+"\" estado=\""+estado+"\" indiceDetalleInventario=\""+indiceDetalleInventario+"\" indiceCabeceraInventario=\""+indiceCabeceraInventario+"\" indicePadre=\""+indicePadre+"\" indiceRegistro=\""+indiceRegistro+"\" ></rs>");
                            out.println("<rs terminal=\""+terminal+"\" ></rs>");
                        }
                    }
                    else
                        out.println("<rs terminal=\"\" ></rs>");
                    
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Datos recuperados, pero no se pudo cerrar la conexion con la BD: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar los datos. Exception: "+hi.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
