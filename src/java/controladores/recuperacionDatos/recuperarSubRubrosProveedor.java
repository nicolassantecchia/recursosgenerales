/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.SubrubroProveedor;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarSubRubrosProveedor extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarSubRubrosProveedor.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        int idProveedor;
        String idRubro;
        String idFamilia;
        out.println("<results>");

        try{ //Bloque 100
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idRubro = request.getParameter("idRubro").trim();
            idFamilia = request.getParameter("idFamilia").trim();

            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    ListaDeSubrubrosProveedor lista = BD.getListaDeSubRubrosProveedor(idProveedor,idFamilia,idRubro);
                    
                    for (int i=0; i < lista.size(); i++){
                        SubrubroProveedor subRubro = lista.getSubrubro(i);
                        String subRubroId = cadenas.cambiarCaracteresReservadosParaXML(subRubro.getId());
                        String nombreSubRubro = cadenas.cambiarCaracteresReservadosParaXML(subRubro.getNombre());
                        out.println("<rs id=\""+i+"\" subRubroId=\""+subRubroId+"\" nombre=\""+nombreSubRubro+"\" ></rs>");
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperados los subrubros, pero no se pudo cerrar la conexion con la ND: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperados los subrubros, pero no se pudo cerrar la conexion con la ND: "+ee.getMessage()+"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar la lista de subrubros. Exception: "+hi.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al recuperar la lista de subrubros. Exception: "+hi.getMessage()+"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al conectar con la BD. Exception: "+fg.getMessage()+"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
