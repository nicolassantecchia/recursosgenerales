/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Sistema.OperacionesSobreBDSistema;
import LibreriaCodimat.Utiles.Cadenas;
import LibreriaCodimat.Ventas.ListaDeTiposDeComprobantes;
import LibreriaCodimat.Ventas.TipoComprobante;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarTiposDeComprobante extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarTiposDeComprobante.java";
    protected Cadenas cadenas;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");

        String claseActual = "Proy. WEB recursosGenerales.controladores.recuperacionDatos.recuperarTiposDeComprobante.jsp";

        out.println("<respuesta >");        

        ListaDeTiposDeComprobantes listaDeTiposDeComprobantes = null; 

        try  //Bloque 100
        {
            OperacionesSobreBDSistema BDSistema = new OperacionesSobreBDSistema();

            try //Bloque 101
            {
                listaDeTiposDeComprobantes = BDSistema.getListaDeTiposDeComprobantesDesdeBD();            

                try  //Bloque 102
                {
                    BDSistema.cerrarConexion(); 

                    TipoComprobante tipo = null;

                    for(int i = 0; i < listaDeTiposDeComprobantes.size(); i++)
                    {
                        tipo = listaDeTiposDeComprobantes.getTipoComprobante(i);
                        out.println("<tipo codigo=\""+tipo.getCodigo()+"\" abbr=\""+cadenas.cambiarCaracteresReservadosParaXML(tipo.getAbreviacion().trim())+"\" detalle=\""+cadenas.cambiarCaracteresReservadosParaXML(tipo.getDetalle().trim())+"\" />");
                    }

                    out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\" />");
                }
                catch(Exception excepcionCerrarBDSistema)  
                {
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo cerrar la sesion con la BD Sistema. Exception: "+excepcionCerrarBDSistema.getMessage());
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo cerrar la sesion con la BD Sistema. Exception: "+excepcionCerrarBDSistema.getMessage()+"\"  />");
                }  
            }
            catch(Exception excepcionRecupListaTipos)
            {
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo recuperar la lista de tipos de comprobante. Exception: "+excepcionRecupListaTipos.getMessage());
                BDSistema.cerrarConexion();
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar la lista de tipos de comprobante. Exception: "+excepcionRecupListaTipos.getMessage()+"\"  />");
            }
        }
        catch(Exception excepcionConectarConBDSistem)
        {
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "No se pudo conectar con la Base De Datos Sistem. Exception: "+excepcionConectarConBDSistem.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo conectar con la Base De Datos Sistem. Exception: "+excepcionConectarConBDSistem.getMessage()+"\"  />");
        }

        out.println("</respuesta >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
