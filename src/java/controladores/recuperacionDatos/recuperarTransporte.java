/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Expedicion.Chofer;
import LibreriaCodimat.Expedicion.ListaDeChoferes;
import LibreriaCodimat.Expedicion.OperacionesSobreBDExpedi;
import LibreriaCodimat.Expedicion.Transporte;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarTransporte extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarTransporte.java";
    protected Cadenas cadenas;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        
        out.println("<results>");
        try{ //Bloque 100	
            int idTransporte = Integer.parseInt(request.getParameter("idTransporte"));
            try{ //Bloque 101
                OperacionesSobreBDExpedi BD = new OperacionesSobreBDExpedi();
                try{ //Bloque 102
                    Transporte transporte = BD.recuperarTransporte(idTransporte);

                    if(transporte !=null){
                        out.println("<transporte "
                                + " idTransporte=\""+transporte.getId()+"\"  "
                                + " nombreTransporte=\""+cadenas.cambiarCaracteresReservadosParaXML(transporte.getNombre())+"\"  "
                                + " cuit=\""+transporte.getDocumentoIdentificador().getNro()+"\"  "
                                + " telefono=\""+cadenas.cambiarCaracteresReservadosParaXML(transporte.getTelefono1())+"\"  "
                                +" >");
                        ListaDeChoferes listaDeChoferes = transporte.getListaDeChoferes();
                        for(int i=0; i < listaDeChoferes.size(); i++){
                            Chofer chofer = listaDeChoferes.getChofer(i);
                            out.println("<chofer "
                                + " idChofer=\""+chofer.getId()+"\"  "
                                + " nombreChofer=\""+cadenas.cambiarCaracteresReservadosParaXML(chofer.getNombreYApellido())+"\"  "
                                + " cuit=\""+chofer.getDocumentoIdentificador().getNro()+"\"  "
                                +" />");
                        }
                        out.println("</transporte>");
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch(Exception fg){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperado el transporte, pero no se pudo cerrar la conexion con la BD . Exception: "+fg.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperado el transporte, pero no se pudo cerrar la conexion con la BD . Exception: "+fg.getMessage()+"\"  />");
                    }

                }
                catch (Exception gh){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar el transporte . Exception: "+gh.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar el transporte . Exception: "+gh.getMessage()+"\"  />");
                }
            }
            catch(Exception f){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la base de datos. Exception: "+f.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al conectar con la base de datos. Exception: "+f.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }	
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
