/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.DatosTotalesModificacionCostos;
import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.ListaDeDatosActualizacionCostos;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.BaseDeDatosPervasive;
import LibreriaCodimat.Utiles.Cadenas;
import LibreriaCodimat.Utiles.Numeros;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario407
 */
public class recuperarDatosTotalesModificacionCostosPorSubrubro extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarDatosTotalesModificacionCostosPorSubrubro.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        response.setContentType("application/xml;charset=UTF-8");
        
        int idInventario = -1;
        int codigoSubrubro = -1;
        int tipoModificacion = -1;
        double totalRecalculado = 0;
        double totalOriginal = 0;
        double diferencia = 0;
        double porcentajeDiferencia = 0;
        String fechaModificacion = "";
        String horaModificacion = "";
        int estado = 0;        
        DatosTotalesModificacionCostos datos = null;
        Numeros numeros = null;
        
        /*int idProveedor;
        String idRubro;
        String idFamilia;*/
        out.println("<results>");

        try{ //Bloque 100
            idInventario = Integer.parseInt(request.getParameter("idInventario"));
            codigoSubrubro = Integer.parseInt(request.getParameter("codigoSubrubro"));
            tipoModificacion = Integer.parseInt(request.getParameter("tipoModificacion"));

            try{ //Bloque 101
                //OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    
                    numeros = new Numeros();
                    //datos = BD.getDatosTotalesModificacionCostosPorSubrubro(codigoSubrubro, idInventario, tipoModificacion);
                    datos = this.getDatosTotalesModificacionCostosPorSubrubro(codigoSubrubro, idInventario, tipoModificacion);
                    
                    if (datos != null)
                    {
                        idInventario = datos.getIdInventario();
                        codigoSubrubro = datos.getCodigoSubrubro();
                        totalRecalculado = datos.getTotalRecalculado();
                        totalOriginal = datos.getTotalOriginal();
                        diferencia = datos.getDiferencia();
                        porcentajeDiferencia = datos.getPorcentajeDiferencia();
                        fechaModificacion = datos.getFechaModificacion();
                        horaModificacion = datos.getHoraModificacion();
                        estado = datos.getEstado();
                        
                        out.println("<rs idInventario=\""+idInventario+"\" codigoSubrubro=\""+codigoSubrubro+"\" totalRecalculado=\""+totalRecalculado+"\" totalOriginal=\""+totalOriginal+"\" diferencia=\""+diferencia+"\" porcentajeDiferencia=\""+porcentajeDiferencia+"\" fechaModificacion=\""+fechaModificacion+"\" horaModificacion=\""+horaModificacion+"\" estado=\""+estado+"\" ></rs>");
                    }
                    else
                        out.println("<rs idInventario=\""+0+"\" codigoSubrubro=\""+0+"\" totalRecalculado=\""+0+"\" totalOriginal=\""+0+"\" diferencia=\""+0+"\" porcentajeDiferencia=\""+0+"\" fechaModificacion=\"\" horaModificacion=\"\" estado=\""+0+"\" ></rs>");
                    
                    try{ //Bloque 103
                        //BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Datos recuperados, pero no se pudo cerrar la conexion con la BD: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar los datos. Exception: "+hi.getMessage());
                    //BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
        }
        out.println("</results >");
        out.close();
    }
    
    /**
     * Retorna una Arreglo con los datos de la modificacion de costos del subrubro ingresado.
     * @param codigoSubrubro
     * @param idInventario
     * @param tipoModificacion
     * @return
     * @throws Exception 
     */
    private DatosTotalesModificacionCostos getDatosTotalesModificacionCostosPorSubrubro(int codigoSubrubro, int idInventario, int tipoModificacion) throws Exception
    {
        BaseDeDatosPervasive BD = null;
        String consulta = "";
        ResultSet rta = null;
        DatosTotalesModificacionCostos datosTotales = null;
        String fecha = "", hora = "";
        String[] arreglo = null;
        //double totalRecalculado=0, totalOriginal=0, diferencia=0, porcentajeDiferencia=0, aux=0;
        BigDecimal totalRecalculado = new BigDecimal(0);
        BigDecimal totalOriginal = new BigDecimal(0);
        BigDecimal diferencia = new BigDecimal(0);
        BigDecimal porcentajeDiferencia = new BigDecimal(0);
        BigDecimal aux = new BigDecimal(0);
        
        Numeros numeros = null;
        
        try{//Bloque 601-1
            
            BD = new BaseDeDatosPervasive("Compras");
            numeros = new Numeros();
            if ((tipoModificacion == 3) || (tipoModificacion == 7))
            {
                consulta = "select mc_recalcul, mc_original, mc_diferenc, mc_porcedif, mc_fchaproc, mc_horaproc, mc_activo from Modicost where mc_codisubr="+codigoSubrubro+" and mc_idinvent="+idInventario+" and mc_activo=1";
                rta = BD.ejecutarConsulta(consulta);
                
                if (rta.next())
                {
                    datosTotales = new DatosTotalesModificacionCostos();
                    
                    totalRecalculado = rta.getBigDecimal("mc_recalcul").setScale(4, RoundingMode.HALF_UP);
                    totalOriginal = rta.getBigDecimal("mc_original").setScale(4, RoundingMode.HALF_UP);
                    diferencia = rta.getBigDecimal("mc_diferenc").setScale(4, RoundingMode.HALF_UP);
                    porcentajeDiferencia = rta.getBigDecimal("mc_porcedif").setScale(4, RoundingMode.HALF_UP);
                    
                    datosTotales.setCodigoSubrubro(codigoSubrubro);
                    datosTotales.setTotalRecalculado(totalRecalculado.doubleValue());                    
                    datosTotales.setTotalOriginal(totalOriginal.doubleValue());                    
                    datosTotales.setDiferencia(diferencia.doubleValue());                    
                    datosTotales.setPorcentajeDiferencia(porcentajeDiferencia.doubleValue());                    

                    arreglo = rta.getString("mc_fchaproc").trim().split("-");
                    fecha = arreglo[2] + "/" + arreglo[1] + "/" + arreglo[0];
                    datosTotales.setFechaModificacion(fecha);
                    datosTotales.setHoraModificacion(rta.getString("mc_horaproc").trim());
                    datosTotales.setIdInventario(idInventario);
                    datosTotales.setEstado(1);
                }
            }
            else
            {
                consulta = "select sum(hp_precactu) as totalPrecioCostoActual from Hispreco where hp_codiarti <> 0 and hp_codisubr="+codigoSubrubro+" and hp_codiinve="+idInventario+" and hp_activo=1 group by hp_codisubr";
                rta = BD.ejecutarConsulta(consulta);
                
                if (rta.next())
                {
                    totalRecalculado = rta.getBigDecimal("totalPrecioCostoActual").setScale(4, RoundingMode.HALF_UP);
                    
                    consulta = "select sum(ad_preccosb) as totalOriginal from Artinvdl where ad_codisubr="+codigoSubrubro+" and ad_incabein="+idInventario+" group by ad_codisubr";
                    rta = BD.ejecutarConsulta(consulta);

                    if (rta.next())
                    {
                        totalOriginal = rta.getBigDecimal("totalOriginal").setScale(4, RoundingMode.HALF_UP);
                        
                        //aux = totalRecalculado - totalOriginal;                        
                        aux = totalRecalculado.subtract(totalOriginal);
                        //diferencia = Double.parseDouble(numeros.redondear(aux, 4));                        
                        diferencia = aux.setScale(4, RoundingMode.HALF_UP);
                        
                        //aux = (diferencia/totalOriginal)*100;
                        aux = (diferencia.subtract(totalOriginal)).multiply(new BigDecimal(100));
                        porcentajeDiferencia = aux.setScale(4, RoundingMode.HALF_UP);
                        
                        datosTotales = new DatosTotalesModificacionCostos();
                    
                        datosTotales.setCodigoSubrubro(codigoSubrubro);
                        datosTotales.setTotalRecalculado(totalRecalculado.doubleValue());
                        datosTotales.setTotalOriginal(totalOriginal.doubleValue());
                        datosTotales.setDiferencia(diferencia.doubleValue());
                        datosTotales.setPorcentajeDiferencia(porcentajeDiferencia.doubleValue());                                                    
                        datosTotales.setIdInventario(idInventario);
                        datosTotales.setEstado(1);
                            
                        consulta = "select top 1 hp_fchamodi, hp_horamodi from Hispreco where hp_codiarti <> 0 and hp_codisubr="+codigoSubrubro+" and hp_codiinve="+idInventario+" and hp_activo=1 order by hp_ingrregi desc";
                        rta = BD.ejecutarConsulta(consulta);

                        if (rta.next())
                        {
                            arreglo = rta.getString("hp_fchamodi").trim().split("-");
                            fecha = arreglo[2] + "/" + arreglo[1] + "/" + arreglo[0];
                            hora = rta.getString("hp_horamodi").trim();
                            
                            datosTotales.setFechaModificacion(fecha);
                            datosTotales.setHoraModificacion(hora);
                        }
                        else
                            throw new Exception("no se pudo obtener la Fecha y Hora de modificación de precios");
                    }
                    else
                        throw new Exception("no se pudo obtener el Costo Original");
                }
                else
                    throw new Exception("no se pudo obtener el Costo Recalculado");
            }
                      
            BD.cerrarConexion();            
            return datosTotales;
        }
        catch(Exception e){
            BD.cerrarConexion();
            String mensaje = claseActual +". Bloque 601-1. No se pudieron recuperar los datos: "+e.getMessage()+". ";
            throw (new Exception(mensaje));
        }        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
