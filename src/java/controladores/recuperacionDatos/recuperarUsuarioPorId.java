/*
 * Recupera el nombre completo de un usuario activo a partir de su id.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.ConfiguracionSistema;
import LibreriaCodimat.Sistema.Excepciones.ConfiguracionException;
import LibreriaCodimat.Sistema.LogError;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Sistema.OperacionesSobreBDSistema;
import LibreriaCodimat.Sistema.Usuario;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Esteban Silva
 */
public class recuperarUsuarioPorId extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarUsuarioPorId.java";    
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    protected ConfiguracionSistema configuracionSistema;
    
    private int sistema = -1;
    private int subsistema = -1;
    private int modulo = -1;

    public void init(ServletConfig config) throws ServletException {
        
        super.init(config);
        
        String mensajeInesperado = "Ocurrió un error inesperado";
        
        try { //Bloque 0

            try { //Bloque 100
                
                this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
                this.configuracionSistema = this.cargadorConfiguracion.getConfigSistema();
                
                mensajeInesperado = this.configuracionSistema.getMensajeErrorInesperadoParaConsola();
                
                this.sistema = this.configuracionSistema.numeroSistemaSistem;
                this.subsistema = this.configuracionSistema.numeroSubSistemaIndiceSistem;
                this.modulo = this.configuracionSistema.numeroModuloABMUsuarios;
                
            }
            catch(ConfiguracionException e) {
                
                LogUsoAplicaciones log = new LogUsoAplicaciones(-1, "", -1, "-", this.sistema, this.subsistema, this.modulo);
                log.guardarComentario("INIT " + this.claseActual);
                LogUsoAplicaciones.imprimirError(log.getIdUsuario(), log.getIpTerminal(), log.getIdEnBD(), claseActual, LogUsoAplicaciones.getIdCompleto(this.sistema, this.subsistema, this.modulo), 100, e.getMessage());

                log.guardarError(new LogError(100, claseActual, e.getMessage()));
                
                throw new ServletException(e.getMessage());
                
            }
            
        }
        catch(ServletException se) {
            
            throw se;
            
        }
        catch(Exception excepcionAlTratarDeRecuperarLasVariablesCompartidas) {
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, LogUsoAplicaciones.getIdCompleto(this.sistema, this.subsistema, this.modulo), 0, mensajeInesperado + " Excepcion: " + excepcionAlTratarDeRecuperarLasVariablesCompartidas.getMessage());            
            throw new ServletException(excepcionAlTratarDeRecuperarLasVariablesCompartidas.getMessage());
            
        }
        
    }
    
    /**
     * Obtiene el nombre de un determinado usuario a partir de su id.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {  
        
        String usuario = "";
        String usuarioLog = "";
        
        int idUsuario = -1;
        
        OperacionesSobreBDSistema BD = null;
        Usuario usuarioObject = null;
        
        String ipServer = request.getRemoteAddr();        
        int idAplicacion = Integer.parseInt(this.configuracionSistema.getIdCompletoModuloABMUsuarios());
        
        PrintWriter out = response.getWriter();
        response.setContentType("application/xml;charset=UTF-8");        
        out.println("<results>");
        
        try { //Bloque 200
            
            usuario = request.getParameter("idUsuario");            
            usuarioLog = request.getParameter("idUsuarioLog");
            
            // Verifico que el dato usuario sea válido.
            if(usuario != null) {
                
                idUsuario = Integer.parseInt(usuario);

            }
            
            try { //Bloque 201
                
                BD = new OperacionesSobreBDSistema(this.cargadorConfiguracion);
                
                try { //Bloque 202
                    
                    usuarioObject = BD.recuperarUsuario_SoloNombre(idUsuario);
                        
                    out.println("<usuario codigo=\"" + usuarioObject.getIdUsuario() + "\" "
                            + " nombre=\"" + usuarioObject.getNombre() + "\" "
                            + "/>");

                    try { //Bloque 203

                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\" />");

                    }
                    catch(Exception fg) {

                        LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, "-", claseActual, idAplicacion, 203, "Recuperado el usuario, pero no pudo cerrarse la conexion con la BD. Excepcion: "+fg.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperado el usuario, pero no pudo cerrarse la conexion con la BD. Excepcion: "+fg.getMessage()+".\" />");

                    }
                    
                }
                catch(Exception i) {
                    
                    LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, "-", claseActual, idAplicacion, 202, "Error no se pudo recuperar el dato. Excepcion: "+i.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error no se pudo recuperar el dato. Excepcion: "+i.getMessage()+".\" />");
                    
                }
                
            }
            catch(Exception g) {
                
                LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, "-", claseActual, idAplicacion, 201, "Error no se pudo conectar con la BD. Excepcion: "+g.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error no se pudo conectar con la BD. Excepcion: "+g.getMessage()+".\" />");
                
            }
            
        }
        catch(Exception ex) {
            
            LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, "-", claseActual, idAplicacion, 200, "Error al procesar los datos de entrada. Excepcion: "+ex.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Excepcion: "+ex.getMessage()+".\" />");
            
        }
        
        out.println("</results >");
        out.close();
        
        BD = null;
        usuarioObject = null;
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
