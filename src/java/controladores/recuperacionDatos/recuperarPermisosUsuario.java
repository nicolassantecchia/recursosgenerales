/*
 * Recupera todos los permisos de un usuario activo a partir de su id.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.RecursosHumanos.ListaDeTarjetas;
import LibreriaCodimat.RecursosHumanos.Tarjeta;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.ConfiguracionSistema;
import LibreriaCodimat.Sistema.Excepciones.ConfiguracionException;
import LibreriaCodimat.Sistema.ListaDePermisos;
import LibreriaCodimat.Sistema.LogError;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Sistema.OperacionesSobreBDSistemaV2;
import LibreriaCodimat.Sistema.Permiso;
import LibreriaCodimat.Sistema.Usuario;
import LibreriaCodimat.Utiles.MensajePorConsola;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Esteban Silva
 */
public class recuperarPermisosUsuario extends HttpServlet {

    protected final String claseActual = "Servlet recuperarPermisosUsuario.java";    
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    protected ConfiguracionSistema configuracionSistema;
    
    protected int idAplicacion = -1;
    
    public void init(ServletConfig config) throws ServletException {
        
        super.init(config);
        
        String mensajeInesperado = "Ocurrió un error inesperado";
        
        try { // Bloque 0

            try { // Bloque 100
                
                this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
                configuracionSistema = this.cargadorConfiguracion.getConfigSistema();
                
                mensajeInesperado = configuracionSistema.getMensajeErrorInesperadoParaConsola();
                
                idAplicacion = Integer.parseInt(configuracionSistema.getIdCompletoModuloABMUsuarios());
                
            }
            catch(ConfiguracionException e) {
                
                LogUsoAplicaciones log = new LogUsoAplicaciones(-1, "", -1, "-", -1, -1, -1);
                log.guardarComentario("INIT " + this.claseActual);
                LogUsoAplicaciones.imprimirError(log.getIdUsuario(), log.getIpTerminal(), log.getIdEnBD(), claseActual, -1, 100, e.getMessage());

                log.guardarError(new LogError(100, claseActual, e.getMessage()));
                
                throw new ServletException(e.getMessage());
                
            }
            
        }
        catch(ServletException se) {
            
            throw se;
            
        }
        catch(Exception excepcionAlTratarDeRecuperarLasVariablesCompartidas) {
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, mensajeInesperado + " Excepcion: " + excepcionAlTratarDeRecuperarLasVariablesCompartidas.getMessage());            
            throw new ServletException(excepcionAlTratarDeRecuperarLasVariablesCompartidas.getMessage());
            
        }
        
    }
    
    /**
     * Obtiene los permisos de un determinado usuario.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String usuario = "";
        String usuarioLog = "";
        
        int idUsuario = -1;
        int idUsuarioLog = -1;
        int menorCodTarj = -1;
        
        Usuario objUsuario = null;
        ListaDeTarjetas listaDeTarjetas = null;
        Tarjeta tarjeta = null;
        ListaDePermisos listaDePermisos = null;
        Permiso permiso = null;
        
        LogUsoAplicaciones log = null;
        LogError logError = new LogError(200, claseActual, "");     
        OperacionesSobreBDSistemaV2 BD = null;
        
        String ipServer = request.getRemoteAddr();
        String idLog = "";
        String operacionActual = "";
        String mensajeError = ""; 
        
        PrintWriter out = response.getWriter();
        response.setContentType("application/xml;charset=UTF-8");        
        out.println("<results>");
        
        try { // Bloque 200	
            
            // Recupero los datos que vienen de la vista.
            operacionActual = " recuperando parámetro idUsuario";
            usuario = request.getParameter("idUsuario");
            
            operacionActual = " recuperando parámetro idUsuarioLog";
            usuarioLog = request.getParameter("idUsuarioLog");
            
            // Verifico que los datos enviados sean válidos.
            if(usuarioLog != null) {

                operacionActual = " casteando parámetro usuarioLog";
                idUsuarioLog = Integer.parseInt(usuarioLog);

            }
            
            // Creo el log de uso con los datos necesarios.
            log = new LogUsoAplicaciones(idUsuarioLog, "", -1, ipServer, idAplicacion);
            idLog = Integer.toString(log.getIdEnBD());
            
            // Verifico que el dato usuario sea válido.
            if(usuario != null) {
                
                operacionActual = " casteando parámetro usuario";
                idUsuario = Integer.parseInt(usuario);

            }
            
            try { // Bloque 210
                
                BD = new OperacionesSobreBDSistemaV2(this.cargadorConfiguracion);
                
                try { // Bloque 220
                    
                    operacionActual = " recuperando el usuario de la base de datos";
                    objUsuario = BD.recuperarUsuario(idUsuario);
                    
                    operacionActual = " recuperando las tarjetas asociadas al usuario";
                    listaDeTarjetas = BD.recuperarTarjetas(idUsuario);
                    
                    operacionActual = " recuperando el menor numero de tarjeta magnetica almacenada en el sistema";
                    menorCodTarj = BD.recuperarMenorNumeroTarjetaMagnetica();
                    
                    operacionActual = " recuperando los permisos del usuario";
                    listaDePermisos = BD.recuperarPermisosUsuarioActivo(idUsuario);
                    
                    try { // Bloque 230

                        out.println("<usuario id=\"" + objUsuario.getIdUsuario()+ "\" "
                                + " nombre=\"" + objUsuario.getNombre()+ "\" "
                                + " nick=\"" + objUsuario.getNick()+ "\" "
                                + " pass=\"" + objUsuario.getPass()+ "\" "
                                + "/>");
                        
                        // Recorro la lista de tarjetas y las guardo en un xml.
                        for(int i = 0; i < listaDeTarjetas.size(); i++) {

                            tarjeta = listaDeTarjetas.getTarjeta(i);
                            this.imprimirTarjeta(out, tarjeta);

                        }
                        
                        out.println("<tarjetaMag id=\"" + menorCodTarj + "\" />");
                        
                        // Recorro la lista de permisos y los guardo en un xml.
                        for(int i = 0; i < listaDePermisos.size(); i++) {
                            
                            permiso = listaDePermisos.getPermiso(i);
                            out.println("<permiso sistema=\"" + permiso.getSistema() + "\" subsistema=\"" + permiso.getSubSistema() + "\" modulo=\"" + permiso.getModulo() + "\" />");
                            
                        }
                        
                        try { // Bloque 240
                            
                            BD.cerrarConexion();
                            out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\" />");
                            
                        }
                        catch(Exception e) {
                            
                            logError = new LogError(240, claseActual, "");

                            mensajeError = "Se recuperaron los permisos, pero no pudo cerrarse la conexion con la BD. Excepcion: " + e.getMessage();
                            logError.setMensaje(mensajeError);

                            log.guardarError(logError);

                            LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, idLog, claseActual, idAplicacion, 240, mensajeError);
                            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                            
                        }
                        
                    }
                    catch (Exception f) {
                        
                        BD.cerrarConexion();
                    
                        logError = new LogError(230, claseActual, "");

                        mensajeError = "Hubo un error " + operacionActual + ". Excepcion: " + f.getMessage();
                        logError.setMensaje(mensajeError);

                        log.guardarError(logError);

                        LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, idLog, claseActual, idAplicacion, 230, mensajeError);

                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        
                    }
                    
                }
                catch(Exception g) {
                    
                    BD.cerrarConexion();
                    
                    logError = new LogError(220, claseActual, "");

                    mensajeError = "Error no se pudo recuperar los permisos. Excepcion: " + g.getMessage();
                    logError.setMensaje(mensajeError);

                    log.guardarError(logError);

                    LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, idLog, claseActual, idAplicacion, 220, mensajeError);

                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    
                }
                
            }
            catch(Exception h) {
                
                logError = new LogError(210, claseActual, "");

                mensajeError = "Error no se pudo conectar con la BD. Excepcion: " + h.getMessage();
                logError.setMensaje(mensajeError);

                log.guardarError(logError);

                LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, idLog, claseActual, idAplicacion, 210, mensajeError);

                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                
            }
            
        }
        catch(Exception i) {
            
            mensajeError = "Hubo un error " + operacionActual + ". Excepcion: " + i.getMessage();
            
            try {
            
                if(log == null) {

                    log = new LogUsoAplicaciones(idUsuarioLog, "", -1, ipServer, idAplicacion);

                }

                logError.setMensaje(mensajeError);
                log.guardarError(logError);
                LogUsoAplicaciones.imprimirError(usuarioLog, ipServer, idLog, claseActual, idAplicacion, 200, mensajeError);
                
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                
            }
            catch(Exception excepcionAlGuardarLog) {
                
                MensajePorConsola.mostrarMensaje(claseActual, "ATENCION: no se pudo guardar el log de uso. Excepcion: " + excepcionAlGuardarLog.getMessage() + ".");
                
            }
            
        }
        
        out.println("</results >");
        out.close();
        
        BD = null;
        listaDePermisos = null;
        permiso = null;        
        log = null;
        logError = null;
        
    }
    
    // Agrega una tarjeta al xml.
    private void imprimirTarjeta(PrintWriter out, Tarjeta tarjeta) {
        
        String tipoTarjeta = "";
        
        out.print("<tarjeta ");
        
            out.print("cod=\"" + tarjeta.getCodigo()+ "\" ");
            
            tipoTarjeta = tarjeta.getClass().getSimpleName();
            if(tipoTarjeta.equals("TarjetaDeProximidad")) {
                
                out.print("tipo=\"" + configuracionSistema.getTipoTarjetaProximidad() + "\" ");
                
            }
            else {
                
                out.print("tipo=\"" + configuracionSistema.getTipoTarjetaMagnetica()+ "\" ");
                
            }
            
        out.print("/>");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
