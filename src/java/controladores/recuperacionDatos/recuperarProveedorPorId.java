/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.Proveedor;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarProveedorPorId extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarProveedorPorId.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        int id;
        out.println("<results>");
        try{ //Bloque 100		
            id = Integer.parseInt(request.getParameter("idProveedor"));
            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    Proveedor proveedor = BD.recuperarProveedor(id);
                    try{ //Bloque 103
                        if (proveedor != null){
                            
                            String IVACodigo = "";
                            String IVADetalle = "";
                            String IVAAbrev = "";
                            
                            if(proveedor.getCondicionIVA() != null) {
                                IVACodigo = " condicionIVACodigo=\"" + proveedor.getCondicionIVA().getCodigo() + "\" ";
                                IVADetalle = " condicionIVADetalle=\"" + proveedor.getCondicionIVA().getDetalle() + "\" "; 
                                IVAAbrev = " condicionIVAAbrev=\"" + proveedor.getCondicionIVA().getAbrev() + "\" ";
                            }
                            
                            out.println("<rs id=\"0\" info=\""+cadenas.cambiarCaracteresReservadosParaXML(proveedor.getNombre())+"\" "
                                    + IVACodigo
                                    + IVADetalle
                                    + IVAAbrev
                                    + " documentoIdentificadorId=\""+proveedor.getDocumentoIdentificador().getId()+"\" "
                                    + " documentoIdentificadorDetalle=\""+proveedor.getDocumentoIdentificador().getDetalle()+"\" "
                                    + " documentoIdentificadorNro=\""+proveedor.getDocumentoIdentificador().getNro()+"\" "
                                    + " direccion=\""+cadenas.cambiarCaracteresReservadosParaXML(proveedor.getDireccion().getDomicilio())+"\" "
                                    + " ciudad=\""+cadenas.cambiarCaracteresReservadosParaXML(proveedor.getDireccion().getCiudad())+"\" "
                                    + " codigoPostal=\""+cadenas.cambiarCaracteresReservadosParaXML(proveedor.getDireccion().getCodigoPostal())+"\" "
                                    + " telefono=\""+proveedor.getTelefono()+"\" "
                                    + ">"+proveedor.getId()+"</rs>");				

                        }  
                        try{ //Bloque 104
                            BD.cerrarConexion();
                            out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                        }
                        catch(Exception fg){
                            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 104, "Recuperado el proveedor, pero no pudo cerrarse la conexion con la BD. Exception: "+fg.getMessage());
                            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperado el proveedor, pero no pudo cerrarse la conexion con la BD. Exception: "+fg.getMessage()+"\"  />");
                        }
                    }
                    catch (Exception e){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Error: "+e.getMessage());
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error: "+e.getMessage()+"\"  />");
                    }
                }
                catch(Exception i){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error no se pudo recuperar el dato: "+i.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error no se pudo recuperar el dato: "+i.getMessage()+"\"  />");
                }
            }
            catch(Exception g){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error no se pudo conectar con la BD: "+g.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error no se pudo conectar con la BD: "+g.getMessage()+"\"  />");
            }
        }
        catch(Exception ex){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada.");
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada.\"  />");
        }	
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
