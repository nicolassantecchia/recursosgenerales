/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.Articulo;
import LibreriaCodimat.Compras.ListaDeArticulos;
import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.ListaDeDatosActualizacionCostos;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Usuario407
 */
public class recuperarArticuloConPrecioUltimaCompra extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarArticuloConPrecioUltimaCompra.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        response.setContentType("application/xml;charset=UTF-8");
        
        int i = 0;
        int idInventario = -1;
        int idProveedor = -1;
        int codigoArticuloCodimat = -1;
        String listaDeArticulos = "";
        JSONObject obj = null, contenido = null;
        JSONParser parser = new JSONParser();
        JSONArray arreglo = null;
        
        Articulo articuloUltimaCompra = null;
                
        out.println("<results>");

        try{ //Bloque 100
            idInventario = Integer.parseInt(request.getParameter("idInventario"));
            idProveedor = Integer.parseInt(request.getParameter("codigoProveedor"));
            codigoArticuloCodimat = Integer.parseInt(request.getParameter("codigoArticulo"));
            //listaDeArticulos = request.getParameter("lista");
            
            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    
                    //obj = (JSONObject)parser.parse(listaDeArticulos);
                    //arreglo = (JSONArray)obj.get("datos");
                    
                    //for(i=0; i<arreglo.size(); i++)
                    //{
                        //contenido = (JSONObject)arreglo.get(i);
                        //codigoArticuloCodimat = Integer.parseInt((String)contenido.get("codigo"));
                        
                        articuloUltimaCompra = BD.getArticuloConPUC(idInventario, idProveedor, codigoArticuloCodimat);

                        if (articuloUltimaCompra != null)                        
                            out.println("<rs codigoArticulo=\""+codigoArticuloCodimat +"--"+ articuloUltimaCompra.getCodigo()+"\" descripcionArticulo=\""+articuloUltimaCompra.getIdentificacion()+"\" precioUltimaCompra=\""+articuloUltimaCompra.getPrecioCosto()+"\" ></rs>");
                        else
                            out.println("<rs codigoArticulo=\""+0+"\" descripcionArticulo=\"\" precioUltimaCompra=\""+0.0+"\" ></rs>");
                    //}
                    
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Datos recuperados, pero no se pudo cerrar la conexion con la ND: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar los datos. Exception: "+hi.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
