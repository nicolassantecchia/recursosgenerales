/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Expedicion.Cot;
import LibreriaCodimat.Expedicion.OperacionesSobreBDExpedi;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alejandro Battista
 */
public class recuperarComprobanteEnCot extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarComprobanteEnCot.java";
    protected Cadenas cadenas;
    
    
    //Bloque 100
    /*public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
    }*/

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        
        out.println("<results>");
        try{ //Bloque 100	
            //int idTransporte = Integer.parseInt(request.getParameter("idTransporte"));
            String comprobante = request.getParameter("idComprobante");
            Cot cot = null;
            String fecha = "";
            String[] arregloFecha = null;
                        
            try{ //Bloque 101
                OperacionesSobreBDExpedi BD = new OperacionesSobreBDExpedi();
                try{ //Bloque 102
                    //Transporte transporte = BD.recuperarTransporte(idTransporte);
                    cot = BD.recuperarComprobanteEnCot(comprobante);

                    if(cot != null)
                    {
                        if (!(cot.getNumeroCot().equalsIgnoreCase(">1")))  //el comprobante se encuentra en un solo parte
                        {
                            arregloFecha = cot.getFechaSolicitud().split("-");
                            fecha = arregloFecha[2] + "/" + arregloFecha[1] + "/" + arregloFecha[0];

                            out.println("<cot "
                                    + " tipoParte=\""+cot.getTipoParte()+"\"  "
                                    + " numeroParte=\""+cot.getNumeroParte()+"\"  "
                                    + " fechaSolicitud=\""+fecha+"\"  "
                                    + " horaSolicitud=\""+cot.getHoraSolicitud()+"\"  "
                                    + " numeroCot=\""+cot.getNumeroCot()+"\"  "
                                    + " codigoIntegridad=\""+cot.getCodigoIntegridad()+"\"  "
                                    + " numeroComprobante=\""+cot.getNumeroComprobante()+"\"  "
                                    + " cantidadRemitos=\""+cot.getCantidadRemitos()+"\"  "
                                    +" >");                        
                            out.println("</cot>");
                        }
                        else
                            throw new Exception("El comprobante ingresado se encuentra en más de un parte");
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"Datos recuperados\"  />");
                    }
                    catch(Exception fg){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperado el Cot, pero no se pudo cerrar la conexion con la BD . Exception: "+fg.getMessage());
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperado el Cot, pero no se pudo cerrar la conexion con la BD . Exception: "+fg.getMessage()+"\"  />");
                    }

                }
                catch (Exception gh){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar el Cot. Exception: "+gh.getMessage());
                    BD.cerrarConexion();
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar el Cot. Exception: "+gh.getMessage()+"\"  />");
                }
            }
            catch(Exception f){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la base de datos. Exception: "+f.getMessage());
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al conectar con la base de datos. Exception: "+f.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }	
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
