/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeVariacionesCostoVinculado;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.VariacionCostoVinculado;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import LibreriaCodimat.Utiles.Numeros;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarVariacionesPorProveedor extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarVariacionesPorProveedor.java";
    protected Cadenas cadenas;
    protected Numeros numeros;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();   
        this.numeros = new Numeros();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");

        int idProveedor;
        
        out.println("<results>");
        try{ //Bloque 100
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras();
                ListaDeVariacionesCostoVinculado lista = BD.getListaDeVariacionesCostoVinculado(idProveedor);
                for (int i=0; i<lista.size();i++){
                    VariacionCostoVinculado variacionCostoVinculado = lista.getVariacionCostoVinculado(i);
                    out.println("<rs calificacion=\""+variacionCostoVinculado.getCalificacion()+"\" idProveedor=\""+variacionCostoVinculado.getProveedor().getId()+"\" porcentaje=\""+numeros.redondear(variacionCostoVinculado.getPorcentaje())+"\" detalle=\""+cadenas.cambiarCaracteresReservadosParaXML(variacionCostoVinculado.getDetalle())+"\" />");
                }
                try{ //Bloque 102
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />"); 
                }
                catch(Exception fg){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Recuperada la lista de variaciones, pero no se pudo cerrar la conexion con la BD. Exception: "+fg.getMessage());
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperada la lista de variaciones, pero no se pudo cerrar la conexion con la BD. Exception: "+fg.getMessage()+"\"  />");
                }
            }
            catch (Exception e){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al recuperar la lista de variaciones: "+e.getMessage());
                
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al recuperar la lista de variaciones: "+e.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada.");
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada.\"  />");
        }
        out.println("</results>");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
