/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ArticuloProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarArticuloProveedor extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarArticuloProveedor.java";
    protected Cadenas cadenas;
    
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        int idProveedor;
        String idArticuloProveedor;
        int idInternoArticulo = 0;
        String inputIdInternoArticulo;
        out.println("<results>");
        try{ //Bloque 100
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idArticuloProveedor = request.getParameter("idArticuloProveedor");
            inputIdInternoArticulo = request.getParameter("idInternoArticuloProveedor");

            if (inputIdInternoArticulo!=null){
                idInternoArticulo = Integer.parseInt(inputIdInternoArticulo);
            }

            try{     //Bloque 101        
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    ArticuloProveedor articulo=null;

                    if (inputIdInternoArticulo==null){

                        articulo = BD.recuperarArticuloProveedor(idProveedor, idArticuloProveedor.trim().toUpperCase());
                    }
                    else{
                        articulo = BD.recuperarArticuloProveedor(idProveedor,idInternoArticulo);
                    }

                    if(articulo !=null){
                        out.println("<rs "+
                                " idProveedor=\""+articulo.getProveedor().getId()+"\" "+
                                " idArticulo=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getIdArticulo())+"\"  "+
                                " idArticuloInterno=\""+articulo.getCodigoInterno()+"\"  "+
                                " nombreArticulo=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getNombreArticulo())+"\" "+
                                " idFamilia=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getFamiliaProveedor().getCodigo())+"\" "+
                                " idRubro=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getRubroProveedor().getId())+"\" "+
                                " idSubRubro=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getSubrubroProveedor().getId())+"\""+
                                " codBarras=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getCodigoDeBarras().getCadenaCompleta().trim())+"\""+
                                " tipoUnidadDeMedida=\""+articulo.getTipoUnidadDeMedida().getIdEnTabla()+"\""+
                                " abrvUnidadDeMedida=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getTipoUnidadDeMedida().getNombreAbreviacion().trim())+"\""+
                                " fechaVigencia=\""+articulo.getFechaVigencia().getSoloFechaFormatoDDMMAAAA()+"\""+
                                //" unidadDePrecio=\""+numeros.redondear(articulo.getUnidadDePrecio())+"\""+
                                " unidadDeFacturacion=\""+articulo.getUnidadDeFacturacion()+"\" "+
                                " precioLista=\""+articulo.getPrecioLista()+"\""+
                                " bonif1=\""+articulo.getBonificacion1FormatoPorcentaje()+"\""+
                                " bonif2=\""+articulo.getBonificacion2FormatoPorcentaje()+"\""+
                                " esBonif2Calculada=\""+articulo.getBonificacion2Calculada()+"\""+
                                " bonificacionesYRecargos=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getBonificacionesYRecargos().trim())+"\""+
                                " precioCosto=\""+articulo.getPrecioCosto()+"\""+
                                " peso=\""+articulo.getPeso()+"\""+
                                " codigoIva=\""+articulo.getCodigoIva()+"\""+
                                " valorIva=\""+articulo.getValorIva()+"\""+
                                " infoAdicional=\""+cadenas.cambiarCaracteresReservadosParaXML(articulo.getInfoAdicional().trim())+"\""+ 
                                " ></rs>");
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");  
                    }
                    catch (Exception e){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Se recupero el articulo, pero no se pudo cerrar la conexion con la Base de Datos. Exception: "+e.getMessage());                        
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Se recupero el articulo, pero no se pudo cerrar la conexion con la Base de Datos. Exception: "+e.getMessage()+"\"  />");
                    }
                }
                catch(Exception h){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar el articulo del proveedor. Exception: "+h.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar el articulo del proveedor. Exception: "+h.getMessage()+"\"  />");
                }
            }
            catch(Exception q){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo conectar con la Base De Datos. Exception: "+q.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo conectar con la Base De Datos. Exception: "+q.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
