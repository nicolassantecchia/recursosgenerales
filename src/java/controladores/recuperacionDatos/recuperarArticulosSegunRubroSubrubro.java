/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeArticulos;
import LibreriaCodimat.Compras.ListaDeSubrubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.ListaDeDatosActualizacionCostos;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import LibreriaCodimat.Utiles.Numeros;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario407
 */
public class recuperarArticulosSegunRubroSubrubro extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarArticulosSegunRubroSubrubro.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        response.setContentType("application/xml;charset=UTF-8");
        
        int i = 0;
        int idInventario = -1;
        int codigoRubro = -1;
        int codigoSubrubro = -1;
        int codigoArticulo = -1;
        String descripcion = "";
        double unidadCalculo = -1;
        double precioCosto = -1;
        double precioCostoBackup = -1;
        
        ListaDeArticulos lista = null;
        Numeros numeros = new Numeros();
                
        out.println("<results>");

        try{ //Bloque 100
            idInventario = Integer.parseInt(request.getParameter("idInventario"));
            codigoRubro = Integer.parseInt(request.getParameter("codigoRubro"));
            codigoSubrubro = Integer.parseInt(request.getParameter("codigoSubrubro"));
            
            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    //ListaDeSubrubrosProveedor lista = BD.getListaDeSubRubrosProveedor(idProveedor,idFamilia,idRubro);
                    lista = BD.getArticulosSegunRubroSubrubro(codigoRubro, codigoSubrubro, idInventario);
                    
                    if ((lista != null) && (lista.cantidadElementos() > 0))
                    {
                        for(i=0; i<lista.cantidadElementos(); i++)
                        {
                            codigoArticulo = lista.getArticulo(i).getCodigo();
                            descripcion = lista.getArticulo(i).getIdentificacion();
                            unidadCalculo = lista.getArticulo(i).getUnidadCalculada();
                            precioCosto = Double.parseDouble(numeros.redondear(lista.getArticulo(i).getPrecioCosto(), 4));
                            precioCostoBackup = Double.parseDouble(numeros.redondear(lista.getArticulo(i).getPrecioCostoBackup(), 4));
                            out.println("<rs codigoArticulo=\""+codigoArticulo+"\" descripcionArticulo=\""+descripcion+"\" unidadCalculo=\""+unidadCalculo+"\" precioCosto=\""+precioCosto+"\" precioCostoBackup=\""+precioCostoBackup+"\"></rs>");
                        }
                    }
                    else
                        out.println("<rs codigoArticulo=\""+0+"\" descripcionArticulo=\"\" precioCosto=\""+0.0+"\" ></rs>");
                    
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch (Exception ee){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Datos recuperados, pero no se pudo cerrar la conexion con la ND: "+ee.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    }         
                }
                catch(Exception hi){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error al recuperar los datos. Exception: "+hi.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al conectar con la BD. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            }         
        }            
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
