/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ListaDeRubrosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.RubroProveedor;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarRubrosProveedor extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarProveedorPorId.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();     
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        //PrintWriter out = response.getWriter();
        response.setContentType("application/xml;charset=UTF-8");
        int idProveedor;
        String idFamilia;
        out.println("<results>");

        String claseActual = "Proy. WEB recursosGenerales.controladores.recuperacionDatos.recuperarRubrosProveedor.jsp";

        try{ //Bloque 100
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idFamilia = request.getParameter("idFamilia").trim();
            try{ //Bloque 101		
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    ListaDeRubrosProveedor lista = BD.getListaDeRubrosProveedor(idProveedor, idFamilia);

                        for (int i=0; i < lista.size(); i++){
                            RubroProveedor rubro = lista.getRubro(i);
                            String nombreRubro = cadenas.cambiarCaracteresReservadosParaXML(rubro.getNombre());
                            String codigoRubro = cadenas.cambiarCaracteresReservadosParaXML(rubro.getId());
                            out.println("<rs id=\""+i+"\" idRubro=\""+codigoRubro+"\" nombre=\""+nombreRubro+" \" ></rs>");
                        }

                        try{ //Bloque 103
                            BD.cerrarConexion();
                            out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                        }
                        catch (Exception ei){
                            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperada la lista de Rubros, pero no se pudo cerrar la conexion con la BD. Exception: "+ei.getMessage());
                            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperada la lista de Rubros, pero no se pudo cerrar la conexion con la BD. Exception: "+ei.getMessage()+"\"  />");
                        }
                }
                catch(Exception gh){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar la lista de rubros. Exception: "+gh.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar la lista de rubros. Exception: "+gh.getMessage()+"\"  />");
                }
            }
            catch(Exception fg){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo conectar con la base de datos. Exception: "+fg.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo conectar con la base de datos. Exception: "+fg.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
