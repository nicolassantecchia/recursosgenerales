/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.ArticuloProveedor;
import LibreriaCodimat.Compras.ListaDeArticulosProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarArticulosPorSubrubro extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarArticulosPorSubrubro.java";
    protected Cadenas cadenas;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();      
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        
        int idProveedor;
        String idSubrubro;
        String idRubro;
        String idFamilia;
        
        out.println("<results >");
        
        try{ //Bloque 100
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idRubro = request.getParameter("idRubro").trim();
            idFamilia = request.getParameter("idFamilia").trim();
            idSubrubro = request.getParameter("idSubrubro").trim();

            try{ //Bloque 101
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 102
                    ListaDeArticulosProveedor lista = BD.recuperarListaArticulosProveedorPorSubrrubro(idProveedor, idFamilia.toUpperCase(), idRubro.toUpperCase(), idSubrubro.toUpperCase());

                    for (int i=0; i < lista.cantidadElementos(); i++){
                        ArticuloProveedor articuloProveedor = lista.getArticuloProveedor(i);
                        String articuloProveedorId = cadenas.cambiarCaracteresReservadosParaXML(articuloProveedor.getIdArticulo());
                        String articuloProveedorNombre = cadenas.cambiarCaracteresReservadosParaXML(articuloProveedor.getNombreArticulo());
                        out.println("<rs id=\""+i+"\" articuloProveedorId=\""+articuloProveedorId+"\" idArticuloInterno=\""+articuloProveedor.getCodigoInterno()+"\" articuloProveedorNombre=\""+articuloProveedorNombre+"\" ></rs>");
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");         
                    }
                    catch (Exception ei){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperada la lista de articulos del subrubro, pero no pudo cerrarse la conexion con la Base de Datos. Exception: "+ei.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperada la lista de articulos del subrubro, pero no pudo cerrarse la conexion con la Base de Datos. Exception: "+ei.getMessage()+"\"  />");
                    }
                }
                catch(Exception q){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo recuperar la lista de articulos de subrubro. Exception: "+q.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo recuperar la lista de articulos de subrubro. Exception: "+q.getMessage()+"\"  />");
                }
            }
            catch(Exception s){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo conectar con la Base de Datos. Exception: "+s.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"No se pudo conectar con la Base de Datos. Exception: "+s.getMessage()+"\"  />");
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al procesar los datos de entrada. Exception: "+e.getMessage());
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error al procesar los datos de entrada. Exception: "+e.getMessage()+"\"  />");
        }
        out.println("</results >");
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
