/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.recuperacionDatos;

import LibreriaCodimat.Compras.Articulo;
import LibreriaCodimat.Compras.ArticuloProveedor;
import LibreriaCodimat.Compras.ListaDeVinculacionesArticuloProveedor;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.Proveedor;
import LibreriaCodimat.Compras.VinculacionArticuloProveedor;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Utiles.Cadenas;
import LibreriaCodimat.Utiles.Numeros;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarVinculacionesArticuloProveedor extends HttpServlet {
    
    protected final String claseActual = "Servlet recursosGenerales.controladores.recuperacionDatos.recuperarVinculacionesArticuloProveedor.java";
    protected Cadenas cadenas;
    protected Numeros numeros;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cadenas = new Cadenas();   
        this.numeros = new Numeros();
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/xml;charset=UTF-8");
        int idProveedor;
        int idInternoArticuloProveedor;
        int idArticuloCodimat = -1;
        ArticuloProveedor articuloProveedor = null;
        Articulo articuloCodimat = null;
        out.println("<!-- VER FECHA DE VIGENCIA!! -->");
        out.println("<results>");
        
        //veo si llaman con un articulo de proveedores o con un articulo de codimat

        try{
            //levanto si hay, datos del articulo de proveedor
            idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
            idInternoArticuloProveedor = Integer.parseInt(request.getParameter("idArticuloProveedor"));
            Proveedor proveedor = new Proveedor(idProveedor,"");
            articuloProveedor = new ArticuloProveedor(proveedor,"","");	
            articuloProveedor.setCodigoInterno(idInternoArticuloProveedor);
            //System.out.println("id proveedor "+idProveedor+" - idInternoArticuloProveedor: "+idInternoArticuloProveedor+"");
        }
        catch(Exception f){
        }

        try{//levanto si hay, datos del articulo de codimat
            idArticuloCodimat = Integer.parseInt(request.getParameter("idArticuloCodimat"));
            articuloCodimat = new Articulo(idArticuloCodimat,"");
        }
        catch(Exception g){

        }

        if(articuloProveedor!=null || articuloCodimat!=null){
            try{ //Bloque 100
                OperacionesSobreBDCompras BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
                try{ //Bloque 101
                    ListaDeVinculacionesArticuloProveedor listaVinculaciones;
                    if (articuloProveedor!=null){
                        listaVinculaciones = BD.recuperarListaDeUltimasVinculacionesArticuloProveedor(articuloProveedor);
                    }
                    else{
                        listaVinculaciones = BD.recuperarListaDeUltimasVinculacionesArticuloProveedor(articuloCodimat);
                    }
                    for (int i=0; i < listaVinculaciones.size(); i++){
                        VinculacionArticuloProveedor vinculacionArticuloProveedor = listaVinculaciones.getVinculacionArticuloProveedor(i);
                        out.print("<vinculacion "); 
                        out.print(" articuloCodimatId=\""+vinculacionArticuloProveedor.getArticuloCodimat().getCodigo()+ "\" ");
                        out.print(" articuloCodimatNombre=\""+cadenas.cambiarCaracteresReservadosParaXML(vinculacionArticuloProveedor.getArticuloCodimat().getIdentificacion())+ "\" ");
                        out.print(" proveedorId=\""+vinculacionArticuloProveedor.getArticuloProveedor().getProveedor().getId()+ "\" ");
                        out.print(" proveedorNombre=\""+cadenas.cambiarCaracteresReservadosParaXML(vinculacionArticuloProveedor.getArticuloProveedor().getProveedor().getNombre())+ "\" ");
                        out.print(" articuloProveedorIdInterno=\""+vinculacionArticuloProveedor.getArticuloProveedor().getCodigoInterno()+ "\" ");
                        out.print(" articuloProveedorId=\""+cadenas.cambiarCaracteresReservadosParaXML(vinculacionArticuloProveedor.getArticuloProveedor().getIdArticulo())+ "\" ");
                        out.print(" articuloProveedorNombre=\""+cadenas.cambiarCaracteresReservadosParaXML(vinculacionArticuloProveedor.getArticuloProveedor().getNombreArticulo())+ "\" ");
                        out.print(" factorCorreccionCosto=\""+vinculacionArticuloProveedor.getFactorCorreccionCosto()+ "\" ");
                        out.print(" fechaVigencia=\""+vinculacionArticuloProveedor.getFechaVigencia().getSoloFechaFormatoDDMMAAAA()+ "\" ");
                        out.print(" relacionEntreArticulos=\""+vinculacionArticuloProveedor.getRelacionEntreArticulos()+ "\" ");/*
                        out.print(" variacionCostoVinculado_Calificacion1=\""+vinculacionArticuloProveedor.getVariacionCostoVinculado1().getCalificacion()+ "\" ");
                        out.print(" variacionCostoVinculado_Porcentaje1=\""+numeros.redondear(vinculacionArticuloProveedor.getVariacionCostoVinculado1().getPorcentaje())+ "\" ");
                        out.print(" variacionCostoVinculado_Calificacion2=\""+vinculacionArticuloProveedor.getVariacionCostoVinculado2().getCalificacion()+ "\" ");
                        out.print(" variacionCostoVinculado_Porcentaje2=\""+numeros.redondear(vinculacionArticuloProveedor.getVariacionCostoVinculado2().getPorcentaje())+ "\" ");
                        out.print(" variacionCostoVinculado_Calificacion3=\""+vinculacionArticuloProveedor.getVariacionCostoVinculado3().getCalificacion()+ "\" ");
                        out.print(" variacionCostoVinculado_Porcentaje3=\""+numeros.redondear(vinculacionArticuloProveedor.getVariacionCostoVinculado3().getPorcentaje())+ "\" ");*/

                        out.println(" /> ");  
                    }
                    try{ //Bloque 103
                        BD.cerrarConexion();
                        out.println("<mensaje estadoOperacion=\"ok\" mensaje=\"\"  />");
                    }
                    catch(Exception fg){
                        LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 103, "Recuperada la lista de últimas vinculaciones, pero no se pudo cerrar la conexion con la BD. Excepción: "+fg.getMessage());
                        out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                        //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Recuperada la lista de &#xFA;ltimas vinculaciones, pero no se pudo cerrar la conexion con la BD. Excepci&#xF3;n: "+fg.getMessage()+"\"  />");
                    }

                }
                catch(Exception g){
                    LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "Error. No se pudo recuperar la lista de últimas vinculaciones. Excepción: "+g.getMessage());
                    BD.cerrarConexion();
                    out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                    //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error. No se pudo recuperar la lista de &#xFA;ltimas vinculaciones. Excepci&#xF3;n: "+g.getMessage()+"\"  />");
                }
            }
            catch(Exception f){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error. No se pudo conectar con la base de datos. Excepción: "+f.getMessage());
                out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
                //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error. No se pudo conectar con la base de datos. Excepci&#xF3;n: "+f.getMessage()+"\"  />");
            }
        }
        else{
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, " Error en los parámetros de entrada.");
            out.println("<mensaje estadoOperacion=\"error\" mensaje=\"\"  />");
            //out.println("<mensaje estadoOperacion=\"error\" mensaje=\"Error en los par&#xE1;metros de entrada\"  />");
        }

        out.println("</results>");
        out.close();
    }   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
