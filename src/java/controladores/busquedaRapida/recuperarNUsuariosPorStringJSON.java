/*
 * Realiza una busqueda rapida de los usuarios activos a partir de un prefijo.
 */

package controladores.busquedaRapida;

import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.ConfiguracionSistema;
import LibreriaCodimat.Sistema.ListaDeUsuarios;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import LibreriaCodimat.Sistema.OperacionesSobreBDSistemaV2;
import LibreriaCodimat.Sistema.Usuario;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 *
 * @author Esteban Silva
 */
public class recuperarNUsuariosPorStringJSON extends HttpServlet {

    protected String claseActual = "Servlet recursosGenerales.recursosGenerales.controladores.busquedaRapida.recuperarNUsuariosPorStringJSON.java";
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    protected int idAplicacion = -1;
    
    public void init(ServletConfig config) throws ServletException {
        
        super.init(config);
        
        try { // Bloque 100
            
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
            ConfiguracionSistema configuracionSistema = this.cargadorConfiguracion.getConfigSistema();
            
            idAplicacion = Integer.parseInt(configuracionSistema.getIdCompletoModuloABMUsuarios());
            
        }
        catch(Exception e) {
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, idAplicacion, 100, " Excepcion: " + e.getMessage());
            throw new ServletException(e.getMessage());
            
        }
        
    }
    
    /**
     * Recupera una lista de n cantidad de usuarios activos.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        String cadena = "no definida";
        int cantidadElementos = 100;
        String ipServer = request.getRemoteAddr();
        
        JSONObject jsonObjUsuario = null;
        ArrayList listaJSONObjUsuario = new ArrayList();
        
        OperacionesSobreBDSistemaV2 BD = null;
        Usuario usuario = null;
        
        try { // Bloque 200

            cadena = request.getParameter("prefijo");
            
            if(cadena != null) {
                
                cadena = cadena.toUpperCase();
                
            }
            
            BD = new OperacionesSobreBDSistemaV2();
            
            // Obtengo la lista de usuarios del sistema.
            ListaDeUsuarios listaDeUsuarios = BD.recuperarListaDeUsuariosActivos(cantidadElementos, cadena);

            try { // Bloque 210
                
                response.setContentType("text/html; charset=UTF-8");

                // Recorro la lista de usuarios activos para guardarlos en una lista de objetos JSON.
                for(int i = 0; i < listaDeUsuarios.size(); i++) {
                    
                    jsonObjUsuario = new JSONObject();
                    usuario = listaDeUsuarios.getUsuario(i);
                    
                    jsonObjUsuario.put("value", usuario.getNombre());
                    jsonObjUsuario.put("data", Integer.toString(usuario.getIdUsuario()));
                    
                    listaJSONObjUsuario.add(jsonObjUsuario);
                    
                }
                
            }
            catch (Exception e) {
                
                LogUsoAplicaciones.imprimirError("-", ipServer, "-", claseActual, idAplicacion, 210, "Error al generar la lista con los usuarios activos. Excepcion: " + e.getMessage());
                
            }
            
        }
        catch(Exception f) {
            
            LogUsoAplicaciones.imprimirError("-", ipServer, "-", claseActual, idAplicacion, 200, "Error al recuperar la lista de usuarios activos. Excepcion: " + f.getMessage());
            
        }
        
        try { // Bloque 220
            
            if(BD != null) {
                
                BD.cerrarConexion();
                
            }
            
        }
        catch(Exception g) {
            
            LogUsoAplicaciones.imprimirError("-", ipServer, "-", claseActual, idAplicacion, 220, "No se pudo cerrar la conexión con la BD Sistem. Excepcion: " + g.getMessage());
            
        }        
        
        // Envio al cliente la lista de usuarios activos.
        jsonObjUsuario = new JSONObject();
        jsonObjUsuario.put("suggestions", listaJSONObjUsuario);
        
        out.print(jsonObjUsuario);
        out.flush();
        out.close();
        
        jsonObjUsuario = null;
        listaJSONObjUsuario = null;        
        BD = null;
        usuario = null;
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
