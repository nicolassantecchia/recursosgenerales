/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.busquedaRapida;

import LibreriaCodimat.Compras.ListaDeProveedores;
import LibreriaCodimat.Compras.OperacionesSobreBDCompras;
import LibreriaCodimat.Compras.Proveedor;
import LibreriaCodimat.Sistema.CargadorConfiguracionesParaServlet;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarNProveedoresActivosJSON extends HttpServlet {
    
    protected String claseActual = "Servlet recursosGenerales.recursosGenerales.controladores.busquedaRapida.recuperarNProveedoresActivosJSON.java";
    protected int cantidadElementos;
    protected CargadorConfiguracionesParaServlet cargadorConfiguracion;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cantidadElementos = 100;
        
        try {
            this.cargadorConfiguracion = new CargadorConfiguracionesParaServlet(config.getServletContext());
        }
        catch(Exception e){
            
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, -1, 0, " Excepcion: " + e.getMessage()); 
            
            throw new ServletException(e.getMessage());
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
        String cadena = "A";
        JSONObject obj;
        ArrayList listaJSON = new ArrayList();
        
        OperacionesSobreBDCompras BD = null;
        
        try{ //Bloque 100

            cadena = request.getParameter("prefijo");
            cantidadElementos = 100;

            BD = new OperacionesSobreBDCompras(this.cargadorConfiguracion);
            ListaDeProveedores lista = BD.recuperarNProveedoresActivos(cantidadElementos, cadena.toUpperCase());

            try{ //Bloque 101
                response.setContentType("text/html; charset=UTF-8");

                for(int i = 0; i < lista.size(); i++){
                    obj=new JSONObject();
                    Proveedor proveedor = lista.getProveedor(i);
                    obj.put("value", proveedor.getNombre());
                    obj.put("data",Integer.toString(proveedor.getId()));
                    listaJSON.add(obj);
                }
            }
            catch (Exception e){
                LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "Error al generar el JSON: "+e.getMessage());
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al recuperar los proveedores activos: "+e.getMessage());
        }
        
        try{ //Bloque 102
            if(BD != null) {
                BD.cerrarConexion();
            }
        }
        catch(Exception f){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 102, "No se pudo cerrar la conexión con la BD Compras: "+f.getMessage());
        }
        
        
        obj = new JSONObject();
        obj.put("suggestions", listaJSON);
        out.print(obj);
        out.flush();
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
