/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores.busquedaRapida;

import LibreriaCodimat.Expedicion.Chofer;
import LibreriaCodimat.Expedicion.ListaDeChoferes;
import LibreriaCodimat.Expedicion.OperacionesSobreBDExpedi;
import LibreriaCodimat.Sistema.LogUsoAplicaciones;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Fernando Prieto
 */
public class recuperarNChoferesActivosJSON extends HttpServlet {
    
    protected String claseActual = "Servlet recursosGenerales.recursosGenerales.controladores.busquedaRapida.recuperarNChoferesActivosJSON.java";
    protected int cantidadElementos;
    
    //Bloque 100
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        
        this.cantidadElementos = 10;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        String cadena="A";
        ArrayList listaJSON = new ArrayList();
        JSONObject obj;
        OperacionesSobreBDExpedi BD = null;
        try{ //Bloque 100
            cadena = request.getParameter("prefijo").toUpperCase();
            BD = new OperacionesSobreBDExpedi();
            ListaDeChoferes lista = BD.recuperarPrimerosNChoferesPorNombre(cantidadElementos,cadena);
            response.setContentType("text/html; charset=UTF-8");

            for(int i = 0; i < lista.size(); i++){
                obj=new JSONObject();
                Chofer chofer = lista.getChofer(i);
                obj.put("value", chofer.getNombreYApellido());
                obj.put("data",Integer.toString(chofer.getId()));
                listaJSON.add(obj);
            }
        }
        catch(Exception e){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 100, "Error al recuperar los Transportes Activos: "+e.getMessage());
        }
        
        try{ //Bloque 101
            if(BD != null) {
                BD.cerrarConexion();
            }
        }
        catch(Exception f){
            LogUsoAplicaciones.imprimirError("-", "-", "-", claseActual, 0, 101, "No se pudo cerrar la conexión con la BD Expedi: "+f.getMessage());
        }
        
        obj = new JSONObject();
        obj.put("suggestions", listaJSON);
        out.print(obj);
        out.flush();
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
