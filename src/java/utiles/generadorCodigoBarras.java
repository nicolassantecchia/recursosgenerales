/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utiles;

import LibreriaCodimatV2.Utiles.CodigoBarras;
import LibreriaCodimatV2.Utiles.ConfiguracionCodigoBarras;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando Prieto
 */
public class generadorCodigoBarras extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try{
            String codigo = request.getParameter("cod"); //unico parametro obligatorio

            String parametroActual = null;

            boolean mostrar = true;
            ConfiguracionCodigoBarras configuracion = null;

            if(codigo != null)
            {
                configuracion = new ConfiguracionCodigoBarras();

                parametroActual = request.getParameter("tipo");

                if(parametroActual != null)
                {
                   parametroActual = parametroActual.toLowerCase();

                   if(parametroActual.equals("ean8")) {
                        configuracion.setTipoEAN8();
                    }
                    else {
                        if(parametroActual.equals("ean13")) {
                            configuracion.setTipoEAN13();
                        }
                        else {
                            if(parametroActual.equals("std25")) {
                                configuracion.setTipoStandard2of5();
                            }
                            else {
                                if(parametroActual.equals("int25")) {
                                    configuracion.setTipoInterleaved2of5();
                                }
                                else {
                                    if(parametroActual.equals("code11")) {
                                        configuracion.setTipoCode11();
                                    }
                                    else {
                                        if(parametroActual.equals("code39")) {
                                            configuracion.setTipoCode39();
                                        }
                                        else {
                                            if(parametroActual.equals("code93")) {
                                                configuracion.setTipoCode93();
                                            }
                                            else {
                                                if(parametroActual.equals("code128")) {
                                                    configuracion.setTipoCode128();
                                                }
                                                else {
                                                    if(parametroActual.equals("codabar")) {
                                                        configuracion.setTipoCodabar();
                                                    }
                                                    else {
                                                        if(parametroActual.equals("msi")) {
                                                            configuracion.setTipoMSI();
                                                        }
                                                        else {
                                                            if(parametroActual.equals("datamatrix")) {
                                                                configuracion.setTipoDatamatrix();
                                                            }
                                                            else {
                                                                mostrar = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                parametroActual = request.getParameter("ext");

                if(parametroActual != null)
                {      

                    parametroActual = parametroActual.toLowerCase();

                    if(parametroActual.equals("gif"))
                    {
                         configuracion.setExtensionGIF();
                    }
                    else
                    {
                        if(parametroActual.equals("jpg"))
                        {
                            configuracion.setExtensionJPG();
                        }
                        else
                        {
                            if(parametroActual.equals("png"))
                            {
                                 configuracion.setExtensionPNG();
                            }
                            else {
                                mostrar = false;
                            }
                        }
                    }
                }

                parametroActual = request.getParameter("anchoMod");

                if(parametroActual != null)
                {
                   configuracion.setAnchoModulo(Integer.parseInt(parametroActual));        
                }

                parametroActual = request.getParameter("altoMod");

                if(parametroActual != null)
                {
                   configuracion.setAltoModulo(Integer.parseInt(parametroActual));        
                } 

                parametroActual = request.getParameter("hri");

                if(parametroActual != null)
                {
                   configuracion.setHri(Boolean.parseBoolean(parametroActual));
                }

                parametroActual = request.getParameter("tamanoTexto");

                if(parametroActual != null)
                {
                   configuracion.setTamanoFuente(Float.parseFloat(parametroActual));        
                }

                parametroActual = request.getParameter("alineacionTexto");

                if(parametroActual != null)
                {
                    if(parametroActual.toLowerCase().equals("izq")) {
                        configuracion.setAlineacionTextoIzq();
                    }
                    else {
                        if(parametroActual.toLowerCase().equals("der")) {
                            configuracion.setAlineacionTextoDer();
                        }
                        else {
                            configuracion.setAlineacionTextoCentro();
                        }
                    }      
                }

                parametroActual = request.getParameter("crc");

                if(parametroActual != null)
                {
                   configuracion.setCrc(new Boolean(parametroActual));
                }

                parametroActual = request.getParameter("anchoImg");

                if(parametroActual != null)
                {
                   try { 
                       configuracion.setAnchoImagen(Integer.parseInt(parametroActual));   
                   }
                   catch(Exception e) {
                       configuracion.setAnchoImagen(-1); 
                   }
                }
                else
                {
                   configuracion.setAnchoImagen(-1);         
                }

                parametroActual = request.getParameter("offsetX");

                if(parametroActual != null)
                {
                   configuracion.setOffsetX(Integer.parseInt(parametroActual));
                }

                parametroActual = request.getParameter("offsetY");

                if(parametroActual != null)
                {
                   configuracion.setOffsetY(Integer.parseInt(parametroActual));
                }

                //Establece la ruta en la que se podrá descargar la imágen correspondiente al código de barras en el servidor.
                parametroActual = request.getParameter("ruta");

                if(parametroActual != null)
                {
                      configuracion.setRuta(parametroActual);            
                }
            }
            else{

                mostrar = false;
            }


            if(mostrar) {

                CodigoBarras codigoBarras = new CodigoBarras(codigo, configuracion);

                if(parametroActual != null) { //ruta         

                    codigoBarras.guardarImagenEnDisco();
                }
                //HTML - Imágen

                response.setContentType("image/"+configuracion.getStringExtension().toLowerCase());
                response.addHeader("Content-Disposition", "filename="+codigo+"."+configuracion.getStringExtension().toLowerCase());  //nombre del archivo para descargar desde el navegador

                OutputStream os = response.getOutputStream();

                BufferedImage codigoBarrasImagen = codigoBarras.getImagen();       

                ImageIO.write(codigoBarrasImagen, configuracion.getStringExtension(), os); 	

                os.close();  
                codigoBarrasImagen.flush();
            }
        }        
        catch (Exception e) {
           //no hago nada -> html en blanco
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
